package uk.ac.aston.provlayer.observer.instrument.aspectj_CDO.aspectInstruments_CDO;

import org.aspectj.lang.JoinPoint;
import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.cdo.common.commit.CDOCommitInfo;
import org.eclipse.emf.cdo.common.revision.CDORevisionKey;
import org.eclipse.emf.cdo.common.revision.delta.CDOFeatureDelta;
import org.eclipse.emf.cdo.common.revision.delta.CDORevisionDelta;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.ecore.EStructuralFeature;

import uk.ac.aston.provlayer.observer.model.*;
import uk.ac.aston.provlayer.observer.model.cdo.*;
import uk.ac.aston.provlayer.observer.thread.ThreadObserver;

public class InspectorEMFeDynamic {

	private ThreadObserver theThreadObserver = ThreadObserver.INSTANCE.get(); // Make sure you get the Observer for the	
	private ModelAccessBuilder modelAccessBuilder = theThreadObserver.getModelAccessBuilderInstance();
	
	public void inspectEMFeDynamic(JoinPoint joinPoint, ObjectAccessMethod objectAccessMethod, long accessTime) {

		if (!theThreadObserver.getAgentName().equals( Thread.currentThread().getName())) {
			System.out.println("!! Inspector Thread error : " + theThreadObserver.getAgentName() + " : " + Thread.currentThread().getName());
		}
		
		CDOObject cdoObjectJP;
		ModelPart part;
		
		EStructuralFeature sFeatureAccessed;
		Object value;		
		ModelFeature feature;
		
		ModelPartFeature partFeature;

		// MODEL PART
		cdoObjectJP = (CDOObject) joinPoint.getThis();
		part = new CDOModelPart(cdoObjectJP);

		// MODEL FEATURE
		sFeatureAccessed = (EStructuralFeature) joinPoint.getArgs()[1];
		value = cdoObjectJP.eGet(sFeatureAccessed);
		feature = new CDOModelFeature(sFeatureAccessed, value);
		
		// MODEL PART FEATURE
		partFeature = new ModelPartFeature(part,feature);

		// REPORT MODEL PART FEATURE
		this.modelAccessBuilder.reportPartFeatureAccess(partFeature, objectAccessMethod, accessTime);
	}

	
	
	public void reportCommit (JoinPoint joinPoint, Object returnObject, ObjectAccessMethod objectAccessMethod, long accessTime) {
		
		CDOTransaction transaction = (CDOTransaction) joinPoint.getTarget();
		CDOCommitInfo commitResults;
		
		if (returnObject != null) {
			commitResults = (CDOCommitInfo) returnObject;
//			System.out.println("COMMIT Objects : " + commitResults.getChangedObjects().size());
//			System.out.println("VersionControl size start : " + versionController.getMapSize());

			// Iterate over the Objects (CDORevisionKeys) in the commit.
			for (CDORevisionKey revKey : commitResults.getChangedObjects()) {
				// Turn the Key in to a RevisionDelta type to get at the Object Features.
				CDORevisionDelta revKeyDelta = (CDORevisionDelta) revKey;
				// Iterate over the Features (CDORevisionDelta) on the Object being committed
				// and report Entities for Memory & Storage
				for (CDOFeatureDelta featureDelta : revKeyDelta.getFeatureDeltas()) {

					// TODO This is a hack to filter out non CDOObjects from the Commits.
					
					if (!"nodes".equals(featureDelta.getFeature().getName())
							&& !"contents".equals(featureDelta.getFeature().getName())) {

						// MODEL PART
						CDOObject commitedCDOObject = (CDOObject) transaction.getObject(revKey.getID());
						CDOModelPart part = new CDOModelPart(commitedCDOObject);
						
						// MODEL FEATURE
						EStructuralFeature sFeatureAccessed = featureDelta.getFeature();
						Object value = commitedCDOObject.eGet(sFeatureAccessed);
						CDOModelFeature feature = new CDOModelFeature(sFeatureAccessed, value);
						
						// MODEL PART FEATURE
						ModelPartFeature partFeature = new ModelPartFeature(part, feature);

						// REPORT MODEL PART FEATURE
						this.modelAccessBuilder.reportPartFeatureAccess(partFeature, objectAccessMethod, accessTime);

					}
				}
			}
		}
	}

}
