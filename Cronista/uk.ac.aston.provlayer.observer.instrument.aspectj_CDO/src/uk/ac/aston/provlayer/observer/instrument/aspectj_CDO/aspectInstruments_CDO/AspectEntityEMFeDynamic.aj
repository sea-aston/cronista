package uk.ac.aston.provlayer.observer.instrument.aspectj_CDO.aspectInstruments_CDO;

import uk.ac.aston.provlayer.observer.annotations.*;
import uk.ac.aston.provlayer.observer.model.ObjectAccessMethod;

public aspect AspectEntityEMFeDynamic {

	// This approach to encapsulation handling could be expanded
	// Constructors are not caught for example, these could be added

	// THREAD LOCAL THESE INSTANCES
	private ThreadLocal<Boolean> localThreadObserveFlag = ThreadLocal.withInitial(()->true);
	private ThreadLocal<InspectorEMFeDynamic> localThreadInspector = new ThreadLocal<>();	
	private InspectorEMFeDynamic getInspectorInstance() {
		InspectorEMFeDynamic instance = localThreadInspector.get();
		
		if (instance==null) {
			localThreadInspector.set(new InspectorEMFeDynamic());
		}	
		
		while (instance == null) {
			System.out.println("Inspector missing try again " + Thread.currentThread().getName());
			instance = localThreadInspector.get(); 
		}
		
		return instance;
	}
	
	// GET()
	after() returning(Object r) : call(* eDynamicGet(..)) 
		&& @within(ObserveModelEMFeDynamic) {
		
		InspectorEMFeDynamic inspector = this.getInspectorInstance();
		if(localThreadObserveFlag.get())
		{
			localThreadObserveFlag.set(false);
			inspector.inspectEMFeDynamic(thisJoinPoint, ObjectAccessMethod.Get, System.currentTimeMillis());
			localThreadObserveFlag.set(true);
		}
	}
	
	// before_SET()
	before() : call(* eDynamicSet(..))
		&& @within(ObserveModelEMFeDynamic) {
		
		InspectorEMFeDynamic inspector = this.getInspectorInstance();
		
		if(localThreadObserveFlag.get())
		{
			localThreadObserveFlag.set(false);
			inspector.inspectEMFeDynamic(thisJoinPoint, ObjectAccessMethod.before_Set, System.currentTimeMillis());	
			localThreadObserveFlag.set(true);
		}
	}
	
	// after_SET()
	after() : call(* eDynamicSet(..))
	&& @within(ObserveModelEMFeDynamic) {
		InspectorEMFeDynamic inspector = this.getInspectorInstance();
		
		if(localThreadObserveFlag.get())
		{
			localThreadObserveFlag.set(false);
			inspector.inspectEMFeDynamic(thisJoinPoint, ObjectAccessMethod.after_Set, System.currentTimeMillis());	
			localThreadObserveFlag.set(true);
		}
	}

	// COMMIT()	
	after() returning (Object returnObject) : call (* commit())
		&& target(org.eclipse.emf.cdo.transaction.CDOTransaction) {
		InspectorEMFeDynamic inspector = this.getInspectorInstance();
		
		if(localThreadObserveFlag.get())
		{
			localThreadObserveFlag.set(false);
			inspector.reportCommit(thisJoinPoint, returnObject, ObjectAccessMethod.Commit, System.currentTimeMillis());
			localThreadObserveFlag.set(true);
		}
	}
}