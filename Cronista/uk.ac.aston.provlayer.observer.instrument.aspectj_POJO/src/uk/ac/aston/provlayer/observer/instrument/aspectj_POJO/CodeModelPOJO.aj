package uk.ac.aston.provlayer.observer.instrument.aspectj_POJO;

import uk.ac.aston.provlayer.observer.model.ObjectAccessMethod;

public aspect CodeModelPOJO {

	// THREAD LOCAL THESE INSTANCES
	private ThreadLocal<Boolean> localThreadObserveFlag = ThreadLocal.withInitial(()->true);
	private ThreadLocal<InspectorPOJO> localThreadInspector = new ThreadLocal<>();	
	private InspectorPOJO getInspectorInstance() {
		InspectorPOJO instance = localThreadInspector.get();
		
		if (instance==null) {
			localThreadInspector.set(new InspectorPOJO());
		}	
		
		while (instance == null) {
			System.out.println("Inspector missing try again " + Thread.currentThread().getName());
			instance = localThreadInspector.get(); 
		}
		
		return instance;
	}
	
	after() returning(Object r) : 
			get(* *) 
			&& ( @within(CodePOJO) || @withincode(CodePOJO) )
//			&& @withincode(CodePOJO)
			&& ( @annotation(ModelPOJO) || @target(ModelPOJO) )
			{
				InspectorPOJO inspector = this.getInspectorInstance();
				if(localThreadObserveFlag.get())
				{
					localThreadObserveFlag.set(false);
					inspector.inspect(thisJoinPoint, ObjectAccessMethod.Get, System.currentTimeMillis());
					localThreadObserveFlag.set(true);
				}				
			}

	before() :
			set(* *)
			&& ( @within(CodePOJO) || @withincode(CodePOJO) )
//			&& @withincode(CodePOJO)
			&& ( @annotation(ModelPOJO) || @target(ModelPOJO) )
			{
				InspectorPOJO inspector = this.getInspectorInstance();
				if(localThreadObserveFlag.get())
				{
					localThreadObserveFlag.set(false);
					inspector.inspect(thisJoinPoint, ObjectAccessMethod.before_Set, System.currentTimeMillis());
					localThreadObserveFlag.set(true);
				}					
			}

	after() :
			set(* *)
			&& ( @within(CodePOJO) || @withincode(CodePOJO) )
//			&& @withincode(CodePOJO)
			&& ( @annotation(ModelPOJO) || @target(ModelPOJO) )
			{
				InspectorPOJO inspector = this.getInspectorInstance();
				if(localThreadObserveFlag.get())
				{
					localThreadObserveFlag.set(false);
					inspector.inspect(thisJoinPoint, ObjectAccessMethod.after_Set, System.currentTimeMillis());
					localThreadObserveFlag.set(true);
				}	
			}

}
