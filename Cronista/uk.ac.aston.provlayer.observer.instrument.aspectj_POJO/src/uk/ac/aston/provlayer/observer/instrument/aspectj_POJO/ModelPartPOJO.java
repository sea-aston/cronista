package uk.ac.aston.provlayer.observer.instrument.aspectj_POJO;

import uk.ac.aston.provlayer.observer.model.ModelPart;

public class ModelPartPOJO implements ModelPart{

	private Object targetObject;
	
	public ModelPartPOJO(Object targetObject) {
		this.targetObject = targetObject;
	}
	
	@Override
	public String getIdentifier() {
		return String.valueOf(targetObject.hashCode());
		//return targetObject.toString();
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return targetObject.getClass().getName();
	}

	@Override
	public Object getRaw() {
		// TODO Auto-generated method stub
		return targetObject;
	}

	@Override
	public boolean hasStorageVersioning() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getStorageVersion() {
		// TODO Auto-generated method stub
		return 0;
	}

}
