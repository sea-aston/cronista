package uk.ac.aston.provlayer.observer.instrument.aspectj_POJO;


import java.lang.reflect.Field;

import org.aspectj.lang.JoinPoint;

import uk.ac.aston.provlayer.observer.model.*;
import uk.ac.aston.provlayer.observer.thread.ThreadObserver;

public class InspectorPOJO {

	// The inspector extract Part and Feature from a join point passing it to ModelAccess for processing
	
	private ThreadObserver theThreadObserver= ThreadObserver.INSTANCE.get();
	private ModelAccessBuilder modelAccessBuilder = theThreadObserver.getModelAccessBuilderInstance();	
	
	public void inspect(JoinPoint joinPoint, ObjectAccessMethod objectAccessMethod, long accessTime) {
		ModelPart part;
		ModelFeature feature;
		ModelPartFeature partFeature;	
		Field fieldAccessed = null;
		Object value = null;

		// MODEL PART
		part = this.getPartFrom(joinPoint);

		// MODEL FEATURE
		fieldAccessed = this.getFieldAccessedFrom(joinPoint);		
		value = this.getAccessableValueFrom(joinPoint, fieldAccessed);  // on a get this should be the "returned value"		
		feature = new ModelFeaturePOJO(fieldAccessed, value);
		
		// COMBINE AND SEND TO MODEL ACCESS FOR PROCESSING
		partFeature = new ModelPartFeature(part, feature);
		this.modelAccessBuilder.reportPartFeatureAccess(partFeature, objectAccessMethod, accessTime);
		
	}

	private ModelPart getPartFrom(JoinPoint joinPoint) {
		ModelPartPOJO part;
		if (joinPoint.getTarget() == null) {
			// THIS IS A CLASS
			part = new ModelPartPOJO(joinPoint.getClass());
		} else {
			// THIS IS AN INSTANCE
			part = new ModelPartPOJO(joinPoint.getTarget());
		}
		return part;
	}

	private Field getFieldAccessedFrom(JoinPoint joinPoint) {
		String fieldName = joinPoint.getSignature().getName();
		Field fieldAccessed = null;
		if (joinPoint.getSignature().getDeclaringType().isPrimitive()) {
			// Primitive
			try {
				fieldAccessed = joinPoint.getSignature().getDeclaringType().getField(fieldName);
			} catch (NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			}
		} else {
			// non-Primitive
			try {
				fieldAccessed = joinPoint.getSignature().getDeclaringType().getDeclaredField(fieldName);
			} catch (NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			}
		}
		return fieldAccessed;
	}

	private Object getAccessableValueFrom(JoinPoint joinPoint, Field fieldAccessed) {
		Object value = null;
		if (joinPoint.getTarget() == null) {
			// Class value
			try {
				value = fieldAccessed.get(joinPoint.getSignature().getDeclaringType());
			} catch (IllegalArgumentException | IllegalAccessException e) {
				//e.printStackTrace();
				value = this.getInAccessableValueFrom(joinPoint, fieldAccessed);  // on a get this should be the returned value
			}
		} else {
			// Instance value
			try {
				value = fieldAccessed.get(joinPoint.getTarget());
			} catch (IllegalArgumentException | IllegalAccessException e) {
				//e.printStackTrace();
				value = this.getInAccessableValueFrom(joinPoint, fieldAccessed);  // on a get this should be the returned value
			}

		}
		return value;
	}

	private Object getInAccessableValueFrom(JoinPoint joinPoint, Field fieldAccessed) {
		Object value = null;
		try {
			fieldAccessed.setAccessible(true);

			if (joinPoint.getTarget() == null) {
				// Class value
				try {
					value = fieldAccessed.get(joinPoint.getSignature().getDeclaringType());
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			} else {

				// Instance value
				try {
					value = fieldAccessed.get(joinPoint.getTarget());
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		} finally {
			fieldAccessed.setAccessible(false);
		}
		return value;
	}



	

}
