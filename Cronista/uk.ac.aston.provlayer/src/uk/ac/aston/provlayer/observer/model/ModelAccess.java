package uk.ac.aston.provlayer.observer.model;

import uk.ac.aston.provlayer.observer.model.ModelAccess.AccessType;
import uk.ac.aston.provlayer.observer.thread.ThreadObserver;
import uk.ac.aston.provlayer.observer.versionController.VersionController;

/**
 * Captures all the necessary information about a model access.
 */
public class ModelAccess {

	public enum AccessType {
		READ, WRITE, COMMIT
	};

	public static class Builder {
		private String entityInstanceID, oldEntityInstanceID;
		private ModelPart eob;
		private ModelFeature feature;
		private Object value, oldValue;
		private long time = System.currentTimeMillis(), commitTime;
		private boolean inModelStorage = false;
		private String memoryVersion, storageVersion;
		private AccessType type = AccessType.READ;

		public Builder entityID(String id) {
			this.entityInstanceID = id;
			return this;
		}

		public Builder oldEntityID(String id) {
			this.oldEntityInstanceID = id;
			return this;
		}

		public Builder eObject(ModelPart obj) {
			this.eob = obj;
			return this;
		}

		public Builder feature(ModelFeature f) {
			this.feature = f;
			return this;
		}

		public Builder value(Object value) {
			this.value = value;
			return this;
		}

		public Builder oldValue(Object value) {
			this.oldValue = value;
			return this;
		}

		public Builder time(long time) {
			this.time = time;
			return this;
		}

		public Builder inModelStorage(boolean b) {
			this.inModelStorage = b;
			return this;
		}

		public Builder memoryVersion(String s) {
			this.memoryVersion = s;
			return this;
		}

		public Builder storageVersion(String s) {
			this.storageVersion = s;
			return this;
		}

		public Builder type(AccessType t) {
			this.type = t;
			return this;
		}

		public Builder commitTime(long time) {
			this.commitTime = time;
			return this;
		}

		public ModelAccess build() {
			// TODO validate report
			return new ModelAccess(entityInstanceID, oldEntityInstanceID, eob, feature, value, oldValue, time,
					inModelStorage, memoryVersion, storageVersion, type, commitTime);
		}
	}

	private final String entityInstanceID, oldEntityInstanceID;
	private final ModelPart eob;
	private final ModelFeature feature;
	private final Object value, oldValue;
	private final long time, commitTime;
	private final boolean inModelStorage;
	private final String memoryVersion, storageVersion;
	private final AccessType accessType;

	private ModelAccess(String entityInstanceID, String oldEntityInstanceID, ModelPart modelObject,
			ModelFeature feature, Object value, Object oldValue, long time, boolean inModelStorage,
			String memoryVersion, String storageVersion, AccessType type, long commitTime) {
		this.entityInstanceID = entityInstanceID;
		this.oldEntityInstanceID = oldEntityInstanceID;
		this.eob = modelObject;
		this.feature = feature;
		this.value = value;
		this.oldValue = oldValue;
		this.time = time;
		this.inModelStorage = inModelStorage;
		this.memoryVersion = memoryVersion;
		this.storageVersion = storageVersion;
		this.accessType = type;
		this.commitTime = commitTime;
	}
	
	public String getEntityInstanceID() {
		return entityInstanceID;
	}

	public String getOldEntityInstanceID() {
		return oldEntityInstanceID;
	}

	public ModelPart getEObject() {
		return eob;
	}

	public ModelFeature getFeature() {
		return feature;
	}

	public Object getValue() {
		return value;
	}

	public Object getOldValue() {
		return oldValue;
	}

	public long getTime() {
		return time;
	}

	public long getCommitTime() {
		return commitTime;
	}

	public boolean isInModelStorage() {
		return inModelStorage;
	}

	public String getMemoryVersion() {
		return memoryVersion;
	}

	public String getStorageVersion() {
		return storageVersion;
	}

	public AccessType getAccessType() {
		return accessType;
	}

}