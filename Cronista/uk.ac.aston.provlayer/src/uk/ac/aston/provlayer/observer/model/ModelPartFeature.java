package uk.ac.aston.provlayer.observer.model;


// TODO Move this to observer.instrument.aspectJ.POJO
public class ModelPartFeature {

	private final ModelPart part;
	private final ModelFeature feature;
	
	public ModelPartFeature(ModelPart part, ModelFeature feature) {
		this.part = part;
		this.feature = feature;
	}
	
	public ModelPart getPart() {
		return part;
	}

	public ModelFeature getFeature() {
		return feature;
	}

	public String getEntityID () {
		String EntityID = String.format("%s.%s",
				part.getIdentifier(),
				feature.getIdentifier()
				);
		return EntityID;
	}
	
	public boolean hasStorageVersioning() {
		if (part.hasStorageVersioning() || feature.hasStorageVersioning()) { 
			return true;
		}		
		return false;
	}
	
	public int getStorageVersion() {	
		if 		(part.hasStorageVersioning()) 	 { return part.getStorageVersion(); 	} 
		else if (feature.hasStorageVersioning()) { return feature.getStorageVersion();	}
		else	return 0;
	}
	

}
