package uk.ac.aston.provlayer.observer.model.cdo;

import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.internal.cdo.CDOObjectImpl;

import uk.ac.aston.provlayer.observer.model.ModelAccessBuilder;
import uk.ac.aston.provlayer.observer.model.ModelFeature;
import uk.ac.aston.provlayer.observer.model.ModelPart;
import uk.ac.aston.provlayer.observer.model.ModelPartFeature;
import uk.ac.aston.provlayer.observer.model.ObjectAccessMethod;
import uk.ac.aston.provlayer.observer.thread.ThreadObserver;

public class LoggingCDOObjectImpl2 extends CDOObjectImpl {

	private ThreadObserver theThreadObserver = ThreadObserver.INSTANCE.get(); // Make sure you get the Observer for the
	private ModelAccessBuilder modelAccessBuilder = theThreadObserver.getModelAccessBuilderInstance();

	@Override
	protected Object eDynamicGet(int dynamicFeatureID, EStructuralFeature eFeature, boolean resolve, boolean coreType) {

		ModelPart part;
		ModelFeature feature;
		ModelPartFeature partFeature;
		
		CDOObject cdoObject = null;
		EStructuralFeature sFeatureAccessed = null;
		Object value = null;
		
		value = super.eDynamicGet(dynamicFeatureID, eFeature, resolve, coreType); // Triggers the model Get
		if (theThreadObserver.getSystemCallFlag()) {
			try {
				theThreadObserver.setSystemCallFlag(false);
				// MODEL PART
				cdoObject = this;
				part = new CDOModelPart(cdoObject);

				// MODEL FEATURE
				sFeatureAccessed = eFeature;
				// GET()
				
				// value should be moved to before the IF - so that we ALWAYS return value not null this would FIX eGet().
				//value = super.eDynamicGet(dynamicFeatureID, eFeature, resolve, coreType); // Triggers the model Get
				feature = new CDOModelFeature(sFeatureAccessed, value);
				
				// Echo to console if these are never true, we use true true on the SET eDynamicGet() for value recovery
				//if(resolve!=true) {System.out.println("resolve!=true");}
				//if(coreType!=true) {System.out.println("coreType!=true");}

				// REPORT MODEL PART FEATURE
				partFeature = new ModelPartFeature(part, feature);
				this.modelAccessBuilder.reportPartFeatureAccess(partFeature, ObjectAccessMethod.Get,
						System.currentTimeMillis());
			} finally {
				theThreadObserver.setSystemCallFlag(true);
			}
		}
		return value;
	}

	@Override
	protected void eDynamicSet(int dynamicFeatureID, EStructuralFeature eFeature, Object newValue) {
		ModelPart part;
		ModelFeature feature;
		ModelPartFeature partFeature;

		CDOObject cdoObject = null;
		EStructuralFeature sFeatureAccessed = null;
		Object value = null;

		if (theThreadObserver.getSystemCallFlag()) {
			try {
				theThreadObserver.setSystemCallFlag(false); // Recursion problems if you call "this" the wrong way!

				// MODEL PART
				cdoObject = this;
				part = new CDOModelPart(cdoObject);

				// MODEL FEATURE
				sFeatureAccessed = eFeature;
				//value = super.eDynamicGet(dynamicFeatureID, eFeature,true,true); // Triggers the model Get
				//value = ((CDOObjectImpl) cdoObject).dynamicGet(dynamicFeatureID);				
				value =cdoObject.eGet(sFeatureAccessed);
				//System.out.println("before Value: " + value + " (newValue: " +newValue+")");  // DEBUG
				
				feature = new CDOModelFeature(sFeatureAccessed, value);

				// REPORT MODEL PART FEATURE
				partFeature = new ModelPartFeature(part, feature);
				this.modelAccessBuilder.reportPartFeatureAccess(partFeature, ObjectAccessMethod.before_Set,
						System.currentTimeMillis());

				//
				// SET()
				super.eDynamicSet(dynamicFeatureID, eFeature, newValue); // SETS THE VALUE
				//
				//

				// MODEL PART
				cdoObject = this;
				part = new CDOModelPart(cdoObject);
				// MODEL FEATURE
				sFeatureAccessed = eFeature;
				//value = super.eDynamicGet(dynamicFeatureID, eFeature,true,true); // Triggers the model Get
				//value = ((CDOObjectImpl) cdoObject).dynamicGet(dynamicFeatureID);
				value =cdoObject.eGet(sFeatureAccessed);
				//System.out.println("after Value: "+value+ " (newValue: " +newValue+")");  // DEBUG
				
				feature = new CDOModelFeature(sFeatureAccessed, value);

				// REPORT MODEL PART FEATURE
				partFeature = new ModelPartFeature(part, feature);
				this.modelAccessBuilder.reportPartFeatureAccess(partFeature, ObjectAccessMethod.after_Set,
						System.currentTimeMillis());

			} finally {
				theThreadObserver.setSystemCallFlag(true);
			}
		}
	}
}
