package uk.ac.aston.provlayer.observer.model.cdo;

import org.eclipse.emf.ecore.EStructuralFeature;

import uk.ac.aston.provlayer.observer.model.ModelFeature;

public class CDOModelFeature implements ModelFeature {

	private EStructuralFeature sf;
	private Object value;

	public CDOModelFeature(EStructuralFeature sf, Object value) {
		this.sf = sf;
		this.value = value;
	}
	
	// This constructor is for Legacy parts of the system using the modelFeature to decode features.
	public CDOModelFeature(EStructuralFeature sf) {
		this.sf = sf;
		this.value = "NOT SET";
	}
	
	@Override
	public String getName() {
		return sf.getName() + "";
	}

	@Override
	public String getIdentifier() {
		return sf.getFeatureID() + "";
	}

	@Override
	public EStructuralFeature getRaw() {
		return sf;
	}

	@Override
	public String getType() {
		return sf.getEType() + "";
	}

	@Override
	public String getValueAsString() {
		return value.toString() + "";
	}

	@Override
	public Object getValue() {
		return value;
	}

	@Override
	public boolean hasStorageVersioning() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getStorageVersion() {
		// TODO Auto-generated method stub
		return 0;
	}
}
