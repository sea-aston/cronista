package uk.ac.aston.provlayer.observer.thread;

import java.util.HashMap;
import java.util.Map;

import uk.ac.aston.provlayer.descriptions.EntityDescription;

public class UncommittedChanges {

	//
	// Created to log the uncommitted change in the CommitReporterCDO on the Observer
	//
	
	
	private Map<String, Map<String, EntityDescription>> pendingByObject = new HashMap<>();

	public void logChange(EntityDescription entityDesc) {
		final Map<String, EntityDescription> pendingByFeature = pendingByObject
				.computeIfAbsent(entityDesc.getObjectIDAsString(), (x) -> new HashMap<>());
		pendingByFeature.put(entityDesc.getAttributeNameAsString(), entityDesc);
	}

	public Map<String, Map<String, EntityDescription>> getChanges() {
		return pendingByObject;
	}

}
