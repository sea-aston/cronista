package uk.ac.aston.provlayer.observer.model;

import uk.ac.aston.provlayer.observer.model.ModelAccess.AccessType;
import uk.ac.aston.provlayer.observer.thread.ThreadObserver;
import uk.ac.aston.provlayer.observer.versionController.VersionController;

public class ModelAccessBuilder {

	//
	//	There should be ONLY ONE INSTANCE of this ModelAccessBuilder on the Observer which Inspectors/Instruments can request
	//
	
	private static final Boolean CONSOLE_ECHO = false;

	private ModelPartFeatureVersion before_SetMPFV; // We will need to track the before and after for WRITE/SET()
	// Given a ModelPartFeature and accessMethod, report based on access and versions.
	private ThreadObserver theThreadObserver = ThreadObserver.INSTANCE.get();
	private VersionController versionController = theThreadObserver.getVersionControllerInstance();
	
	// This will be the ONE method which the instruments/inspectors call to report in	
	public void reportPartFeatureAccess(ModelPartFeature modelPartFeature, ObjectAccessMethod objectAccessMethod, Long accessTime) {

		
		ModelPartFeatureVersion currentMPFV, updatedMPFV, lastMemoryMPFV;
		int s, m;
		
		ModelAccess maReport = null;
		
		switch (objectAccessMethod) {
			case Get:

				// CURRENT MEMORY VERSION
				currentMPFV = versionController.getCurrentVersion(modelPartFeature);
				//System.out.println(" GET reportPartFeatureAccess() " + currentMPFV.getEntityInstanceID());  // DEBUG
				
				// Version based actions
				s = currentMPFV.getStorageVersion();
				m = currentMPFV.getMemoryVersion();
				if(s == 0) {
					if(m == 0) {
						// s=0  m=0  CREATE
						maReport = ModelAccessBuilder.createModelAccess(currentMPFV,accessTime);
					} else {
						// s=0  m1>  READ (memory only)
						maReport = ModelAccessBuilder.readModelAccess(currentMPFV, accessTime);
					}
				} else {  // Model Storage only action
					if(m == 0) {
						// s1>  m=0  LOAD > m=0+1
						ModelPartFeatureVersion storageMPFV = currentMPFV;
						ModelPartFeatureVersion memoryMPFV = versionController.updateMemoryVersion(modelPartFeature);
						maReport = ModelAccessBuilder.loadModelAccess(memoryMPFV ,storageMPFV ,accessTime);
					} else {
						// s1>  m1>  READ (memory)
						maReport = ModelAccessBuilder.readModelAccess(currentMPFV, accessTime);
					}
				} break;
			case before_Set:
				// CURRENT MEMORY VERSION
				
				before_SetMPFV = versionController.getCurrentVersion(modelPartFeature);
				//System.out.println(" before_set reportPartFeatureAccess() " +before_SetMPFV.getEntityInstanceID());  // DEBUG

				// Version based actions
				/*
				s = before_SetMPFV.getStorageVersion();
				m = before_SetMPFV.getMemoryVersion();
				if(s == 0) {
					if(m == 0) {				
						// s=0  m=0  beforeCREATE  // EMPTY OBJECT -- before_SetMPFV = null
					} else {
						// s=0  m1>  beforeUPDATE (memory only)						
					}
				} else { // Model Storage only action
					if(m == 0) {
						// s1>  m=0  beforeLOAD
					} else {
						// s1>  m1>  beforeUPDATE (memory)
					}
				}
				*/ 
				break;
			case after_Set:
				// INCREASE MEMORY VERSION +1
				
				updatedMPFV = versionController.updateMemoryVersion(modelPartFeature);
				//System.out.println(" after_Set reportPartFeatureAccess()" + updatedMPFV.getEntityInstanceID());  // DEBUG
				
				// Version based actions
				s = updatedMPFV.getStorageVersion();
				m = updatedMPFV.getMemoryVersion();
				if(s == 0) {
					if(m == 0+1) {
						// s=0  m=0+1  afterCREATE
						maReport = ModelAccessBuilder.createModelAccess(updatedMPFV,accessTime);
					} else {
						// s=0  m1+1>  afterUPDATE (memory only)
						maReport = ModelAccessBuilder.updateModelAccess(updatedMPFV, before_SetMPFV, accessTime);
					}
				} else { // Model Storage only action
					if(m == 0+1) {
						// s1>  m=0+1  afterLOAD
						maReport = ModelAccessBuilder.loadModelAccess(updatedMPFV, before_SetMPFV, accessTime);
					} else {
						// s1>  m1+1>  afterUPDATE (memory)
						maReport = ModelAccessBuilder.updateModelAccess(updatedMPFV, before_SetMPFV, accessTime);
					}					
				} 
				before_SetMPFV = null;
				break;
			case Commit:
				// CURRENT s*m0 - LAST s*m*  - REMOVE FROM VERSIONING 			
				lastMemoryMPFV = versionController.getLastKnownVersion(modelPartFeature);
				currentMPFV = versionController.getCurrentVersion(modelPartFeature);				
				
				maReport = ModelAccessBuilder.commitModelAccess(currentMPFV, lastMemoryMPFV, accessTime);
				versionController.removeCommitted(modelPartFeature);

				// Version based actions
				/*
				s = currentMPFV.getStorageVersion();
				m = currentMPFV.getMemoryVersion();
				if(s == 0) {
					if(m == 0) {
						// s=0  m=0 // -INVALID-
					} else {
						// s=0  m1> // lastMemoryVersion beforeFirstCOMMIT
					}
				} else {
					if(m == 0) { // Model Storage only action

						// s1>  m=0 // afterCOMMIT
					} else {
						// s1>  m1> // lastMemoryVersion beforeCOMMIT
					}
				} 
				*/
				break;
			default :
				System.out.println("ERROR: reportPartFeatureVersion() -- UNKNOWN ObjectAccessMethod");
				break;
			}
		
			if (maReport!=null) theThreadObserver.reportAccess(maReport);
			
	}
	
	private static ModelAccess readModelAccess(ModelPartFeatureVersion after, Long accessTime) {
		//if(CONSOLE_ECHO) {System.out.println("readModelAccess: " +after.getEntityInstanceID());}
		return new ModelAccess.Builder()
				.entityID(after.getEntityInstanceID())
				.eObject(after.getPart())
				.feature(after.getFeature())
				.value(after.getFeatureValue())
				.memoryVersion(after.getMemorySpaceVersionString())
				.type(AccessType.READ)
				.time(accessTime)
				.build();
	}

	private static ModelAccess createModelAccess(ModelPartFeatureVersion after, Long accessTime) {
		if(CONSOLE_ECHO) {System.out.println("createModelAccess: " + after.getEntityInstanceID());}
		
		return new ModelAccess.Builder()
				.entityID(after.getEntityInstanceID())
				.oldEntityID(null)
				.eObject(after.getPart())
				.feature(after.getFeature())
				.value(after.getFeatureValue())
				.memoryVersion(after.getMemorySpaceVersionString())
				.type(AccessType.WRITE)
				.time(accessTime)
				.build();
	}

	private static ModelAccess updateModelAccess(ModelPartFeatureVersion after,ModelPartFeatureVersion before, Long accessTime) {
		if(CONSOLE_ECHO) {System.out.println("updateModelAccess: " + before.getEntityInstanceID() + " > " + after.getEntityInstanceID());}
		
		return new ModelAccess.Builder()
				.entityID(after.getEntityInstanceID())
				.oldEntityID(before.getEntityInstanceID())
				.eObject(after.getPart())
				.feature(after.getFeature())
				.value(after.getFeatureValue())
				.oldValue(before.getFeatureValue())
				.memoryVersion(after.getMemorySpaceVersionString())
				.type(AccessType.WRITE)
				.time(accessTime)
				.build();
	}

	// Storage based models only
	private static ModelAccess loadModelAccess(ModelPartFeatureVersion after, ModelPartFeatureVersion before, Long accessTime) {
		if(CONSOLE_ECHO) {System.out.println("loadModelAccess: " + before.getEntityInstanceID() + " > " + after.getEntityInstanceID());}
		
		return new ModelAccess.Builder()
				.entityID(after.getEntityInstanceID())
				.oldEntityID(before.getEntityInstanceID())
				.eObject(after.getPart())
				.feature(before.getFeature())
				.value(after.getFeatureValue())
				.oldValue(before.getFeatureValue())
				.inModelStorage(before.isInStorage())
				.memoryVersion(after.getMemorySpaceVersionString())
				.storageVersion(after.getStorageVersionString())
				.type(AccessType.READ).time(accessTime).build();
	}

	// Storage based models only
	private static ModelAccess commitModelAccess(ModelPartFeatureVersion after, ModelPartFeatureVersion before,	Long commitTime) {
		if(CONSOLE_ECHO) {System.out.println("commitModelAccess: " + before.getEntityInstanceID() + " > " + after.getEntityInstanceID());}
		
		
		if(before == null) { // Filter off any commits where we don't have a "before version"
			//System.out.println("Dropped commit no before version");
			return null;}
		
		return new ModelAccess.Builder()
				.entityID(after.getEntityInstanceID())
				.oldEntityID(before.getEntityInstanceID())
				.commitTime(commitTime)
				.eObject(after.getPart())
				.feature(before.getFeature())
				.value(after.getFeatureValue())
				.inModelStorage(after.isInStorage())
				.memoryVersion(after.getMemorySpaceVersionString())
				.storageVersion(after.getStorageVersionString())
				.type(AccessType.COMMIT)
				.build();

	}
	
}
