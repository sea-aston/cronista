package uk.ac.aston.provlayer.observer.model.cdo;

import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.cdo.common.commit.CDOCommitInfo;
import org.eclipse.emf.cdo.common.revision.CDORevision;
import org.eclipse.emf.cdo.common.revision.CDORevisionKey;
import org.eclipse.emf.cdo.common.revision.delta.CDOFeatureDelta;
import org.eclipse.emf.cdo.common.revision.delta.CDORevisionDelta;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CommitException;
import org.eclipse.emf.cdo.util.ConcurrentAccessException;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.internal.cdo.CDOObjectImpl;

import uk.ac.aston.provlayer.descriptions.EntityDescription;
import uk.ac.aston.provlayer.observer.model.ModelAccess;
import uk.ac.aston.provlayer.observer.model.ModelAccessBuilder;
import uk.ac.aston.provlayer.observer.model.ModelPartFeature;
import uk.ac.aston.provlayer.observer.model.ObjectAccessMethod;
import uk.ac.aston.provlayer.observer.model.ModelAccess.AccessType;
import uk.ac.aston.provlayer.observer.thread.ThreadObserver;
import uk.ac.aston.provlayer.observer.thread.UncommittedChanges;

public class CommitReporterCDO {

	// This is the new commit processor for using Model Part Feature
	public CDOCommitInfo newCommit(CDOTransaction transaction) {
		ThreadObserver theThreadObserver = ThreadObserver.INSTANCE.get(); // Make sure you get the Observer for the
		ModelAccessBuilder modelAccessBuilder = theThreadObserver.getModelAccessBuilderInstance();
		CDOCommitInfo commitResults = null;

		if (theThreadObserver.getSystemCallFlag()) {
			try {
				theThreadObserver.setSystemCallFlag(false); // Recursion problems if you call "this" the wrong way!

				try {
					commitResults = transaction.commit();

					// Iterate over the Objects (CDORevisionKeys) in the commit.
					for (CDORevisionKey revKey : commitResults.getChangedObjects()) {
						// Turn the Key in to a RevisionDelta type to get at the Object Features.
						CDORevisionDelta revKeyDelta = (CDORevisionDelta) revKey;
						// Iterate over the Features (CDORevisionDelta) on the Object being committed
						// and report Entities for Memory & Storage
						for (CDOFeatureDelta featureDelta : revKeyDelta.getFeatureDeltas()) {

							// TODO This is a hack to filter out non CDOObjects from the Commits.
							if (!"nodes".equals(featureDelta.getFeature().getName())
									&& !"contents".equals(featureDelta.getFeature().getName())) {

								// MODEL PART
								CDOObject commitedCDOObject = (CDOObject) transaction.getObject(revKey.getID());
								CDOModelPart part = new CDOModelPart(commitedCDOObject);

								// MODEL FEATURE
								EStructuralFeature sFeatureAccessed = featureDelta.getFeature();
								//Object value = commitedCDOObject.eGet(sFeatureAccessed);  // returns nulls							
								//Object value = commitedCDOObject.eDynamicGet(sFeatureAccessed, true); // doesnt work wants to case to EObjectBasicImpl
								Object value = ((CDOObjectImpl) commitedCDOObject).dynamicGet(featureDelta.getFeature().getFeatureID());
								//if(value!=null) {System.out.println("COMMIT value: "+value.toString());}  // DEBUG
								
								
								CDOModelFeature feature = new CDOModelFeature(sFeatureAccessed, value);

								// MODEL PART FEATURE
								ModelPartFeature partFeature = new ModelPartFeature(part, feature);

								// REPORT MODEL PART FEATURE
								modelAccessBuilder.reportPartFeatureAccess(partFeature, ObjectAccessMethod.Commit,
										commitResults.getTimeStamp());

							}
						}
					}
				} catch (ConcurrentAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (CommitException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} finally {
				theThreadObserver.setSystemCallFlag(true);
			}
		}

		return commitResults;
	}

	// The methods below are legacy

	private final UncommittedChanges uncommitted = new UncommittedChanges();

	public CDOCommitInfo commit(CDOTransaction transaction) throws ConcurrentAccessException, CommitException {
		final ThreadObserver theThreadObserver = ThreadObserver.INSTANCE.get();
		final CDOCommitInfo commitResults = transaction.commit();
		final long commitTime = commitResults.getTimeStamp();

		final Map<String, Map<String, EntityDescription>> changes = uncommitted.getChanges();
		for (CDORevisionKey revKey : commitResults.getChangedObjects()) {
			final CDOObject cdoObject = transaction.getObject(revKey.getID());
			final String objectID = EcoreUtil.getURI(cdoObject).fragment();
			final CDORevision revision = cdoObject.cdoRevision();
			final int newVersion = revision.getVersion();

			for (EntityDescription ed : changes.getOrDefault(objectID, Collections.emptyMap()).values()) {
				final String entityInstanceID = ThreadObserver.createEntityInstanceID(String.valueOf(revision.getID()),
						ed.getAttributeIDAsString(), "0", String.valueOf(newVersion));

				final String oldEntityInstanceID = ed.getEntityInstanceID();

				theThreadObserver.reportAccess(new ModelAccess.Builder().entityID(entityInstanceID)
						.oldEntityID(oldEntityInstanceID).commitTime(commitTime).eObject(ed.getObjectStorageReference())
						.feature(new CDOModelFeature((EStructuralFeature) ed.getAttributeMetadataStorageReference()))
						.value(ed.getAttributeValueAsString()).inModelStorage(true).memoryVersion("0")
						.storageVersion(String.valueOf(revision)).type(AccessType.COMMIT).build());
			}
		}
		uncommitted.getChanges().clear();

		return commitResults;
	}

	public UncommittedChanges getUncommittedChanges() {
		return uncommitted;
	}

}