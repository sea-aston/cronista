package uk.ac.aston.provlayer.observer.model;

//TODO Move this to observer.instrument.aspectJ.POJO
public class ModelPartFeatureVersion {

	private final ModelPartFeature partFeature;
	private final int storageVersion;
	private final String memorySpace;
	private final int memoryVersion;
	private final boolean storageVersioning;

	public ModelPartFeatureVersion(ModelPartFeature partFeature, int storageVersion, String memorySpace, int memoryVersion) {
		this.partFeature = partFeature;
		this.storageVersion = storageVersion;
		this.memorySpace = memorySpace;
		this.memoryVersion = memoryVersion;
		this.storageVersioning = partFeature.hasStorageVersioning();
	}
	
	public ModelPart getPart() {
		return partFeature.getPart();
	}

	public ModelFeature getFeature() {
		return partFeature.getFeature();
	}
	
	public Object getFeatureValue() {
		return partFeature.getFeature().getValue();
	}

	public int getStorageVersion() {
		return storageVersion;
	}
	
	public String getStorageVersionString() {
		return String.format("s:%d", storageVersion);
	}

	public int getMemoryVersion() {
		return memoryVersion;
	}

	public String getMemorySpace() {
		return memorySpace;
	}
	
	public String getMemorySpaceVersionString() {
		return String.format("m:%s_%d", memorySpace, memoryVersion);
	}

	public ModelPartFeatureVersion setStorageVersion(int newVersion) {
		return new ModelPartFeatureVersion(partFeature, newVersion, memorySpace, 0);
	}

	public ModelPartFeatureVersion setMemoryVersion(int newVersion) {
		return new ModelPartFeatureVersion(partFeature, storageVersion, memorySpace, newVersion);
	}

	public boolean isInStorage() {
		return memoryVersion == 0 && storageVersion > 0;
	}
	
	public String getEntityID() {
		return partFeature.getEntityID();
	}
	
	public String getEntityInstanceID() {
	
		String EIID = String.format("%s@", partFeature.getEntityID());
		
		if (this.storageVersioning) { // Storage and Memory Model
			
			if (this.getMemoryVersion() == 0) { // Storage version
				return EIID = String.format("%s%s",
						EIID,
						this.getStorageVersionString()
						);	
				
			} else { // Storage Version Loaded in Memory
				return EIID = String.format("%s%s.%s",
						EIID,
						this.getStorageVersionString(),
						this.getMemorySpaceVersionString()
						);		
			}
			
		} else { // Memory only Model
			return EIID = String.format("%s%s", EIID, this.getMemorySpaceVersionString());
		}
		
		
		

	}

}
