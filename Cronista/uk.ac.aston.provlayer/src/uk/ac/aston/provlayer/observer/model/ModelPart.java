package uk.ac.aston.provlayer.observer.model;

public interface ModelPart {

	String getIdentifier();

	String getType();
	
	Object getRaw();
	
	boolean hasStorageVersioning();
	
	int getStorageVersion();

}
