package uk.ac.aston.provlayer.observer.model.cdo;

import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.cdo.CDOState;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import uk.ac.aston.provlayer.observer.model.ModelPart;

public class CDOModelPart implements ModelPart {

	private EObject eObject;
	private CDOObject cdoObject;
	private boolean isCDOObject = false;
	private boolean hasStorageVersioning;

	public CDOModelPart(EObject eObject) {
		this.eObject = eObject;

		if (eObject instanceof CDOObject) {

			this.cdoObject = (CDOObject) eObject;
			this.isCDOObject = true;
			this.hasStorageVersioning = true;

		}
	}

	@Override
	public String getIdentifier() {
		if (isCDOObject)
			return String.valueOf(cdoObject.cdoID());
		else
			return EcoreUtil.getIdentification(eObject);
	}

	@Override
	public String getType() {
		return cdoObject.getClass().getName();
	}

	@Override
	public EObject getRaw() {
		if (isCDOObject)
			return cdoObject;
		else
			return eObject;

	}

	@Override
	public boolean hasStorageVersioning() {
		return this.hasStorageVersioning;
	}

	@Override
	public int getStorageVersion() {
		int cdoVer = 0; 
		if (hasStorageVersioning) {
			// Handle Transient states where no CDOObject has been committed to CDO.
			if (cdoObject.cdoState() != CDOState.TRANSIENT) {
				cdoVer = cdoObject.cdoRevision().getVersion();
			}
		}
		return cdoVer;
	}

	/*
	 * 
	 * private EObject eob; public CDOModelPart(EObject eob) { this.eob = eob; }
	 * 
	 * @Override public String getIdentifier() { if (eob instanceof CDOObject) {
	 * final CDOObject cdoObject = (CDOObject) eob; //return
	 * cdoObject.cdoRevision().toString(); //return
	 * EcoreUtil.getURI(cdoObject).fragment(); //return
	 * cdoObject.cdoID().toString(); return String.valueOf(cdoObject.cdoID()); }
	 * return EcoreUtil.getIdentification(eob); }
	 * 
	 * @Override public String getType() { return eob.getClass().getName(); }
	 * 
	 * @Override public EObject getRaw() { return eob; }
	 */
}