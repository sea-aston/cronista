package uk.ac.aston.provlayer.observer.versionController;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

import uk.ac.aston.provlayer.observer.model.ModelPartFeature;
import uk.ac.aston.provlayer.observer.model.ModelPartFeatureVersion;

// This version controller assumes the Storage system is the primary Version source for StorageVersions

public class VersionController {

	private final String memorySpace = Thread.currentThread().getName();

	private class storageMemoryVersion {

		private AtomicInteger storageVersion;
		private AtomicInteger memoryVersion;

		public storageMemoryVersion(int storageVersion, int memoryVersion) {
			this.storageVersion = new AtomicInteger(storageVersion);
			this.memoryVersion = new AtomicInteger(memoryVersion);
		}

		public int getStorageVersion() {
			return this.storageVersion.get();
		}

		public void setStorageVersion(int storageVersion) {
			this.storageVersion.set(storageVersion);
			this.memoryVersion.set(0);
		}

		public int getAtomicMemoryVersion() {
			return memoryVersion.get();
		}

		public int incMemoryVersion() {
			return this.memoryVersion.incrementAndGet();
		}

		public String toString() {
			return String.format("[s%s,m%s]", storageVersion.get(), memoryVersion.get());
		}
	}

	// HashMap <Key:EntityID Value:storageMemoryVersion>
	private HashMap<String, storageMemoryVersion> versionStorageMemoryMap = new HashMap<>();
	private HashMap<String, AtomicInteger> versionMemoryMap = new HashMap<>();

	public ModelPartFeatureVersion getCurrentVersion(ModelPartFeature modelPartFeature) {
		if (modelPartFeature.hasStorageVersioning()) { // STORAGE model
			return getCurrentStorageMemory(modelPartFeature);
		} else { // MEMORY only model
			return getCurrentMemory(modelPartFeature);
		}

	}

	public ModelPartFeatureVersion updateMemoryVersion(ModelPartFeature modelPartFeature) {
		if (modelPartFeature.hasStorageVersioning()) { // Model Storage in use
			return updateStorageMemory(modelPartFeature);
		} else { // Memory only model
			return updateMemory(modelPartFeature);
		}
	}

	public ModelPartFeatureVersion getLastKnownVersion(ModelPartFeature modelPartFeature) {
		if (modelPartFeature.hasStorageVersioning()) { // STORAGE model
			return getLastKnownStorageMemory(modelPartFeature);
		} else { // Memory only model
			return getLastKnownMemory(modelPartFeature);
		}
	}

	public void removeCommitted(ModelPartFeature modelPartFeature) {
		String partFeatureEntityID = modelPartFeature.getEntityID();
		// System.out.println("VC: Remove(): " + partFeatureEntityID); // DEBUG
		versionStorageMemoryMap.remove(partFeatureEntityID);
	}

	public int getMapSize() {
		return versionStorageMemoryMap.size();
	}

	//
	// Memory ONLY
	//

	private ModelPartFeatureVersion getCurrentMemory(ModelPartFeature modelPartFeature) {

		ModelPartFeatureVersion partFeatureVersion = null;
		String partFeatureEntityID = modelPartFeature.getEntityID();

		storageMemoryVersion mapSMV = versionStorageMemoryMap.get(partFeatureEntityID);
		if (mapSMV == null) { // NEW Entry - Memory only Versions start at 1 to avoid StorageVersion processes
			mapSMV = new storageMemoryVersion(0, 0);
			versionStorageMemoryMap.put(partFeatureEntityID, mapSMV);
		}
		partFeatureVersion = new ModelPartFeatureVersion(modelPartFeature, mapSMV.getStorageVersion(), memorySpace,
				mapSMV.getAtomicMemoryVersion());
//		System.out.println("VC: getCurrentMemory(): " + partFeatureVersion.getEntityInstanceID());  // DEBUG
		return partFeatureVersion;
	}

	private ModelPartFeatureVersion updateMemory(ModelPartFeature modelPartFeature) {
		ModelPartFeatureVersion partFeatureVersion = null;
		String partFeatureEntityID = modelPartFeature.getEntityID();

		storageMemoryVersion mapSMV = versionStorageMemoryMap.get(partFeatureEntityID);
		if (mapSMV == null) { // NEW Entry
			mapSMV = new storageMemoryVersion(0, 0);
			versionStorageMemoryMap.put(partFeatureEntityID, mapSMV);
		} else {
			// inc Memory version store and return
			mapSMV.incMemoryVersion();
			versionStorageMemoryMap.put(partFeatureEntityID, mapSMV);
		}
		partFeatureVersion = new ModelPartFeatureVersion(modelPartFeature, mapSMV.getStorageVersion(), memorySpace,
				mapSMV.getAtomicMemoryVersion());
//		System.out.println("VC: updateMemory(): " + partFeatureVersion.getEntityInstanceID());  // DEBUG
		return partFeatureVersion;
	}

	private ModelPartFeatureVersion getLastKnownMemory(ModelPartFeature modelPartFeature) {
		ModelPartFeatureVersion partFeatureVersion = null;
		String partFeatureEntityID = modelPartFeature.getEntityID();

		storageMemoryVersion mapSMV = versionStorageMemoryMap.get(partFeatureEntityID);
		if (mapSMV != null) {
			partFeatureVersion = new ModelPartFeatureVersion(modelPartFeature, mapSMV.getStorageVersion(), memorySpace,
					mapSMV.getAtomicMemoryVersion());
		}
		
		// DEBUG
		// System.out.println("VC: getLastKnownMemory(): " + partFeatureEntityID + " :
		// "+ mapSMV.toString());

		return partFeatureVersion;
	}

	//
	// Storage and Memory
	//

	private ModelPartFeatureVersion getCurrentStorageMemory(ModelPartFeature modelPartFeature) {
		ModelPartFeatureVersion partFeatureVersion = null;
		String partFeatureEntityID = modelPartFeature.getEntityID();
		int storageVersion = modelPartFeature.getStorageVersion();

		storageMemoryVersion mapSMV = versionStorageMemoryMap.get(partFeatureEntityID);
		if (mapSMV == null) { // NEW Entry -- Loaded or new in memory?

			mapSMV = new storageMemoryVersion(storageVersion, 0);
			versionStorageMemoryMap.put(partFeatureEntityID, mapSMV);

			// DEBUG
			// System.out.println("VC: getCurrentStorageMemory(): NEW Entry " +
			// partFeatureEntityID + " : "+ mapSMV.toString());
			
			return new ModelPartFeatureVersion(modelPartFeature, mapSMV.getStorageVersion(), memorySpace,
					mapSMV.getAtomicMemoryVersion());

		}
		if (storageVersion > mapSMV.getStorageVersion()) { // Loaded or commit?

			// DEBUG
			// System.out.println("VC: getCurrentStorageMemory(): Reset memory version
			// sv>smv " + partFeatureEntityID + " : " + storageVersion + " > " +
			// mapSMV.toString());

			mapSMV = new storageMemoryVersion(storageVersion, 0);
			versionStorageMemoryMap.replace(partFeatureEntityID, mapSMV);

			// DEBUG
			// System.out.println("VC: getCurrentStorageMemory(): RESET TO mapSMV = " +
			// partFeatureEntityID + " : " + mapSMV.toString());

			return new ModelPartFeatureVersion(modelPartFeature, mapSMV.getStorageVersion(), memorySpace,
					mapSMV.getAtomicMemoryVersion());

		}

		return new ModelPartFeatureVersion(modelPartFeature, mapSMV.getStorageVersion(), memorySpace,
				mapSMV.getAtomicMemoryVersion());
	}

	private ModelPartFeatureVersion updateStorageMemory(ModelPartFeature modelPartFeature) {
		ModelPartFeatureVersion partFeatureVersion = null;
		String partFeatureEntityID = modelPartFeature.getEntityID();
		int storageVersion = modelPartFeature.getStorageVersion();

		storageMemoryVersion mapSMV = versionStorageMemoryMap.get(partFeatureEntityID);
		if (mapSMV == null) { // NEW Entry

			mapSMV = new storageMemoryVersion(storageVersion, 1);
			versionStorageMemoryMap.put(partFeatureEntityID, mapSMV);
			
			// DEBUG
			// System.out.println("VC: updateStorageMemory(): NEW Entry " +
			// partFeatureEntityID + " : "+ mapSMV.toString()); 

			return new ModelPartFeatureVersion(modelPartFeature, mapSMV.getStorageVersion(), memorySpace,
					mapSMV.getAtomicMemoryVersion());
		}

		if (storageVersion > mapSMV.getStorageVersion()) { // Loaded or commit?

			// DEBUG
			// System.out.println("VC: updateStorageMemory(): Reset memory version sv>smv "
			// + partFeatureEntityID + " : " + storageVersion + " > " + mapSMV.toString());


			mapSMV = new storageMemoryVersion(storageVersion, 1);
			versionStorageMemoryMap.replace(partFeatureEntityID, mapSMV);

			// DEBUG
			// System.out.println("VC: updateStorageMemory(): RESET TO mapSMV = " +
			// partFeatureEntityID + " : " + mapSMV.toString()); // DEBUG

			return new ModelPartFeatureVersion(modelPartFeature, mapSMV.getStorageVersion(), memorySpace,
					mapSMV.getAtomicMemoryVersion());

		}

		// inc Memory version store and return
		mapSMV.incMemoryVersion();
		versionStorageMemoryMap.put(partFeatureEntityID, mapSMV);
		return new ModelPartFeatureVersion(modelPartFeature, mapSMV.getStorageVersion(), memorySpace,
				mapSMV.getAtomicMemoryVersion());

	}

	private ModelPartFeatureVersion getLastKnownStorageMemory(ModelPartFeature modelPartFeature) {
		ModelPartFeatureVersion partFeatureVersion = null;
		String partFeatureEntityID = modelPartFeature.getEntityID();
		int storageVersion = modelPartFeature.getStorageVersion();

		storageMemoryVersion mapSMV = versionStorageMemoryMap.get(partFeatureEntityID);
		if (mapSMV != null) {
			partFeatureVersion = new ModelPartFeatureVersion(modelPartFeature, mapSMV.getStorageVersion(), memorySpace,
					mapSMV.getAtomicMemoryVersion());
		} else {
			// This should never ever happen, but it can happen use the line below to DEBUG
			//System.out.println("NOW WHAT? getLastKnownStorageMemory() == null ! " + partFeatureEntityID	+ " StorageVersion: " + storageVersion);  // DEBUG
			
			// Reasons for being here
			// COMMIT() is reporting an unversioned "before" Model Part Feature, which is likely due to the model part being outside of @ObserverModel
			
		}
		
		// DEBUG
		// System.out.println("VC: getLastKnownStorageMemory(): " + partFeatureEntityID
		// + " : "+ mapSMV.toString());

		return partFeatureVersion;
	}

}
