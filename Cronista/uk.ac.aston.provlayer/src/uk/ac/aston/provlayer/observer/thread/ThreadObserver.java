package uk.ac.aston.provlayer.observer.thread;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

import org.eclipse.emf.cdo.common.commit.CDOCommitInfo;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CommitException;
import org.eclipse.emf.cdo.util.ConcurrentAccessException;

import uk.ac.aston.provlayer.curator.messages.ActivityEndedMessage;
import uk.ac.aston.provlayer.curator.messages.*;
import uk.ac.aston.provlayer.descriptions.ActivityDescription;
import uk.ac.aston.provlayer.descriptions.AgentDescription;
import uk.ac.aston.provlayer.descriptions.EntityDescription;
import uk.ac.aston.provlayer.observer.model.*;
import uk.ac.aston.provlayer.observer.model.ModelAccess.AccessType;
import uk.ac.aston.provlayer.observer.model.cdo.CommitReporterCDO;
import uk.ac.aston.provlayer.observer.versionController.VersionController;

public class ThreadObserver {
	
	private final AgentDescription agentDescription;
	private final Deque<ActivityDescription> activityStack = new ArrayDeque<>();
	private final Set<ObserverMessageProcessor> subscribers = new HashSet<>();

	// OBSERVER FOR THIS THREAD ONLY
	public static final ThreadLocal<ThreadObserver> INSTANCE = new ThreadLocal<>() {
		@Override
		protected ThreadObserver initialValue() {
			final ThreadObserver obs = new ThreadObserver(Thread.currentThread().getName());
			for (Consumer<ThreadObserver> c : OBSERVER_HOOKS) {
				c.accept(obs);
			}
			return obs;
		}
	};

	private ThreadObserver(String name) {
		this.agentDescription = new AgentDescription(name);
		System.out.println("An Observer is watching " + name);
	}

	public String getAgentName() {
		return agentDescription.getID();
	}

	// =================================
	// SUBSCRIBERS
	// =================================

	private static final Set<Consumer<ThreadObserver>> OBSERVER_HOOKS = new HashSet<>();
	
	public static boolean addNewObserverHook(Consumer<ThreadObserver> hook) {
		synchronized (OBSERVER_HOOKS) {
			return OBSERVER_HOOKS.add(hook);
		}
	}

	public static boolean removeNewObserverHook(Consumer<ThreadObserver> hook) {
		synchronized (OBSERVER_HOOKS) {
			return OBSERVER_HOOKS.remove(hook);
		}
	}

	/*
	 * Using subscribers rather than hardwiring the TO to the Curator helps
	 * testability, as we can just wire up something else during testing and
	 * we avoid the complexity of multithreading in simple tests.
	 */

	public boolean subscribe(ObserverMessageProcessor proc) {
		return subscribers.add(proc);
	}

	public boolean unsubscribe(ObserverMessageProcessor proc) {
		return subscribers.remove(proc);
	}

	// =============================================
	//  METHODS USED BY MODEL INSPECTOR/INSTRUMENTS
	// =============================================
	
	// VERSION CONTROLLER INSTANCE FOR THIS THREAD ONLY
	private ThreadLocal<VersionController> localVersionController = new ThreadLocal<>();
	public VersionController getVersionControllerInstance () {
		VersionController versionControllerInstance = localVersionController.get();
		if (versionControllerInstance==null) {
			localVersionController.set(new VersionController());
		}
		while (versionControllerInstance==null) {
			System.out.println("Waiting on versionControllerInstance...");
			versionControllerInstance = localVersionController.get();
		}
		return versionControllerInstance;
	}
	
	
	// MODEL ACCESS INSTANCE FOR THIS THREAD ONLY
	private ThreadLocal<ModelAccessBuilder> localModelAccessBuilder = new ThreadLocal<>();
	public ModelAccessBuilder getModelAccessBuilderInstance() {
		ModelAccessBuilder modelAccessInstance = localModelAccessBuilder.get();
		if (modelAccessInstance == null) {
			localModelAccessBuilder.set(new ModelAccessBuilder());
		}
		while (modelAccessInstance == null) {
			System.out.println("Waiting on modelAccessInstance...");
			modelAccessInstance = localModelAccessBuilder.get();
		}
		return modelAccessInstance;
	}
	
	// MODEL COMMIT (CDO) 
	// TODO can we move this CDO detail out of here?
	// Short term solution for handling CDO Commit catching, point your commits at
	// this method on the observer for the thread.
	private final CommitReporterCDO commitReporter = new CommitReporterCDO();
	public CDOCommitInfo commit(CDOTransaction transaction)
			throws ConcurrentAccessException, CommitException {
		//return commitReporter.commit(transaction);
		return commitReporter.newCommit(transaction);
	}
	
	
	// Inspector/Instruments use these the systemCallFlag to turn set()/get() logging on/off
	private boolean systemCallFlag = true;
	public boolean getSystemCallFlag() {
		return systemCallFlag;
	}

	public void setSystemCallFlag(boolean systemCall) {
		systemCallFlag = systemCall;
	}


	// ModelAccess > Message conversion

	private static final int USE_OLD_ENTITY             = 0x1;
	private static final int FORCE_STORAGE_VERSION_TRUE = 0x2;
	
	public void reportAccess(ModelAccess access) {
		switch (access.getAccessType()) {
		case READ: reportRead(access); break;
		case WRITE: reportWrite(access); break;
		case COMMIT: reportCommit(access); break;
		default: throw new IllegalArgumentException("Unknown access type " + access.getAccessType());
		}
	}

	protected void reportCommit(ModelAccess access) {
		sendMessage(new EntityCommittedMessage(
			getMessageID(),	agentDescription, getCurrentActivity(),
			buildEntityDescription(access, USE_OLD_ENTITY),
			buildEntityDescription(access, 0),
			access.getCommitTime()
			));
	}

	protected void reportWrite(ModelAccess access) {
		// GENBY
		if (access.getOldEntityInstanceID() == null) {
//			System.out.println("reportWrite - CREATE");
			// new set
			sendMessage(new EntityCreateMessage(
					getMessageID(), agentDescription, getCurrentActivity(),
					buildEntityDescription(access, 0)
					));
			// Log uncommitted change
			commitReporter.getUncommittedChanges().logChange(buildEntityDescription (access,0));
			
		} else {
			// TODO Reporting here could be improved to report the old values at the moment we send a blank message, to make an empty entity
			// updated attribute
			sendMessage(new EntityUpdatedMessage(
					getMessageID(), agentDescription, getCurrentActivity(),
					buildEntityDescription(access, USE_OLD_ENTITY),
					buildEntityDescription(access, 0)
					));
			// Log uncommitted change
			commitReporter.getUncommittedChanges().logChange(buildEntityDescription (access,0));
			
		}
	}

	protected void reportRead(ModelAccess access) {
		// USED
		if (access.getOldEntityInstanceID() == null) {
			sendMessage(new EntityReadMessage(
				getMessageID(), agentDescription, getCurrentActivity(),
				buildEntityDescription(access, 0)
				));
		} else {
			sendMessage(new EntityLoadedMessage(
				getMessageID(), agentDescription, getCurrentActivity(),
				buildEntityDescription(access, USE_OLD_ENTITY | FORCE_STORAGE_VERSION_TRUE),
				buildEntityDescription(access, 0)				
				));
		}
	}

	private synchronized EntityDescription buildEntityDescription(ModelAccess access, int flags) {
		// TODO There is a lot of cleaning up and renaming needed here.  The EntityDescription model needs better attribute names.
		// modelObject -- attributeMetaData -- attributeData as more generalised descriptions for the kind of information that should be captured? 
		final EntityDescription currentEntityDescription = new EntityDescription();

		// ==========================================================
		// Description of the model to be stored in an Entity entry
		// ==========================================================
		final String entityInstanceID = (flags & USE_OLD_ENTITY) != 0 ? access.getOldEntityInstanceID() : access.getEntityInstanceID(); 
		currentEntityDescription.setEntityInstanceID(entityInstanceID);  // This is a composition of the below

		// These are all the references to identify an Eobject at a point in time/version
		final ModelPart modelObject = (flags & USE_OLD_ENTITY) != 0 ? null : access.getEObject();
		if (modelObject != null)
		{  // Objects available to reference
			currentEntityDescription.setObjectIDAsString(modelObject.getIdentifier());
			
			// This is null when reporting CDO Commits
			final ModelFeature attributeMetaData = access.getFeature();
			if (attributeMetaData != null) {
				currentEntityDescription.setAttributeIDAsString(attributeMetaData.getIdentifier());
			}
			currentEntityDescription.setMemoryVersionAsString(access.getMemoryVersion());
			currentEntityDescription.setStorageVersionAsString(access.getStorageVersion());
		
			// When an entity is describing an object that is in a model store (E.g. CDO) we can reference it!
			final boolean inModelStorage = (flags & FORCE_STORAGE_VERSION_TRUE) != 0 ? true : access.isInModelStorage();
			currentEntityDescription.setInModelStorage(inModelStorage);
			if (inModelStorage)
			{
				// These should point to the CDO objects @ some version point...  --  We need to check how to do this.
				currentEntityDescription.setObjectStorageReference(modelObject);
				currentEntityDescription.setAttributeMetadataStorageReference(attributeMetaData);

				// TODO needs something else here (don't cast down to EObject)
				currentEntityDescription.setAttributeDataStorageReference(access.getValue());
			}
			
			// We can keep can persist a picture of the object attribute state as Strings on the description here -- Not every thing can string
			currentEntityDescription.setObjectName(modelObject.getIdentifier());
			currentEntityDescription.setObjectType(modelObject.getType());
			
			if (attributeMetaData != null) {
				currentEntityDescription.setAttributeNameAsString(attributeMetaData.getName());
			}

			Object attributeData = access.getValue();
			if (attributeData != null) {
				currentEntityDescription.setAttributeTypeAsString(attributeData.getClass().getName());
				currentEntityDescription.setAttributeValueAsString(attributeData.toString());
			} else {
				currentEntityDescription.setAttributeTypeAsString("null");
				currentEntityDescription.setAttributeValueAsString("null");
			}
		} else {
			// No objects for reference
			currentEntityDescription.setObjectIDAsString("No objects for reference");
			currentEntityDescription.setMemoryVersionAsString(access.getMemoryVersion());
			currentEntityDescription.setStorageVersionAsString(access.getStorageVersion());
		}
		
		// Start and End time -- The Curator will resolve this
	
		// Describe the access
		currentEntityDescription.setLastAccessType(access.getAccessType() == AccessType.READ ? 'R' : 'W');
		currentEntityDescription.setLastAccessTime(access.getTime());

		return currentEntityDescription;
	}

	// Messaging sending

	private long messageNumber = 0;
	private String getMessageID() {
		return String.format("obs.%s.%d", getAgentName(), messageNumber++);
	}

	private void sendMessage(ObserverMessage m) {
		if (m.getActivity() == null) {
			// System.out.println("dropped message NO ACTIVITY " + m.getId());  // DEBUG
			return;
		}
		for (ObserverMessageProcessor proc : subscribers) {
			m.accept(proc);
		}
	}

	// ===============
	// ACTIVITY SCOPES
	// ===============
	
	private long activityNumber = 0;
	private synchronized long getActivityNumber() {
		// TODO This needs to be replaced with the ECore UUID generator for creating unique strings for use in the ActivityInstanceID
		activityNumber++;
		return activityNumber;
	}
	
	private synchronized ActivityDescription getCurrentActivity() {
		return activityStack.isEmpty() ? null : activityStack.peek();
	}
	
	public synchronized void startActivity(String activityName) {
		ActivityDescription newActivity = new ActivityDescription(createActivityInstanceID(agentDescription.getID(),activityName),activityName, System.currentTimeMillis());
		activityStack.push(newActivity);
		sendMessage(new ActivityStartedMessage(getMessageID(), agentDescription, getCurrentActivity()));
	}

	public synchronized void endActivity() {
		// Update myCurrentActivity.EndTime
		// Move myCurrentActivity.InterupptedActivityOpposite (got back to the last one)
		// NULL the myCurrentActivity.InterupptedActivity -- The Activity is not longer
		// referenced here, and holds in the message queue object
		if (!activityStack.isEmpty()) {
			ActivityDescription myCurrentActivity = activityStack.pop();
			myCurrentActivity.setEndTime(System.currentTimeMillis());
			sendMessage(new ActivityEndedMessage(getMessageID(), agentDescription, getCurrentActivity()));
		}
	}

	// ===============
	// ID CREATION
	// ===============

	// This is here for legacy support, has been replaced with features in the ModelPartFeature 
	public static String createEntityInstanceID(String ObjectID, String AttributeID, String memoryVersion, String storageVersion) {
		//return ObjectID + "@" + AttributeID + "@" + memoryVersion + "@" + storageVersion;
		return ObjectID + "@" + AttributeID + "@s" + storageVersion + "@m" + memoryVersion;
	}

	public String createActivityInstanceID(String AgentName, String ActivityName) {
		// TODO We should probably use Name + UUID to be sure
		return AgentName + "@" + ActivityName + "@" + getActivityNumber();
	}

	public static String createAgentInstanceID(String AgentName) {
		// TODO We should probably use Name + UUID to be sure
		return AgentName;
	}

}