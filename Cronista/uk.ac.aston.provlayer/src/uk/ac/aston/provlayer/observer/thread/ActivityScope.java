package uk.ac.aston.provlayer.observer.thread;

import uk.ac.aston.provlayer.provlayercontrol.ProvLayerControl;

public class ActivityScope implements AutoCloseable {

	// In order to make close() idempotent
	private boolean open = true;

	public ActivityScope(String activityName) {
		if (ProvLayerControl.getProvLayerControl() != null) {
			final ThreadObserver obs = ThreadObserver.INSTANCE.get();
			obs.startActivity(activityName);
		}
	}

	@Override
	public void close() // throws Exception
	{
		if (ProvLayerControl.getProvLayerControl() != null) {
			if (open) {
				final ThreadObserver obs = ThreadObserver.INSTANCE.get();
				obs.endActivity();
				open = false;
			}
		}
	}

}
