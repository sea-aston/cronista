package uk.ac.aston.provlayer.observer.model;

public interface ModelFeature {

	String getIdentifier();
	
	String getName();

	String getType();
	
	Object getValue();
	
	String getValueAsString();

	Object getRaw();
	
	boolean hasStorageVersioning();
	
	int getStorageVersion();

}
