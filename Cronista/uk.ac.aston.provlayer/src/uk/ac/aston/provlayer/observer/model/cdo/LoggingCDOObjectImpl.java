package uk.ac.aston.provlayer.observer.model.cdo;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.internal.cdo.CDOObjectImpl;

import uk.ac.aston.provlayer.observer.model.ModelAccess;
import uk.ac.aston.provlayer.observer.model.ModelAccess.AccessType;
import uk.ac.aston.provlayer.observer.thread.ThreadObserver;

public class LoggingCDOObjectImpl extends CDOObjectImpl {

	// Add in-memory version counter per feature by name
	private Map<String, Long> memoryVersions = new HashMap<>();

	// Needed to detect if a commit changes the CDO version
	private int lastAccessedCDOver;

	public String getMemoryVersion(String prefix, String feature) {
		// TODO - Memory version should denote Memory space (private public)
		// There might be a 0 condition need here, when 0 we are referring to the storage version

		return prefix + ":" + String.valueOf(memoryVersions.getOrDefault(feature, 0L));
	}

	private long incrementMemoryVersion(String feature) {
		final long memoryVersion = memoryVersions.computeIfAbsent(feature, (x) -> 0L);
		memoryVersions.put(feature, memoryVersion + 1);
		return memoryVersion;
	}

	@Override
	protected Object eDynamicGet(int dynamicFeatureID, EStructuralFeature eFeature, boolean resolve, boolean coreType) {
		final ThreadObserver theThreadObserver = ThreadObserver.INSTANCE.get();	
		if (theThreadObserver.getSystemCallFlag()) {
			try {
				theThreadObserver.setSystemCallFlag(false);
				return reportGet(dynamicFeatureID, eFeature, resolve, coreType, theThreadObserver);
			} finally {
				theThreadObserver.setSystemCallFlag(true);
			}
		}
		return super.eDynamicGet(dynamicFeatureID, eFeature, resolve, coreType);
	}

	private Object reportGet(int dynamicFeatureID, EStructuralFeature eFeature, boolean resolve, boolean coreType, final ThreadObserver theThreadObserver) {
		final int cdoVersion = cdoVersionSafe();
		
		//  COLLECT DATA 
		Object value = super.eDynamicGet(dynamicFeatureID, eFeature, resolve, coreType);
		EStructuralFeature dynamicFeatureIDObject = this.eClass().getEStructuralFeature(dynamicFeatureID);

		// entityInstanceID for the access
		long memoryVersion = memoryVersions.getOrDefault(eFeature.getName(), 0L);
		String entityInstanceID = null;
		String oldEntityInstanceID = null;
		if (memoryVersion==0) {
			// Special case, loading from Storage point oldEntityInstanceID to the storage version
			// This could be the first access since opening a transaction OR the first access AFTER a commit!
			incrementMemoryVersion(eFeature.getName());
			entityInstanceID = ThreadObserver.createEntityInstanceID(
					String.valueOf(this.cdoID()), 
					String.valueOf(dynamicFeatureIDObject.getFeatureID()), 
					getMemoryVersion(theThreadObserver.getAgentName(), eFeature.getName()),
					String.valueOf(cdoVersion));

			oldEntityInstanceID = ThreadObserver.createEntityInstanceID(
					String.valueOf(this.cdoID()),
					String.valueOf(dynamicFeatureIDObject.getFeatureID()),
					"0",
					String.valueOf(cdoVersion));
		} else {
			// Using something that is already in memory
			entityInstanceID = ThreadObserver.createEntityInstanceID(
					String.valueOf(this.cdoID()), 
					String.valueOf(dynamicFeatureIDObject.getFeatureID()), 
					getMemoryVersion(theThreadObserver.getAgentName(), eFeature.getName()),
					String.valueOf(cdoVersion));
		}
			
		
		//
		// REPORT
		//
		theThreadObserver.reportAccess(new ModelAccess.Builder()
			.entityID(entityInstanceID)
			.oldEntityID(oldEntityInstanceID)
			.eObject(new CDOModelPart(this)).feature(new CDOModelFeature(dynamicFeatureIDObject)).value(value)
			.memoryVersion(getMemoryVersion(theThreadObserver.getAgentName(), eFeature.getName()))
			.storageVersion(cdoVersion + "")
			.build()
		);

		return value;
	}

	@Override
	protected void eDynamicSet(int dynamicFeatureID, EStructuralFeature eFeature, Object newValue) {
		final ThreadObserver obs = ThreadObserver.INSTANCE.get();
		if (obs.getSystemCallFlag()) {
			try {
				obs.setSystemCallFlag(false); // Recursion problems if you call "this" the wrong way!
				reportSet(dynamicFeatureID, eFeature, newValue, obs);
			} finally {
				obs.setSystemCallFlag(true);
			}
		}
	}

	private void reportSet(int dynamicFeatureID, EStructuralFeature eFeature, Object newValue,
			final ThreadObserver obs) {
		int safeCDORev = cdoVersionSafe();
		
		// TODO : Should we provide more details about the Object:Attribute we are over writing?
		// AttributeValue - AttributeMeta - Version
		final EStructuralFeature oldDynamicFeatureIDObject = this.eClass().getEStructuralFeature(dynamicFeatureID);
		
		// Special case -- memoryVersion 0 should reference a StorageVersion
		final long memoryVersion = memoryVersions.getOrDefault(eFeature.getName(), 0L);
		String oldEntityInstanceID = null;
		if(memoryVersion == 0) {
			// We need to indicate this was from Storage Version
			oldEntityInstanceID = ThreadObserver.createEntityInstanceID(
					String.valueOf(this.cdoID()),
					String.valueOf(oldDynamicFeatureIDObject.getFeatureID()),
					"0",
					String.valueOf(safeCDORev));
		} else {
			// This is in memory no worries
			oldEntityInstanceID = ThreadObserver.createEntityInstanceID(
					String.valueOf(this.cdoID()), 
					String.valueOf(oldDynamicFeatureIDObject.getFeatureID()),
					getMemoryVersion(obs.getAgentName(), eFeature.getName()),
					String.valueOf(safeCDORev));
		}
		
		//  DO THE SET
		incrementMemoryVersion(eFeature.getName());
		super.eDynamicSet(dynamicFeatureID, eFeature, newValue);

		//  COLLECT DATA FOR THE ENTITY AFTER CHANGE
		safeCDORev = cdoVersionSafe();
		
		//newValue given in call
		EStructuralFeature newDynamicFeatureIDObject = this.eClass().getEStructuralFeature(dynamicFeatureID);
		String newEntityInstanceID = ThreadObserver.createEntityInstanceID(
				String.valueOf(this.cdoID()), 
				String.valueOf(newDynamicFeatureIDObject.getFeatureID()),
				getMemoryVersion(obs.getAgentName(), eFeature.getName()),
				String.valueOf(safeCDORev));
		
		// REPORT Setter
		obs.reportAccess(new ModelAccess.Builder()
				.entityID(newEntityInstanceID)
				.oldEntityID(oldEntityInstanceID)
				.eObject(new CDOModelPart(this)).feature(new CDOModelFeature(eFeature))
				.value(newValue)
				.memoryVersion(getMemoryVersion(obs.getAgentName(), eFeature.getName()))
				.storageVersion(safeCDORev + "")
				.type(AccessType.WRITE)
				.build());
	}

	private int cdoVersionSafe() {
		if (cdoRevision() != null) {
			final int safeCDORev = cdoRevision().getVersion();

			// CHECK THE CDO Version, if a commit has been done but the transaction is still open
			// This puts the memory version back to 0 if the CDO version has changed.
			if(safeCDORev != lastAccessedCDOver) {
				memoryVersions.clear();
				lastAccessedCDOver = safeCDORev;
			}

			return safeCDORev; 
		}

		return 0;
	}

}
