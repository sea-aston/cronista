package uk.ac.aston.provlayer.observer.model;

public enum ObjectAccessMethod {
		Get, 
		before_Set, 
		after_Set,
		Commit
}
