package uk.ac.aston.provlayer.storage.api;

import java.util.Set;

import uk.ac.aston.provlayer.descriptions.ActivityDescription;

public interface Activity {

	String getID();
	void setID(String id);

	ActivityDescription getActivityDescription();
	void setActivityDescription(ActivityDescription desc);

	Agent getWasAssociatedWith();
	void setWasAssociatedWith(Agent agent);

	/**
	 * Returns an unmodifiable set with the activities that informed this one.
	 */
	Set<Activity> getWasInformedBy();
	void addWasInformedBy(Activity InformingAct);
	
	/**
	 * Returns an unmodifiable set with the entities used by this activity.
	 */
	Set<Entity> getUsed();
	void addUsed(Entity usedEntity);

}
