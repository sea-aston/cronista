package uk.ac.aston.provlayer.storage.api;

public interface TimeWindow {

	long getStartLocalTime();
	void setStartLocalTime(long l);

	long getEndLocalTime();
	void setEndLocalTime(long t);

	ProvenanceGraph getProvenanceGraph();
	void setProvenanceGraph(ProvenanceGraph pg);

	SystemModelState getSystemModelState();
	void setSystemModelState(SystemModelState sms);
	
}
