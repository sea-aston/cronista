package uk.ac.aston.provlayer.storage.api;

import uk.ac.aston.provlayer.descriptions.ActivityDescription;
import uk.ac.aston.provlayer.descriptions.AgentDescription;
import uk.ac.aston.provlayer.descriptions.EntityDescription;

public interface ProvenanceGraph {

	Iterable<Activity> getActivities();
	Iterable<Entity> getEntities();
	Iterable<Agent> getAgents();

	Agent searchAddProvAgent(AgentDescription agent);
	Activity searchAddProvActivity(AgentDescription agent, ActivityDescription activity);
	Entity searchProvEntity(EntityDescription oldEntity);
	Entity addProvEntity(EntityDescription oldEntity);

	default Entity searchAddProvEntity(EntityDescription entity) {
		Entity provEntity = searchProvEntity(entity);
		if (provEntity == null) {
			provEntity = addProvEntity(entity);
		}
		return provEntity;
	}

	default Entity searchAddProvEntityDescription(EntityDescription entityDesc) {
		final Entity provGraphNode = searchAddProvEntity(entityDesc);
		//provGraphNode.getEntityDescriptions().add(entityDesc);  // No add on this "list"
		provGraphNode.addEntityDescription(entityDesc);
		return provGraphNode;
	}

}
