package uk.ac.aston.provlayer.storage.api;

public interface SystemModelState {

	long getFirstCommitTime();
	void setFirstCommitTime(long l);

}
