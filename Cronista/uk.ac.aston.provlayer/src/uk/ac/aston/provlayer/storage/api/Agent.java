package uk.ac.aston.provlayer.storage.api;

public interface Agent {

	String getID();
	void setID(String id);

}
