package uk.ac.aston.provlayer.storage.api;

public interface ProvenanceGraphStorage {

	void connect() throws Exception;

	TimeWindow newTimeWindow(long commitTime);

	void shutdown();

}
