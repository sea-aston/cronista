package uk.ac.aston.provlayer.storage.api;

import java.util.List;

import uk.ac.aston.provlayer.descriptions.EntityDescription;

public interface Entity {
	String getID();
	void setID(String id);

	/**
	 * Returns an unmodifiable list with the descriptions for this entity.
	 */
	List<EntityDescription> getEntityDescriptions();
	void addEntityDescription(EntityDescription oldEntity);

	Agent getWasAttributedTo();
	void setWasAttributedTo(Agent agent);

	Activity getWasGeneratedBy();
	void setWasGeneratedBy(Activity activity);

	Entity getWasDerivedFrom();
	void setWasDerivedFrom(Entity oldGraphEntity);

}
