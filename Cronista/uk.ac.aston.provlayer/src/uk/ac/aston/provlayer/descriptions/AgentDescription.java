package uk.ac.aston.provlayer.descriptions;

public class AgentDescription {

	private final String id;

	public AgentDescription(String agentID) {
		this.id = agentID;
	}

	public String getID() {
		return id;
	}

}
