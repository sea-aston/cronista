package uk.ac.aston.provlayer.descriptions;

public class ActivityDescription {

	private final String id;
	private final String name;
	private final long startTime;
	private long endTime;

	public ActivityDescription(String id, String name, long currentTimeMillis) {
		this.id = id;
		this.name = name;
		this.startTime = currentTimeMillis;
		this.endTime = 0;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public String getID() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public long getStartTime() {
		return startTime;
	}

}
