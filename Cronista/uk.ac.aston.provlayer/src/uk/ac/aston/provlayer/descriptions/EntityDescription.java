package uk.ac.aston.provlayer.descriptions;

import uk.ac.aston.provlayer.observer.model.ModelFeature;
import uk.ac.aston.provlayer.observer.model.ModelPart;

public class EntityDescription {

	private String messageID;

	private ModelPart objectStorageReference;
	private ModelFeature attributeMetadataStorageReference;
	private String entityInstanceID, objectIDAsString, attributeIDAsString, memoryVersionAsString,
			storageVersionAsString;
	private String attributeNameAsString, attributeTypeAsString;

	private boolean inModelStorage;

	private Object attributeDataStorageReference;

	private String objectName, objectType;
	private String attributeValueAsString;

	private char lastAccessType;

	private long startTime, endTime, lastAccessTime;

	public String getMessageID() {
		return messageID;
	}

	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}

	public String getEntityInstanceID() {
		return entityInstanceID;
	}

	public void setEntityInstanceID(String entityInstanceID) {
		this.entityInstanceID = entityInstanceID;
	}

	public String getObjectIDAsString() {
		return objectIDAsString;
	}

	public void setObjectIDAsString(String objectIDAsString) {
		this.objectIDAsString = objectIDAsString;
	}

	public String getAttributeIDAsString() {
		return attributeIDAsString;
	}

	public void setAttributeIDAsString(String attributeIDAsString) {
		this.attributeIDAsString = attributeIDAsString;
	}

	public String getMemoryVersionAsString() {
		return memoryVersionAsString;
	}

	public void setMemoryVersionAsString(String memoryVersionAsString) {
		this.memoryVersionAsString = memoryVersionAsString;
	}

	public String getStorageVersionAsString() {
		return storageVersionAsString;
	}

	public void setStorageVersionAsString(String storageVersionAsString) {
		this.storageVersionAsString = storageVersionAsString;
	}

	public boolean isInModelStorage() {
		return inModelStorage;
	}

	public void setInModelStorage(boolean inModelStorage) {
		this.inModelStorage = inModelStorage;
	}

	public ModelPart getObjectStorageReference() {
		return objectStorageReference;
	}

	public void setObjectStorageReference(ModelPart objectStorageReference) {
		this.objectStorageReference = objectStorageReference;
	}

	public ModelFeature getAttributeMetadataStorageReference() {
		return attributeMetadataStorageReference;
	}

	public void setAttributeMetadataStorageReference(ModelFeature attributeMetadataStorageReference) {
		this.attributeMetadataStorageReference = attributeMetadataStorageReference;
	}

	public Object getAttributeDataStorageReference() {
		return attributeDataStorageReference;
	}

	public void setAttributeDataStorageReference(Object attributeDataStorageReference) {
		this.attributeDataStorageReference = attributeDataStorageReference;
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getAttributeNameAsString() {
		return attributeNameAsString;
	}

	public void setAttributeNameAsString(String attributeNameAsString) {
		this.attributeNameAsString = attributeNameAsString;
	}

	public String getAttributeTypeAsString() {
		return attributeTypeAsString;
	}

	public void setAttributeTypeAsString(String attributeTypeAsString) {
		this.attributeTypeAsString = attributeTypeAsString;
	}

	public String getAttributeValueAsString() {
		return attributeValueAsString;
	}

	public void setAttributeValueAsString(String attributeValueAsString) {
		this.attributeValueAsString = attributeValueAsString;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public long getLastAccessTime() {
		return lastAccessTime;
	}

	public void setLastAccessTime(long lastAccessTime) {
		this.lastAccessTime = lastAccessTime;
	}

	public char getLastAccessType() {
		return lastAccessType;
	}

	public void setLastAccessType(char lastAccessType) {
		this.lastAccessType = lastAccessType;
	}
}
