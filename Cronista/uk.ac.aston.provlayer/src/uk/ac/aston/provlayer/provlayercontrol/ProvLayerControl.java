package uk.ac.aston.provlayer.provlayercontrol;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import uk.ac.aston.provlayer.curator.Curator;
import uk.ac.aston.provlayer.curator.memqueue.BlockingQueueProcessor;
import uk.ac.aston.provlayer.curator.messages.ObserverMessage;
import uk.ac.aston.provlayer.observer.thread.ThreadObserver;
import uk.ac.aston.provlayer.storage.api.ProvenanceGraphStorage;


public class ProvLayerControl {
	
	private static ProvLayerControl instance;
	private static Thread tCurator = null;
	private static Curator curator = null;
	
	private static void ProvLayerControl() {}
	
	public static ProvLayerControl startProvLayer (ProvenanceGraphStorage HistoryDB) {
		System.out.println("startProvLayer");
		if(instance == null) {
			instance = new ProvLayerControl();
			instance.ProvLayerSetup (HistoryDB);
			}
		return instance;
	}
	
	private static void ProvLayerSetup(ProvenanceGraphStorage HistoryDB) {	
		if (HistoryDB != null) {
			System.out.println("ProvLayer ON");
			BlockingQueue<ObserverMessage> mQueue = new ArrayBlockingQueue<>(10000);
			BlockingQueueProcessor mQueueProcessor = new BlockingQueueProcessor(mQueue);
			ThreadObserver.addNewObserverHook((obs) -> obs.subscribe(mQueueProcessor));
			curator = new Curator(mQueueProcessor, HistoryDB);
			tCurator = new Thread(curator);
			tCurator.start();
		} else {
			System.out.println("ProvLayer OFF");
		}
	}
	
	public static ProvLayerControl getProvLayerControl () {
		return instance;
	}
	
	public static Curator getCurator() {
		return curator;
	}

	public static Thread getThreadCurator() {
		return tCurator;
	}
	
	public static boolean shutdownProvLayer() {
		if (curator != null) {
			curator.setDone(true);
			try {
				tCurator.join();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}			
		}
		return true;
	}
	

}
