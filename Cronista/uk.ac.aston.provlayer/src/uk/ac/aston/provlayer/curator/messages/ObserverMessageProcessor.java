package uk.ac.aston.provlayer.curator.messages;

/**
 * Generic interface for anything that processes messages coming from the observer.
 */
public interface ObserverMessageProcessor {

	void handle(ActivityStartedMessage m);
	void handle(ActivityEndedMessage m);

	void handle(EntityReadMessage m);
	void handle(EntityCreateMessage m);
	void handle(EntityUpdatedMessage m);
	
	// Model Storage Only
	void handle(EntityLoadedMessage m);
	void handle(EntityCommittedMessage m);

}
