package uk.ac.aston.provlayer.curator.memqueue;

import java.util.Collection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import uk.ac.aston.provlayer.curator.MessageQueueProcessor;
import uk.ac.aston.provlayer.curator.messages.*;

/**
 * Puts all incoming messages into a blocking queue, which the user can poll.
 */
public class BlockingQueueProcessor implements MessageQueueProcessor {

	private final BlockingQueue<ObserverMessage> queue;

	public BlockingQueueProcessor(BlockingQueue<ObserverMessage> queue) {
		this.queue = queue;
	}

	@Override
	public void handle(ActivityStartedMessage m) {
		queue.add(m);
	}

	@Override
	public void handle(ActivityEndedMessage m) {
		queue.add(m);
	}

	@Override
	public void handle(EntityReadMessage m) {
		queue.add(m);
	}

	@Override
	public void handle(EntityCreateMessage m) {
		queue.add(m);
	}

	@Override
	public void handle(EntityUpdatedMessage m) {
		queue.add(m);
	}

	@Override
	public void handle(EntityLoadedMessage m) {
		queue.add(m);
	}
	
	@Override
	public void handle(EntityCommittedMessage m) {
		queue.add(m);
	}

	@Override
	public ObserverMessage poll() {
		return queue.poll();
	}

	@Override
	public ObserverMessage poll(long timeout, TimeUnit timeUnit) throws InterruptedException {
		return queue.poll(timeout, timeUnit);
	}

	public void drainTo(Collection<? super ObserverMessage> m) {
		queue.drainTo(m);
	}

	public int size() {
		return queue.size();
	}
}
