package uk.ac.aston.provlayer.curator.messages;

import uk.ac.aston.provlayer.descriptions.ActivityDescription;
import uk.ac.aston.provlayer.descriptions.AgentDescription;

public abstract class ObserverMessage {

	private final String id;
	private final AgentDescription agent;
	private final ActivityDescription activity;

	public ObserverMessage(String id, AgentDescription agent, ActivityDescription activity) {
		this.id = id;
		this.agent = agent;
		this.activity = activity;
	}

	public String getId() {
		return id;
	}

	public ActivityDescription getActivity() {
		return activity;
	}

	public AgentDescription getAgent() {
		return agent;
	}

	public abstract void accept(ObserverMessageProcessor proc);
}
