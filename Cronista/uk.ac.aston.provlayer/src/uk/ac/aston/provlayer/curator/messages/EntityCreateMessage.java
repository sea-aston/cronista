package uk.ac.aston.provlayer.curator.messages;

import uk.ac.aston.provlayer.descriptions.ActivityDescription;
import uk.ac.aston.provlayer.descriptions.AgentDescription;
import uk.ac.aston.provlayer.descriptions.EntityDescription;

public class EntityCreateMessage extends ObserverMessage {

	private final EntityDescription entity;

	public EntityCreateMessage(String id, AgentDescription agent, ActivityDescription activity, EntityDescription entity) {
		super(id, agent, activity);
		this.entity = entity;
	}

	public EntityDescription getEntity() {
		return entity;
	}
	
	@Override
	public void accept(ObserverMessageProcessor proc) {
		proc.handle(this);
	}
}
