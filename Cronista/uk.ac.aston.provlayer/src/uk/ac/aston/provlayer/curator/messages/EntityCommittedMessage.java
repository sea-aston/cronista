package uk.ac.aston.provlayer.curator.messages;

import uk.ac.aston.provlayer.descriptions.ActivityDescription;
import uk.ac.aston.provlayer.descriptions.AgentDescription;
import uk.ac.aston.provlayer.descriptions.EntityDescription;

public class EntityCommittedMessage extends ObserverMessage {

	private final EntityDescription oldEntity, newEntity;
	private final long commitTime;

	public EntityCommittedMessage(String id, AgentDescription agent, ActivityDescription activity, EntityDescription oldEntity,
			EntityDescription newEntity, long commitTime) {
		super(id, agent, activity);
		this.oldEntity = oldEntity;
		this.newEntity = newEntity;
		this.commitTime = commitTime;
	}

	public EntityDescription getOldEntity() {
		return oldEntity;
	}

	public EntityDescription getNewEntity() {
		return newEntity;
	}

	public long getCommitTime() {
		return commitTime;
	}

	@Override
	public void accept(ObserverMessageProcessor proc) {
		proc.handle(this);
	}
}
