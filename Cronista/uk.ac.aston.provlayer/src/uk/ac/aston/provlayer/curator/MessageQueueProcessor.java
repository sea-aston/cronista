package uk.ac.aston.provlayer.curator;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

import uk.ac.aston.provlayer.curator.messages.ObserverMessage;
import uk.ac.aston.provlayer.curator.messages.ObserverMessageProcessor;

public interface MessageQueueProcessor extends ObserverMessageProcessor {
	/**
	 * Retrieves and removes the head of the message queue, or returns {@code null}
	 * if this queue is empty.
	 */
	ObserverMessage poll();

	/**
	 * Retrieves and removes the head of the message queue, waiting up to the
	 * specified wait time if necessary for an element to become available.
	 *
	 * @param timeout  Time to wait.
	 * @param timeUnit Unit of measurement for the timeout.
	 * @throws InterruptedException Wait was interrupted. 
	 */
	public ObserverMessage poll(long timeout, TimeUnit timeUnit) throws InterruptedException;

	/**
	 * Removes all available elements from this queue and adds them to the given
	 * collection.
	 */
	public void drainTo(Collection<? super ObserverMessage> m);
	

	/**
	 * Returns the number of elements in the queue.
	 */
	public int size();
}
