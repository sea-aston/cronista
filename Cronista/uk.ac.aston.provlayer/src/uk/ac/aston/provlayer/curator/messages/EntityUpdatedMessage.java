package uk.ac.aston.provlayer.curator.messages;

import uk.ac.aston.provlayer.descriptions.ActivityDescription;
import uk.ac.aston.provlayer.descriptions.AgentDescription;
import uk.ac.aston.provlayer.descriptions.EntityDescription;

public class EntityUpdatedMessage extends ObserverMessage {

	private final EntityDescription oldEntity, newEntity;

	public EntityUpdatedMessage(String id, AgentDescription agent, ActivityDescription activity, 
			EntityDescription oldEntity,
			EntityDescription newEntity) {
		super(id, agent, activity);
		this.oldEntity = oldEntity;
		this.newEntity = newEntity;
	}

	public EntityDescription getOldEntity() {
		return oldEntity;
	}

	public EntityDescription getNewEntity() {
		return newEntity;
	}

	@Override
	public void accept(ObserverMessageProcessor proc) {
		proc.handle(this);
	}
}
