package uk.ac.aston.provlayer.curator.messages;

import uk.ac.aston.provlayer.descriptions.ActivityDescription;
import uk.ac.aston.provlayer.descriptions.AgentDescription;
import uk.ac.aston.provlayer.descriptions.EntityDescription;

public class EntityLoadedMessage extends ObserverMessage {

	private final EntityDescription memoryEntity, storageEntity;
// Storage "OldEntity" Memory "NewEntity"
	public EntityLoadedMessage(String id, AgentDescription agent, ActivityDescription activity, 
			EntityDescription storageEntity,
			EntityDescription memoryEntity
			) {
		super(id, agent, activity);
		this.memoryEntity = memoryEntity;
		this.storageEntity = storageEntity;
	}

	public EntityDescription getMemoryEntity() {
		return memoryEntity;
	}

	public EntityDescription getStorageEntity() {
		return storageEntity;
	}

	@Override
	public void accept(ObserverMessageProcessor proc) {
		proc.handle(this);
	}
}
