package uk.ac.aston.provlayer.curator;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import uk.ac.aston.provlayer.curator.memqueue.BlockingQueueProcessor;
import uk.ac.aston.provlayer.curator.messages.*;
import uk.ac.aston.provlayer.descriptions.EntityDescription;
import uk.ac.aston.provlayer.observer.thread.ThreadObserver;
import uk.ac.aston.provlayer.storage.api.Activity;
import uk.ac.aston.provlayer.storage.api.Agent;
import uk.ac.aston.provlayer.storage.api.Entity;
import uk.ac.aston.provlayer.storage.api.ProvenanceGraph;
import uk.ac.aston.provlayer.storage.api.ProvenanceGraphStorage;
import uk.ac.aston.provlayer.storage.api.TimeWindow;

public class Curator implements ObserverMessageProcessor, Runnable {

	public static final Long MAX_TIME_WINDOW_MILLIS = (long) 1000 * 10;

	// The curator prints the size of the message queue every X messages
	private static final int PRINT_QSIZE_EVERY = 100;

	private final MessageQueueProcessor queue;
	private final ProvenanceGraphStorage storage;
	private volatile boolean done = false;

	private TimeWindow currentTimeWindow;

	/**
	 * Creates a new Curator, given a certain {@link BlockingQueueProcessor}. The
	 * user should have already set up a thread observer hook through
	 * {@link ThreadObserver#addNewObserverHook(java.util.function.Consumer)}
	 * before creating this object.
	 *
	 * @param queue Queue processor to read messages from.
	 * @param storage Storage component to use for the provenance graph.
	 */
	public Curator(MessageQueueProcessor queue, ProvenanceGraphStorage storage) {
		this.queue = queue;
		this.storage = storage;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	@Override
	public void run() {
		try {
			storage.connect();

			/*
			 * First time window -- may need to tweak the time on this or handle a special
			 * case for the initial start.
			 */
			currentTimeWindow = storage.newTimeWindow(System.currentTimeMillis());
			System.out.println("Curator run getStartTime" + currentTimeWindow.getStartLocalTime());

			/*
			 * Keep the curator running until done: check flag after each message or at
			 * least every 500ms.
			 */
			try {
				int queueLengthCounter = PRINT_QSIZE_EVERY;
				while (!done) {
					final ObserverMessage itemToLog = queue.poll(500, TimeUnit.MILLISECONDS);
					if (itemToLog != null) {
						itemToLog.accept(this);
					}

					--queueLengthCounter;
					if (queueLengthCounter == 0) {
						System.out.println("Queue size: " + queue.size());
						queueLengthCounter = PRINT_QSIZE_EVERY;
					}
				}
			} catch (InterruptedException e) {
				System.err.println("Curator loop was interrupted");
			}

			// Process pending messages before finishing
			List<ObserverMessage> pending = new ArrayList<>();
			queue.drainTo(pending);
			pending.forEach(m -> m.accept(this));
			System.out.println("Curator in "
				+ Thread.currentThread().getName()
				+ " has processed all pending messages");

		} catch (Exception e1) {
			// TODO Use a proper logging framework (slf4j)
			e1.printStackTrace();
		} finally {
			storage.shutdown();
		}
		
	}

	@Override
	public void handle(ActivityStartedMessage m) {
//		System.out.println("CURATOR: ActivityStartedMessage " + m.getId());  // DEBUG
		final ProvenanceGraph g = getCurrentGraph();
		final Agent agent = g.searchAddProvAgent(m.getAgent());
		final Activity activity = g.searchAddProvActivity(m.getAgent(), m.getActivity());
		
		activity.setWasAssociatedWith(agent);
	}

	@Override
	public void handle(ActivityEndedMessage m) {
//		System.out.println("CURATOR: ActivityEndedMessage " + m.getId());  // DEBUG
		final ProvenanceGraph g = getCurrentGraph();
		final Agent agent = g.searchAddProvAgent(m.getAgent());
		final Activity activity = g.searchAddProvActivity(m.getAgent(), m.getActivity());
		
		// If closing an activity on a new time window we will make a new activity node and connect it to the agent.
		if (activity.getWasAssociatedWith() == null) {
			activity.setWasAssociatedWith(agent); 
			}
		
	}

	@Override
	public void handle(EntityReadMessage m) {
//		System.out.println("CURATOR: EntityReadMessage " + m.getId());  // DEBUG
		final ProvenanceGraph g = getCurrentGraph();
		final Agent agent = g.searchAddProvAgent(m.getAgent());
		final Activity activity = g.searchAddProvActivity(m.getAgent(), m.getActivity());
		handleRead(agent, activity, m.getEntity());
	}



	private Entity handleRead(final Agent agent, final Activity activity, final EntityDescription entityDesc) {
		Entity provEntity = getCurrentGraph().searchAddProvEntityDescription(entityDesc);
	
		// if this provEntity has a GenBy, we are informed by it
		Activity informingActivity = provEntity.getWasGeneratedBy();
		if (informingActivity != null) {
			//informingActivity.getWasInformedBy().add(informingActivity);
			informingActivity.addWasInformedBy(informingActivity);
		}

		provEntity.setWasAttributedTo(agent);
		//activity.getUsed().add(provEntity);  // Not supported
		activity.addUsed(provEntity);	// Fixes line above
		return provEntity;
	}
	
	@Override
	public void handle(EntityCreateMessage m) {
//		System.out.println("CURATOR: EntityCreateMessage " + m.getId());  // DEBUG
		final ProvenanceGraph g = getCurrentGraph();
		final Agent agent = g.searchAddProvAgent(m.getAgent());
		final Activity activity = g.searchAddProvActivity(m.getAgent(), m.getActivity());
		
//		System.out.println("CURATOR: CREATE "+ m.getEntity().getEntityInstanceID());  // DEBUG
		Entity provEntity = g.searchAddProvEntityDescription(m.getEntity());
		provEntity.setWasAttributedTo(agent);
		provEntity.setWasGeneratedBy(activity);					
	}

	@Override
	public void handle(EntityUpdatedMessage m) {
//		System.out.println("CURATOR: EntityUpdatedMessage " + m.getId());  // DEBUG
		final ProvenanceGraph g = getCurrentGraph();
		final Agent agent = g.searchAddProvAgent(m.getAgent());
		final Activity activity = g.searchAddProvActivity(m.getAgent(), m.getActivity());
		
		//System.out.println("CURATOR: UPDATE " + m.getNewEntity().getEntityInstanceID()+" "+ m.getOldEntity().getEntityInstanceID());
		// UPDATE Attribute and is derived from an earlier ENTITY
		final Entity provNewEntity = g.searchAddProvEntityDescription(m.getNewEntity());
		final Entity provOldEntity = g.searchAddProvEntityDescription(m.getOldEntity());

		provNewEntity.setWasAttributedTo(agent);
		provNewEntity.setWasGeneratedBy(activity);
		
		//Debugging Derived from
		//System.out.println("UpdateMessage ["+m.getId() + "]\n mOE " + m.getOldEntity().getEntityInstanceID() + " > mNE " + m.getNewEntity().getEntityInstanceID());
		//System.out.print("EntityUpdatedMessage [" + m.getId() + "] ");
		provNewEntity.setWasDerivedFrom(provOldEntity);
	}
	
	@Override
	public void handle(EntityLoadedMessage m) {
//		System.out.println("CURATOR: EntityLoadedMessage "  + m.getId());  // DEBUG
		final ProvenanceGraph g = getCurrentGraph();
		final Agent agent = g.searchAddProvAgent(m.getAgent());
		final Activity activity = g.searchAddProvActivity(m.getAgent(), m.getActivity());
		final Entity provEntity = handleRead(agent, activity, m.getMemoryEntity());

		// Special condition, loaded from Storage into memory
		Entity entity = g.searchAddProvEntityDescription(m.getStorageEntity());
		//activity.getUsed().add(entity);
		activity.addUsed(entity);
		
		// Debugging Derived from
		//System.out.println("LoadedMessage ["+m.getId()+ "] \n mME" + m.getMemoryEntity().getEntityInstanceID() + " > mSE " + m.getStorageEntity().getEntityInstanceID() );
		//System.out.print("EntityLoadedMessage [" + m.getId() + "] ");
		provEntity.setWasDerivedFrom(entity);
	}

	@Override
	public void handle(EntityCommittedMessage m) {
		if (m.getCommitTime() > currentTimeWindow.getStartLocalTime()+MAX_TIME_WINDOW_MILLIS) {
			currentTimeWindow = storage.newTimeWindow(m.getCommitTime());
		}
	
		final ProvenanceGraph g = getCurrentGraph();
		Agent agent = g.searchAddProvAgent(m.getAgent());
		Activity activity = g.searchAddProvActivity(m.getAgent(), m.getActivity());
		if (m.getNewEntity() == null) {
			return;
		}
		
		final Entity provNewEntity = g.searchAddProvEntityDescription(m.getNewEntity());
		final Entity provOldEntity = g.searchAddProvEntityDescription(m.getOldEntity());

		provNewEntity.setWasAttributedTo(agent);
		provNewEntity.setWasGeneratedBy(activity);
		provNewEntity.setWasDerivedFrom(provOldEntity);
		
		
		
		
		/*
		 
		// TODO FIX THE COMMIT PROCESS
		Entity oldGraphEntity = g.searchProvEntity(m.getOldEntity());
		//
		//  This is replacing the ID on the last Memory version
		//	the new ModelAccess and obsVersionController will 
		//  provide correct old version information to create
		//  the last MemoryVersion Entity node.
		//
		
		if (oldGraphEntity != null) {
		//	System.out.println("**Replace ID" + m.getOldEntity().getEntityInstanceID() +" >> " +  m.getNewEntity().getEntityInstanceID() );
			// Replace ID with new one
			oldGraphEntity.setID(m.getNewEntity().getEntityInstanceID());
			// Add the description of the new Entity
			//oldGraphEntity.getEntityDescriptions().add(m.getNewEntity());  // Add not supported
			oldGraphEntity.addEntityDescription(m.getNewEntity());  // Fix for line above
		} else {
		//	System.out.println("**New OldNode " + m.getOldEntity().getEntityInstanceID());
			oldGraphEntity = g.addProvEntity(m.getOldEntity());
			Entity newGraphEntity = g.searchProvEntity(m.getNewEntity());

			if (newGraphEntity == null) {	
		//		System.out.println("**New NewNode " + m.getNewEntity().getEntityInstanceID());
				newGraphEntity = g.addProvEntity(m.getNewEntity());
				newGraphEntity.setWasAttributedTo(agent);
				newGraphEntity.setWasGeneratedBy(activity);
				
				//Debugging for Derived from
				//System.out.println("CommitMessage [" + m.getId() + "]\n mOE" + m.getOldEntity().getEntityInstanceID() + " > mNE " + m.getNewEntity().getEntityInstanceID());
				//System.out.print("EntityCommittedMessage [" + m.getId() + "] ");
				newGraphEntity.setWasDerivedFrom(oldGraphEntity);
			}
		}
		
		*/
		
	}

	private ProvenanceGraph getCurrentGraph() {
		return currentTimeWindow.getProvenanceGraph();
	}
}
