package uk.ac.aston.provlayer.curator.messages;

import uk.ac.aston.provlayer.descriptions.ActivityDescription;
import uk.ac.aston.provlayer.descriptions.AgentDescription;

public class ActivityStartedMessage extends ObserverMessage {

	public ActivityStartedMessage(String id, AgentDescription agent, ActivityDescription activity) {
		super(id, agent, activity);
	}

	@Override
	public void accept(ObserverMessageProcessor proc) {
		proc.handle(this);
	}
}
