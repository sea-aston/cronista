package uk.ac.aston.provlayer.storage.cdo;

// Date time
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

// CDO
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.net4j.CDONet4jSessionConfiguration;
import org.eclipse.emf.cdo.net4j.CDONet4jUtil;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CommitException;
import org.eclipse.emf.cdo.util.ConcurrentAccessException;
//import org.eclipse.net4j.*;
import org.eclipse.net4j.Net4jUtil;
import org.eclipse.net4j.connector.IConnector;
import org.eclipse.net4j.tcp.TCPUtil;
import org.eclipse.net4j.util.container.ContainerUtil;
import org.eclipse.net4j.util.container.IManagedContainer;
import org.eclipse.net4j.util.lifecycle.LifecycleUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import historyModel.HistoryModelFactory;
import uk.ac.aston.provlayer.storage.api.ProvenanceGraph;
import uk.ac.aston.provlayer.storage.api.ProvenanceGraphStorage;


public class CDOProvenanceGraph implements ProvenanceGraphStorage {

	//CDO Host DB info  -- CURATOR
	private static final String DEFAULT_HOSTNAME = "127.0.0.1";
	private static final int DEFAULT_PORT = 2036;
	private static final String DEFAULT_REPONAME = "repoHistory";

	private static final Logger LOGGER = LoggerFactory.getLogger(CDOProvenanceGraph.class);
	
	private IManagedContainer container;
	private IConnector connector;
	private CDONet4jSessionConfiguration configuration;
	private CDOSession session;
	private CDOTransaction transaction;
	private CDOResource resource;
	
	private String historyModelReference;
	private TimeWindowAdapter currentTimeWindow;

	public CDOProvenanceGraph()
	{
		System.out.println("CDO Storage");
		
		try {
			connect();
		} catch (Exception e) {
			LOGGER.error("Failed to connect to CDO", e);
		}
	}
	
	@Override
	public void connect() throws Exception {
		final String hostname = System.getProperty("prov.cdo.hostname", DEFAULT_HOSTNAME);

		final String sPort = System.getProperty("prov.cdo.port");
		int port;
		if (sPort == null) {
			port = DEFAULT_PORT;
		} else {
			port = Integer.valueOf(sPort);
		}

		final String repoName = System.getProperty("prov.cdo.name", DEFAULT_REPONAME);
		
		connect(hostname, port, repoName);
	}

	public void connect(String hostname, int port, String repoName) throws ConcurrentAccessException, CommitException
	{
		LOGGER.info("Connecting Database");

		// Prepare container
		container = ContainerUtil.createContainer();
		Net4jUtil.prepareContainer(container); // Register Net4j factories
	    TCPUtil.prepareContainer(container); // Register TCP factories
	    CDONet4jUtil.prepareContainer(container); // Register CDO factories
	    container.activate();
	    
	    // Create connector
	    connector = TCPUtil.getConnector(container, hostname + ":" + port); //$NON-NLS-1$
	    
	    // Create configuration
	    configuration = CDONet4jUtil.createNet4jSessionConfiguration();
	    configuration.setConnector(connector);
	    configuration.setRepositoryName(repoName); //$NON-NLS-1$
	    
	    // Open session
	    session = configuration.openNet4jSession();
	    
	    LOGGER.info("Creating new history model");
	    //Make a new model for the run	    
	    historyModelReference = newHistoryModelReference();	    
	    transaction = session.openTransaction();
	    resource = transaction.getOrCreateResource(historyModelReference);

	    LOGGER.debug("Committing...");
		transaction.commit();

		LOGGER.debug("ResourceURI:" + resource.getURI());
	}	
	
	private void commit() throws ConcurrentAccessException, CommitException
	{
		transaction.commit();
	}
	
	private String newHistoryModelReference()
	{
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss");  
		LocalDateTime now = LocalDateTime.now();  
		System.out.println(dtf.format(now));  
		return ("/PROV/Started@"+dtf.format(now));
	}
	
	@Override
	public TimeWindowAdapter newTimeWindow(long commitTime) {
		if(currentTimeWindow != null)
		{
			resource.getContents().add(currentTimeWindow.getCDOObject());
			try {
				commit();
			} catch (CommitException e) {
				LOGGER.error("Failed to commit", e);
			}
		}

		ProvenanceGraph currentProvenanceGraph = new ProvenceGraphAdapter(
			HistoryModelFactory.eINSTANCE.createProvenanceGraph()
		);
		currentTimeWindow = new TimeWindowAdapter(
			HistoryModelFactory.eINSTANCE.createTimeWindow()
		);
		currentTimeWindow.setProvenanceGraph(currentProvenanceGraph);
		currentTimeWindow.setStartLocalTime(commitTime);

		return currentTimeWindow;
	}

	@Override
	public void shutdown() {
		
		System.out.println("CDOProvenanceGraph Shutting down...");	
		if(currentTimeWindow != null)
		{	
			currentTimeWindow.setEndLocalTime(System.currentTimeMillis());
			
			resource.getContents().add(currentTimeWindow.getCDOObject());
			try {
				commit();
			} catch (CommitException e) {
				LOGGER.error("Failed to commit", e);
			}
		}
		session.close();
		connector.close();
		container.deactivate();
		
		System.out.println("CDOProvenanceGraph Shutdown.");
		
	}


}
