package uk.ac.aston.provlayer.storage.cdo;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import uk.ac.aston.provlayer.descriptions.ActivityDescription;
import uk.ac.aston.provlayer.storage.api.Activity;
import uk.ac.aston.provlayer.storage.api.Agent;
import uk.ac.aston.provlayer.storage.api.Entity;

public class ActivityAdapter implements Activity{

	private final ProvDM.Activity cdoActivity;
	private final DescriptionConverter DC = new DescriptionConverter();
	

	public ActivityAdapter(ProvDM.Activity cdoActivity) {
		this.cdoActivity = cdoActivity;
	}
	
	protected ProvDM.Activity getCDOActivity()
	{
		return cdoActivity;
	}
	
	@Override
	public String getID() {
		return cdoActivity.getID();
	}

	@Override
	public void setID(String id) {
		cdoActivity.setID(id);		
	}

	@Override
	public ActivityDescription getActivityDescription() {
		return DC.toProvLayerActivityDescription(cdoActivity.getActivityDescription());
	}

	@Override
	public void setActivityDescription(ActivityDescription desc) {
		cdoActivity.setActivityDescription(DC.toHistoryModelActivityDescription(desc));
	}

	@Override
	public Agent getWasAssociatedWith() {
		if(cdoActivity.getWasAssociatedWith() != null)
			return new AgentAdapter(cdoActivity.getWasAssociatedWith());
		else
			return null;
	}

	@Override
	public void setWasAssociatedWith(Agent agent) {
		final AgentAdapter adptAgent = (AgentAdapter) agent;
		final ProvDM.Agent cdoAgent = adptAgent.getCDOAgent();
		cdoActivity.setWasAssociatedWith(cdoAgent);
	}

	@Override
	public Set<Activity> getWasInformedBy() {
		Set<Activity> plWasInformedByActivity = new LinkedHashSet<>();
		for(ProvDM.Activity infoAct : cdoActivity.getWasInformedBy())
		{
			plWasInformedByActivity.add(new ActivityAdapter(infoAct));
		}
		return Collections.unmodifiableSet(plWasInformedByActivity);
	}

	@Override
	public void addWasInformedBy(Activity InformingAct) {
		final ActivityAdapter adptAct = (ActivityAdapter) InformingAct;
		final ProvDM.Activity cdoAct = adptAct.getCDOActivity();
		cdoActivity.getWasInformedBy().add(cdoAct);
	}

	@Override
	public Set<Entity> getUsed() {
		
		Set<Entity> plUsedEntity = new LinkedHashSet<>();
		for(ProvDM.Entity cdoEnt : cdoActivity.getUsed())
		{
			plUsedEntity.add(new EntityAdapter(cdoEnt));
		}
		return Collections.unmodifiableSet(plUsedEntity);
	}
	
	@Override
	public void addUsed(Entity usedEntity) {
		final EntityAdapter adptEntity = (EntityAdapter) usedEntity;
		final ProvDM.Entity cdoEntity = adptEntity.getCDOEntity();
		cdoActivity.getUsed().add(cdoEntity);
		
	}
	


}
