package uk.ac.aston.provlayer.storage.cdo.util;

import java.util.Iterator;
import java.util.function.Function;

/**
 * Produces an {@link Iterable} of a second type from an {@link Iterable} of the
 * first type, plus a mapping from the first type to the second type.
 */
public class WrapperIterable<T, U> implements Iterable<U> {
	
	private final Iterable<T> original;
	private final Function<T, U> mapping;

	public WrapperIterable(Iterable<T> original, Function<T, U> mapping) {
		this.original = original;
		this.mapping = mapping;
	}

	@Override
	public Iterator<U> iterator() {
		final Iterator<T> itOriginal = original.iterator();

		return new Iterator<U>() {
			@Override
			public boolean hasNext() {
				return itOriginal.hasNext();
			}

			@Override
			public U next() {
				T nextOriginal = itOriginal.next();
				return mapping.apply(nextOriginal);
			}
		};
	}
}
