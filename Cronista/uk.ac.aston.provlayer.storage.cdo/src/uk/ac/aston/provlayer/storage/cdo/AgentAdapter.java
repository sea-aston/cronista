package uk.ac.aston.provlayer.storage.cdo;

import uk.ac.aston.provlayer.storage.api.Agent;

public class AgentAdapter implements Agent {
	
	private final ProvDM.Agent cdoAgent;
	
	public AgentAdapter(ProvDM.Agent cdoAgent) {
		this.cdoAgent = cdoAgent;
	}
	
	protected ProvDM.Agent getCDOAgent() {
		return cdoAgent;
	}
	
	@Override
	public String getID() {
		return cdoAgent.getID();
	}

	@Override
	public void setID(String id) {
		cdoAgent.setID(id);
	}
	
}
