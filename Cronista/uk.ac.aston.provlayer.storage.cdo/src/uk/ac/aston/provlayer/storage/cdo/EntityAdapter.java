package uk.ac.aston.provlayer.storage.cdo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import uk.ac.aston.provlayer.descriptions.EntityDescription;
import uk.ac.aston.provlayer.storage.api.Activity;
import uk.ac.aston.provlayer.storage.api.Agent;
import uk.ac.aston.provlayer.storage.api.Entity;

public class EntityAdapter implements Entity {

	private final ProvDM.Entity cdoEntity;
	private final DescriptionConverter DC = new DescriptionConverter();
	
	public EntityAdapter(ProvDM.Entity cdoEntity)
	{
		this.cdoEntity = cdoEntity;
	}
	
	protected ProvDM.Entity getCDOEntity()
	{
		return cdoEntity;
	}
		
	@Override
	public String getID() {
		return cdoEntity.getID();
	}

	@Override
	public void setID(String id) {
		cdoEntity.setID(id);
	}

	@Override
	public List<EntityDescription> getEntityDescriptions() {
		List<EntityDescription> plEntityDescriptions = new ArrayList<>(cdoEntity.getEntityDescription().size());
		for(ProvDescriptions.EntityDescription cdoEntDesc : cdoEntity.getEntityDescription())
		{
			plEntityDescriptions.add(DC.toProvLayerEntityDescription(cdoEntDesc));
		}
		return Collections.unmodifiableList(plEntityDescriptions);
	}

	@Override
	public void addEntityDescription(EntityDescription entityDescription)
	{	
		cdoEntity.getEntityDescription().add(DC.toHistoryModelEntityDescription(entityDescription));
	}
	
	@Override
	public Agent getWasAttributedTo() {
		if(cdoEntity.getWasAttributedTo() != null)
			return new AgentAdapter(cdoEntity.getWasAttributedTo());
		else
			return null;
	}

	@Override
	public void setWasAttributedTo(Agent agent) {
		final AgentAdapter adptAgent = (AgentAdapter) agent;
		final ProvDM.Agent cdoAgent = adptAgent.getCDOAgent();
		cdoEntity.setWasAttributedTo(cdoAgent);
	}

	@Override
	public Activity getWasGeneratedBy() {
		if(cdoEntity.getWasGeneratedBy() != null)
			return new ActivityAdapter(cdoEntity.getWasGeneratedBy());
		else
			return null;
	}

	@Override
	public void setWasGeneratedBy(Activity activity) {
		final ActivityAdapter adptActivity = (ActivityAdapter) activity;
		final ProvDM.Activity cdoActivity = adptActivity.getCDOActivity();
		cdoEntity.setWasGeneratedBy(cdoActivity); 
	}

	@Override
	public Entity getWasDerivedFrom() {
		if(cdoEntity.getWasDerivedFrom() != null)
			return new EntityAdapter(cdoEntity.getWasDerivedFrom());
		else
			return null;
	}

	@Override
	public void setWasDerivedFrom(Entity oldGraphEntity) {
		final EntityAdapter adptEntity = (EntityAdapter) oldGraphEntity;
		final ProvDM.Entity oldCdoEntity = adptEntity.getCDOEntity();
		cdoEntity.setWasDerivedFrom(oldCdoEntity);
	}
	

}
