package uk.ac.aston.provlayer.storage.cdo;

import uk.ac.aston.provlayer.storage.api.ProvenanceGraph;
import uk.ac.aston.provlayer.storage.api.SystemModelState;


public class TimeWindowAdapter implements uk.ac.aston.provlayer.storage.api.TimeWindow{

	private historyModel.TimeWindow window;
	private ProvenceGraphAdapter provGraph;
	
	public TimeWindowAdapter(historyModel.TimeWindow tw) {
		this.window = tw;
	}

	public historyModel.TimeWindow getCDOObject() {
		return window;
	}
	
	@Override
	public long getStartLocalTime() {
		return window.getStartLocalTime();
	}

	@Override
	public void setStartLocalTime(long l) {
		window.setStartLocalTime(l);		
	}

	@Override
	public long getEndLocalTime() {
		return window.getEndLocalTime();
	}

	@Override
	public void setEndLocalTime(long t) {
		window.setEndLocalTime(t);
	}

	@Override
	public ProvenanceGraph getProvenanceGraph() {
		return provGraph;
	}

	@Override
	public void setProvenanceGraph(ProvenanceGraph pg) {
		provGraph = (ProvenceGraphAdapter) pg;
		window.setProvenanceGraph(provGraph.getCDOObject());
	}

	@Override
	public SystemModelState getSystemModelState() {
		// TODO GET System Model State
		
		//return new SystemModelState(window.getSystemModelState());
		return null;
	}

	@Override
	public void setSystemModelState(SystemModelState sms) {
		// TODO SET System Model State
		//window.setSystemModelState(sms.getCDOObject());
		//theHistoryModelTimeWindow.setSystemModelState((historyModel.SystemModelState) sms);
	}
	
}
