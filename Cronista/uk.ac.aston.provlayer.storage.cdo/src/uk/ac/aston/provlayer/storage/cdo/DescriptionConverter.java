package uk.ac.aston.provlayer.storage.cdo;

import org.eclipse.emf.ecore.EStructuralFeature;

import uk.ac.aston.provlayer.descriptions.ActivityDescription;
import uk.ac.aston.provlayer.descriptions.AgentDescription;
import uk.ac.aston.provlayer.descriptions.EntityDescription;
import uk.ac.aston.provlayer.observer.model.cdo.CDOModelFeature;
import uk.ac.aston.provlayer.observer.model.cdo.CDOModelPart;

public class DescriptionConverter {
	
	
	public ProvDescriptions.AgentDescription toHistoryModelAgentDescription(AgentDescription plAgentDesc)
	{
		ProvDescriptions.AgentDescription hmAgent = ProvDescriptions.ProvDescriptionsFactory.eINSTANCE.createAgentDescription();
		// Map the values to transfer
		hmAgent.setAgentInstanceID(plAgentDesc.getID());
		return hmAgent;
	}
	
	public AgentDescription toProvLayerAgentDescription(ProvDescriptions.AgentDescription hmAgentDesc)
	{
		AgentDescription plAgent = new AgentDescription(hmAgentDesc.getAgentInstanceID());
		// Map the values to transfer
		// nothing to map yet
		return plAgent;
	}
	
	public ProvDescriptions.ActivityDescription toHistoryModelActivityDescription(ActivityDescription plActivityDesc)
	{
		// TODO Review the attributes on the Activities
		ProvDescriptions.ActivityDescription hmActivity = ProvDescriptions.ProvDescriptionsFactory.eINSTANCE.createActivityDescription();
		// Map the values to transfer
		hmActivity.setActivityInstanceID		( plActivityDesc.getID() );
		
		hmActivity.setName						( plActivityDesc.getName() );	// Missing from the provLayer description
		//hmActivity.getActivityStack().addall	( );	// Missing from the provLayer description
		hmActivity.setStartTime					( plActivityDesc.getStartTime() );
		hmActivity.setEndTime					( plActivityDesc.getEndTime() );
		
		//hmActivity.setMessageID				( );  	// Missing from the provLayer description  --  Not needed?
		
		return hmActivity;
	}
	
	public ActivityDescription toProvLayerActivityDescription(ProvDescriptions.ActivityDescription hmActivityDesc)
	{
		// TODO Review the attributes on the Activities
		ActivityDescription plActivity = new ActivityDescription(
				hmActivityDesc.getActivityInstanceID(),
				hmActivityDesc.getName(),
				hmActivityDesc.getStartTime());
		// Map the values to transfer
		
		//plActivity.setName();						// Missing from the provLayer description
		//plActivity.getActivityStack().addall();	// Missing from the provLayer description
		//plActivity.setStartTime();				// Set on creation
		plActivity.setEndTime						(hmActivityDesc.getEndTime());
		return plActivity;
		
	}
	
	public ProvDescriptions.EntityDescription toHistoryModelEntityDescription(EntityDescription plEntityDesc)
	{
		// TODO Review the attributes on the Entities
		//System.out.println("toHistoryModelEntityDescription()"); // DEBUG
		ProvDescriptions.EntityDescription hmEntity = ProvDescriptions.ProvDescriptionsFactory.eINSTANCE.createEntityDescription();
		
		// Entity data
		hmEntity.setEntityInstanceID					( plEntityDesc.getEntityInstanceID() );
		hmEntity.setStartTime							( plEntityDesc.getStartTime() );
		hmEntity.setEndTime								( plEntityDesc.getEndTime() );
				
		// Message data
		hmEntity.setMessageID							( plEntityDesc.getMessageID() );
		hmEntity.setLastAccessTime						( plEntityDesc.getLastAccessTime() );
		hmEntity.setLastAccessType						( plEntityDesc.getLastAccessType() );
				
		// References for model component

		// TODO make it so these do not depend as much on EMF specifics?
		//  The references below are trying to pull object representation, do we really need these?
		//  Pulling out objects from a CDO/EMF and storing in the history model isnt something that would translate for other HMS or programming languages.
		
		// These two lines breaks if it recieves a NULL
		//hmEntity.setObjectStorageReference				( ( (CDOModelPart) plEntityDesc.getObjectStorageReference() ).getRaw() );		
		//hmEntity.setAttributeMetadataStorageReference	( ( (CDOModelFeature) plEntityDesc.getAttributeMetadataStorageReference() ).getRaw() );	// MetaData > name/label?
		
		// There is a problem here converting types?
		//hmEntity.setAttributeDataStorageReference		( ( (CDOModelFeature) plEntityDesc.getAttributeDataStorageReference() ).getRaw() );		// Data > value?
				
		// These might need to be changed to "references" and the Strings moved to the Picture
		hmEntity.setMemoryVersionAsString				( plEntityDesc.getMemoryVersionAsString() );
		hmEntity.setStorageVersionAsString				( plEntityDesc.getStorageVersionAsString() );
		hmEntity.setInModelStorage						( plEntityDesc.isInModelStorage() );		
				
		// Picture of model component

		// Object bits
		hmEntity.setObjectIDAsString					( plEntityDesc.getObjectIDAsString() );
		hmEntity.setObjectName							( plEntityDesc.getObjectName() );			// AsString
		hmEntity.setObjectType							( plEntityDesc.getObjectType() );			// AsString
				
		// Attribute bits
		hmEntity.setAttributeIDAsString					( plEntityDesc.getAttributeIDAsString() );
		hmEntity.setAttributeNameAsString				( plEntityDesc.getAttributeNameAsString() );
		hmEntity.setAttributeTypeAsString				( plEntityDesc.getAttributeTypeAsString() );
		hmEntity.setAttributeValueAsString				( plEntityDesc.getAttributeValueAsString() );			
		
		return hmEntity;
	}
	
	public EntityDescription toProvLayerEntityDescription(ProvDescriptions.EntityDescription hmEntityDesc) 
	{ 
		EntityDescription plEntity = new EntityDescription();
		// Map the values to transfer
		
		// Entity data
		plEntity.setEntityInstanceID					( hmEntityDesc.getEntityInstanceID() );
		plEntity.setStartTime							( hmEntityDesc.getStartTime() );
		plEntity.setEndTime								( hmEntityDesc.getEndTime() );
		
		// Message data
		plEntity.setMessageID							( hmEntityDesc.getMessageID() );
		plEntity.setLastAccessTime						( hmEntityDesc.getLastAccessTime() );
		plEntity.setLastAccessType						( hmEntityDesc.getLastAccessType() );
		
		// References for model component (TODO - EMF dependency here, fix?)
		plEntity.setObjectStorageReference				( new CDOModelPart(hmEntityDesc.getObjectStorageReference()) );
		plEntity.setAttributeMetadataStorageReference	( new CDOModelFeature((EStructuralFeature) hmEntityDesc.getAttributeMetadataStorageReference()) );	// MetaData > name/label?
		plEntity.setAttributeDataStorageReference		( new CDOModelFeature((EStructuralFeature) hmEntityDesc.getAttributeDataStorageReference()) );		// Data > value?

		// These might need to be changed to "references" and the Strings moved to the Picture
		plEntity.setMemoryVersionAsString				( hmEntityDesc.getMemoryVersionAsString() );
		plEntity.setStorageVersionAsString				( hmEntityDesc.getStorageVersionAsString() );
		plEntity.setInModelStorage						( hmEntityDesc.isInModelStorage() );		
		
		// Picture of model component

		// Object bits
		plEntity.setObjectIDAsString					( hmEntityDesc.getObjectIDAsString() );
		plEntity.setObjectName							( hmEntityDesc.getObjectName() );			// AsString
		plEntity.setObjectType							( hmEntityDesc.getObjectType() );			// AsString
		
		// Attribute bits
		plEntity.setAttributeIDAsString					( hmEntityDesc.getAttributeIDAsString() );
		plEntity.setAttributeNameAsString				( hmEntityDesc.getAttributeNameAsString() );
		plEntity.setAttributeTypeAsString				( hmEntityDesc.getAttributeTypeAsString() );
		plEntity.setAttributeValueAsString				( hmEntityDesc.getAttributeValueAsString() );	
		
		return plEntity;
	}

}
