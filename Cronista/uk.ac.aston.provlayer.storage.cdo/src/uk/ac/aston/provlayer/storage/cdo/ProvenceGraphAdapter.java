package uk.ac.aston.provlayer.storage.cdo;

import ProvDM.ProvDMFactory;
import uk.ac.aston.provlayer.descriptions.ActivityDescription;
import uk.ac.aston.provlayer.descriptions.AgentDescription;
import uk.ac.aston.provlayer.descriptions.EntityDescription;
import uk.ac.aston.provlayer.storage.api.Activity;
import uk.ac.aston.provlayer.storage.api.Agent;
import uk.ac.aston.provlayer.storage.api.Entity;
import uk.ac.aston.provlayer.storage.cdo.util.WrapperIterable;

public class ProvenceGraphAdapter implements uk.ac.aston.provlayer.storage.api.ProvenanceGraph{

	private final historyModel.ProvenanceGraph cdoGraph;
	private DescriptionConverter DC = new DescriptionConverter();
	
	public ProvenceGraphAdapter(historyModel.ProvenanceGraph cdoGraph)
	{
		this.cdoGraph = cdoGraph;
	}
	
	public historyModel.ProvenanceGraph getCDOObject()
	{
		return cdoGraph;
	}

	@Override
	public Iterable<Activity> getActivities() {
		return new WrapperIterable<>(cdoGraph.getActivity(),
				(original) -> new ActivityAdapter(original));
	}

	@Override
	public Iterable<Entity> getEntities() {
		return new WrapperIterable<>(cdoGraph.getEntity(),
				(original) -> new EntityAdapter(original));
	}

	@Override
	public Iterable<Agent> getAgents() {
		return new WrapperIterable<>(
			cdoGraph.getAgent(),
			(original) -> new AgentAdapter(original));
	}

	@Override
	public Agent searchAddProvAgent(AgentDescription agent) {
		for (ProvDM.Agent current : cdoGraph.getAgent()) {
			if (current.getID().equals(agent.getID())) {
				return new AgentAdapter(current);
			}
		}

		// else make a new one
		ProvDM.Agent newAgent = ProvDMFactory.eINSTANCE.createAgent();
		newAgent.setID(agent.getID());
		cdoGraph.getAgent().add(newAgent);
		return new AgentAdapter(newAgent);
	}

	@Override
	public Activity searchAddProvActivity(AgentDescription agent, ActivityDescription activity) {
		if(!cdoGraph.getActivity().isEmpty())
		{	for(ProvDM.Activity current : cdoGraph.getActivity())
			{
				if(current.getID().equals(activity.getID()))
				{
					Activity rActivity = new ActivityAdapter(current);
					return rActivity;
				}			
			}
		}
		// else make a new one add it to the graph
		ProvDM.Activity newActivity = ProvDM.ProvDMFactory.eINSTANCE.createActivity();
		newActivity.setID(activity.getID());
		newActivity.setActivityDescription(DC.toHistoryModelActivityDescription(activity));
		cdoGraph.getActivity().add(newActivity);
		
		Activity rActivity = new ActivityAdapter(newActivity);
		return rActivity;
	}

	@Override
	public Entity searchProvEntity(EntityDescription oldEntity) {
		if(!cdoGraph.getEntity().isEmpty())
		{
			for(ProvDM.Entity current : cdoGraph.getEntity())
			{
				if(current.getID().equals(oldEntity.getEntityInstanceID()))
				{
					Entity rEntity = new EntityAdapter(current);
					return rEntity;
				}
			}
		}

		return null;
	}

	@Override
	public Entity addProvEntity(EntityDescription oldEntity) {
		// Entity
		ProvDM.Entity newEntity = ProvDM.ProvDMFactory.eINSTANCE.createEntity();
		newEntity.setID(oldEntity.getEntityInstanceID());
		
		// Entity Description
		newEntity.getEntityDescription().add(DC.toHistoryModelEntityDescription(oldEntity)); // Create a cdo Entity Description			
		
		// Add to graph
		cdoGraph.getEntity().add(newEntity);		
		
		return new EntityAdapter(newEntity);
		
	}
	
}
