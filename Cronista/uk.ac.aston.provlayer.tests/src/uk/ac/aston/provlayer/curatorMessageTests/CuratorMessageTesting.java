package uk.ac.aston.provlayer.curatorMessageTests;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import uk.ac.aston.provlayer.curator.Curator;
import uk.ac.aston.provlayer.curator.memqueue.BlockingQueueProcessor;
import uk.ac.aston.provlayer.curator.messages.ActivityEndedMessage;
import uk.ac.aston.provlayer.curator.messages.ActivityStartedMessage;
import uk.ac.aston.provlayer.curator.messages.EntityCommittedMessage;
import uk.ac.aston.provlayer.curator.messages.EntityLoadedMessage;
import uk.ac.aston.provlayer.curator.messages.EntityReadMessage;
import uk.ac.aston.provlayer.curator.messages.EntityUpdatedMessage;
import uk.ac.aston.provlayer.curator.messages.ObserverMessage;
import uk.ac.aston.provlayer.descriptions.ActivityDescription;
import uk.ac.aston.provlayer.descriptions.AgentDescription;
import uk.ac.aston.provlayer.descriptions.EntityDescription;
import uk.ac.aston.provlayer.observer.thread.ThreadObserver;
import uk.ac.aston.provlayer.storage.cdo.CDOProvenanceGraph;
import uk.ac.aston.provlayer.storage.tinkerpop.TpProvenanceGraphStorage;

public class CuratorMessageTesting {

	public static void main(String[] args) {

	// PROV LAYER SETUP - NO NEED FOR SINGLETON
		BlockingQueue<ObserverMessage> mQueue = new ArrayBlockingQueue<>(10000);
		BlockingQueueProcessor mQueueProcessor = new BlockingQueueProcessor(mQueue);
		ThreadObserver.addNewObserverHook((obs) -> obs.subscribe(mQueueProcessor));
		
		
		//Curator curator = new Curator(mQueueProcessor, new CDOProvenanceGraph());
		
		Curator curator = new Curator(mQueueProcessor, new TpProvenanceGraphStorage());
		
		final Thread tCurator = new Thread(curator);
		
		snooze(1);
		
		tCurator.start();
		snooze(1);
		
		AgentDescription agentD = new AgentDescription("AgentSmith");
		ActivityDescription actD = new ActivityDescription("ActID1","Activity1",1);
		
		EntityDescription entD1 = new EntityDescription();
		EntityDescription entD2 = new EntityDescription();
		
		snooze(1);
		
		entD1.setEntityInstanceID("ENT1.1");
		entD1.setObjectName("Object1");
		entD1.setAttributeNameAsString("Attr1");
		entD1.setAttributeValueAsString("AttrVal 1");
		entD1.setMessageID("Mid1");
		
		entD2.setEntityInstanceID("ENT1.2");
		entD2.setObjectName("Object1");
		entD2.setAttributeNameAsString("Attr1");
		entD2.setAttributeValueAsString("AttrVal 1");
		entD2.setMessageID("Mid2");
		
		ActivityStartedMessage actstartmsg = new ActivityStartedMessage ("IDactSt", agentD, actD);
		
		EntityLoadedMessage entL = new EntityLoadedMessage("IDEntLoad", agentD, actD, entD1, entD1);
		EntityReadMessage entR = new EntityReadMessage("IDEread",agentD, actD, entD1);
		EntityUpdatedMessage entU = new EntityUpdatedMessage("IDEntUpdated", agentD, actD, entD1, entD2);
		
		ActivityEndedMessage actendmsg = new ActivityEndedMessage("IDactSt", agentD, actD);
		
		EntityCommittedMessage entCom = new EntityCommittedMessage ("IDcom", agentD, actD, entD1,null,System.currentTimeMillis()+3000*10);
		
		mQueue.add(actstartmsg);
		
		mQueue.add(entL);
		mQueue.add(entR);		
		mQueue.add(entU);
		
		mQueue.add(actendmsg);
		mQueue.add(entCom);
		
		
		System.out.println("-=End=-");


	}

	
	private static void snooze (int i) {
		try {
			Thread.sleep(i*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
