package uk.ac.aston.provlayer.adapterTests;

import uk.ac.aston.provlayer.descriptions.ActivityDescription;
import uk.ac.aston.provlayer.descriptions.AgentDescription;
import uk.ac.aston.provlayer.descriptions.EntityDescription;
import uk.ac.aston.provlayer.storage.api.Activity;
import uk.ac.aston.provlayer.storage.api.Agent;
import uk.ac.aston.provlayer.storage.api.Entity;
import uk.ac.aston.provlayer.storage.api.ProvenanceGraph;
import uk.ac.aston.provlayer.storage.api.ProvenanceGraphStorage;
import uk.ac.aston.provlayer.storage.api.TimeWindow;
import uk.ac.aston.provlayer.storage.cdo.CDOProvenanceGraph;

public class ProvGraphAdapterTesting {
	
	private static TimeWindow currentTimeWindow;
	
	private static ProvenanceGraphStorage storage = new CDOProvenanceGraph();
	
	
	public static void main(String[] args) {
	
	ProvenanceGraph pGraph;
	
	
	EntityDescription entDes1 = new EntityDescription();
	EntityDescription entDes2 = new EntityDescription();
	ActivityDescription actDes1 = new ActivityDescription("AcID1","Act1", 1);
	ActivityDescription actDes2 = new ActivityDescription("AcID2", "Act2", 10);
	AgentDescription agtDes = new AgentDescription("AGT1");
	
	Agent		agt  = null;
	Activity	act1 = null;
	Activity	act2 = null;
	Entity 		ent1 = null;
	Entity 		ent2 = null;
	

	
		
	currentTimeWindow = storage.newTimeWindow(System.currentTimeMillis());
	pGraph = currentTimeWindow.getProvenanceGraph();
	
	agt = pGraph.searchAddProvAgent(agtDes);
	
	//pGraph.addProvEntity(entDes);
	entDes1.setEntityInstanceID("EID1");
	entDes1.setAttributeNameAsString("Some Name 1");
	entDes1.setAttributeValueAsString("Some Value 1");
	
	ent1 = pGraph.searchAddProvEntity(entDes1);
	ent1.setWasAttributedTo(agt);
	
	act1 = pGraph.searchAddProvActivity(agtDes, actDes1);	
	act1.setWasAssociatedWith(agt);
	act1.addUsed(ent1);
	
	entDes2.setEntityInstanceID("EID2");
	entDes2.setAttributeNameAsString("Some Name 2");
	entDes2.setAttributeValueAsString("Some Value 2");
	
	ent2 = pGraph.searchAddProvEntity(entDes2);
	ent2.setWasAttributedTo(agt);
	ent2.setWasGeneratedBy(act1);
	
	act2 = pGraph.searchAddProvActivity(agtDes, actDes2);
	act2.setWasAssociatedWith(agt);
	act2.addUsed(ent2);
	act2.addWasInformedBy(act1);
	
	showEntities(pGraph);
	showActivities(pGraph);

	
	


	currentTimeWindow = storage.newTimeWindow(System.currentTimeMillis());
	}
	
	static void showEntities(ProvenanceGraph pGraph)
	{
		System.out.println(String.format("\nEntities: %s",pGraph.getEntities().toString()));
		for (Entity ent : pGraph.getEntities())
		{
			System.out.println(String.format("ID: %s", ent.getID()));
			System.out.println("Descriptions: ");
			for(EntityDescription eDes : ent.getEntityDescriptions())
			{
				System.out.println(String.format("Attribute : %s - Value : %s", eDes.getAttributeNameAsString(), eDes.getAttributeNameAsString()));
			}
			
			System.out.println(String.format("WasAttributedTo: %s ", ent.getWasAttributedTo().getID()));
			if(ent.getWasGeneratedBy()!=null) {
				System.out.println(String.format("WasGeneratedBy: %s ", ent.getWasGeneratedBy().getID()));
			}
			
			if(ent.getWasDerivedFrom()!=null) {
				System.out.println(String.format("WasDerivedFrom: %s ", ent.getWasDerivedFrom().getID()));
			}
			
		}
	}
	
	static void showActivities(ProvenanceGraph pGraph)
	{
		System.out.println("\nActivities:");
		for (Activity act : pGraph.getActivities()) {
			System.out.println(String.format("ID: %s - start %d : endtime : %d"
					, act.getID()
					, act.getActivityDescription().getStartTime()
					, act.getActivityDescription().getEndTime()
					));
			
			System.out.println(String.format("WasAssociatedWith: %s", act.getWasAssociatedWith().getID()));
			
			System.out.println("Used: ");
			for(Entity entUsed : act.getUsed()) {
				System.out.println(String.format("EntID: %s",entUsed.getID()));
			}
			
			System.out.println("WasInformedBy: ");
			for(Activity actInfo : act.getWasInformedBy()) {
				System.out.println(String.format("ActID: %s" ,actInfo.getID()));
			}
			
			System.out.println();
		
		}	
	}
	
	
}
