package uk.ac.aston.provlayer.observerFunctionTest;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import uk.ac.aston.provlayer.curator.Curator;
import uk.ac.aston.provlayer.curator.memqueue.BlockingQueueProcessor;
import uk.ac.aston.provlayer.curator.messages.ObserverMessage;
import uk.ac.aston.provlayer.observer.thread.ActivityScope;
import uk.ac.aston.provlayer.observer.thread.ThreadObserver;
import uk.ac.aston.provlayer.storage.cdo.CDOProvenanceGraph;

public class ObserverFuntionTesting {
	
	public static void main(String[] args) {
		System.out.println("STARTING TEST");
			
		// PROV LAYER SETUP - NO NEED FOR SINGLETON
		BlockingQueue<ObserverMessage> mQueue = new ArrayBlockingQueue<>(10000);
		BlockingQueueProcessor mQueueProcessor = new BlockingQueueProcessor(mQueue);
		ThreadObserver.addNewObserverHook((obs) -> obs.subscribe(mQueueProcessor));
		Curator curator = new Curator(mQueueProcessor, new CDOProvenanceGraph());
		final Thread tCurator = new Thread(curator);
		
		snooze(1);
		tCurator.start();
		snooze(1);
	
		
		
		System.out.println("Curator and Queue setup");
		
		try (ActivityScope scope0 = new ActivityScope("Monitor")) { 
			
			System.out.println("Activity Monitor");
			
		}
		
		for (int i = 0; i < 10; i++)
			System.out.println(String.format("mQueue %d", mQueue.size() ) );
	
	}
	
	
	private static void snooze (int i) {
		try {
			Thread.sleep(i*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
