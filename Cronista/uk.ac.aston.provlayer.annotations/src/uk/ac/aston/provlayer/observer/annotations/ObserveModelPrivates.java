package uk.ac.aston.provlayer.observer.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;

//@Target({ElementType.PACKAGE,ElementType.TYPE, ElementType.METHOD, ElementType.CONSTRUCTOR, ElementType.FIELD})
@Target({ElementType.PACKAGE,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)

public @interface ObserveModelPrivates {
	
}
