package uk.ac.aston.provlayer.observer.instrument.aspectj.aspectInstruments;

import uk.ac.aston.provlayer.observer.annotations.*;

//
//Needs thread safety updates for the inspector 
//

privileged public aspect AspectEntityBeanMethod {

	private boolean observe = false;
	private InspectorPOJOFeature inspector = null;  // This inspector would need to be replaced with one for Methods

	// GET METHOD
	pointcut beanGet(Object t) : 
		target (t)
			&& execution(* get*()) 
			&& @within(ObserveModelBeanMethod);
	
	after(Object t) returning (Object r) : beanGet(t)
	{
		if (observe) {
			observe = false;
			// System.out.println("get_after System=" + observe);
			if (inspector == null) {
				System.out.println("BM: Get new inspector");
				inspector = new InspectorPOJOFeature();
			}
			
			System.out.println("target: " + t + "::" + thisJoinPoint);
			
			//inspector.inspectPrivate(thisJoinPoint, r, System.currentTimeMillis(), ObjectAccessType.Get,
			//		versionControl.versionThis(thisJoinPoint));
			inspector = null;
		}
		observe = true;
	}

	// SET METHOD
	pointcut setBean(Object t, Object a) : 
		target(t) && args (a)
			&& execution (* set*(..))
			&& @within(ObserveModelBeanMethod);
	
	before(Object t, Object a) : setBean(t,a)
	{
		if (observe) {
			observe = false;
			if (inspector == null) {
				System.out.println("BM: Set_before new inspector");
				inspector = new InspectorPOJOFeature();
			}
			
			System.out.println("target: " + t + "::" + thisJoinPoint);

			//inspector.inspectPrivate(thisJoinPoint, a, System.currentTimeMillis(), ObjectAccessType.before_Set,
			//		versionControl.versionThis(thisJoinPoint));
			observe = true;
		}
	}

	after(Object t, Object a) : setBean(t,a)
		//target (t) && args (a)
	 	//	&& execution (* set*(..))
	 	//	&& @within(ObserveModelBeanMethod) 
	{
		System.out.println("BM: set_after System=" + observe);
		if (observe) {
			observe = false;
			if (inspector == null) {
				System.out.println("BM: Set_after new inspector");
				inspector = new InspectorPOJOFeature();
			}
			Object  tC = t.getClass();
			System.out.println("target: " + t + "::" + thisJoinPoint);

			
			//inspector.inspectPrivate(thisJoinPoint, a, System.currentTimeMillis(), ObjectAccessType.after_Set,
			//		versionControl.updateVersion(thisJoinPoint));
			inspector = null;
			observe = true;
		}
	}	
}