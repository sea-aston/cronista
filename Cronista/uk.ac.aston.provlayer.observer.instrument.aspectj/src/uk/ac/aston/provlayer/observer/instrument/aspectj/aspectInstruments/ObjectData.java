package uk.ac.aston.provlayer.observer.instrument.aspectj.aspectInstruments;

import java.lang.reflect.Field;

import uk.ac.aston.provlayer.observer.model.ModelAccess;
import uk.ac.aston.provlayer.observer.model.ModelAccess.AccessType;
import uk.ac.aston.provlayer.observer.model.ModelPart;
import uk.ac.aston.provlayer.observer.model.ModelPartFeature;
import uk.ac.aston.provlayer.observer.model.ObjectAccessMethod;
import uk.ac.aston.provlayer.observer.model.ModelFeature;
import uk.ac.aston.provlayer.observer.thread.ThreadObserver;

public class ObjectData {
	/*
	 * Put in Object field data, get out Entity data
	 * TODO ObjectData should be rename InstrumentData, it should use Model Part/Attribute Part more for collecting data
	 */


	private ModelPartFeature mPartFeature;
	
	private ModelPart targetObject;	// Model Part
	private ModelFeature  field;			// Model Attribute
	private String targetObjectAsString;
	private String fieldName;
	private String fieldType;
	private Object fieldValue;
	private String fieldValueAsString;
	private Object intentArg;
	private String intentArgAsString;
	private ObjectAccessMethod objectAccessMethod;
	private long accessTime;
	private int memoryVersion, storageVersion;
	
	/*
	public ObjectData (ModelPartFeature partFeature) {
		this.mPartFeature = partFeature;	
	}
	*/
	
	public void setModelPartFeature (ModelPartFeature partFeature) {
		mPartFeature = partFeature;
		
		this.setTargetObject(partFeature.getPart());
		this.setField(partFeature.getFeature());
		this.setFeildValue(partFeature.getFeature().getValue());
				
	}

	public void setTargetObject(ModelPart targetObject)
	{
		this.targetObject = targetObject;
		this.targetObjectAsString = targetObject.getIdentifier();
	}
	
	public ModelPart getTargetObject() {
		return this.targetObject;
	}
	
	public void setTargetObjectAsString(String targetObject) {
		// this can be used to override the string when targetObject.toString() doesn't work as expected.
		this.targetObjectAsString = targetObject;
	}
	
	public String getTargetObjectAsString() {
		return this.targetObjectAsString;
	}

	public void setField(ModelFeature field) {
		this.field = field;
		this.fieldName = field.getName();
		this.fieldType = field.getType();
	}
	
	public ModelFeature getField() {
		return this.field;
	}
	
	
	public void setFeildName(String feildName) {
		this.fieldName = feildName;
	}

	public String getAttribute() {
		return this.fieldName;
	}

	public void setFeildType(String feildType) {
		this.fieldType = feildType;
	}

	public String getType() {
		return this.fieldType;
	}

	public void setFeildValue(Object feildValue) {
		this.fieldValue = feildValue;

		if (feildValue != null) {
			this.fieldValueAsString = feildValue.toString();
		}

	}

	public Object getFeildValue() {
		return this.fieldValue;
	}

	// This is done when the FeildValue is object is set
//	public void setFeildValueAsString(String feildValueAsString) {
//		this.feildValueAsString = feildValueAsString;
//	}

	public String getStateAsString() {

		return this.fieldValueAsString;
	}

	public void setObjectAccessType(ObjectAccessMethod accessType) {
		this.objectAccessMethod = accessType;
	}

	public ObjectAccessMethod getObjectAccessType() {
		return this.objectAccessMethod;
	}

	public void setAccessTime(Long accessTime) {
		this.accessTime = accessTime;
	}

	public Long getAccessTime() {
		return this.accessTime;
	}

	public void setIntentArg(Object intentArg) {
		this.intentArg = intentArg;
		this.intentArgAsString = intentArg.toString();
	}

	public Object getIntentArg() {
		return this.intentArg;
	}
	
	public String getIntentArgAsString ()
	{
		return intentArgAsString;
	}
	
	public void setMemoryVersion(int version) {
		this.memoryVersion = version;
	}
	
	public int getMemoryVersion() {
		return this.memoryVersion;
	}
	
	public void setStorageVersion(int version) {
		this.storageVersion = version;
	}
	
	public int getStorageVersion() {
		return this.storageVersion;
	}	
	
	public void oDataToConsole() {
		System.out.println(">> oData : "
				+ "AccessType=" + this.getObjectAccessType().toString() + " " 
				+ "AccessTime=" + this.getAccessTime().toString() + " " 
				+ "TargetObjectAsString=" + this.getTargetObjectAsString() + " " 
				+ "Attribute=" + this.getAttribute() + " " 
				+ "MemoryVersion=" + this.getMemoryVersion() + " "
				+ "StorageVersion=" + this.getStorageVersion() + " "
				+ "StateAsString=" + this.getStateAsString() + " " 
				+ "IntentArg=" + this.getIntentArg() + " ");
	}
	
	public String getEntityID(ThreadObserver theThreadObserver) {
		
		// Old EMF Object version used fieldID not fieldName
		return ThreadObserver.createEntityInstanceID(
				targetObjectAsString,
				String.valueOf(field.getIdentifier()), 
				getMemoryVersionString(theThreadObserver,this.getMemoryVersion()), 
				String.valueOf(this.storageVersion)
				);
		
		
	}
	
	private String getMemoryVersionString (ThreadObserver theThreadObserver, int memoryVersion) {
		// Memory String formatting is based on the original EMF Object version
		if (memoryVersion == 0) { return (String.valueOf(0)); }
		else { return theThreadObserver.getAgentName() + ":" + String.valueOf(memoryVersion); }
	}
	
	public void reportAsModelAccess(ThreadObserver theThreadObserver, ObjectData oDataBeforeSet) {
		String entityID, oldEntityID;

		entityID = this.getEntityID(theThreadObserver);

		switch (this.getObjectAccessType()) {
		case Get:
			// Report Get
			//oDataToConsole(oData);
			if (oDataBeforeSet != null) { // LOAD MODEL ACCESS
				// Loaded from Storage
				oldEntityID = oDataBeforeSet.getEntityID(theThreadObserver);
				theThreadObserver.reportAccess(
						new ModelAccess.Builder()
								.entityID(entityID)
								.oldEntityID(oldEntityID)
								.eObject(targetObject)
								.feature(field)
								.value(this.getFeildValue())
								.oldValue(oDataBeforeSet.getFeildValue())
								.inModelStorage(true)
								.memoryVersion(getMemoryVersionString(theThreadObserver,this.getMemoryVersion()))
								.storageVersion(String.valueOf(this.storageVersion))
								.type(AccessType.READ)
								.time(this.getAccessTime())
								.build());
			} else {  // READ MODEL ACCESS
				// In memory 
				theThreadObserver.reportAccess(
						new ModelAccess.Builder()
								.entityID(entityID)
								.eObject(targetObject)
								.feature(field)
								.value(this.getFeildValue())
								.memoryVersion(getMemoryVersionString(theThreadObserver,this.getMemoryVersion()))
								.type(AccessType.READ)
								.time(this.getAccessTime())
								.build());
			}

			break;
		case before_Set:
			System.out.println("Error before_Set model access report call on oData");
			break;
		case after_Set:
			// Report Set
			if (oDataBeforeSet != null) {  // UPDATE MODEL ACCESS
			//	oDataToConsole(oDataBeforeSet);
			//	oDataToConsole(oData);

				oldEntityID = oDataBeforeSet.getEntityID(theThreadObserver);

				theThreadObserver.reportAccess(new ModelAccess.Builder()
						.entityID(entityID)
						.oldEntityID(oldEntityID)
						.eObject(targetObject)
						.feature(field)
						.value(this.getFeildValue())
						.oldValue(oDataBeforeSet.getFeildValue())
						.memoryVersion(getMemoryVersionString(theThreadObserver,this.getMemoryVersion()))
						.type(AccessType.WRITE)
						.time(this.getAccessTime())
						.build());
			} else { // CREATE MODEL ACCESS
			//	oDataToConsole(oData);
				theThreadObserver.reportAccess(new ModelAccess.Builder()
						.entityID(entityID)
						.eObject(targetObject)
						.feature(field)
						.value(this.getFeildValue())
						.memoryVersion(getMemoryVersionString(theThreadObserver,this.getMemoryVersion()))
						.type(AccessType.WRITE)
						.time(this.getAccessTime())
						.build());
			}
			break;
		case Commit:	// COMMIT MODEL ACCESS
			
			oldEntityID = null;
			try {
				oldEntityID = oDataBeforeSet.getEntityID(theThreadObserver);
			} catch (Exception e) {
				System.out.println("Error getting oldEntityID");
				e.printStackTrace();
			}
			try {
				theThreadObserver.reportAccess(new ModelAccess.Builder()
						.entityID(entityID)
						.oldEntityID(oldEntityID)
						.commitTime(this.getAccessTime())
						.eObject(targetObject)
						.feature(field)
						.value(this.getFeildValue())
						.inModelStorage(true)
						.memoryVersion(getMemoryVersionString(theThreadObserver,this.getMemoryVersion()))
						.storageVersion(String.valueOf(this.storageVersion))
						.type(AccessType.COMMIT)
						.build());
			} catch (Exception e) {
				System.out.println("Error Reporting access");
				e.printStackTrace();
			}
			//System.out.println("Commited " + oldEntityID + " > " + entityID);
			break;
		}
	}

}
