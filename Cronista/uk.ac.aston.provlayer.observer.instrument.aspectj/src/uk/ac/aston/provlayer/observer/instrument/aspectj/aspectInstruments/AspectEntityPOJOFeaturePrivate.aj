package uk.ac.aston.provlayer.observer.instrument.aspectj.aspectInstruments;

import uk.ac.aston.provlayer.observer.annotations.*;
import uk.ac.aston.provlayer.observer.model.ObjectAccessMethod;

public aspect AspectEntityPOJOFeaturePrivate {

	//private InspectorPOJOFeature inspector;

	private InspectorPOJOFeature inspector = new InspectorPOJOFeature();
	
	/*
	 * Advice is only triggered when A Package/Class/Method is annotated Observe_Me
	 * AND it access a PROTECTED on a Package/Class/Instance annotated as
	 * ObserveModelPrivate
	 */

	// GET PRIVATE
	after() returning(Object r) : get(private * *) 
		&& within(!aspect_Instruments.*)
		&& (@within(ObserveMe) || @withincode(ObserveMe))
		&& @target(ObserveModelPrivates) {
	//	inspector = new InspectorPOJOFeature();
		inspector.inspectPrivate(thisJoinPoint, ObjectAccessMethod.Get, System.currentTimeMillis());
	//	inspector = null;
	}

	// SET PRIVATE
	before(Object o) : set(private * *) && args (o)
		&& within(!aspect_Instruments.*)
		&& (@within(ObserveMe) || @withincode(ObserveMe))
		&& @target(ObserveModelPrivates) {
	//	inspector = new InspectorPOJOFeature();
		inspector.inspectPrivate(thisJoinPoint, ObjectAccessMethod.before_Set, System.currentTimeMillis());
	}

	after(Object o) : set(private * *) && args(o)
		&& within(!aspect_Instruments.*)
		&& (@within(ObserveMe) || @withincode(ObserveMe))
		&& @target(ObserveModelPrivates) {
		inspector.inspectPrivate(thisJoinPoint, ObjectAccessMethod.after_Set, System.currentTimeMillis());
	//	inspector = null;
	}
}
