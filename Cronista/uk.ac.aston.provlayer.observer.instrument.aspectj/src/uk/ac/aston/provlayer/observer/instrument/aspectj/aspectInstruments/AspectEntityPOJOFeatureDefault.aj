package uk.ac.aston.provlayer.observer.instrument.aspectj.aspectInstruments;

import uk.ac.aston.provlayer.observer.annotations.*;
import uk.ac.aston.provlayer.observer.model.ObjectAccessMethod;

public aspect AspectEntityPOJOFeatureDefault {

	private InspectorPOJOFeature inspector;

	/*
	 * Advice is only triggered when A Package/Class/Method is annotated Observe_Me
	 * AND it access a PROTECTED on a Package/Class/Instance annotated as
	 * ObserveModelProtected
	 * 
	 * PROTECTEDs need to be inspected like a "Private", but are accessed by code more like a "Public"
	 */

	// GET DEFAULT
	after() returning(Object r) : get(!private !public !protected * *) 
		&& within(!aspect_Instruments.*)
		&& (@within(ObserveMe) || @withincode(ObserveMe))
		&& @target(ObserveModelDefaults) {
		inspector = new InspectorPOJOFeature();
		inspector.inspectPrivate(thisJoinPoint, ObjectAccessMethod.Get, System.currentTimeMillis());
		inspector = null;
	}

	// before_SET DEFAULT
	before(Object o) : set(!private !public !protected * *) && args (o)
		&& within(!aspect_Instruments.*)
		&& (@within(ObserveMe) || @withincode(ObserveMe))
		&& @target(ObserveModelDefaults) {
		inspector = new InspectorPOJOFeature();
		inspector.inspectPrivate(thisJoinPoint, ObjectAccessMethod.before_Set, System.currentTimeMillis());
	}

	//after_SET DEFAULT
	after(Object o) : set(!private !public !protected * *) && args(o)
		&& within(!aspect_Instruments.*)
		&& (@within(ObserveMe) || @withincode(ObserveMe))
		&& @target(ObserveModelDefaults) {
		inspector.inspectPrivate(thisJoinPoint, ObjectAccessMethod.after_Set, System.currentTimeMillis());
		inspector = null;
	}
}
