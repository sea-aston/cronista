package uk.ac.aston.provlayer.observer.instrument.aspectj.aspectInstruments;

import uk.ac.aston.provlayer.observer.annotations.*;
import uk.ac.aston.provlayer.observer.model.ObjectAccessMethod;

public aspect AspectEntityPOJOFeaturePublic {

	private InspectorPOJOFeature inspector;
	
	/*
	 * Advice is only triggered when A Package/Class/Method is annotated Observe_Me
	 * AND it access a PUBLIC on a Package/Class/Instance annotated as
	 * Observe_Model_Publics
	 */

	// GET PUBLIC
	after() returning(Object r) : get(public * *) 
		&& within(!aspect_Instruments.*)
		&& (@within(ObserveMe) || @withincode(ObserveMe))
		&& @target(ObserveModelPublics) {
		inspector = new InspectorPOJOFeature();
		inspector.inspectPublic(thisJoinPoint, ObjectAccessMethod.Get, System.currentTimeMillis());
		inspector = null;
	}

	// SET PUBLIC
	before(Object o) : set(public * *) && args (o)
		&& within(!aspect_Instruments.*)
		&& (@within(ObserveMe) || @withincode(ObserveMe))
		&& @target(ObserveModelPublics) {
		inspector = new InspectorPOJOFeature();
		inspector.inspectPublic(thisJoinPoint, ObjectAccessMethod.before_Set, System.currentTimeMillis());
	}

	after(Object o) : set(public * *) && args(o)
		&& within(!aspect_Instruments.*)
		&& (@within(ObserveMe) || @withincode(ObserveMe))
		&& @target(ObserveModelPublics) {
		inspector.inspectPublic(thisJoinPoint, ObjectAccessMethod.after_Set, System.currentTimeMillis());
		inspector = null;
	}

	/*
	 * 
	 * // Wide open pointcuts which will advise on before and after on anything
	 * outside of this package
	 * 
	 * // GET PUBLIC pointcut getLog() : get(public * *) &&
	 * within(!aspect_Instruments.*);
	 * 
	 * after() returning(Object r) : get(public * *) &&
	 * within(!aspect_Instruments.*) { inspector = new Inspector();
	 * inspector.inspect(thisJoinPoint, r, System.currentTimeMillis(),
	 * AccessType.Get); }
	 * 
	 * 
	 * // SET PUBLIC
	 * 
	 * pointcut publicSet() : set(public * *) && args(o) &&
	 * within(!aspect_Instruments.*);
	 * 
	 * 
	 * before(Object o) : set(public * *) && args (o) &&
	 * within(!aspect_Instruments.*) { inspector = new Inspector();
	 * inspector.inspect(thisJoinPoint, o, System.currentTimeMillis(),
	 * AccessType.before_Set); }
	 * 
	 * after(Object o) : set(public * *) && args(o) && within(!aspect_Instruments.*)
	 * { inspector = new Inspector(); inspector.inspect(thisJoinPoint, o,
	 * System.currentTimeMillis(), AccessType.after_Set); }
	 */

}
