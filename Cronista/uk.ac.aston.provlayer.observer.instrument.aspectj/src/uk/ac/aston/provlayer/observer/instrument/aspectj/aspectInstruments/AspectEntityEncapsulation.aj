package uk.ac.aston.provlayer.observer.instrument.aspectj.aspectInstruments;

import uk.ac.aston.provlayer.observer.annotations.*;
import uk.ac.aston.provlayer.observer.model.ObjectAccessMethod;

//
//Needs thread safety updates for the inspector 
//

privileged public aspect AspectEntityEncapsulation {

	// This approach to encapsulation handling could be expanded
	// Constructors are not caught for example, these could be added

	private boolean observe = false;

	private InspectorPOJOFeature inspector = null;

	// This point cut sets and resets the observe status,
	// depending on who is calling the get/set.
	// Only an object marked @Observer_Me should be tracked

	pointcut pc() : (call(* get* ()) || call (* set* (*))) 
		&& (@within(ObserveMe) || @withincode(ObserveMe))
		&& @target(ObserveModelEncapsulation) ;
//		&& within(!uk.ac.aston.provlayer.observer.instrument.aspectj.*)

	before() : pc(){
		// System.out.println("PC-before");
		observe = true;
	}

	after() : pc(){
		// System.out.println("PC-after");
		observe = false;
	}

	// Intercepting the model status depends on field interactions.
	// Therefore the get/set of a private/public should be intercepted
	// but only if the observe status is set which denotes a model
	// interaction should be observed.

	// GET PRIVATE
	after() returning(Object r) : 
		get(private * *)
			&& within(!aspect_Instruments.*)
			&& (@within(ObserveMe) || @withincode(ObserveMe))
			&& @within(ObserveModelEncapsulation) {
		// System.out.print("!!!private!!!");
		if (observe) {
			// System.out.println("get_after System=" + observe);
			if (inspector == null) {
				System.out.println("1Get new inspector");
				inspector = new InspectorPOJOFeature();
			}
			inspector.inspectPrivate(thisJoinPoint, ObjectAccessMethod.Get,
					System.currentTimeMillis());
			inspector = null;
		}
	}

	// SET PRIVATE
	before(Object o) : 
		set(private * *) && args (o)
			&& within(!aspect_Instruments.*)
			&& (@within(ObserveMe) || @withincode(ObserveMe))
			&& @within(ObserveModelEncapsulation) {
		// System.out.println("set_before System=" + observe);
		if (observe) {
			if (inspector == null) {
				System.out.println("1Set_before new inspector");
				inspector = new InspectorPOJOFeature();
			}

			inspector.inspectPrivate(thisJoinPoint, ObjectAccessMethod.before_Set,
					System.currentTimeMillis());
		}
	}

	after(Object o) : 
		set(private * *) && args(o)
			&& within(!aspect_Instruments.*)
			&& (@within(ObserveMe) || @withincode(ObserveMe))
			&& @within(ObserveModelEncapsulation) {
		// System.out.println("set_after System=" + observe);
		if (observe) {
			if (inspector == null) {
				System.out.println("1Set_after new inspector");
				inspector = new InspectorPOJOFeature();
			}

			inspector.inspectPrivate(thisJoinPoint, ObjectAccessMethod.after_Set,
					System.currentTimeMillis());
			inspector = null;
		}
	}
}