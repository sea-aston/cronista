package uk.ac.aston.provlayer.observer.instrument.aspectj.aspectInstruments;

import java.lang.reflect.Field;

import uk.ac.aston.provlayer.observer.model.ModelFeature;

public class ModelFeaturePOJOFeature implements ModelFeature {
	
	private Field field;
	private Object value;
	
	public ModelFeaturePOJOFeature (Field field, Object value) {
		this.field = field;
		this.value = value;
	}
	
	@Override
	public String getIdentifier() {
		return field.getName() + "";
	}
	
	@Override
	public String getName() {
		return field.getName() + "";
	}

	@Override
	public String getType() {
		return field.getType() + "";
	}
	
	@Override
	public Object getValue() {
		return value;
	}
	
	@Override
	public String getValueAsString() {
		return value.toString() + "";
	}

	@Override
	public Field getRaw() {
		// TODO we could have a Class to contain the field and value objects
		return field;
	}

	@Override
	public boolean hasStorageVersioning() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getStorageVersion() {
		// TODO Auto-generated method stub
		return 0;
	}



}
