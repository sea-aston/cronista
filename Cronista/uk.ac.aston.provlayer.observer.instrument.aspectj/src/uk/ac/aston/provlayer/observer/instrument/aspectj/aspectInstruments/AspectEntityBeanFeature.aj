package uk.ac.aston.provlayer.observer.instrument.aspectj.aspectInstruments;

import uk.ac.aston.provlayer.observer.annotations.*;
import uk.ac.aston.provlayer.observer.model.ObjectAccessMethod;


//
//  Needs thread safety updates for the inspector 
//


privileged public aspect AspectEntityBeanFeature {
	
	private boolean observe = false;

	private InspectorPOJOFeature inspector = null;

	// GET FEATURE
	after() returning (Object r) : 
		get(* *)
			&& withincode(* get*())
			&& @within(ObserveModelBeanFeature)	
	{
		if (observe) {
			observe = false;
			// System.out.println("get_after System=" + observe);
			if (inspector == null) {
				System.out.println("BF: Get new inspector");
				inspector = new InspectorPOJOFeature();
			}
			inspector.inspectPrivate(thisJoinPoint, ObjectAccessMethod.Get, System.currentTimeMillis());
			inspector = null;
		}
		observe = true;
	}

	// SET FEATURE
	before(Object o) :
		set(* *) && args (o)
		 	&& withincode (* set*(..))
			&& @within(ObserveModelBeanFeature)
	{
		if (observe) {
			observe = false;
			if (inspector == null) {
				System.out.println("BF: Set_before new inspector");
				inspector = new InspectorPOJOFeature();
			}

			inspector.inspectPrivate(thisJoinPoint, ObjectAccessMethod.before_Set, System.currentTimeMillis());
			observe = true;
		}
	}

	after(Object o) : 
		set(* *) && args (o)
	 		&& withincode (* set*(..))
	 		&& @within(ObserveModelBeanFeature) 
	{
		System.out.println("BF: set_after System=" + observe);
		if (observe) {
			observe = false;
			if (inspector == null) {
				System.out.println("BF: Set_after new inspector");
				inspector = new InspectorPOJOFeature();
			}

			inspector.inspectPrivate(thisJoinPoint, ObjectAccessMethod.after_Set, System.currentTimeMillis());
			inspector = null;
			observe = true;
		}
	}


}