package uk.ac.aston.provlayer.tinkerpop.provDM;

import java.util.ArrayList;
import java.util.List;

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;

import uk.ac.aston.provlayer.descriptions.AgentDescription;
import uk.ac.aston.provlayer.storage.api.Agent;
import uk.ac.aston.provlayer.storage.tinkerpop.TpProvenanceGraphStorage;

public class TpAgent {

	private Vertex vAgent; // Reference to the Tinkerpop Vertex

	// Local copy of the Properties on the Agent Vertex
	private String ID;

	// Relationships
	
	// ActedOnBehalfOf Agent
	private Vertex ActedOnBeHalfOf;  // NOT USED YET

	// WasAttributedTo Entity  ** REVERSE - not standard 
	private List<Vertex> WasAttributedTo  = new ArrayList<Vertex>();
	// WasAssociatedWith Activity  ** REVERSE - not standard
	private List<Vertex> WasAssociatedWith  = new ArrayList<Vertex>();

	public TpAgent(Object agent) {
		if (agent.getClass().equals(AgentDescription.class)) { // Turn a description into Vertex
			AgentDescription agentD = (AgentDescription) agent;
			// Set locals
			ID = agentD.getID();
			
			// Create Vertex
			GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
			vAgent = g.addV("Agent").
					property("ID", ID).
					next();
		} else if (agent.getClass().equals(Vertex.class)) { 
			// Turn a Vertex back into a Tp object

		} else { // Catch the bad things here for now
			System.out.println("ERROR: don't put that in here");
		}
	}

	// Reads
	public String getID() {
		return ID;
	}

	public Vertex getVagent() {
		return vAgent;
	}
	
	// Update Properties
	
	// Update Relationships


	// Traversal clean up
	private void gClose(GraphTraversalSource g) {
		try {
			g.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
