package uk.ac.aston.provlayer.tinkerpop.historyModel;

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;

import uk.ac.aston.provlayer.storage.tinkerpop.TpProvenanceGraphStorage;

public class TpTimeWindow {

	private Vertex vTimeWindow; // This is the Vertex for the time window on the graph.

	private long StartLocalTime, EndLocalTime;

	private TpTimeWindow NextTimeWindow, PreviousTimeWindow; // Should these be TpTimeWindow object or just a Vertex
																// reference...

	private TpSystemModelState tpSystemModelState; // could be some memory saving here unless we connect to vertexes in
													// the snapshot...
	private TpProvenanceGraph tpProvGraph;
	
	public TpTimeWindow(long startTime) {
		// TODO We should ask for the System Model state when creating a new Time Window
		this.StartLocalTime = startTime;
		
		
		GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
		// Create Vertex
		vTimeWindow = g.addV("TimeWindow").property("StartTime", startTime).next();
		
		// Create the System Model State and Provenance Graphs (create their own Vertexes)
		this.tpSystemModelState = new TpSystemModelState("I.O.U. a Reference",startTime,vTimeWindow);
		this.tpProvGraph = new TpProvenanceGraph(vTimeWindow);
		
		
		// Attach edge to the prior time window or start Vertex
		//g.V(vTimeWindow).addE("previous (after)").to(previousTimeWindow).iterate();
		
		TpProvenanceGraphStorage.closeG(g);
	}
	
	public Vertex getvTimeWindow()
	{
		return this.vTimeWindow;
	}

	public long GetStartTime() {
		// used by the curator through the adapter for checking passage of time
		return this.StartLocalTime;
	}

	public void setEndLocalTime(long endTime) {
		// Set by graphStorageAdapter when opening a new time window
		this.EndLocalTime = endTime;
	}

	public long getEndTime() {
		// shouldn't be required for building the history model
		return this.EndLocalTime;
	}

	public void setStartLocalTime(long l) {
		// Time is set on creation shouldn't need updating

	}
	

	public TpProvenanceGraph getTpProvenanceGraph() {
		return this.tpProvGraph;
	}

	public TpSystemModelState getTpSystemModelState() {
		return this.tpSystemModelState;
	}

	public void setNextTimeWindow(TpTimeWindow tpTimeWindow) {
		this.NextTimeWindow = tpTimeWindow;
		
		// Create an Edge
		GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
		g.V(vTimeWindow).addE("Next (before)").to(tpTimeWindow.getvTimeWindow()).iterate();
		TpProvenanceGraphStorage.closeG(g);
		
	}

	public TpTimeWindow getNextTimeWindow() {
		return NextTimeWindow;
	}

	public void setPreviousTimeWindow(TpTimeWindow tpTimeWindow) {
		this.PreviousTimeWindow = tpTimeWindow;
		// Create an Edge
		GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
		g.V(vTimeWindow).addE("Previous (After)").to(tpTimeWindow.getvTimeWindow()).iterate();
		TpProvenanceGraphStorage.closeG(g);
	}

	public TpTimeWindow getPreviousTimeWindow() {
		return this.PreviousTimeWindow;
	}
}
