package uk.ac.aston.provlayer.tinkerpop.provDM;

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;

import uk.ac.aston.provlayer.descriptions.EntityDescription;
import uk.ac.aston.provlayer.storage.tinkerpop.TpProvenanceGraphStorage;

public class TpEntityDescription {
	// With these PROVDM parts, we can probably drop storing the details and only keep the Vertex and methods to pull from the graph
	
	private Vertex vEntityDescription;
	private EntityDescription entityD; // shortcut, hold the description
	
	public TpEntityDescription (EntityDescription entityD) {
		this.entityD = entityD;

		// Create the Vertex
		GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
		vEntityDescription = g.addV("EntityDescription")
				.property("Object", entityD.getObjectName() + "")
				.property("AttributeName", entityD.getAttributeNameAsString() + "")
				.property("AttributeValue", entityD.getAttributeValueAsString() + "")
				.next();
	}
	
	public Vertex getVEntityDescription () {
		return vEntityDescription;
	}

}
