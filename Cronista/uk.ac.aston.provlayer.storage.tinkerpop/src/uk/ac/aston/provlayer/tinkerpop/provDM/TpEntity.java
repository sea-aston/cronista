package uk.ac.aston.provlayer.tinkerpop.provDM;

import java.util.ArrayList;
import java.util.List;

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;

import uk.ac.aston.provlayer.descriptions.EntityDescription;
import uk.ac.aston.provlayer.storage.api.Activity;
import uk.ac.aston.provlayer.storage.api.Agent;
import uk.ac.aston.provlayer.storage.api.Entity;
import uk.ac.aston.provlayer.storage.tinkerpop.TpProvenanceGraphStorage;

public class TpEntity {

	private Vertex vEntity; // Reference to get at the Tinkerpop Vertex

	// Local copy of the Properties on the Entity Vertex
	private String ID;
	private Long StartTime, EndTime;

	// collection of the descriptions from messages
	private List<TpEntityDescription> TpEntityDescriptions = new ArrayList<TpEntityDescription>();

	// Relationships

	// WasGeneratedBy Activity
	private List<Vertex> WasGeneratedBy = new ArrayList<Vertex>();
	// WasDerivedFrom Entity
	private List<Vertex> WasDerivedFrom = new ArrayList<Vertex>();
	// WasAttributedTo Agent
	private List<Vertex> WasAttributedTo = new ArrayList<Vertex>();

	// Reverse relationships commited for now

	public TpEntity(Object entity) {
		if (entity.getClass().equals(EntityDescription.class)) {
			EntityDescription entD = (EntityDescription) entity;
			// Set locals
			ID = entD.getEntityInstanceID();
			StartTime = entD.getStartTime();
			EndTime = entD.getEndTime();

			// Create Vertex
			GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
			vEntity = g.addV("Entity").
					property("ID", ID).
					property("StartTime", String.valueOf(StartTime)).
					property("EndTime", String.valueOf(EndTime)).
					property("ObjectName", entD.getObjectName()+"").
					property("ObjectType", entD.getObjectType()+"").
					property("AttributeName", entD.getAttributeNameAsString()+"").
					property("AttributeValue", entD.getAttributeValueAsString()+"").
					property("AttributeType", entD.getAttributeTypeAsString()+"").					
					next();
			/*
			if(entD.getAttributeNameAsString()!=null)		
				g.V(vEntity).property("AttributeName", entD.getAttributeNameAsString()).next();
			if(entD.getAttributeValueAsString()!=null)
				g.V(vEntity).property("AttributeValue", entD.getAttributeValueAsString()).next();
			if(entD.getObjectName()!=null)
				g.V(vEntity).property("Object", entD.getObjectName()).next();
			*/
			TpProvenanceGraphStorage.closeG(g);
		} else if (entity.getClass().equals(Vertex.class)) {
			// Turn a Vertex back into a Tp object

		} else { // Catch the bad things here for now
			System.out.println("ERROR: don't put that in here");
		}

	}
	
	public Vertex getVentity() {
		return vEntity;
	}
	

	// Reads
	public String getID() {
		return ID;
	}

	public Long getStartTime() {
		return StartTime;
	}

	public Long getEndTime() {
		return EndTime;
	}

	public List<TpEntityDescription> getEntityDescriptions() {
		return TpEntityDescriptions;	
	}
	// Update Properties
	
	public void setID(String ID) {
		// Local
		this.ID = ID;

		// Remote
		GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
		g.V(vEntity).property("ID", ID).next();
		TpProvenanceGraphStorage.closeG(g);
	}

	public void setEndTime(long endTime) {
		// Local
		this.EndTime = endTime;

		// Remote
		GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
		g.V(vEntity).property("EndTime", String.valueOf(EndTime)).next();
		TpProvenanceGraphStorage.closeG(g);
	}

	// Update Relationships

	public void setWasGeneratedBy(Vertex vActivity) {
		// local
		WasGeneratedBy.add(vActivity);
		// remote
		GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
		g.V(vEntity).addE("WasGeneratedBy").to(vActivity).iterate();
		TpProvenanceGraphStorage.closeG(g);
	}

	public void setWasDerivedFrom(Vertex vEntityOrg) {
		// local
		WasDerivedFrom.add(vEntityOrg);
		// remote
		GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
		g.V(vEntity).addE("WasDerivedFrom").to(vEntityOrg).iterate();
		TpProvenanceGraphStorage.closeG(g);
	}

	public void setWasAttributedTo(Vertex vAgent) {
		if(WasAttributedTo.size()==0)
		{
			// local
			WasAttributedTo.add(vAgent);
			// remote
			GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
			g.V(vEntity).addE("WasAttributedTo").to(vAgent).iterate();
			TpProvenanceGraphStorage.closeG(g);
		}
	}
	
	public void addEntityDescription(EntityDescription entityD) {
		TpEntityDescription newEntD = new TpEntityDescription(entityD);
		TpEntityDescriptions.add(newEntD);
		
		// Connect the edge
		GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
		g.V(vEntity).addE("Message Describing").to(newEntD.getVEntityDescription()).iterate();
		TpProvenanceGraphStorage.closeG(g);
	}



}
