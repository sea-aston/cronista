package uk.ac.aston.provlayer.tinkerpop.provDM;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;

import uk.ac.aston.provlayer.descriptions.ActivityDescription;
import uk.ac.aston.provlayer.storage.api.Activity;
import uk.ac.aston.provlayer.storage.api.Agent;
import uk.ac.aston.provlayer.storage.api.Entity;
import uk.ac.aston.provlayer.storage.tinkerpop.TpProvenanceGraphStorage;

public class TpActivity {

	private Vertex vActivity; // Reference to get at the Tinkerpop Vertex

	// Local copy of the Properties on the Activity Vertex
	private String ID, Name;
	private long StartTime, EndTime; // Save dropping into the descriptions for time

	// Collection of the descriptions from messages
	private List<Vertex> ActivityDescriptions  = new ArrayList<Vertex>();

	// Relationships
	
	// WasAssociatedWith Agent
	private List<Vertex> WasAssociatedWith = new ArrayList<Vertex>();
	// Used Entity
	private List<Vertex> Used = new ArrayList<Vertex>();
	// WasInformedBy Activity
	private List<Vertex> WasInformedBy = new ArrayList<Vertex>();
		
	// Generates Entity ** REVERSE - not standard
	private Iterable<Vertex> Generates;

	public TpActivity(Object activity) {
		if (activity.getClass().equals(ActivityDescription.class)) {  // Turn a description into a Vertex
			ActivityDescription actD = (ActivityDescription) activity;
			// Set locals
			ID = actD.getID();
			Name = actD.getName();
			StartTime = actD.getStartTime();
			EndTime = actD.getEndTime();
			
			// Create Vertex
			GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
			vActivity = g.addV("Activity").
					property("ID",ID).
					property("Name", Name).
					property("StartTime", String.valueOf(StartTime)).
					property("EndTime", String.valueOf(EndTime)).
					next();
			TpProvenanceGraphStorage.closeG(g);
		} else if(activity.getClass().equals(Vertex.class)) { 
			// Turn a Vertex back into a Tp object
			
		} else { // Catch the bad things here for now
			System.out.println("ERROR: don't put that in here");
		}
			
	}
	
	public Vertex getVactivity() {
		return vActivity;
	}
	
	// Reads
	public String getID() {
		return ID;
	}
	
	public String getName() {
		return Name;
	}
	
	public Long getStartTime() {
		return StartTime;
	}
	
	public Long getEndTime() {
		return EndTime;
	}
	
	// Update Properties
	
	public void setEndTime(long endTime) {
		// Local
		this.EndTime = endTime;

		// Remote
		GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
		g.V(vActivity).property("EndTime", String.valueOf(EndTime)).next();
		TpProvenanceGraphStorage.closeG(g);
	}
	
	// Update Relationships
	
	public void setWasAssociatedWith(Vertex vAgent) {
		// local
		if(!WasAssociatedWith.contains(vAgent)) {
		WasAssociatedWith.add(vAgent); 
		
		// remote
		GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
		g.V(vActivity).addE("WasAssociatedWith").to(vAgent).iterate();
		TpProvenanceGraphStorage.closeG(g);
		}
	}
	
	
	public void setUsed(Vertex vEntity) {
		// local
		Used.add(vEntity);
		// remote
		GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
		g.V(vActivity).addE("Used").to(vEntity).iterate();
		TpProvenanceGraphStorage.closeG(g);
	}
	
	public void setWasInformedBy(Vertex vActivity) {
		// local
		WasInformedBy.add(vActivity);
		// remote
		GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
		g.V(vActivity).addE("WasInformedBy").to(vActivity).iterate();
		TpProvenanceGraphStorage.closeG(g);
	}
	

	
	/*
	@Override
	public void setID(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ActivityDescription getActivityDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setActivityDescription(ActivityDescription desc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Agent getWasAssociatedWith() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setWasAssociatedWith(Agent agent) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Set<Activity> getWasInformedBy() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addWasInformedBy(Activity InformingAct) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Set<Entity> getUsed() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addUsed(Entity usedEntity) {
		// TODO Auto-generated method stub
		
	}
	*/

}
