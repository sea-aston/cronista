package uk.ac.aston.provlayer.tinkerpop.historyModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;

import uk.ac.aston.provlayer.descriptions.ActivityDescription;
import uk.ac.aston.provlayer.descriptions.AgentDescription;
import uk.ac.aston.provlayer.descriptions.EntityDescription;
import uk.ac.aston.provlayer.storage.tinkerpop.TpProvenanceGraphStorage;
import uk.ac.aston.provlayer.tinkerpop.provDM.TpActivity;
import uk.ac.aston.provlayer.tinkerpop.provDM.TpAgent;
import uk.ac.aston.provlayer.tinkerpop.provDM.TpEntity;

public class TpProvenanceGraph {

	// The Tinker-pop Graph layer here is keeps some objects that mirror Vertexes
	// created in the graph.
	// This approach is taken to hopefully reduce the amount of graph searching
	// required. However,
	// this layer could be replaced with an approach that works on just graph
	// queries reducing the memory
	// foot print of the curator process. A graph only approach is likely to run
	// slower as the requests
	// would be subject to increase latency from traversing a network connection and
	// be subject to
	// graph database performance limitations.

	private Vertex vProvenanceGraph; // This is the vertex this object should mirror

	// Keep a local buffer of the vertexes on the graph we handle 
	// -- this should cut down latency by avoiding graph searches
	
	private List<TpAgent> vAgents = new ArrayList<TpAgent>();
	private List<TpActivity> vActivities = new ArrayList<TpActivity>();
	private List<TpEntity> vEntities = new ArrayList<TpEntity>();
	
	public TpProvenanceGraph (Vertex vTimeWindow) {
		GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
		
		// Create a Vertex
		vProvenanceGraph = g.addV("ProvenanceGraph").next();
		
		// Attach edge to the time window
		g.V(vProvenanceGraph).addE("in Time Window").to(vTimeWindow).iterate();
		
		TpProvenanceGraphStorage.closeG(g);
		
	}
	
	

	public TpAgent addTpAgent(AgentDescription agentD) {	
		TpAgent agent = new TpAgent(agentD);
		vAgents.add(agent);
		
		// Create the Edge to attach it to the provenance graph
		GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
		g.V(vProvenanceGraph).addE("Agent").to(agent.getVagent()).iterate();
		TpProvenanceGraphStorage.closeG(g);
		
		return agent;
	}

	public TpAgent getTpAgent(String agentID) {
		TpAgent agent;
		// check local buffer
		agent = searchLocalAgents(agentID);
		// TODO check remote -- Only applicable if there is no buffer or more than one curator
		return agent;		
	}

	public TpActivity addTpActivity(ActivityDescription activityD) {
		TpActivity activity = new TpActivity(activityD);
		vActivities.add(activity);
		// Create the Edge to attach it to the provenance graph
		GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
		g.V(vProvenanceGraph).addE("Activity").to(activity.getVactivity()).iterate();
		TpProvenanceGraphStorage.closeG(g);
		return activity;
	}

	public TpActivity getTpActivity(String activityID) {
		TpActivity activity;
		// check local buffer
		activity = searchLocalActivitys(activityID);
		// TODO check remote -- Only applicable if there is no buffer or more than one curator
		return activity;
	}

	public TpEntity addTpEntity(EntityDescription entityD) {
		TpEntity entity = new TpEntity(entityD);
		vEntities.add(entity);
		// Create the Edge to attach it to the provenance graph
		GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
		g.V(vProvenanceGraph).addE("Entity").to(entity.getVentity()).iterate();
		TpProvenanceGraphStorage.closeG(g);
		return entity;
	}

	public TpEntity getTpEntity(EntityDescription entityD) {
		TpEntity entity;
		// check local buffer
		entity = searchLocalEntities(entityD.getEntityInstanceID());
		return entity;
	}

	public TpEntity addTpEntityDescriptionVertex(EntityDescription entity, Vertex vEntity) {
		// This process might be folded into addEntityVertex under and if exists, then
		// add to.
		// However, that could result in additional searching.
		return null;
	}
	
	
	
	// Search the local buffers
	private TpAgent searchLocalAgents (String agentID) {
		for (TpAgent agentInList : vAgents) {
			if(agentInList.getID().equals(agentID))
					return agentInList;
		}
		return null;
	}
	
	private TpActivity searchLocalActivitys (String ActivityID) {
		for (TpActivity activityInList : vActivities) {
			if(activityInList.getID().equals(ActivityID))
					return activityInList;
		}
		return null;
	}
	
	private TpEntity searchLocalEntities (String entityID) {
		for (TpEntity entityInList : vEntities) {
			if(entityInList.getID().equals(entityID))
					return entityInList;
		}
		return null;
	}
	
	
	

}
