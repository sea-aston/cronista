package uk.ac.aston.provlayer.tinkerpop.historyModel;

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;

import uk.ac.aston.provlayer.storage.tinkerpop.TpProvenanceGraphStorage;

public class TpSystemModelState {

	private Vertex vSystemModelState; // This is the vertex that this object should mirror

	private String SystemStateReferenceAsString;
	private long FirstCommitTime;

	public TpSystemModelState(String SystemStateReferenceString, long firstCommitTime, Vertex vTimeWindow) {
		this.FirstCommitTime = firstCommitTime;
		this.SystemStateReferenceAsString = SystemStateReferenceString;

		GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();

		// Create a Vertex
		vSystemModelState = g.addV("SystemModelState").property("firstCommitTime", firstCommitTime)
				.property("SystemStateReferenceAsString", SystemStateReferenceAsString).next();

		// Attach edge to the time window
		g.V(vSystemModelState).addE("in Time Window").to(vTimeWindow).iterate();

		TpProvenanceGraphStorage.closeG(g);
	}

	public long getFirstCommitTime() {
		return this.FirstCommitTime;
	}

	public void setFirstcommitTime(long time) {
		// I don't think we will be needing this, time should be set on creation and not
		// changed...
		this.FirstCommitTime = time;

		// Update the Vertex

	}
}
