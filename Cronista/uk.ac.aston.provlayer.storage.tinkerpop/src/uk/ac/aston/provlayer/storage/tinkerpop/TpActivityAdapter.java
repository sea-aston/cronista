package uk.ac.aston.provlayer.storage.tinkerpop;

import java.util.Set;

import uk.ac.aston.provlayer.descriptions.ActivityDescription;
import uk.ac.aston.provlayer.storage.api.Activity;
import uk.ac.aston.provlayer.storage.api.Agent;
import uk.ac.aston.provlayer.storage.api.Entity;
import uk.ac.aston.provlayer.tinkerpop.provDM.TpActivity;
import uk.ac.aston.provlayer.tinkerpop.provDM.TpEntity;

public class TpActivityAdapter implements Activity{
	// This should be a Tinkerpop PROVDM object...
	private final TpActivity tpActivity;
	private final TpDescriptionConverter DC = new TpDescriptionConverter();


	public TpActivityAdapter(TpActivity tpActivity)
	{
		this.tpActivity = tpActivity;
	}
	
	protected TpActivity getTpActivity () {
		return tpActivity;
	}

	public void setEndTime(long endTime) {
		tpActivity.setEndTime(endTime);
	}

	@Override
	public String getID() {
		// not used
		return null;
	}


	@Override
	public void setID(String id) {
		// not used -- Set on creation
		
	}


	@Override
	public ActivityDescription getActivityDescription() {
		// not used
		return null;
	}


	@Override
	public void setActivityDescription(ActivityDescription desc) {
		// not used Descriptions are not detailed enough yet
		
	}


	@Override
	public Agent getWasAssociatedWith() {
		// not used
		return null;
	}


	@Override
	public void setWasAssociatedWith(Agent agent) {
		final TpAgentAdapter adptAgent = (TpAgentAdapter) agent;
		tpActivity.setWasAssociatedWith(adptAgent.getTpAgent().getVagent());	
	}


	@Override
	public Set<Activity> getWasInformedBy() {
		// not used
		return null;
	}


	@Override
	public void addWasInformedBy(Activity InformingAct) {
		final TpActivityAdapter adptAct = (TpActivityAdapter) InformingAct;
		tpActivity.setWasInformedBy(adptAct.getTpActivity().getVactivity());
	}


	@Override
	public Set<Entity> getUsed() {
		// not used
		return null;
	}


	@Override
	public void addUsed(Entity usedEntity) {
		final TpEntityAdapter adptEntity = (TpEntityAdapter) usedEntity;
		tpActivity.setUsed(adptEntity.getTpEntity().getVentity());
	}

}