package uk.ac.aston.provlayer.storage.tinkerpop;

import java.util.List;

import uk.ac.aston.provlayer.descriptions.EntityDescription;
import uk.ac.aston.provlayer.storage.api.Activity;
import uk.ac.aston.provlayer.storage.api.Agent;
import uk.ac.aston.provlayer.storage.api.Entity;
import uk.ac.aston.provlayer.tinkerpop.provDM.TpAgent;
import uk.ac.aston.provlayer.tinkerpop.provDM.TpEntity;

public class TpEntityAdapter implements Entity{
	
	private final TpEntity tpEntity;

	public TpEntityAdapter (TpEntity tpEntity) {
		this.tpEntity = tpEntity;
	}
	
	protected TpEntity getTpEntity() {
		return tpEntity;
	}
	
	public void setEndTime(long endTime) {
		tpEntity.setEndTime(endTime);
	}
	
	@Override
	public String getID() {
		// not used
		return null;
	}

	@Override
	public void setID(String id) {
		tpEntity.setID(id);
	}

	@Override
	public List<EntityDescription> getEntityDescriptions() {
		// not used
		return null;
	}

	@Override
	public void addEntityDescription(EntityDescription oldEntity) {
		// If we skip this we can leave the entity description nodes out of the graph
		// The descriptions serve more for verification of the entity and the messages
		// that describe it at runtime.
		
		//tpEntity.addEntityDescription(oldEntity);
	}

	@Override
	public Agent getWasAttributedTo() {
		// not used
		return null;
	}

	@Override
	public void setWasAttributedTo(Agent agent) {
		final TpAgentAdapter adptAgent = (TpAgentAdapter) agent;
		tpEntity.setWasAttributedTo(adptAgent.getTpAgent().getVagent());
	}

	@Override
	public Activity getWasGeneratedBy() {
		// not used
		return null;
	}

	@Override
	public void setWasGeneratedBy(Activity activity) {
		final TpActivityAdapter adptAct = (TpActivityAdapter) activity;
		tpEntity.setWasGeneratedBy(adptAct.getTpActivity().getVactivity());
	}

	@Override
	public Entity getWasDerivedFrom() {
		// not used
		return null;
	}

	@Override
	public void setWasDerivedFrom(Entity oldGraphEntity) {
		final TpEntityAdapter adptEnt = (TpEntityAdapter) oldGraphEntity;
		tpEntity.setWasDerivedFrom(adptEnt.getTpEntity().getVentity());
		
	}

}
