package uk.ac.aston.provlayer.storage.tinkerpop;

import uk.ac.aston.provlayer.descriptions.ActivityDescription;
import uk.ac.aston.provlayer.descriptions.AgentDescription;
import uk.ac.aston.provlayer.descriptions.EntityDescription;
import uk.ac.aston.provlayer.storage.api.Activity;
import uk.ac.aston.provlayer.storage.api.Agent;
import uk.ac.aston.provlayer.storage.api.Entity;
import uk.ac.aston.provlayer.storage.api.ProvenanceGraph;
import uk.ac.aston.provlayer.tinkerpop.historyModel.TpProvenanceGraph;
import uk.ac.aston.provlayer.tinkerpop.provDM.TpActivity;
import uk.ac.aston.provlayer.tinkerpop.provDM.TpAgent;
import uk.ac.aston.provlayer.tinkerpop.provDM.TpEntity;

public class TpProvenanceGraphAdapter implements ProvenanceGraph{

	private TpProvenanceGraph vProvenanceGraph;
	private TpDescriptionConverter DC = new TpDescriptionConverter();
	
	public TpProvenanceGraphAdapter(TpProvenanceGraph TpProvenanceGraph)
	{
		this.vProvenanceGraph = TpProvenanceGraph;
	}
	
	public TpProvenanceGraph getTpProvenanceGraph ()
	{
		return vProvenanceGraph;
	}
	
	@Override
	public Iterable<Activity> getActivities() {
		// TODO Auto-generated method stub
		// Only used in Test code on CDO
		return null;
	}

	@Override
	public Iterable<Entity> getEntities() {
		// TODO Auto-generated method stub
		// Only used in Test code on CDO
		return null;
	}

	@Override
	public Iterable<Agent> getAgents() {
		// TODO Auto-generated method stub
		// Only used in Test code on CDO
		return null;
	}

	@Override
	public Agent searchAddProvAgent(AgentDescription agentD) {
		// TODO Auto-generated method stub
		
		TpAgent tpAgent; 
		
		// Try to get the agent from the Provenance graph
		tpAgent = vProvenanceGraph.getTpAgent(agentD.getID());
		
		// if matches adapter wrap it and return
		if(tpAgent != null)
			return new TpAgentAdapter(tpAgent);
		// else make a new one and adapter wrap it and return
		return new TpAgentAdapter(vProvenanceGraph.addTpAgent(agentD));
	}

	@Override
	public Activity searchAddProvActivity(AgentDescription agent, ActivityDescription activityD) {
		
		TpActivity tpActivity;
		tpActivity = vProvenanceGraph.getTpActivity(activityD.getID());
		
		// if matches adapter wrap it and return
		if(tpActivity != null)
		{
			// do we need to update the end time?
			if(activityD.getEndTime()!=0)  // only one message should contain an EndTime (the last one)
				tpActivity.setEndTime(activityD.getEndTime());
			return new TpActivityAdapter(tpActivity);
		}
		// else make a new one and adapter wrap it and return
		return new TpActivityAdapter(vProvenanceGraph.addTpActivity(activityD));
	}

	@Override
	public Entity searchProvEntity(EntityDescription entityD) {
		
		TpEntity tpEntity;
		tpEntity = vProvenanceGraph.getTpEntity(entityD);
		
		// if matches adapter wrap it and return
		if(tpEntity != null)
			return new TpEntityAdapter(tpEntity);
		// else
		return null;
		
	}

	@Override
	public Entity addProvEntity(EntityDescription entityD) {

		return new TpEntityAdapter(vProvenanceGraph.addTpEntity(entityD));
	}

}
