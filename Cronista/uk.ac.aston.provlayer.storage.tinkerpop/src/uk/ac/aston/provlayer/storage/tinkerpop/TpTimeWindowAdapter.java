package uk.ac.aston.provlayer.storage.tinkerpop;

import uk.ac.aston.provlayer.storage.api.ProvenanceGraph;
import uk.ac.aston.provlayer.storage.api.SystemModelState;
import uk.ac.aston.provlayer.tinkerpop.historyModel.TpTimeWindow;

public class TpTimeWindowAdapter implements uk.ac.aston.provlayer.storage.api.TimeWindow{

	// We have a Time window adapter, but it isn't likely to be needed - Storage adapter uses time window directly
	
	private final TpTimeWindow window;
	
	public TpTimeWindowAdapter (TpTimeWindow timeWindow)
	{
		this.window = timeWindow;
	}
	
	
	@Override
	public long getStartLocalTime() {
		// Used by the curator for seeing passage of time for commits
		return window.GetStartTime();
	}

	@Override
	public void setStartLocalTime(long l) {
		// should not be required		
	}

	@Override
	public long getEndLocalTime() {
		// should not be required
		return window.getEndTime();
	}

	@Override
	public void setEndLocalTime(long t) {
		// should not be required
		window.setEndLocalTime(t);
		
	}

	@Override
	public ProvenanceGraph getProvenanceGraph() {
		// This is used by the curator for message processing
		return new TpProvenanceGraphAdapter(window.getTpProvenanceGraph());
	}

	@Override
	public void setProvenanceGraph(ProvenanceGraph pg) {
		// Not needed the Provenance Graph was created with the time window
		
	}

	@Override
	public SystemModelState getSystemModelState() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSystemModelState(SystemModelState sms) {
		// Not needed the System Model State was created with the time window
		
	}

}
