package uk.ac.aston.provlayer.storage.tinkerpop;

import org.apache.tinkerpop.gremlin.structure.Vertex;

import uk.ac.aston.provlayer.storage.api.Agent;
import uk.ac.aston.provlayer.tinkerpop.provDM.TpActivity;
import uk.ac.aston.provlayer.tinkerpop.provDM.TpAgent;

public class TpAgentAdapter implements Agent{
	
	private final TpAgent tpAgent;
	
	public TpAgentAdapter(TpAgent tpAgent)
	{
		this.tpAgent = tpAgent;
	}
	
	public TpAgent getTpAgent() {
		return tpAgent;
	}
	

	@Override
	public String getID() {
		// not used
		return null;
	}

	@Override
	public void setID(String id) {
		// not used
	}

}
