package uk.ac.aston.provlayer.storage.tinkerpop;

import static org.apache.tinkerpop.gremlin.process.traversal.AnonymousTraversalSource.traversal;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.tinkerpop.gremlin.driver.Client;
import org.apache.tinkerpop.gremlin.driver.Cluster;
import org.apache.tinkerpop.gremlin.driver.Cluster.Builder;
import org.apache.tinkerpop.gremlin.driver.remote.DriverRemoteConnection;
import org.apache.tinkerpop.gremlin.process.traversal.AnonymousTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;

import uk.ac.aston.provlayer.storage.api.ProvenanceGraphStorage;
import uk.ac.aston.provlayer.tinkerpop.historyModel.TpTimeWindow;

public class TpProvenanceGraphStorage implements ProvenanceGraphStorage {

	// Tinker-pop connection information
	// TODO add the port and IP
	private static final String DEFAULT_HOSTNAME = "localhost";
	private static final int DEFAULT_PORT = 8182;

	private Vertex historyModelStart;
	
	private TpTimeWindow currentTpTimeWindow = null;

	private static GraphTraversalSource g;
	private static Client client;
	private static Cluster cluster;
	
	public TpProvenanceGraphStorage(){
		System.out.println("TinkerPop Storage");
	}
		
	@Override
	public void connect() throws Exception {
		// TODO Auto-generated method stub

		// Are we going to have a one time connection?

		// This connection routine should place a node in the graph for the HistoryModel
		// "root" for the runtime session

		GraphTraversalSource g = getGraphTraversalSource();
		historyModelStart = g.addV("Start").
			property("HistoryModelReference", newHistoryModelReference()).
			next();
//		TpProvenanceGraphStorage.closeG(g);
	}

	public synchronized static GraphTraversalSource getGraphTraversalSource() {
		if (g == null) {
			cluster = Cluster.build().addContactPoints(DEFAULT_HOSTNAME).port(DEFAULT_PORT).create();
			client = cluster.connect();
			g = traversal().withRemote(DriverRemoteConnection.using(client, "g"));
		}
		return g;
	}

	// Traversal clean up
	public static void closeG(GraphTraversalSource g) {
		// TODO replace this with a global shutdown function
		
//		try {
//			if (g != null) {
//				g.close();
//			}
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	private String newHistoryModelReference() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		System.out.println(dtf.format(now));
		return ("System_Started@" + dtf.format(now));
	}
	
	@Override
	public TpTimeWindowAdapter newTimeWindow(long commitTime) {

		TpTimeWindow newTpTimeWindow;

		if (currentTpTimeWindow != null) {
			// we have a time window open it will need to reference the new one
			newTpTimeWindow = new TpTimeWindow(commitTime);
			newTpTimeWindow.setPreviousTimeWindow(currentTpTimeWindow);
			
			currentTpTimeWindow.setNextTimeWindow(newTpTimeWindow);
			currentTpTimeWindow.setEndLocalTime(commitTime);
		}
		else {
			// The first time windows should be connected to the HistoryModelStart Vertex
			newTpTimeWindow = new TpTimeWindow(commitTime);
			GraphTraversalSource g = TpProvenanceGraphStorage.getGraphTraversalSource();
			g.V(newTpTimeWindow.getvTimeWindow()).addE("Started").to(historyModelStart).iterate();
			TpProvenanceGraphStorage.closeG(g);
			
		}
		currentTpTimeWindow = newTpTimeWindow;
		
		// TODO Need to work on the snapshot taking and referencing.
		
		return new TpTimeWindowAdapter(currentTpTimeWindow);
	}

	@Override
	public void shutdown() {
		/*
		 * We need to close all three (traversal, client and cluster) so Gremlin will stop
		 * all its client threads and allow the Java program to terminate normally.
		 */

		if (g != null) {
			try {
				g.close();
				client.close();
				cluster.close();

				cluster = null;
				client = null;
				g = null;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
