/**
 */
package ProvDescriptions.impl;

import ProvDescriptions.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ProvDescriptionsFactoryImpl extends EFactoryImpl implements ProvDescriptionsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ProvDescriptionsFactory init() {
		try {
			ProvDescriptionsFactory theProvDescriptionsFactory = (ProvDescriptionsFactory)EPackage.Registry.INSTANCE.getEFactory(ProvDescriptionsPackage.eNS_URI);
			if (theProvDescriptionsFactory != null) {
				return theProvDescriptionsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ProvDescriptionsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvDescriptionsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ProvDescriptionsPackage.ENTITY_DESCRIPTION: return (EObject)createEntityDescription();
			case ProvDescriptionsPackage.ACTIVITY_DESCRIPTION: return (EObject)createActivityDescription();
			case ProvDescriptionsPackage.AGENT_DESCRIPTION: return (EObject)createAgentDescription();
			case ProvDescriptionsPackage.ACTIVITY_STACK_ENTRY: return (EObject)createActivityStackEntry();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntityDescription createEntityDescription() {
		EntityDescriptionImpl entityDescription = new EntityDescriptionImpl();
		return entityDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityDescription createActivityDescription() {
		ActivityDescriptionImpl activityDescription = new ActivityDescriptionImpl();
		return activityDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AgentDescription createAgentDescription() {
		AgentDescriptionImpl agentDescription = new AgentDescriptionImpl();
		return agentDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityStackEntry createActivityStackEntry() {
		ActivityStackEntryImpl activityStackEntry = new ActivityStackEntryImpl();
		return activityStackEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvDescriptionsPackage getProvDescriptionsPackage() {
		return (ProvDescriptionsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ProvDescriptionsPackage getPackage() {
		return ProvDescriptionsPackage.eINSTANCE;
	}

} //ProvDescriptionsFactoryImpl
