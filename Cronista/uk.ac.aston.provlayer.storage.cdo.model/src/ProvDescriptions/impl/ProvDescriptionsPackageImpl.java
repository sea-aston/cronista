/**
 */
package ProvDescriptions.impl;

import ProvDM.ProvDMPackage;

import ProvDM.impl.ProvDMPackageImpl;

import ProvDescriptions.ActivityDescription;
import ProvDescriptions.ActivityStackEntry;
import ProvDescriptions.AgentDescription;
import ProvDescriptions.EntityDescription;
import ProvDescriptions.ProvDescriptionsFactory;
import ProvDescriptions.ProvDescriptionsPackage;

import historyModel.HistoryModelPackage;

import historyModel.impl.HistoryModelPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ProvDescriptionsPackageImpl extends EPackageImpl implements ProvDescriptionsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass entityDescriptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activityDescriptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass agentDescriptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activityStackEntryEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ProvDescriptions.ProvDescriptionsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ProvDescriptionsPackageImpl() {
		super(eNS_URI, ProvDescriptionsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ProvDescriptionsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ProvDescriptionsPackage init() {
		if (isInited) return (ProvDescriptionsPackage)EPackage.Registry.INSTANCE.getEPackage(ProvDescriptionsPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredProvDescriptionsPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ProvDescriptionsPackageImpl theProvDescriptionsPackage = registeredProvDescriptionsPackage instanceof ProvDescriptionsPackageImpl ? (ProvDescriptionsPackageImpl)registeredProvDescriptionsPackage : new ProvDescriptionsPackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HistoryModelPackage.eNS_URI);
		HistoryModelPackageImpl theHistoryModelPackage = (HistoryModelPackageImpl)(registeredPackage instanceof HistoryModelPackageImpl ? registeredPackage : HistoryModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ProvDMPackage.eNS_URI);
		ProvDMPackageImpl theProvDMPackage = (ProvDMPackageImpl)(registeredPackage instanceof ProvDMPackageImpl ? registeredPackage : ProvDMPackage.eINSTANCE);

		// Create package meta-data objects
		theProvDescriptionsPackage.createPackageContents();
		theHistoryModelPackage.createPackageContents();
		theProvDMPackage.createPackageContents();

		// Initialize created meta-data
		theProvDescriptionsPackage.initializePackageContents();
		theHistoryModelPackage.initializePackageContents();
		theProvDMPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theProvDescriptionsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ProvDescriptionsPackage.eNS_URI, theProvDescriptionsPackage);
		return theProvDescriptionsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEntityDescription() {
		return entityDescriptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntityDescription_EntityInstanceID() {
		return (EAttribute)entityDescriptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntityDescription_ObjectIDAsString() {
		return (EAttribute)entityDescriptionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntityDescription_AttributeIDAsString() {
		return (EAttribute)entityDescriptionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntityDescription_MemoryVersionAsString() {
		return (EAttribute)entityDescriptionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntityDescription_StorageVersionAsString() {
		return (EAttribute)entityDescriptionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntityDescription_InModelStorage() {
		return (EAttribute)entityDescriptionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEntityDescription_ObjectStorageReference() {
		return (EReference)entityDescriptionEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEntityDescription_AttributeMetadataStorageReference() {
		return (EReference)entityDescriptionEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEntityDescription_AttributeDataStorageReference() {
		return (EReference)entityDescriptionEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntityDescription_ObjectName() {
		return (EAttribute)entityDescriptionEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntityDescription_ObjectType() {
		return (EAttribute)entityDescriptionEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntityDescription_AttributeNameAsString() {
		return (EAttribute)entityDescriptionEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntityDescription_AttributeTypeAsString() {
		return (EAttribute)entityDescriptionEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntityDescription_AttributeValueAsString() {
		return (EAttribute)entityDescriptionEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntityDescription_StartTime() {
		return (EAttribute)entityDescriptionEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntityDescription_EndTime() {
		return (EAttribute)entityDescriptionEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntityDescription_LastAccessTime() {
		return (EAttribute)entityDescriptionEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntityDescription_LastAccessType() {
		return (EAttribute)entityDescriptionEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntityDescription_MessageID() {
		return (EAttribute)entityDescriptionEClass.getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActivityDescription() {
		return activityDescriptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActivityDescription_ActivityInstanceID() {
		return (EAttribute)activityDescriptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActivityDescription_Name() {
		return (EAttribute)activityDescriptionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActivityDescription_StartTime() {
		return (EAttribute)activityDescriptionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActivityDescription_EndTime() {
		return (EAttribute)activityDescriptionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivityDescription_ActivityStack() {
		return (EReference)activityDescriptionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActivityDescription_MessageID() {
		return (EAttribute)activityDescriptionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAgentDescription() {
		return agentDescriptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAgentDescription_AgentInstanceID() {
		return (EAttribute)agentDescriptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAgentDescription_Name() {
		return (EAttribute)agentDescriptionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAgentDescription_Type() {
		return (EAttribute)agentDescriptionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAgentDescription_Group() {
		return (EAttribute)agentDescriptionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAgentDescription_MessageID() {
		return (EAttribute)agentDescriptionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActivityStackEntry() {
		return activityStackEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActivityStackEntry_ActivityInstanceID() {
		return (EAttribute)activityStackEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActivityStackEntry_Possition() {
		return (EAttribute)activityStackEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvDescriptionsFactory getProvDescriptionsFactory() {
		return (ProvDescriptionsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		entityDescriptionEClass = createEClass(ENTITY_DESCRIPTION);
		createEAttribute(entityDescriptionEClass, ENTITY_DESCRIPTION__ENTITY_INSTANCE_ID);
		createEAttribute(entityDescriptionEClass, ENTITY_DESCRIPTION__OBJECT_ID_AS_STRING);
		createEAttribute(entityDescriptionEClass, ENTITY_DESCRIPTION__ATTRIBUTE_ID_AS_STRING);
		createEAttribute(entityDescriptionEClass, ENTITY_DESCRIPTION__MEMORY_VERSION_AS_STRING);
		createEAttribute(entityDescriptionEClass, ENTITY_DESCRIPTION__STORAGE_VERSION_AS_STRING);
		createEAttribute(entityDescriptionEClass, ENTITY_DESCRIPTION__IN_MODEL_STORAGE);
		createEReference(entityDescriptionEClass, ENTITY_DESCRIPTION__OBJECT_STORAGE_REFERENCE);
		createEReference(entityDescriptionEClass, ENTITY_DESCRIPTION__ATTRIBUTE_METADATA_STORAGE_REFERENCE);
		createEReference(entityDescriptionEClass, ENTITY_DESCRIPTION__ATTRIBUTE_DATA_STORAGE_REFERENCE);
		createEAttribute(entityDescriptionEClass, ENTITY_DESCRIPTION__OBJECT_NAME);
		createEAttribute(entityDescriptionEClass, ENTITY_DESCRIPTION__OBJECT_TYPE);
		createEAttribute(entityDescriptionEClass, ENTITY_DESCRIPTION__ATTRIBUTE_NAME_AS_STRING);
		createEAttribute(entityDescriptionEClass, ENTITY_DESCRIPTION__ATTRIBUTE_TYPE_AS_STRING);
		createEAttribute(entityDescriptionEClass, ENTITY_DESCRIPTION__ATTRIBUTE_VALUE_AS_STRING);
		createEAttribute(entityDescriptionEClass, ENTITY_DESCRIPTION__START_TIME);
		createEAttribute(entityDescriptionEClass, ENTITY_DESCRIPTION__END_TIME);
		createEAttribute(entityDescriptionEClass, ENTITY_DESCRIPTION__LAST_ACCESS_TIME);
		createEAttribute(entityDescriptionEClass, ENTITY_DESCRIPTION__LAST_ACCESS_TYPE);
		createEAttribute(entityDescriptionEClass, ENTITY_DESCRIPTION__MESSAGE_ID);

		activityDescriptionEClass = createEClass(ACTIVITY_DESCRIPTION);
		createEAttribute(activityDescriptionEClass, ACTIVITY_DESCRIPTION__ACTIVITY_INSTANCE_ID);
		createEAttribute(activityDescriptionEClass, ACTIVITY_DESCRIPTION__NAME);
		createEAttribute(activityDescriptionEClass, ACTIVITY_DESCRIPTION__START_TIME);
		createEAttribute(activityDescriptionEClass, ACTIVITY_DESCRIPTION__END_TIME);
		createEReference(activityDescriptionEClass, ACTIVITY_DESCRIPTION__ACTIVITY_STACK);
		createEAttribute(activityDescriptionEClass, ACTIVITY_DESCRIPTION__MESSAGE_ID);

		agentDescriptionEClass = createEClass(AGENT_DESCRIPTION);
		createEAttribute(agentDescriptionEClass, AGENT_DESCRIPTION__AGENT_INSTANCE_ID);
		createEAttribute(agentDescriptionEClass, AGENT_DESCRIPTION__NAME);
		createEAttribute(agentDescriptionEClass, AGENT_DESCRIPTION__TYPE);
		createEAttribute(agentDescriptionEClass, AGENT_DESCRIPTION__GROUP);
		createEAttribute(agentDescriptionEClass, AGENT_DESCRIPTION__MESSAGE_ID);

		activityStackEntryEClass = createEClass(ACTIVITY_STACK_ENTRY);
		createEAttribute(activityStackEntryEClass, ACTIVITY_STACK_ENTRY__ACTIVITY_INSTANCE_ID);
		createEAttribute(activityStackEntryEClass, ACTIVITY_STACK_ENTRY__POSSITION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(entityDescriptionEClass, EntityDescription.class, "EntityDescription", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEntityDescription_EntityInstanceID(), ecorePackage.getEString(), "EntityInstanceID", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEntityDescription_ObjectIDAsString(), ecorePackage.getEString(), "objectIDAsString", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEntityDescription_AttributeIDAsString(), ecorePackage.getEString(), "attributeIDAsString", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEntityDescription_MemoryVersionAsString(), ecorePackage.getEString(), "memoryVersionAsString", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEntityDescription_StorageVersionAsString(), ecorePackage.getEString(), "storageVersionAsString", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEntityDescription_InModelStorage(), ecorePackage.getEBoolean(), "inModelStorage", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEntityDescription_ObjectStorageReference(), ecorePackage.getEObject(), null, "objectStorageReference", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEntityDescription_AttributeMetadataStorageReference(), ecorePackage.getEObject(), null, "attributeMetadataStorageReference", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEntityDescription_AttributeDataStorageReference(), ecorePackage.getEObject(), null, "attributeDataStorageReference", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEntityDescription_ObjectName(), ecorePackage.getEString(), "objectName", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getEntityDescription_ObjectType(), ecorePackage.getEString(), "objectType", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEntityDescription_AttributeNameAsString(), ecorePackage.getEString(), "AttributeNameAsString", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEntityDescription_AttributeTypeAsString(), ecorePackage.getEString(), "AttributeTypeAsString", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEntityDescription_AttributeValueAsString(), ecorePackage.getEString(), "AttributeValueAsString", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEntityDescription_StartTime(), ecorePackage.getELong(), "StartTime", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEntityDescription_EndTime(), ecorePackage.getELong(), "EndTime", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEntityDescription_LastAccessTime(), ecorePackage.getELong(), "LastAccessTime", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEntityDescription_LastAccessType(), ecorePackage.getEChar(), "LastAccessType", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEntityDescription_MessageID(), ecorePackage.getEString(), "MessageID", null, 0, 1, EntityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activityDescriptionEClass, ActivityDescription.class, "ActivityDescription", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getActivityDescription_ActivityInstanceID(), ecorePackage.getEString(), "ActivityInstanceID", null, 0, 1, ActivityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActivityDescription_Name(), ecorePackage.getEString(), "Name", null, 0, 1, ActivityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getActivityDescription_StartTime(), ecorePackage.getELong(), "StartTime", null, 0, 1, ActivityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActivityDescription_EndTime(), ecorePackage.getELong(), "EndTime", null, 0, 1, ActivityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActivityDescription_ActivityStack(), this.getActivityStackEntry(), null, "ActivityStack", null, 0, -1, ActivityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActivityDescription_MessageID(), ecorePackage.getEString(), "MessageID", null, 0, 1, ActivityDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(agentDescriptionEClass, AgentDescription.class, "AgentDescription", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAgentDescription_AgentInstanceID(), ecorePackage.getEString(), "AgentInstanceID", null, 0, 1, AgentDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAgentDescription_Name(), ecorePackage.getEString(), "Name", null, 0, 1, AgentDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAgentDescription_Type(), ecorePackage.getEString(), "Type", null, 0, 1, AgentDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAgentDescription_Group(), ecorePackage.getEString(), "Group", null, 0, 1, AgentDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAgentDescription_MessageID(), ecorePackage.getEString(), "MessageID", null, 0, 1, AgentDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(activityStackEntryEClass, ActivityStackEntry.class, "ActivityStackEntry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getActivityStackEntry_ActivityInstanceID(), ecorePackage.getEString(), "ActivityInstanceID", null, 0, 1, ActivityStackEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActivityStackEntry_Possition(), ecorePackage.getEString(), "Possition", null, 0, 1, ActivityStackEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //ProvDescriptionsPackageImpl
