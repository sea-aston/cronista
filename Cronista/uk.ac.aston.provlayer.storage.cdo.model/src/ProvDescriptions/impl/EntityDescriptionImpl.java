/**
 */
package ProvDescriptions.impl;

import ProvDescriptions.EntityDescription;
import ProvDescriptions.ProvDescriptionsPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.internal.cdo.CDOObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Entity Description</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#getEntityInstanceID <em>Entity Instance ID</em>}</li>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#getObjectIDAsString <em>Object ID As String</em>}</li>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#getAttributeIDAsString <em>Attribute ID As String</em>}</li>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#getMemoryVersionAsString <em>Memory Version As String</em>}</li>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#getStorageVersionAsString <em>Storage Version As String</em>}</li>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#isInModelStorage <em>In Model Storage</em>}</li>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#getObjectStorageReference <em>Object Storage Reference</em>}</li>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#getAttributeMetadataStorageReference <em>Attribute Metadata Storage Reference</em>}</li>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#getAttributeDataStorageReference <em>Attribute Data Storage Reference</em>}</li>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#getObjectName <em>Object Name</em>}</li>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#getObjectType <em>Object Type</em>}</li>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#getAttributeNameAsString <em>Attribute Name As String</em>}</li>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#getAttributeTypeAsString <em>Attribute Type As String</em>}</li>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#getAttributeValueAsString <em>Attribute Value As String</em>}</li>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#getLastAccessTime <em>Last Access Time</em>}</li>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#getLastAccessType <em>Last Access Type</em>}</li>
 *   <li>{@link ProvDescriptions.impl.EntityDescriptionImpl#getMessageID <em>Message ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EntityDescriptionImpl extends CDOObjectImpl implements EntityDescription {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EntityDescriptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEntityInstanceID() {
		return (String)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__ENTITY_INSTANCE_ID, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntityInstanceID(String newEntityInstanceID) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__ENTITY_INSTANCE_ID, newEntityInstanceID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getObjectIDAsString() {
		return (String)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__OBJECT_ID_AS_STRING, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectIDAsString(String newObjectIDAsString) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__OBJECT_ID_AS_STRING, newObjectIDAsString);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAttributeIDAsString() {
		return (String)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__ATTRIBUTE_ID_AS_STRING, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributeIDAsString(String newAttributeIDAsString) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__ATTRIBUTE_ID_AS_STRING, newAttributeIDAsString);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMemoryVersionAsString() {
		return (String)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__MEMORY_VERSION_AS_STRING, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMemoryVersionAsString(String newMemoryVersionAsString) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__MEMORY_VERSION_AS_STRING, newMemoryVersionAsString);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStorageVersionAsString() {
		return (String)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__STORAGE_VERSION_AS_STRING, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStorageVersionAsString(String newStorageVersionAsString) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__STORAGE_VERSION_AS_STRING, newStorageVersionAsString);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isInModelStorage() {
		return (Boolean)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__IN_MODEL_STORAGE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInModelStorage(boolean newInModelStorage) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__IN_MODEL_STORAGE, newInModelStorage);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getObjectStorageReference() {
		return (EObject)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__OBJECT_STORAGE_REFERENCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectStorageReference(EObject newObjectStorageReference) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__OBJECT_STORAGE_REFERENCE, newObjectStorageReference);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getAttributeMetadataStorageReference() {
		return (EObject)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__ATTRIBUTE_METADATA_STORAGE_REFERENCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributeMetadataStorageReference(EObject newAttributeMetadataStorageReference) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__ATTRIBUTE_METADATA_STORAGE_REFERENCE, newAttributeMetadataStorageReference);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getAttributeDataStorageReference() {
		return (EObject)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__ATTRIBUTE_DATA_STORAGE_REFERENCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributeDataStorageReference(EObject newAttributeDataStorageReference) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__ATTRIBUTE_DATA_STORAGE_REFERENCE, newAttributeDataStorageReference);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getObjectName() {
		return (String)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__OBJECT_NAME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectName(String newObjectName) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__OBJECT_NAME, newObjectName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getObjectType() {
		return (String)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__OBJECT_TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectType(String newObjectType) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__OBJECT_TYPE, newObjectType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAttributeNameAsString() {
		return (String)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__ATTRIBUTE_NAME_AS_STRING, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributeNameAsString(String newAttributeNameAsString) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__ATTRIBUTE_NAME_AS_STRING, newAttributeNameAsString);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAttributeTypeAsString() {
		return (String)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__ATTRIBUTE_TYPE_AS_STRING, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributeTypeAsString(String newAttributeTypeAsString) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__ATTRIBUTE_TYPE_AS_STRING, newAttributeTypeAsString);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAttributeValueAsString() {
		return (String)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__ATTRIBUTE_VALUE_AS_STRING, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributeValueAsString(String newAttributeValueAsString) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__ATTRIBUTE_VALUE_AS_STRING, newAttributeValueAsString);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getStartTime() {
		return (Long)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__START_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(long newStartTime) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__START_TIME, newStartTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getEndTime() {
		return (Long)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__END_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(long newEndTime) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__END_TIME, newEndTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getLastAccessTime() {
		return (Long)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__LAST_ACCESS_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastAccessTime(long newLastAccessTime) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__LAST_ACCESS_TIME, newLastAccessTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public char getLastAccessType() {
		return (Character)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__LAST_ACCESS_TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastAccessType(char newLastAccessType) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__LAST_ACCESS_TYPE, newLastAccessType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMessageID() {
		return (String)eGet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__MESSAGE_ID, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMessageID(String newMessageID) {
		eSet(ProvDescriptionsPackage.Literals.ENTITY_DESCRIPTION__MESSAGE_ID, newMessageID);
	}

} //EntityDescriptionImpl
