/**
 */
package ProvDescriptions.impl;

import ProvDescriptions.ActivityDescription;
import ProvDescriptions.ActivityStackEntry;
import ProvDescriptions.ProvDescriptionsPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.internal.cdo.CDOObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Activity Description</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ProvDescriptions.impl.ActivityDescriptionImpl#getActivityInstanceID <em>Activity Instance ID</em>}</li>
 *   <li>{@link ProvDescriptions.impl.ActivityDescriptionImpl#getName <em>Name</em>}</li>
 *   <li>{@link ProvDescriptions.impl.ActivityDescriptionImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link ProvDescriptions.impl.ActivityDescriptionImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link ProvDescriptions.impl.ActivityDescriptionImpl#getActivityStack <em>Activity Stack</em>}</li>
 *   <li>{@link ProvDescriptions.impl.ActivityDescriptionImpl#getMessageID <em>Message ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActivityDescriptionImpl extends CDOObjectImpl implements ActivityDescription {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActivityDescriptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProvDescriptionsPackage.Literals.ACTIVITY_DESCRIPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getActivityInstanceID() {
		return (String)eGet(ProvDescriptionsPackage.Literals.ACTIVITY_DESCRIPTION__ACTIVITY_INSTANCE_ID, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActivityInstanceID(String newActivityInstanceID) {
		eSet(ProvDescriptionsPackage.Literals.ACTIVITY_DESCRIPTION__ACTIVITY_INSTANCE_ID, newActivityInstanceID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return (String)eGet(ProvDescriptionsPackage.Literals.ACTIVITY_DESCRIPTION__NAME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		eSet(ProvDescriptionsPackage.Literals.ACTIVITY_DESCRIPTION__NAME, newName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getStartTime() {
		return (Long)eGet(ProvDescriptionsPackage.Literals.ACTIVITY_DESCRIPTION__START_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(long newStartTime) {
		eSet(ProvDescriptionsPackage.Literals.ACTIVITY_DESCRIPTION__START_TIME, newStartTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getEndTime() {
		return (Long)eGet(ProvDescriptionsPackage.Literals.ACTIVITY_DESCRIPTION__END_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(long newEndTime) {
		eSet(ProvDescriptionsPackage.Literals.ACTIVITY_DESCRIPTION__END_TIME, newEndTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ActivityStackEntry> getActivityStack() {
		return (EList<ActivityStackEntry>)eGet(ProvDescriptionsPackage.Literals.ACTIVITY_DESCRIPTION__ACTIVITY_STACK, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMessageID() {
		return (String)eGet(ProvDescriptionsPackage.Literals.ACTIVITY_DESCRIPTION__MESSAGE_ID, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMessageID(String newMessageID) {
		eSet(ProvDescriptionsPackage.Literals.ACTIVITY_DESCRIPTION__MESSAGE_ID, newMessageID);
	}

} //ActivityDescriptionImpl
