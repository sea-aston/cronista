/**
 */
package ProvDescriptions.impl;

import ProvDescriptions.ActivityStackEntry;
import ProvDescriptions.ProvDescriptionsPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.internal.cdo.CDOObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Activity Stack Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ProvDescriptions.impl.ActivityStackEntryImpl#getActivityInstanceID <em>Activity Instance ID</em>}</li>
 *   <li>{@link ProvDescriptions.impl.ActivityStackEntryImpl#getPossition <em>Possition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActivityStackEntryImpl extends CDOObjectImpl implements ActivityStackEntry {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActivityStackEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProvDescriptionsPackage.Literals.ACTIVITY_STACK_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getActivityInstanceID() {
		return (String)eGet(ProvDescriptionsPackage.Literals.ACTIVITY_STACK_ENTRY__ACTIVITY_INSTANCE_ID, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActivityInstanceID(String newActivityInstanceID) {
		eSet(ProvDescriptionsPackage.Literals.ACTIVITY_STACK_ENTRY__ACTIVITY_INSTANCE_ID, newActivityInstanceID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPossition() {
		return (String)eGet(ProvDescriptionsPackage.Literals.ACTIVITY_STACK_ENTRY__POSSITION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPossition(String newPossition) {
		eSet(ProvDescriptionsPackage.Literals.ACTIVITY_STACK_ENTRY__POSSITION, newPossition);
	}

} //ActivityStackEntryImpl
