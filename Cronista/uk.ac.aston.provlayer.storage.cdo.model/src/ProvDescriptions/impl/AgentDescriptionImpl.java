/**
 */
package ProvDescriptions.impl;

import ProvDescriptions.AgentDescription;
import ProvDescriptions.ProvDescriptionsPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.internal.cdo.CDOObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Agent Description</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ProvDescriptions.impl.AgentDescriptionImpl#getAgentInstanceID <em>Agent Instance ID</em>}</li>
 *   <li>{@link ProvDescriptions.impl.AgentDescriptionImpl#getName <em>Name</em>}</li>
 *   <li>{@link ProvDescriptions.impl.AgentDescriptionImpl#getType <em>Type</em>}</li>
 *   <li>{@link ProvDescriptions.impl.AgentDescriptionImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link ProvDescriptions.impl.AgentDescriptionImpl#getMessageID <em>Message ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AgentDescriptionImpl extends CDOObjectImpl implements AgentDescription {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AgentDescriptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProvDescriptionsPackage.Literals.AGENT_DESCRIPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAgentInstanceID() {
		return (String)eGet(ProvDescriptionsPackage.Literals.AGENT_DESCRIPTION__AGENT_INSTANCE_ID, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAgentInstanceID(String newAgentInstanceID) {
		eSet(ProvDescriptionsPackage.Literals.AGENT_DESCRIPTION__AGENT_INSTANCE_ID, newAgentInstanceID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return (String)eGet(ProvDescriptionsPackage.Literals.AGENT_DESCRIPTION__NAME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		eSet(ProvDescriptionsPackage.Literals.AGENT_DESCRIPTION__NAME, newName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return (String)eGet(ProvDescriptionsPackage.Literals.AGENT_DESCRIPTION__TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		eSet(ProvDescriptionsPackage.Literals.AGENT_DESCRIPTION__TYPE, newType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getGroup() {
		return (String)eGet(ProvDescriptionsPackage.Literals.AGENT_DESCRIPTION__GROUP, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGroup(String newGroup) {
		eSet(ProvDescriptionsPackage.Literals.AGENT_DESCRIPTION__GROUP, newGroup);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMessageID() {
		return (String)eGet(ProvDescriptionsPackage.Literals.AGENT_DESCRIPTION__MESSAGE_ID, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMessageID(String newMessageID) {
		eSet(ProvDescriptionsPackage.Literals.AGENT_DESCRIPTION__MESSAGE_ID, newMessageID);
	}

} //AgentDescriptionImpl
