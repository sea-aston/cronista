/**
 */
package ProvDescriptions;

import org.eclipse.emf.cdo.CDOObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agent Description</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ProvDescriptions.AgentDescription#getAgentInstanceID <em>Agent Instance ID</em>}</li>
 *   <li>{@link ProvDescriptions.AgentDescription#getName <em>Name</em>}</li>
 *   <li>{@link ProvDescriptions.AgentDescription#getType <em>Type</em>}</li>
 *   <li>{@link ProvDescriptions.AgentDescription#getGroup <em>Group</em>}</li>
 *   <li>{@link ProvDescriptions.AgentDescription#getMessageID <em>Message ID</em>}</li>
 * </ul>
 *
 * @see ProvDescriptions.ProvDescriptionsPackage#getAgentDescription()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface AgentDescription extends CDOObject {
	/**
	 * Returns the value of the '<em><b>Agent Instance ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agent Instance ID</em>' attribute.
	 * @see #setAgentInstanceID(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getAgentDescription_AgentInstanceID()
	 * @model id="true"
	 * @generated
	 */
	String getAgentInstanceID();

	/**
	 * Sets the value of the '{@link ProvDescriptions.AgentDescription#getAgentInstanceID <em>Agent Instance ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Agent Instance ID</em>' attribute.
	 * @see #getAgentInstanceID()
	 * @generated
	 */
	void setAgentInstanceID(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getAgentDescription_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link ProvDescriptions.AgentDescription#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getAgentDescription_Type()
	 * @model
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link ProvDescriptions.AgentDescription#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute.
	 * @see #setGroup(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getAgentDescription_Group()
	 * @model
	 * @generated
	 */
	String getGroup();

	/**
	 * Sets the value of the '{@link ProvDescriptions.AgentDescription#getGroup <em>Group</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group</em>' attribute.
	 * @see #getGroup()
	 * @generated
	 */
	void setGroup(String value);

	/**
	 * Returns the value of the '<em><b>Message ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message ID</em>' attribute.
	 * @see #setMessageID(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getAgentDescription_MessageID()
	 * @model
	 * @generated
	 */
	String getMessageID();

	/**
	 * Sets the value of the '{@link ProvDescriptions.AgentDescription#getMessageID <em>Message ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message ID</em>' attribute.
	 * @see #getMessageID()
	 * @generated
	 */
	void setMessageID(String value);

} // AgentDescription
