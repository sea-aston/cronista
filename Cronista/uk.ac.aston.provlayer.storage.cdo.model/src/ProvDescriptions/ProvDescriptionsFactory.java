/**
 */
package ProvDescriptions;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ProvDescriptions.ProvDescriptionsPackage
 * @generated
 */
public interface ProvDescriptionsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ProvDescriptionsFactory eINSTANCE = ProvDescriptions.impl.ProvDescriptionsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Entity Description</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Entity Description</em>'.
	 * @generated
	 */
	EntityDescription createEntityDescription();

	/**
	 * Returns a new object of class '<em>Activity Description</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Activity Description</em>'.
	 * @generated
	 */
	ActivityDescription createActivityDescription();

	/**
	 * Returns a new object of class '<em>Agent Description</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Agent Description</em>'.
	 * @generated
	 */
	AgentDescription createAgentDescription();

	/**
	 * Returns a new object of class '<em>Activity Stack Entry</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Activity Stack Entry</em>'.
	 * @generated
	 */
	ActivityStackEntry createActivityStackEntry();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ProvDescriptionsPackage getProvDescriptionsPackage();

} //ProvDescriptionsFactory
