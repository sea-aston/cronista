/**
 */
package ProvDescriptions;

import org.eclipse.emf.cdo.CDOObject;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entity Description</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ProvDescriptions.EntityDescription#getEntityInstanceID <em>Entity Instance ID</em>}</li>
 *   <li>{@link ProvDescriptions.EntityDescription#getObjectIDAsString <em>Object ID As String</em>}</li>
 *   <li>{@link ProvDescriptions.EntityDescription#getAttributeIDAsString <em>Attribute ID As String</em>}</li>
 *   <li>{@link ProvDescriptions.EntityDescription#getMemoryVersionAsString <em>Memory Version As String</em>}</li>
 *   <li>{@link ProvDescriptions.EntityDescription#getStorageVersionAsString <em>Storage Version As String</em>}</li>
 *   <li>{@link ProvDescriptions.EntityDescription#isInModelStorage <em>In Model Storage</em>}</li>
 *   <li>{@link ProvDescriptions.EntityDescription#getObjectStorageReference <em>Object Storage Reference</em>}</li>
 *   <li>{@link ProvDescriptions.EntityDescription#getAttributeMetadataStorageReference <em>Attribute Metadata Storage Reference</em>}</li>
 *   <li>{@link ProvDescriptions.EntityDescription#getAttributeDataStorageReference <em>Attribute Data Storage Reference</em>}</li>
 *   <li>{@link ProvDescriptions.EntityDescription#getObjectName <em>Object Name</em>}</li>
 *   <li>{@link ProvDescriptions.EntityDescription#getObjectType <em>Object Type</em>}</li>
 *   <li>{@link ProvDescriptions.EntityDescription#getAttributeNameAsString <em>Attribute Name As String</em>}</li>
 *   <li>{@link ProvDescriptions.EntityDescription#getAttributeTypeAsString <em>Attribute Type As String</em>}</li>
 *   <li>{@link ProvDescriptions.EntityDescription#getAttributeValueAsString <em>Attribute Value As String</em>}</li>
 *   <li>{@link ProvDescriptions.EntityDescription#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link ProvDescriptions.EntityDescription#getEndTime <em>End Time</em>}</li>
 *   <li>{@link ProvDescriptions.EntityDescription#getLastAccessTime <em>Last Access Time</em>}</li>
 *   <li>{@link ProvDescriptions.EntityDescription#getLastAccessType <em>Last Access Type</em>}</li>
 *   <li>{@link ProvDescriptions.EntityDescription#getMessageID <em>Message ID</em>}</li>
 * </ul>
 *
 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface EntityDescription extends CDOObject {
	/**
	 * Returns the value of the '<em><b>Entity Instance ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity Instance ID</em>' attribute.
	 * @see #setEntityInstanceID(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_EntityInstanceID()
	 * @model id="true"
	 * @generated
	 */
	String getEntityInstanceID();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#getEntityInstanceID <em>Entity Instance ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entity Instance ID</em>' attribute.
	 * @see #getEntityInstanceID()
	 * @generated
	 */
	void setEntityInstanceID(String value);

	/**
	 * Returns the value of the '<em><b>Object ID As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object ID As String</em>' attribute.
	 * @see #setObjectIDAsString(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_ObjectIDAsString()
	 * @model
	 * @generated
	 */
	String getObjectIDAsString();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#getObjectIDAsString <em>Object ID As String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object ID As String</em>' attribute.
	 * @see #getObjectIDAsString()
	 * @generated
	 */
	void setObjectIDAsString(String value);

	/**
	 * Returns the value of the '<em><b>Attribute ID As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute ID As String</em>' attribute.
	 * @see #setAttributeIDAsString(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_AttributeIDAsString()
	 * @model
	 * @generated
	 */
	String getAttributeIDAsString();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#getAttributeIDAsString <em>Attribute ID As String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute ID As String</em>' attribute.
	 * @see #getAttributeIDAsString()
	 * @generated
	 */
	void setAttributeIDAsString(String value);

	/**
	 * Returns the value of the '<em><b>Memory Version As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Memory Version As String</em>' attribute.
	 * @see #setMemoryVersionAsString(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_MemoryVersionAsString()
	 * @model
	 * @generated
	 */
	String getMemoryVersionAsString();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#getMemoryVersionAsString <em>Memory Version As String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Memory Version As String</em>' attribute.
	 * @see #getMemoryVersionAsString()
	 * @generated
	 */
	void setMemoryVersionAsString(String value);

	/**
	 * Returns the value of the '<em><b>Storage Version As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Version As String</em>' attribute.
	 * @see #setStorageVersionAsString(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_StorageVersionAsString()
	 * @model
	 * @generated
	 */
	String getStorageVersionAsString();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#getStorageVersionAsString <em>Storage Version As String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Storage Version As String</em>' attribute.
	 * @see #getStorageVersionAsString()
	 * @generated
	 */
	void setStorageVersionAsString(String value);

	/**
	 * Returns the value of the '<em><b>In Model Storage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Model Storage</em>' attribute.
	 * @see #setInModelStorage(boolean)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_InModelStorage()
	 * @model
	 * @generated
	 */
	boolean isInModelStorage();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#isInModelStorage <em>In Model Storage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>In Model Storage</em>' attribute.
	 * @see #isInModelStorage()
	 * @generated
	 */
	void setInModelStorage(boolean value);

	/**
	 * Returns the value of the '<em><b>Object Storage Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Storage Reference</em>' reference.
	 * @see #setObjectStorageReference(EObject)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_ObjectStorageReference()
	 * @model
	 * @generated
	 */
	EObject getObjectStorageReference();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#getObjectStorageReference <em>Object Storage Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Storage Reference</em>' reference.
	 * @see #getObjectStorageReference()
	 * @generated
	 */
	void setObjectStorageReference(EObject value);

	/**
	 * Returns the value of the '<em><b>Attribute Metadata Storage Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Metadata Storage Reference</em>' reference.
	 * @see #setAttributeMetadataStorageReference(EObject)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_AttributeMetadataStorageReference()
	 * @model
	 * @generated
	 */
	EObject getAttributeMetadataStorageReference();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#getAttributeMetadataStorageReference <em>Attribute Metadata Storage Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Metadata Storage Reference</em>' reference.
	 * @see #getAttributeMetadataStorageReference()
	 * @generated
	 */
	void setAttributeMetadataStorageReference(EObject value);

	/**
	 * Returns the value of the '<em><b>Attribute Data Storage Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Data Storage Reference</em>' reference.
	 * @see #setAttributeDataStorageReference(EObject)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_AttributeDataStorageReference()
	 * @model
	 * @generated
	 */
	EObject getAttributeDataStorageReference();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#getAttributeDataStorageReference <em>Attribute Data Storage Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Data Storage Reference</em>' reference.
	 * @see #getAttributeDataStorageReference()
	 * @generated
	 */
	void setAttributeDataStorageReference(EObject value);

	/**
	 * Returns the value of the '<em><b>Object Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Name</em>' attribute.
	 * @see #setObjectName(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_ObjectName()
	 * @model ordered="false"
	 * @generated
	 */
	String getObjectName();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#getObjectName <em>Object Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Name</em>' attribute.
	 * @see #getObjectName()
	 * @generated
	 */
	void setObjectName(String value);

	/**
	 * Returns the value of the '<em><b>Object Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Type</em>' attribute.
	 * @see #setObjectType(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_ObjectType()
	 * @model
	 * @generated
	 */
	String getObjectType();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#getObjectType <em>Object Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Type</em>' attribute.
	 * @see #getObjectType()
	 * @generated
	 */
	void setObjectType(String value);

	/**
	 * Returns the value of the '<em><b>Attribute Name As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Name As String</em>' attribute.
	 * @see #setAttributeNameAsString(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_AttributeNameAsString()
	 * @model
	 * @generated
	 */
	String getAttributeNameAsString();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#getAttributeNameAsString <em>Attribute Name As String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Name As String</em>' attribute.
	 * @see #getAttributeNameAsString()
	 * @generated
	 */
	void setAttributeNameAsString(String value);

	/**
	 * Returns the value of the '<em><b>Attribute Type As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Type As String</em>' attribute.
	 * @see #setAttributeTypeAsString(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_AttributeTypeAsString()
	 * @model
	 * @generated
	 */
	String getAttributeTypeAsString();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#getAttributeTypeAsString <em>Attribute Type As String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Type As String</em>' attribute.
	 * @see #getAttributeTypeAsString()
	 * @generated
	 */
	void setAttributeTypeAsString(String value);

	/**
	 * Returns the value of the '<em><b>Attribute Value As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Value As String</em>' attribute.
	 * @see #setAttributeValueAsString(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_AttributeValueAsString()
	 * @model
	 * @generated
	 */
	String getAttributeValueAsString();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#getAttributeValueAsString <em>Attribute Value As String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Value As String</em>' attribute.
	 * @see #getAttributeValueAsString()
	 * @generated
	 */
	void setAttributeValueAsString(String value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(long)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_StartTime()
	 * @model
	 * @generated
	 */
	long getStartTime();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(long value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(long)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_EndTime()
	 * @model
	 * @generated
	 */
	long getEndTime();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(long value);

	/**
	 * Returns the value of the '<em><b>Last Access Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Access Time</em>' attribute.
	 * @see #setLastAccessTime(long)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_LastAccessTime()
	 * @model
	 * @generated
	 */
	long getLastAccessTime();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#getLastAccessTime <em>Last Access Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Access Time</em>' attribute.
	 * @see #getLastAccessTime()
	 * @generated
	 */
	void setLastAccessTime(long value);

	/**
	 * Returns the value of the '<em><b>Last Access Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Access Type</em>' attribute.
	 * @see #setLastAccessType(char)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_LastAccessType()
	 * @model
	 * @generated
	 */
	char getLastAccessType();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#getLastAccessType <em>Last Access Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Access Type</em>' attribute.
	 * @see #getLastAccessType()
	 * @generated
	 */
	void setLastAccessType(char value);

	/**
	 * Returns the value of the '<em><b>Message ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message ID</em>' attribute.
	 * @see #setMessageID(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getEntityDescription_MessageID()
	 * @model
	 * @generated
	 */
	String getMessageID();

	/**
	 * Sets the value of the '{@link ProvDescriptions.EntityDescription#getMessageID <em>Message ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message ID</em>' attribute.
	 * @see #getMessageID()
	 * @generated
	 */
	void setMessageID(String value);

} // EntityDescription
