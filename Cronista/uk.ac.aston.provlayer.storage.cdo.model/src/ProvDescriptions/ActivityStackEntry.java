/**
 */
package ProvDescriptions;

import org.eclipse.emf.cdo.CDOObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activity Stack Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ProvDescriptions.ActivityStackEntry#getActivityInstanceID <em>Activity Instance ID</em>}</li>
 *   <li>{@link ProvDescriptions.ActivityStackEntry#getPossition <em>Possition</em>}</li>
 * </ul>
 *
 * @see ProvDescriptions.ProvDescriptionsPackage#getActivityStackEntry()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface ActivityStackEntry extends CDOObject {
	/**
	 * Returns the value of the '<em><b>Activity Instance ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activity Instance ID</em>' attribute.
	 * @see #setActivityInstanceID(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getActivityStackEntry_ActivityInstanceID()
	 * @model
	 * @generated
	 */
	String getActivityInstanceID();

	/**
	 * Sets the value of the '{@link ProvDescriptions.ActivityStackEntry#getActivityInstanceID <em>Activity Instance ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Activity Instance ID</em>' attribute.
	 * @see #getActivityInstanceID()
	 * @generated
	 */
	void setActivityInstanceID(String value);

	/**
	 * Returns the value of the '<em><b>Possition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Possition</em>' attribute.
	 * @see #setPossition(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getActivityStackEntry_Possition()
	 * @model
	 * @generated
	 */
	String getPossition();

	/**
	 * Sets the value of the '{@link ProvDescriptions.ActivityStackEntry#getPossition <em>Possition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Possition</em>' attribute.
	 * @see #getPossition()
	 * @generated
	 */
	void setPossition(String value);

} // ActivityStackEntry
