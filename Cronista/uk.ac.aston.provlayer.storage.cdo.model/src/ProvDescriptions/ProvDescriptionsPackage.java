/**
 */
package ProvDescriptions;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ProvDescriptions.ProvDescriptionsFactory
 * @model kind="package"
 * @generated
 */
public interface ProvDescriptionsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ProvDescriptions";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/ProvDescriptions";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ProvDescriptions";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ProvDescriptionsPackage eINSTANCE = ProvDescriptions.impl.ProvDescriptionsPackageImpl.init();

	/**
	 * The meta object id for the '{@link ProvDescriptions.impl.EntityDescriptionImpl <em>Entity Description</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ProvDescriptions.impl.EntityDescriptionImpl
	 * @see ProvDescriptions.impl.ProvDescriptionsPackageImpl#getEntityDescription()
	 * @generated
	 */
	int ENTITY_DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Entity Instance ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__ENTITY_INSTANCE_ID = 0;

	/**
	 * The feature id for the '<em><b>Object ID As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__OBJECT_ID_AS_STRING = 1;

	/**
	 * The feature id for the '<em><b>Attribute ID As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__ATTRIBUTE_ID_AS_STRING = 2;

	/**
	 * The feature id for the '<em><b>Memory Version As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__MEMORY_VERSION_AS_STRING = 3;

	/**
	 * The feature id for the '<em><b>Storage Version As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__STORAGE_VERSION_AS_STRING = 4;

	/**
	 * The feature id for the '<em><b>In Model Storage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__IN_MODEL_STORAGE = 5;

	/**
	 * The feature id for the '<em><b>Object Storage Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__OBJECT_STORAGE_REFERENCE = 6;

	/**
	 * The feature id for the '<em><b>Attribute Metadata Storage Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__ATTRIBUTE_METADATA_STORAGE_REFERENCE = 7;

	/**
	 * The feature id for the '<em><b>Attribute Data Storage Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__ATTRIBUTE_DATA_STORAGE_REFERENCE = 8;

	/**
	 * The feature id for the '<em><b>Object Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__OBJECT_NAME = 9;

	/**
	 * The feature id for the '<em><b>Object Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__OBJECT_TYPE = 10;

	/**
	 * The feature id for the '<em><b>Attribute Name As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__ATTRIBUTE_NAME_AS_STRING = 11;

	/**
	 * The feature id for the '<em><b>Attribute Type As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__ATTRIBUTE_TYPE_AS_STRING = 12;

	/**
	 * The feature id for the '<em><b>Attribute Value As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__ATTRIBUTE_VALUE_AS_STRING = 13;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__START_TIME = 14;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__END_TIME = 15;

	/**
	 * The feature id for the '<em><b>Last Access Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__LAST_ACCESS_TIME = 16;

	/**
	 * The feature id for the '<em><b>Last Access Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__LAST_ACCESS_TYPE = 17;

	/**
	 * The feature id for the '<em><b>Message ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION__MESSAGE_ID = 18;

	/**
	 * The number of structural features of the '<em>Entity Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION_FEATURE_COUNT = 19;

	/**
	 * The number of operations of the '<em>Entity Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DESCRIPTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ProvDescriptions.impl.ActivityDescriptionImpl <em>Activity Description</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ProvDescriptions.impl.ActivityDescriptionImpl
	 * @see ProvDescriptions.impl.ProvDescriptionsPackageImpl#getActivityDescription()
	 * @generated
	 */
	int ACTIVITY_DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Activity Instance ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DESCRIPTION__ACTIVITY_INSTANCE_ID = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DESCRIPTION__NAME = 1;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DESCRIPTION__START_TIME = 2;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DESCRIPTION__END_TIME = 3;

	/**
	 * The feature id for the '<em><b>Activity Stack</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DESCRIPTION__ACTIVITY_STACK = 4;

	/**
	 * The feature id for the '<em><b>Message ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DESCRIPTION__MESSAGE_ID = 5;

	/**
	 * The number of structural features of the '<em>Activity Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DESCRIPTION_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Activity Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DESCRIPTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ProvDescriptions.impl.AgentDescriptionImpl <em>Agent Description</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ProvDescriptions.impl.AgentDescriptionImpl
	 * @see ProvDescriptions.impl.ProvDescriptionsPackageImpl#getAgentDescription()
	 * @generated
	 */
	int AGENT_DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Agent Instance ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_DESCRIPTION__AGENT_INSTANCE_ID = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_DESCRIPTION__NAME = 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_DESCRIPTION__TYPE = 2;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_DESCRIPTION__GROUP = 3;

	/**
	 * The feature id for the '<em><b>Message ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_DESCRIPTION__MESSAGE_ID = 4;

	/**
	 * The number of structural features of the '<em>Agent Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_DESCRIPTION_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Agent Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_DESCRIPTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ProvDescriptions.impl.ActivityStackEntryImpl <em>Activity Stack Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ProvDescriptions.impl.ActivityStackEntryImpl
	 * @see ProvDescriptions.impl.ProvDescriptionsPackageImpl#getActivityStackEntry()
	 * @generated
	 */
	int ACTIVITY_STACK_ENTRY = 3;

	/**
	 * The feature id for the '<em><b>Activity Instance ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_STACK_ENTRY__ACTIVITY_INSTANCE_ID = 0;

	/**
	 * The feature id for the '<em><b>Possition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_STACK_ENTRY__POSSITION = 1;

	/**
	 * The number of structural features of the '<em>Activity Stack Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_STACK_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Activity Stack Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_STACK_ENTRY_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link ProvDescriptions.EntityDescription <em>Entity Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity Description</em>'.
	 * @see ProvDescriptions.EntityDescription
	 * @generated
	 */
	EClass getEntityDescription();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.EntityDescription#getEntityInstanceID <em>Entity Instance ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Entity Instance ID</em>'.
	 * @see ProvDescriptions.EntityDescription#getEntityInstanceID()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EAttribute getEntityDescription_EntityInstanceID();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.EntityDescription#getObjectIDAsString <em>Object ID As String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Object ID As String</em>'.
	 * @see ProvDescriptions.EntityDescription#getObjectIDAsString()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EAttribute getEntityDescription_ObjectIDAsString();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.EntityDescription#getAttributeIDAsString <em>Attribute ID As String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attribute ID As String</em>'.
	 * @see ProvDescriptions.EntityDescription#getAttributeIDAsString()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EAttribute getEntityDescription_AttributeIDAsString();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.EntityDescription#getMemoryVersionAsString <em>Memory Version As String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Memory Version As String</em>'.
	 * @see ProvDescriptions.EntityDescription#getMemoryVersionAsString()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EAttribute getEntityDescription_MemoryVersionAsString();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.EntityDescription#getStorageVersionAsString <em>Storage Version As String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Storage Version As String</em>'.
	 * @see ProvDescriptions.EntityDescription#getStorageVersionAsString()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EAttribute getEntityDescription_StorageVersionAsString();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.EntityDescription#isInModelStorage <em>In Model Storage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>In Model Storage</em>'.
	 * @see ProvDescriptions.EntityDescription#isInModelStorage()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EAttribute getEntityDescription_InModelStorage();

	/**
	 * Returns the meta object for the reference '{@link ProvDescriptions.EntityDescription#getObjectStorageReference <em>Object Storage Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object Storage Reference</em>'.
	 * @see ProvDescriptions.EntityDescription#getObjectStorageReference()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EReference getEntityDescription_ObjectStorageReference();

	/**
	 * Returns the meta object for the reference '{@link ProvDescriptions.EntityDescription#getAttributeMetadataStorageReference <em>Attribute Metadata Storage Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute Metadata Storage Reference</em>'.
	 * @see ProvDescriptions.EntityDescription#getAttributeMetadataStorageReference()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EReference getEntityDescription_AttributeMetadataStorageReference();

	/**
	 * Returns the meta object for the reference '{@link ProvDescriptions.EntityDescription#getAttributeDataStorageReference <em>Attribute Data Storage Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute Data Storage Reference</em>'.
	 * @see ProvDescriptions.EntityDescription#getAttributeDataStorageReference()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EReference getEntityDescription_AttributeDataStorageReference();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.EntityDescription#getObjectName <em>Object Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Object Name</em>'.
	 * @see ProvDescriptions.EntityDescription#getObjectName()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EAttribute getEntityDescription_ObjectName();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.EntityDescription#getObjectType <em>Object Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Object Type</em>'.
	 * @see ProvDescriptions.EntityDescription#getObjectType()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EAttribute getEntityDescription_ObjectType();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.EntityDescription#getAttributeNameAsString <em>Attribute Name As String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attribute Name As String</em>'.
	 * @see ProvDescriptions.EntityDescription#getAttributeNameAsString()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EAttribute getEntityDescription_AttributeNameAsString();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.EntityDescription#getAttributeTypeAsString <em>Attribute Type As String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attribute Type As String</em>'.
	 * @see ProvDescriptions.EntityDescription#getAttributeTypeAsString()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EAttribute getEntityDescription_AttributeTypeAsString();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.EntityDescription#getAttributeValueAsString <em>Attribute Value As String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attribute Value As String</em>'.
	 * @see ProvDescriptions.EntityDescription#getAttributeValueAsString()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EAttribute getEntityDescription_AttributeValueAsString();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.EntityDescription#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see ProvDescriptions.EntityDescription#getStartTime()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EAttribute getEntityDescription_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.EntityDescription#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see ProvDescriptions.EntityDescription#getEndTime()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EAttribute getEntityDescription_EndTime();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.EntityDescription#getLastAccessTime <em>Last Access Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Access Time</em>'.
	 * @see ProvDescriptions.EntityDescription#getLastAccessTime()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EAttribute getEntityDescription_LastAccessTime();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.EntityDescription#getLastAccessType <em>Last Access Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Access Type</em>'.
	 * @see ProvDescriptions.EntityDescription#getLastAccessType()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EAttribute getEntityDescription_LastAccessType();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.EntityDescription#getMessageID <em>Message ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message ID</em>'.
	 * @see ProvDescriptions.EntityDescription#getMessageID()
	 * @see #getEntityDescription()
	 * @generated
	 */
	EAttribute getEntityDescription_MessageID();

	/**
	 * Returns the meta object for class '{@link ProvDescriptions.ActivityDescription <em>Activity Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Activity Description</em>'.
	 * @see ProvDescriptions.ActivityDescription
	 * @generated
	 */
	EClass getActivityDescription();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.ActivityDescription#getActivityInstanceID <em>Activity Instance ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Activity Instance ID</em>'.
	 * @see ProvDescriptions.ActivityDescription#getActivityInstanceID()
	 * @see #getActivityDescription()
	 * @generated
	 */
	EAttribute getActivityDescription_ActivityInstanceID();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.ActivityDescription#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see ProvDescriptions.ActivityDescription#getName()
	 * @see #getActivityDescription()
	 * @generated
	 */
	EAttribute getActivityDescription_Name();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.ActivityDescription#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see ProvDescriptions.ActivityDescription#getStartTime()
	 * @see #getActivityDescription()
	 * @generated
	 */
	EAttribute getActivityDescription_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.ActivityDescription#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see ProvDescriptions.ActivityDescription#getEndTime()
	 * @see #getActivityDescription()
	 * @generated
	 */
	EAttribute getActivityDescription_EndTime();

	/**
	 * Returns the meta object for the reference list '{@link ProvDescriptions.ActivityDescription#getActivityStack <em>Activity Stack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Activity Stack</em>'.
	 * @see ProvDescriptions.ActivityDescription#getActivityStack()
	 * @see #getActivityDescription()
	 * @generated
	 */
	EReference getActivityDescription_ActivityStack();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.ActivityDescription#getMessageID <em>Message ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message ID</em>'.
	 * @see ProvDescriptions.ActivityDescription#getMessageID()
	 * @see #getActivityDescription()
	 * @generated
	 */
	EAttribute getActivityDescription_MessageID();

	/**
	 * Returns the meta object for class '{@link ProvDescriptions.AgentDescription <em>Agent Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Agent Description</em>'.
	 * @see ProvDescriptions.AgentDescription
	 * @generated
	 */
	EClass getAgentDescription();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.AgentDescription#getAgentInstanceID <em>Agent Instance ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Agent Instance ID</em>'.
	 * @see ProvDescriptions.AgentDescription#getAgentInstanceID()
	 * @see #getAgentDescription()
	 * @generated
	 */
	EAttribute getAgentDescription_AgentInstanceID();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.AgentDescription#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see ProvDescriptions.AgentDescription#getName()
	 * @see #getAgentDescription()
	 * @generated
	 */
	EAttribute getAgentDescription_Name();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.AgentDescription#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see ProvDescriptions.AgentDescription#getType()
	 * @see #getAgentDescription()
	 * @generated
	 */
	EAttribute getAgentDescription_Type();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.AgentDescription#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Group</em>'.
	 * @see ProvDescriptions.AgentDescription#getGroup()
	 * @see #getAgentDescription()
	 * @generated
	 */
	EAttribute getAgentDescription_Group();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.AgentDescription#getMessageID <em>Message ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message ID</em>'.
	 * @see ProvDescriptions.AgentDescription#getMessageID()
	 * @see #getAgentDescription()
	 * @generated
	 */
	EAttribute getAgentDescription_MessageID();

	/**
	 * Returns the meta object for class '{@link ProvDescriptions.ActivityStackEntry <em>Activity Stack Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Activity Stack Entry</em>'.
	 * @see ProvDescriptions.ActivityStackEntry
	 * @generated
	 */
	EClass getActivityStackEntry();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.ActivityStackEntry#getActivityInstanceID <em>Activity Instance ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Activity Instance ID</em>'.
	 * @see ProvDescriptions.ActivityStackEntry#getActivityInstanceID()
	 * @see #getActivityStackEntry()
	 * @generated
	 */
	EAttribute getActivityStackEntry_ActivityInstanceID();

	/**
	 * Returns the meta object for the attribute '{@link ProvDescriptions.ActivityStackEntry#getPossition <em>Possition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Possition</em>'.
	 * @see ProvDescriptions.ActivityStackEntry#getPossition()
	 * @see #getActivityStackEntry()
	 * @generated
	 */
	EAttribute getActivityStackEntry_Possition();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ProvDescriptionsFactory getProvDescriptionsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ProvDescriptions.impl.EntityDescriptionImpl <em>Entity Description</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ProvDescriptions.impl.EntityDescriptionImpl
		 * @see ProvDescriptions.impl.ProvDescriptionsPackageImpl#getEntityDescription()
		 * @generated
		 */
		EClass ENTITY_DESCRIPTION = eINSTANCE.getEntityDescription();

		/**
		 * The meta object literal for the '<em><b>Entity Instance ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_DESCRIPTION__ENTITY_INSTANCE_ID = eINSTANCE.getEntityDescription_EntityInstanceID();

		/**
		 * The meta object literal for the '<em><b>Object ID As String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_DESCRIPTION__OBJECT_ID_AS_STRING = eINSTANCE.getEntityDescription_ObjectIDAsString();

		/**
		 * The meta object literal for the '<em><b>Attribute ID As String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_DESCRIPTION__ATTRIBUTE_ID_AS_STRING = eINSTANCE.getEntityDescription_AttributeIDAsString();

		/**
		 * The meta object literal for the '<em><b>Memory Version As String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_DESCRIPTION__MEMORY_VERSION_AS_STRING = eINSTANCE.getEntityDescription_MemoryVersionAsString();

		/**
		 * The meta object literal for the '<em><b>Storage Version As String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_DESCRIPTION__STORAGE_VERSION_AS_STRING = eINSTANCE.getEntityDescription_StorageVersionAsString();

		/**
		 * The meta object literal for the '<em><b>In Model Storage</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_DESCRIPTION__IN_MODEL_STORAGE = eINSTANCE.getEntityDescription_InModelStorage();

		/**
		 * The meta object literal for the '<em><b>Object Storage Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_DESCRIPTION__OBJECT_STORAGE_REFERENCE = eINSTANCE.getEntityDescription_ObjectStorageReference();

		/**
		 * The meta object literal for the '<em><b>Attribute Metadata Storage Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_DESCRIPTION__ATTRIBUTE_METADATA_STORAGE_REFERENCE = eINSTANCE.getEntityDescription_AttributeMetadataStorageReference();

		/**
		 * The meta object literal for the '<em><b>Attribute Data Storage Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_DESCRIPTION__ATTRIBUTE_DATA_STORAGE_REFERENCE = eINSTANCE.getEntityDescription_AttributeDataStorageReference();

		/**
		 * The meta object literal for the '<em><b>Object Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_DESCRIPTION__OBJECT_NAME = eINSTANCE.getEntityDescription_ObjectName();

		/**
		 * The meta object literal for the '<em><b>Object Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_DESCRIPTION__OBJECT_TYPE = eINSTANCE.getEntityDescription_ObjectType();

		/**
		 * The meta object literal for the '<em><b>Attribute Name As String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_DESCRIPTION__ATTRIBUTE_NAME_AS_STRING = eINSTANCE.getEntityDescription_AttributeNameAsString();

		/**
		 * The meta object literal for the '<em><b>Attribute Type As String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_DESCRIPTION__ATTRIBUTE_TYPE_AS_STRING = eINSTANCE.getEntityDescription_AttributeTypeAsString();

		/**
		 * The meta object literal for the '<em><b>Attribute Value As String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_DESCRIPTION__ATTRIBUTE_VALUE_AS_STRING = eINSTANCE.getEntityDescription_AttributeValueAsString();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_DESCRIPTION__START_TIME = eINSTANCE.getEntityDescription_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_DESCRIPTION__END_TIME = eINSTANCE.getEntityDescription_EndTime();

		/**
		 * The meta object literal for the '<em><b>Last Access Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_DESCRIPTION__LAST_ACCESS_TIME = eINSTANCE.getEntityDescription_LastAccessTime();

		/**
		 * The meta object literal for the '<em><b>Last Access Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_DESCRIPTION__LAST_ACCESS_TYPE = eINSTANCE.getEntityDescription_LastAccessType();

		/**
		 * The meta object literal for the '<em><b>Message ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_DESCRIPTION__MESSAGE_ID = eINSTANCE.getEntityDescription_MessageID();

		/**
		 * The meta object literal for the '{@link ProvDescriptions.impl.ActivityDescriptionImpl <em>Activity Description</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ProvDescriptions.impl.ActivityDescriptionImpl
		 * @see ProvDescriptions.impl.ProvDescriptionsPackageImpl#getActivityDescription()
		 * @generated
		 */
		EClass ACTIVITY_DESCRIPTION = eINSTANCE.getActivityDescription();

		/**
		 * The meta object literal for the '<em><b>Activity Instance ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVITY_DESCRIPTION__ACTIVITY_INSTANCE_ID = eINSTANCE.getActivityDescription_ActivityInstanceID();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVITY_DESCRIPTION__NAME = eINSTANCE.getActivityDescription_Name();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVITY_DESCRIPTION__START_TIME = eINSTANCE.getActivityDescription_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVITY_DESCRIPTION__END_TIME = eINSTANCE.getActivityDescription_EndTime();

		/**
		 * The meta object literal for the '<em><b>Activity Stack</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY_DESCRIPTION__ACTIVITY_STACK = eINSTANCE.getActivityDescription_ActivityStack();

		/**
		 * The meta object literal for the '<em><b>Message ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVITY_DESCRIPTION__MESSAGE_ID = eINSTANCE.getActivityDescription_MessageID();

		/**
		 * The meta object literal for the '{@link ProvDescriptions.impl.AgentDescriptionImpl <em>Agent Description</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ProvDescriptions.impl.AgentDescriptionImpl
		 * @see ProvDescriptions.impl.ProvDescriptionsPackageImpl#getAgentDescription()
		 * @generated
		 */
		EClass AGENT_DESCRIPTION = eINSTANCE.getAgentDescription();

		/**
		 * The meta object literal for the '<em><b>Agent Instance ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGENT_DESCRIPTION__AGENT_INSTANCE_ID = eINSTANCE.getAgentDescription_AgentInstanceID();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGENT_DESCRIPTION__NAME = eINSTANCE.getAgentDescription_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGENT_DESCRIPTION__TYPE = eINSTANCE.getAgentDescription_Type();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGENT_DESCRIPTION__GROUP = eINSTANCE.getAgentDescription_Group();

		/**
		 * The meta object literal for the '<em><b>Message ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGENT_DESCRIPTION__MESSAGE_ID = eINSTANCE.getAgentDescription_MessageID();

		/**
		 * The meta object literal for the '{@link ProvDescriptions.impl.ActivityStackEntryImpl <em>Activity Stack Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ProvDescriptions.impl.ActivityStackEntryImpl
		 * @see ProvDescriptions.impl.ProvDescriptionsPackageImpl#getActivityStackEntry()
		 * @generated
		 */
		EClass ACTIVITY_STACK_ENTRY = eINSTANCE.getActivityStackEntry();

		/**
		 * The meta object literal for the '<em><b>Activity Instance ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVITY_STACK_ENTRY__ACTIVITY_INSTANCE_ID = eINSTANCE.getActivityStackEntry_ActivityInstanceID();

		/**
		 * The meta object literal for the '<em><b>Possition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVITY_STACK_ENTRY__POSSITION = eINSTANCE.getActivityStackEntry_Possition();

	}

} //ProvDescriptionsPackage
