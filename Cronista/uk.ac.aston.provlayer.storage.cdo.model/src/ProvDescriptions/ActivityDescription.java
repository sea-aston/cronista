/**
 */
package ProvDescriptions;

import org.eclipse.emf.cdo.CDOObject;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activity Description</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ProvDescriptions.ActivityDescription#getActivityInstanceID <em>Activity Instance ID</em>}</li>
 *   <li>{@link ProvDescriptions.ActivityDescription#getName <em>Name</em>}</li>
 *   <li>{@link ProvDescriptions.ActivityDescription#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link ProvDescriptions.ActivityDescription#getEndTime <em>End Time</em>}</li>
 *   <li>{@link ProvDescriptions.ActivityDescription#getActivityStack <em>Activity Stack</em>}</li>
 *   <li>{@link ProvDescriptions.ActivityDescription#getMessageID <em>Message ID</em>}</li>
 * </ul>
 *
 * @see ProvDescriptions.ProvDescriptionsPackage#getActivityDescription()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface ActivityDescription extends CDOObject {
	/**
	 * Returns the value of the '<em><b>Activity Instance ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activity Instance ID</em>' attribute.
	 * @see #setActivityInstanceID(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getActivityDescription_ActivityInstanceID()
	 * @model id="true"
	 * @generated
	 */
	String getActivityInstanceID();

	/**
	 * Sets the value of the '{@link ProvDescriptions.ActivityDescription#getActivityInstanceID <em>Activity Instance ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Activity Instance ID</em>' attribute.
	 * @see #getActivityInstanceID()
	 * @generated
	 */
	void setActivityInstanceID(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getActivityDescription_Name()
	 * @model ordered="false"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link ProvDescriptions.ActivityDescription#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(long)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getActivityDescription_StartTime()
	 * @model
	 * @generated
	 */
	long getStartTime();

	/**
	 * Sets the value of the '{@link ProvDescriptions.ActivityDescription#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(long value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(long)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getActivityDescription_EndTime()
	 * @model
	 * @generated
	 */
	long getEndTime();

	/**
	 * Sets the value of the '{@link ProvDescriptions.ActivityDescription#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(long value);

	/**
	 * Returns the value of the '<em><b>Activity Stack</b></em>' reference list.
	 * The list contents are of type {@link ProvDescriptions.ActivityStackEntry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activity Stack</em>' reference list.
	 * @see ProvDescriptions.ProvDescriptionsPackage#getActivityDescription_ActivityStack()
	 * @model
	 * @generated
	 */
	EList<ActivityStackEntry> getActivityStack();

	/**
	 * Returns the value of the '<em><b>Message ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message ID</em>' attribute.
	 * @see #setMessageID(String)
	 * @see ProvDescriptions.ProvDescriptionsPackage#getActivityDescription_MessageID()
	 * @model
	 * @generated
	 */
	String getMessageID();

	/**
	 * Sets the value of the '{@link ProvDescriptions.ActivityDescription#getMessageID <em>Message ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message ID</em>' attribute.
	 * @see #getMessageID()
	 * @generated
	 */
	void setMessageID(String value);

} // ActivityDescription
