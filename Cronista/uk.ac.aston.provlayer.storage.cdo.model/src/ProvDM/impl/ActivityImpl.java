/**
 */
package ProvDM.impl;

import ProvDM.Activity;
import ProvDM.Agent;
import ProvDM.Entity;
import ProvDM.ProvDMPackage;

import ProvDescriptions.ActivityDescription;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.internal.cdo.CDOObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Activity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ProvDM.impl.ActivityImpl#getID <em>ID</em>}</li>
 *   <li>{@link ProvDM.impl.ActivityImpl#getUsed <em>Used</em>}</li>
 *   <li>{@link ProvDM.impl.ActivityImpl#getWasInformedBy <em>Was Informed By</em>}</li>
 *   <li>{@link ProvDM.impl.ActivityImpl#getWasAssociatedWith <em>Was Associated With</em>}</li>
 *   <li>{@link ProvDM.impl.ActivityImpl#getActivityDescription <em>Activity Description</em>}</li>
 *   <li>{@link ProvDM.impl.ActivityImpl#getGenerates <em>Generates</em>}</li>
 *   <li>{@link ProvDM.impl.ActivityImpl#getInforms <em>Informs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActivityImpl extends CDOObjectImpl implements Activity {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActivityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProvDMPackage.Literals.ACTIVITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getID() {
		return (String)eGet(ProvDMPackage.Literals.ACTIVITY__ID, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(String newID) {
		eSet(ProvDMPackage.Literals.ACTIVITY__ID, newID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Entity> getUsed() {
		return (EList<Entity>)eGet(ProvDMPackage.Literals.ACTIVITY__USED, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Activity> getWasInformedBy() {
		return (EList<Activity>)eGet(ProvDMPackage.Literals.ACTIVITY__WAS_INFORMED_BY, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Agent getWasAssociatedWith() {
		return (Agent)eGet(ProvDMPackage.Literals.ACTIVITY__WAS_ASSOCIATED_WITH, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWasAssociatedWith(Agent newWasAssociatedWith) {
		eSet(ProvDMPackage.Literals.ACTIVITY__WAS_ASSOCIATED_WITH, newWasAssociatedWith);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityDescription getActivityDescription() {
		return (ActivityDescription)eGet(ProvDMPackage.Literals.ACTIVITY__ACTIVITY_DESCRIPTION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActivityDescription(ActivityDescription newActivityDescription) {
		eSet(ProvDMPackage.Literals.ACTIVITY__ACTIVITY_DESCRIPTION, newActivityDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Entity> getGenerates() {
		return (EList<Entity>)eGet(ProvDMPackage.Literals.ACTIVITY__GENERATES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Activity> getInforms() {
		return (EList<Activity>)eGet(ProvDMPackage.Literals.ACTIVITY__INFORMS, true);
	}

} //ActivityImpl
