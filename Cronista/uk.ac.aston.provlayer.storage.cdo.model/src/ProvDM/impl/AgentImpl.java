/**
 */
package ProvDM.impl;

import ProvDM.Activity;
import ProvDM.Agent;
import ProvDM.Entity;
import ProvDM.ProvDMPackage;

import ProvDescriptions.AgentDescription;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.internal.cdo.CDOObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Agent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ProvDM.impl.AgentImpl#getID <em>ID</em>}</li>
 *   <li>{@link ProvDM.impl.AgentImpl#getActedOnBehalfOf <em>Acted On Behalf Of</em>}</li>
 *   <li>{@link ProvDM.impl.AgentImpl#getAgentDescription <em>Agent Description</em>}</li>
 *   <li>{@link ProvDM.impl.AgentImpl#getWasAttributedTo <em>Was Attributed To</em>}</li>
 *   <li>{@link ProvDM.impl.AgentImpl#getWasAssociatedWith <em>Was Associated With</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AgentImpl extends CDOObjectImpl implements Agent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AgentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProvDMPackage.Literals.AGENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getID() {
		return (String)eGet(ProvDMPackage.Literals.AGENT__ID, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(String newID) {
		eSet(ProvDMPackage.Literals.AGENT__ID, newID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Agent getActedOnBehalfOf() {
		return (Agent)eGet(ProvDMPackage.Literals.AGENT__ACTED_ON_BEHALF_OF, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActedOnBehalfOf(Agent newActedOnBehalfOf) {
		eSet(ProvDMPackage.Literals.AGENT__ACTED_ON_BEHALF_OF, newActedOnBehalfOf);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AgentDescription getAgentDescription() {
		return (AgentDescription)eGet(ProvDMPackage.Literals.AGENT__AGENT_DESCRIPTION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAgentDescription(AgentDescription newAgentDescription) {
		eSet(ProvDMPackage.Literals.AGENT__AGENT_DESCRIPTION, newAgentDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Entity> getWasAttributedTo() {
		return (EList<Entity>)eGet(ProvDMPackage.Literals.AGENT__WAS_ATTRIBUTED_TO, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Activity> getWasAssociatedWith() {
		return (EList<Activity>)eGet(ProvDMPackage.Literals.AGENT__WAS_ASSOCIATED_WITH, true);
	}

} //AgentImpl
