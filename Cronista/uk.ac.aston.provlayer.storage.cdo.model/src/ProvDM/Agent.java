/**
 */
package ProvDM;

import ProvDescriptions.AgentDescription;

import org.eclipse.emf.cdo.CDOObject;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ProvDM.Agent#getID <em>ID</em>}</li>
 *   <li>{@link ProvDM.Agent#getActedOnBehalfOf <em>Acted On Behalf Of</em>}</li>
 *   <li>{@link ProvDM.Agent#getAgentDescription <em>Agent Description</em>}</li>
 *   <li>{@link ProvDM.Agent#getWasAttributedTo <em>Was Attributed To</em>}</li>
 *   <li>{@link ProvDM.Agent#getWasAssociatedWith <em>Was Associated With</em>}</li>
 * </ul>
 *
 * @see ProvDM.ProvDMPackage#getAgent()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface Agent extends CDOObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see ProvDM.ProvDMPackage#getAgent_ID()
	 * @model id="true"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link ProvDM.Agent#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>Acted On Behalf Of</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acted On Behalf Of</em>' reference.
	 * @see #setActedOnBehalfOf(Agent)
	 * @see ProvDM.ProvDMPackage#getAgent_ActedOnBehalfOf()
	 * @model
	 * @generated
	 */
	Agent getActedOnBehalfOf();

	/**
	 * Sets the value of the '{@link ProvDM.Agent#getActedOnBehalfOf <em>Acted On Behalf Of</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acted On Behalf Of</em>' reference.
	 * @see #getActedOnBehalfOf()
	 * @generated
	 */
	void setActedOnBehalfOf(Agent value);

	/**
	 * Returns the value of the '<em><b>Agent Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agent Description</em>' containment reference.
	 * @see #setAgentDescription(AgentDescription)
	 * @see ProvDM.ProvDMPackage#getAgent_AgentDescription()
	 * @model containment="true"
	 * @generated
	 */
	AgentDescription getAgentDescription();

	/**
	 * Sets the value of the '{@link ProvDM.Agent#getAgentDescription <em>Agent Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Agent Description</em>' containment reference.
	 * @see #getAgentDescription()
	 * @generated
	 */
	void setAgentDescription(AgentDescription value);

	/**
	 * Returns the value of the '<em><b>Was Attributed To</b></em>' reference list.
	 * The list contents are of type {@link ProvDM.Entity}.
	 * It is bidirectional and its opposite is '{@link ProvDM.Entity#getWasAttributedTo <em>Was Attributed To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Was Attributed To</em>' reference list.
	 * @see ProvDM.ProvDMPackage#getAgent_WasAttributedTo()
	 * @see ProvDM.Entity#getWasAttributedTo
	 * @model opposite="WasAttributedTo"
	 * @generated
	 */
	EList<Entity> getWasAttributedTo();

	/**
	 * Returns the value of the '<em><b>Was Associated With</b></em>' reference list.
	 * The list contents are of type {@link ProvDM.Activity}.
	 * It is bidirectional and its opposite is '{@link ProvDM.Activity#getWasAssociatedWith <em>Was Associated With</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Was Associated With</em>' reference list.
	 * @see ProvDM.ProvDMPackage#getAgent_WasAssociatedWith()
	 * @see ProvDM.Activity#getWasAssociatedWith
	 * @model opposite="WasAssociatedWith"
	 * @generated
	 */
	EList<Activity> getWasAssociatedWith();

} // Agent
