/**
 */
package historyModel;

import ProvDM.Activity;
import ProvDM.Agent;
import ProvDM.Entity;

import org.eclipse.emf.cdo.CDOObject;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provenance Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This is the container for creating the provenance graph objects for this window.
 * 
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link historyModel.ProvenanceGraph#getAgent <em>Agent</em>}</li>
 *   <li>{@link historyModel.ProvenanceGraph#getActivity <em>Activity</em>}</li>
 *   <li>{@link historyModel.ProvenanceGraph#getEntity <em>Entity</em>}</li>
 * </ul>
 *
 * @see historyModel.HistoryModelPackage#getProvenanceGraph()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface ProvenanceGraph extends CDOObject {
	/**
	 * Returns the value of the '<em><b>Agent</b></em>' containment reference list.
	 * The list contents are of type {@link ProvDM.Agent}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agent</em>' containment reference list.
	 * @see historyModel.HistoryModelPackage#getProvenanceGraph_Agent()
	 * @model containment="true"
	 * @generated
	 */
	EList<Agent> getAgent();

	/**
	 * Returns the value of the '<em><b>Activity</b></em>' containment reference list.
	 * The list contents are of type {@link ProvDM.Activity}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activity</em>' containment reference list.
	 * @see historyModel.HistoryModelPackage#getProvenanceGraph_Activity()
	 * @model containment="true"
	 * @generated
	 */
	EList<Activity> getActivity();

	/**
	 * Returns the value of the '<em><b>Entity</b></em>' containment reference list.
	 * The list contents are of type {@link ProvDM.Entity}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity</em>' containment reference list.
	 * @see historyModel.HistoryModelPackage#getProvenanceGraph_Entity()
	 * @model containment="true"
	 * @generated
	 */
	EList<Entity> getEntity();

} // ProvenanceGraph
