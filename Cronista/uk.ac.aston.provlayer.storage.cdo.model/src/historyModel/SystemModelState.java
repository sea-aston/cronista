/**
 */
package historyModel;

import org.eclipse.emf.cdo.CDOObject;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>System Model State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This is an object which can contain a copy of the runtime model or a reference to a runtime model version
 * 
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link historyModel.SystemModelState#getFirstCommitTime <em>First Commit Time</em>}</li>
 *   <li>{@link historyModel.SystemModelState#getPointMeAtRuntimeModelVersion <em>Point Me At Runtime Model Version</em>}</li>
 * </ul>
 *
 * @see historyModel.HistoryModelPackage#getSystemModelState()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface SystemModelState extends CDOObject {
	/**
	 * Returns the value of the '<em><b>First Commit Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Commit Time</em>' attribute.
	 * @see #setFirstCommitTime(long)
	 * @see historyModel.HistoryModelPackage#getSystemModelState_FirstCommitTime()
	 * @model
	 * @generated
	 */
	long getFirstCommitTime();

	/**
	 * Sets the value of the '{@link historyModel.SystemModelState#getFirstCommitTime <em>First Commit Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Commit Time</em>' attribute.
	 * @see #getFirstCommitTime()
	 * @generated
	 */
	void setFirstCommitTime(long value);

	/**
	 * Returns the value of the '<em><b>Point Me At Runtime Model Version</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This is the reference that needs to point to the version of the runtime model for this time window
	 * 
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Point Me At Runtime Model Version</em>' reference.
	 * @see #setPointMeAtRuntimeModelVersion(EObject)
	 * @see historyModel.HistoryModelPackage#getSystemModelState_PointMeAtRuntimeModelVersion()
	 * @model
	 * @generated
	 */
	EObject getPointMeAtRuntimeModelVersion();

	/**
	 * Sets the value of the '{@link historyModel.SystemModelState#getPointMeAtRuntimeModelVersion <em>Point Me At Runtime Model Version</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Point Me At Runtime Model Version</em>' reference.
	 * @see #getPointMeAtRuntimeModelVersion()
	 * @generated
	 */
	void setPointMeAtRuntimeModelVersion(EObject value);

} // SystemModelState
