/**
 */
package historyModel;

import org.eclipse.emf.cdo.CDOObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Window</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Time windows are interconnected as a linked list for a time line
 * 
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link historyModel.TimeWindow#getProvenanceGraph <em>Provenance Graph</em>}</li>
 *   <li>{@link historyModel.TimeWindow#getSystemModelState <em>System Model State</em>}</li>
 *   <li>{@link historyModel.TimeWindow#getNextTimeWindow <em>Next Time Window</em>}</li>
 *   <li>{@link historyModel.TimeWindow#getPreviousTimeWindow <em>Previous Time Window</em>}</li>
 *   <li>{@link historyModel.TimeWindow#getStartLocalTime <em>Start Local Time</em>}</li>
 *   <li>{@link historyModel.TimeWindow#getEndLocalTime <em>End Local Time</em>}</li>
 * </ul>
 *
 * @see historyModel.HistoryModelPackage#getTimeWindow()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface TimeWindow extends CDOObject {
	/**
	 * Returns the value of the '<em><b>Provenance Graph</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provenance Graph</em>' containment reference.
	 * @see #setProvenanceGraph(ProvenanceGraph)
	 * @see historyModel.HistoryModelPackage#getTimeWindow_ProvenanceGraph()
	 * @model containment="true"
	 * @generated
	 */
	ProvenanceGraph getProvenanceGraph();

	/**
	 * Sets the value of the '{@link historyModel.TimeWindow#getProvenanceGraph <em>Provenance Graph</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Provenance Graph</em>' containment reference.
	 * @see #getProvenanceGraph()
	 * @generated
	 */
	void setProvenanceGraph(ProvenanceGraph value);

	/**
	 * Returns the value of the '<em><b>System Model State</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Model State</em>' containment reference.
	 * @see #setSystemModelState(SystemModelState)
	 * @see historyModel.HistoryModelPackage#getTimeWindow_SystemModelState()
	 * @model containment="true"
	 * @generated
	 */
	SystemModelState getSystemModelState();

	/**
	 * Sets the value of the '{@link historyModel.TimeWindow#getSystemModelState <em>System Model State</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Model State</em>' containment reference.
	 * @see #getSystemModelState()
	 * @generated
	 */
	void setSystemModelState(SystemModelState value);

	/**
	 * Returns the value of the '<em><b>Next Time Window</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link historyModel.TimeWindow#getPreviousTimeWindow <em>Previous Time Window</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next Time Window</em>' reference.
	 * @see #setNextTimeWindow(TimeWindow)
	 * @see historyModel.HistoryModelPackage#getTimeWindow_NextTimeWindow()
	 * @see historyModel.TimeWindow#getPreviousTimeWindow
	 * @model opposite="PreviousTimeWindow"
	 * @generated
	 */
	TimeWindow getNextTimeWindow();

	/**
	 * Sets the value of the '{@link historyModel.TimeWindow#getNextTimeWindow <em>Next Time Window</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next Time Window</em>' reference.
	 * @see #getNextTimeWindow()
	 * @generated
	 */
	void setNextTimeWindow(TimeWindow value);

	/**
	 * Returns the value of the '<em><b>Previous Time Window</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link historyModel.TimeWindow#getNextTimeWindow <em>Next Time Window</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Previous Time Window</em>' reference.
	 * @see #setPreviousTimeWindow(TimeWindow)
	 * @see historyModel.HistoryModelPackage#getTimeWindow_PreviousTimeWindow()
	 * @see historyModel.TimeWindow#getNextTimeWindow
	 * @model opposite="NextTimeWindow"
	 * @generated
	 */
	TimeWindow getPreviousTimeWindow();

	/**
	 * Sets the value of the '{@link historyModel.TimeWindow#getPreviousTimeWindow <em>Previous Time Window</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Previous Time Window</em>' reference.
	 * @see #getPreviousTimeWindow()
	 * @generated
	 */
	void setPreviousTimeWindow(TimeWindow value);

	/**
	 * Returns the value of the '<em><b>Start Local Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Local Time</em>' attribute.
	 * @see #setStartLocalTime(long)
	 * @see historyModel.HistoryModelPackage#getTimeWindow_StartLocalTime()
	 * @model
	 * @generated
	 */
	long getStartLocalTime();

	/**
	 * Sets the value of the '{@link historyModel.TimeWindow#getStartLocalTime <em>Start Local Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Local Time</em>' attribute.
	 * @see #getStartLocalTime()
	 * @generated
	 */
	void setStartLocalTime(long value);

	/**
	 * Returns the value of the '<em><b>End Local Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Local Time</em>' attribute.
	 * @see #setEndLocalTime(long)
	 * @see historyModel.HistoryModelPackage#getTimeWindow_EndLocalTime()
	 * @model
	 * @generated
	 */
	long getEndLocalTime();

	/**
	 * Sets the value of the '{@link historyModel.TimeWindow#getEndLocalTime <em>End Local Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Local Time</em>' attribute.
	 * @see #getEndLocalTime()
	 * @generated
	 */
	void setEndLocalTime(long value);

} // TimeWindow
