/**
 */
package historyModel.impl;

import historyModel.HistoryModelPackage;
import historyModel.SystemModelState;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.internal.cdo.CDOObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>System Model State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link historyModel.impl.SystemModelStateImpl#getFirstCommitTime <em>First Commit Time</em>}</li>
 *   <li>{@link historyModel.impl.SystemModelStateImpl#getPointMeAtRuntimeModelVersion <em>Point Me At Runtime Model Version</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SystemModelStateImpl extends CDOObjectImpl implements SystemModelState {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SystemModelStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HistoryModelPackage.Literals.SYSTEM_MODEL_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getFirstCommitTime() {
		return (Long)eGet(HistoryModelPackage.Literals.SYSTEM_MODEL_STATE__FIRST_COMMIT_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstCommitTime(long newFirstCommitTime) {
		eSet(HistoryModelPackage.Literals.SYSTEM_MODEL_STATE__FIRST_COMMIT_TIME, newFirstCommitTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getPointMeAtRuntimeModelVersion() {
		return (EObject)eGet(HistoryModelPackage.Literals.SYSTEM_MODEL_STATE__POINT_ME_AT_RUNTIME_MODEL_VERSION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPointMeAtRuntimeModelVersion(EObject newPointMeAtRuntimeModelVersion) {
		eSet(HistoryModelPackage.Literals.SYSTEM_MODEL_STATE__POINT_ME_AT_RUNTIME_MODEL_VERSION, newPointMeAtRuntimeModelVersion);
	}

} //SystemModelStateImpl
