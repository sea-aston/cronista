/**
 */
package historyModel.impl;

import ProvDM.ProvDMPackage;

import ProvDM.impl.ProvDMPackageImpl;

import ProvDescriptions.ProvDescriptionsPackage;

import ProvDescriptions.impl.ProvDescriptionsPackageImpl;

import historyModel.HistoryModelFactory;
import historyModel.HistoryModelPackage;
import historyModel.ProvenanceGraph;
import historyModel.SystemModelState;
import historyModel.TimeWindow;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class HistoryModelPackageImpl extends EPackageImpl implements HistoryModelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timeWindowEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass systemModelStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass provenanceGraphEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see historyModel.HistoryModelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private HistoryModelPackageImpl() {
		super(eNS_URI, HistoryModelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link HistoryModelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static HistoryModelPackage init() {
		if (isInited) return (HistoryModelPackage)EPackage.Registry.INSTANCE.getEPackage(HistoryModelPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredHistoryModelPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		HistoryModelPackageImpl theHistoryModelPackage = registeredHistoryModelPackage instanceof HistoryModelPackageImpl ? (HistoryModelPackageImpl)registeredHistoryModelPackage : new HistoryModelPackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ProvDMPackage.eNS_URI);
		ProvDMPackageImpl theProvDMPackage = (ProvDMPackageImpl)(registeredPackage instanceof ProvDMPackageImpl ? registeredPackage : ProvDMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ProvDescriptionsPackage.eNS_URI);
		ProvDescriptionsPackageImpl theProvDescriptionsPackage = (ProvDescriptionsPackageImpl)(registeredPackage instanceof ProvDescriptionsPackageImpl ? registeredPackage : ProvDescriptionsPackage.eINSTANCE);

		// Create package meta-data objects
		theHistoryModelPackage.createPackageContents();
		theProvDMPackage.createPackageContents();
		theProvDescriptionsPackage.createPackageContents();

		// Initialize created meta-data
		theHistoryModelPackage.initializePackageContents();
		theProvDMPackage.initializePackageContents();
		theProvDescriptionsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theHistoryModelPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(HistoryModelPackage.eNS_URI, theHistoryModelPackage);
		return theHistoryModelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimeWindow() {
		return timeWindowEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTimeWindow_ProvenanceGraph() {
		return (EReference)timeWindowEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTimeWindow_SystemModelState() {
		return (EReference)timeWindowEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTimeWindow_NextTimeWindow() {
		return (EReference)timeWindowEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTimeWindow_PreviousTimeWindow() {
		return (EReference)timeWindowEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeWindow_StartLocalTime() {
		return (EAttribute)timeWindowEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeWindow_EndLocalTime() {
		return (EAttribute)timeWindowEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSystemModelState() {
		return systemModelStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSystemModelState_FirstCommitTime() {
		return (EAttribute)systemModelStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystemModelState_PointMeAtRuntimeModelVersion() {
		return (EReference)systemModelStateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProvenanceGraph() {
		return provenanceGraphEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProvenanceGraph_Agent() {
		return (EReference)provenanceGraphEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProvenanceGraph_Activity() {
		return (EReference)provenanceGraphEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProvenanceGraph_Entity() {
		return (EReference)provenanceGraphEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HistoryModelFactory getHistoryModelFactory() {
		return (HistoryModelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		timeWindowEClass = createEClass(TIME_WINDOW);
		createEReference(timeWindowEClass, TIME_WINDOW__PROVENANCE_GRAPH);
		createEReference(timeWindowEClass, TIME_WINDOW__SYSTEM_MODEL_STATE);
		createEReference(timeWindowEClass, TIME_WINDOW__NEXT_TIME_WINDOW);
		createEReference(timeWindowEClass, TIME_WINDOW__PREVIOUS_TIME_WINDOW);
		createEAttribute(timeWindowEClass, TIME_WINDOW__START_LOCAL_TIME);
		createEAttribute(timeWindowEClass, TIME_WINDOW__END_LOCAL_TIME);

		systemModelStateEClass = createEClass(SYSTEM_MODEL_STATE);
		createEAttribute(systemModelStateEClass, SYSTEM_MODEL_STATE__FIRST_COMMIT_TIME);
		createEReference(systemModelStateEClass, SYSTEM_MODEL_STATE__POINT_ME_AT_RUNTIME_MODEL_VERSION);

		provenanceGraphEClass = createEClass(PROVENANCE_GRAPH);
		createEReference(provenanceGraphEClass, PROVENANCE_GRAPH__AGENT);
		createEReference(provenanceGraphEClass, PROVENANCE_GRAPH__ACTIVITY);
		createEReference(provenanceGraphEClass, PROVENANCE_GRAPH__ENTITY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ProvDMPackage theProvDMPackage = (ProvDMPackage)EPackage.Registry.INSTANCE.getEPackage(ProvDMPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(timeWindowEClass, TimeWindow.class, "TimeWindow", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTimeWindow_ProvenanceGraph(), this.getProvenanceGraph(), null, "ProvenanceGraph", null, 0, 1, TimeWindow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTimeWindow_SystemModelState(), this.getSystemModelState(), null, "SystemModelState", null, 0, 1, TimeWindow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTimeWindow_NextTimeWindow(), this.getTimeWindow(), this.getTimeWindow_PreviousTimeWindow(), "NextTimeWindow", null, 0, 1, TimeWindow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTimeWindow_PreviousTimeWindow(), this.getTimeWindow(), this.getTimeWindow_NextTimeWindow(), "PreviousTimeWindow", null, 0, 1, TimeWindow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeWindow_StartLocalTime(), ecorePackage.getELong(), "StartLocalTime", null, 0, 1, TimeWindow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeWindow_EndLocalTime(), ecorePackage.getELong(), "EndLocalTime", null, 0, 1, TimeWindow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(systemModelStateEClass, SystemModelState.class, "SystemModelState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSystemModelState_FirstCommitTime(), ecorePackage.getELong(), "FirstCommitTime", null, 0, 1, SystemModelState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSystemModelState_PointMeAtRuntimeModelVersion(), ecorePackage.getEObject(), null, "PointMeAtRuntimeModelVersion", null, 0, 1, SystemModelState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(provenanceGraphEClass, ProvenanceGraph.class, "ProvenanceGraph", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProvenanceGraph_Agent(), theProvDMPackage.getAgent(), null, "Agent", null, 0, -1, ProvenanceGraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProvenanceGraph_Activity(), theProvDMPackage.getActivity(), null, "Activity", null, 0, -1, ProvenanceGraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProvenanceGraph_Entity(), theProvDMPackage.getEntity(), null, "Entity", null, 0, -1, ProvenanceGraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //HistoryModelPackageImpl
