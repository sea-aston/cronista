/**
 */
package historyModel.impl;

import ProvDM.Activity;
import ProvDM.Agent;
import ProvDM.Entity;

import historyModel.HistoryModelPackage;
import historyModel.ProvenanceGraph;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.internal.cdo.CDOObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Provenance Graph</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link historyModel.impl.ProvenanceGraphImpl#getAgent <em>Agent</em>}</li>
 *   <li>{@link historyModel.impl.ProvenanceGraphImpl#getActivity <em>Activity</em>}</li>
 *   <li>{@link historyModel.impl.ProvenanceGraphImpl#getEntity <em>Entity</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProvenanceGraphImpl extends CDOObjectImpl implements ProvenanceGraph {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProvenanceGraphImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HistoryModelPackage.Literals.PROVENANCE_GRAPH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Agent> getAgent() {
		return (EList<Agent>)eGet(HistoryModelPackage.Literals.PROVENANCE_GRAPH__AGENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Activity> getActivity() {
		return (EList<Activity>)eGet(HistoryModelPackage.Literals.PROVENANCE_GRAPH__ACTIVITY, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Entity> getEntity() {
		return (EList<Entity>)eGet(HistoryModelPackage.Literals.PROVENANCE_GRAPH__ENTITY, true);
	}

} //ProvenanceGraphImpl
