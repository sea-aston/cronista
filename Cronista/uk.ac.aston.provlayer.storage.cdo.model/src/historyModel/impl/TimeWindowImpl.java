/**
 */
package historyModel.impl;

import historyModel.HistoryModelPackage;
import historyModel.ProvenanceGraph;
import historyModel.SystemModelState;
import historyModel.TimeWindow;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.internal.cdo.CDOObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Window</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link historyModel.impl.TimeWindowImpl#getProvenanceGraph <em>Provenance Graph</em>}</li>
 *   <li>{@link historyModel.impl.TimeWindowImpl#getSystemModelState <em>System Model State</em>}</li>
 *   <li>{@link historyModel.impl.TimeWindowImpl#getNextTimeWindow <em>Next Time Window</em>}</li>
 *   <li>{@link historyModel.impl.TimeWindowImpl#getPreviousTimeWindow <em>Previous Time Window</em>}</li>
 *   <li>{@link historyModel.impl.TimeWindowImpl#getStartLocalTime <em>Start Local Time</em>}</li>
 *   <li>{@link historyModel.impl.TimeWindowImpl#getEndLocalTime <em>End Local Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimeWindowImpl extends CDOObjectImpl implements TimeWindow {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimeWindowImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HistoryModelPackage.Literals.TIME_WINDOW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvenanceGraph getProvenanceGraph() {
		return (ProvenanceGraph)eGet(HistoryModelPackage.Literals.TIME_WINDOW__PROVENANCE_GRAPH, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProvenanceGraph(ProvenanceGraph newProvenanceGraph) {
		eSet(HistoryModelPackage.Literals.TIME_WINDOW__PROVENANCE_GRAPH, newProvenanceGraph);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemModelState getSystemModelState() {
		return (SystemModelState)eGet(HistoryModelPackage.Literals.TIME_WINDOW__SYSTEM_MODEL_STATE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystemModelState(SystemModelState newSystemModelState) {
		eSet(HistoryModelPackage.Literals.TIME_WINDOW__SYSTEM_MODEL_STATE, newSystemModelState);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeWindow getNextTimeWindow() {
		return (TimeWindow)eGet(HistoryModelPackage.Literals.TIME_WINDOW__NEXT_TIME_WINDOW, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNextTimeWindow(TimeWindow newNextTimeWindow) {
		eSet(HistoryModelPackage.Literals.TIME_WINDOW__NEXT_TIME_WINDOW, newNextTimeWindow);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeWindow getPreviousTimeWindow() {
		return (TimeWindow)eGet(HistoryModelPackage.Literals.TIME_WINDOW__PREVIOUS_TIME_WINDOW, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreviousTimeWindow(TimeWindow newPreviousTimeWindow) {
		eSet(HistoryModelPackage.Literals.TIME_WINDOW__PREVIOUS_TIME_WINDOW, newPreviousTimeWindow);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getStartLocalTime() {
		return (Long)eGet(HistoryModelPackage.Literals.TIME_WINDOW__START_LOCAL_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartLocalTime(long newStartLocalTime) {
		eSet(HistoryModelPackage.Literals.TIME_WINDOW__START_LOCAL_TIME, newStartLocalTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getEndLocalTime() {
		return (Long)eGet(HistoryModelPackage.Literals.TIME_WINDOW__END_LOCAL_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndLocalTime(long newEndLocalTime) {
		eSet(HistoryModelPackage.Literals.TIME_WINDOW__END_LOCAL_TIME, newEndLocalTime);
	}

} //TimeWindowImpl
