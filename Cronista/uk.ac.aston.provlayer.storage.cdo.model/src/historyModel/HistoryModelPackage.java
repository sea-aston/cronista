/**
 */
package historyModel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see historyModel.HistoryModelFactory
 * @model kind="package"
 * @generated
 */
public interface HistoryModelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "historyModel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/historyModel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "historyModel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	HistoryModelPackage eINSTANCE = historyModel.impl.HistoryModelPackageImpl.init();

	/**
	 * The meta object id for the '{@link historyModel.impl.TimeWindowImpl <em>Time Window</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see historyModel.impl.TimeWindowImpl
	 * @see historyModel.impl.HistoryModelPackageImpl#getTimeWindow()
	 * @generated
	 */
	int TIME_WINDOW = 0;

	/**
	 * The feature id for the '<em><b>Provenance Graph</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_WINDOW__PROVENANCE_GRAPH = 0;

	/**
	 * The feature id for the '<em><b>System Model State</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_WINDOW__SYSTEM_MODEL_STATE = 1;

	/**
	 * The feature id for the '<em><b>Next Time Window</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_WINDOW__NEXT_TIME_WINDOW = 2;

	/**
	 * The feature id for the '<em><b>Previous Time Window</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_WINDOW__PREVIOUS_TIME_WINDOW = 3;

	/**
	 * The feature id for the '<em><b>Start Local Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_WINDOW__START_LOCAL_TIME = 4;

	/**
	 * The feature id for the '<em><b>End Local Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_WINDOW__END_LOCAL_TIME = 5;

	/**
	 * The number of structural features of the '<em>Time Window</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_WINDOW_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Time Window</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_WINDOW_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link historyModel.impl.SystemModelStateImpl <em>System Model State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see historyModel.impl.SystemModelStateImpl
	 * @see historyModel.impl.HistoryModelPackageImpl#getSystemModelState()
	 * @generated
	 */
	int SYSTEM_MODEL_STATE = 1;

	/**
	 * The feature id for the '<em><b>First Commit Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_MODEL_STATE__FIRST_COMMIT_TIME = 0;

	/**
	 * The feature id for the '<em><b>Point Me At Runtime Model Version</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_MODEL_STATE__POINT_ME_AT_RUNTIME_MODEL_VERSION = 1;

	/**
	 * The number of structural features of the '<em>System Model State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_MODEL_STATE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>System Model State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_MODEL_STATE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link historyModel.impl.ProvenanceGraphImpl <em>Provenance Graph</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see historyModel.impl.ProvenanceGraphImpl
	 * @see historyModel.impl.HistoryModelPackageImpl#getProvenanceGraph()
	 * @generated
	 */
	int PROVENANCE_GRAPH = 2;

	/**
	 * The feature id for the '<em><b>Agent</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVENANCE_GRAPH__AGENT = 0;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVENANCE_GRAPH__ACTIVITY = 1;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVENANCE_GRAPH__ENTITY = 2;

	/**
	 * The number of structural features of the '<em>Provenance Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVENANCE_GRAPH_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Provenance Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVENANCE_GRAPH_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link historyModel.TimeWindow <em>Time Window</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time Window</em>'.
	 * @see historyModel.TimeWindow
	 * @generated
	 */
	EClass getTimeWindow();

	/**
	 * Returns the meta object for the containment reference '{@link historyModel.TimeWindow#getProvenanceGraph <em>Provenance Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Provenance Graph</em>'.
	 * @see historyModel.TimeWindow#getProvenanceGraph()
	 * @see #getTimeWindow()
	 * @generated
	 */
	EReference getTimeWindow_ProvenanceGraph();

	/**
	 * Returns the meta object for the containment reference '{@link historyModel.TimeWindow#getSystemModelState <em>System Model State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>System Model State</em>'.
	 * @see historyModel.TimeWindow#getSystemModelState()
	 * @see #getTimeWindow()
	 * @generated
	 */
	EReference getTimeWindow_SystemModelState();

	/**
	 * Returns the meta object for the reference '{@link historyModel.TimeWindow#getNextTimeWindow <em>Next Time Window</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Next Time Window</em>'.
	 * @see historyModel.TimeWindow#getNextTimeWindow()
	 * @see #getTimeWindow()
	 * @generated
	 */
	EReference getTimeWindow_NextTimeWindow();

	/**
	 * Returns the meta object for the reference '{@link historyModel.TimeWindow#getPreviousTimeWindow <em>Previous Time Window</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Previous Time Window</em>'.
	 * @see historyModel.TimeWindow#getPreviousTimeWindow()
	 * @see #getTimeWindow()
	 * @generated
	 */
	EReference getTimeWindow_PreviousTimeWindow();

	/**
	 * Returns the meta object for the attribute '{@link historyModel.TimeWindow#getStartLocalTime <em>Start Local Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Local Time</em>'.
	 * @see historyModel.TimeWindow#getStartLocalTime()
	 * @see #getTimeWindow()
	 * @generated
	 */
	EAttribute getTimeWindow_StartLocalTime();

	/**
	 * Returns the meta object for the attribute '{@link historyModel.TimeWindow#getEndLocalTime <em>End Local Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Local Time</em>'.
	 * @see historyModel.TimeWindow#getEndLocalTime()
	 * @see #getTimeWindow()
	 * @generated
	 */
	EAttribute getTimeWindow_EndLocalTime();

	/**
	 * Returns the meta object for class '{@link historyModel.SystemModelState <em>System Model State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System Model State</em>'.
	 * @see historyModel.SystemModelState
	 * @generated
	 */
	EClass getSystemModelState();

	/**
	 * Returns the meta object for the attribute '{@link historyModel.SystemModelState#getFirstCommitTime <em>First Commit Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First Commit Time</em>'.
	 * @see historyModel.SystemModelState#getFirstCommitTime()
	 * @see #getSystemModelState()
	 * @generated
	 */
	EAttribute getSystemModelState_FirstCommitTime();

	/**
	 * Returns the meta object for the reference '{@link historyModel.SystemModelState#getPointMeAtRuntimeModelVersion <em>Point Me At Runtime Model Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Point Me At Runtime Model Version</em>'.
	 * @see historyModel.SystemModelState#getPointMeAtRuntimeModelVersion()
	 * @see #getSystemModelState()
	 * @generated
	 */
	EReference getSystemModelState_PointMeAtRuntimeModelVersion();

	/**
	 * Returns the meta object for class '{@link historyModel.ProvenanceGraph <em>Provenance Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provenance Graph</em>'.
	 * @see historyModel.ProvenanceGraph
	 * @generated
	 */
	EClass getProvenanceGraph();

	/**
	 * Returns the meta object for the containment reference list '{@link historyModel.ProvenanceGraph#getAgent <em>Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Agent</em>'.
	 * @see historyModel.ProvenanceGraph#getAgent()
	 * @see #getProvenanceGraph()
	 * @generated
	 */
	EReference getProvenanceGraph_Agent();

	/**
	 * Returns the meta object for the containment reference list '{@link historyModel.ProvenanceGraph#getActivity <em>Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Activity</em>'.
	 * @see historyModel.ProvenanceGraph#getActivity()
	 * @see #getProvenanceGraph()
	 * @generated
	 */
	EReference getProvenanceGraph_Activity();

	/**
	 * Returns the meta object for the containment reference list '{@link historyModel.ProvenanceGraph#getEntity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Entity</em>'.
	 * @see historyModel.ProvenanceGraph#getEntity()
	 * @see #getProvenanceGraph()
	 * @generated
	 */
	EReference getProvenanceGraph_Entity();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	HistoryModelFactory getHistoryModelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link historyModel.impl.TimeWindowImpl <em>Time Window</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see historyModel.impl.TimeWindowImpl
		 * @see historyModel.impl.HistoryModelPackageImpl#getTimeWindow()
		 * @generated
		 */
		EClass TIME_WINDOW = eINSTANCE.getTimeWindow();

		/**
		 * The meta object literal for the '<em><b>Provenance Graph</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_WINDOW__PROVENANCE_GRAPH = eINSTANCE.getTimeWindow_ProvenanceGraph();

		/**
		 * The meta object literal for the '<em><b>System Model State</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_WINDOW__SYSTEM_MODEL_STATE = eINSTANCE.getTimeWindow_SystemModelState();

		/**
		 * The meta object literal for the '<em><b>Next Time Window</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_WINDOW__NEXT_TIME_WINDOW = eINSTANCE.getTimeWindow_NextTimeWindow();

		/**
		 * The meta object literal for the '<em><b>Previous Time Window</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_WINDOW__PREVIOUS_TIME_WINDOW = eINSTANCE.getTimeWindow_PreviousTimeWindow();

		/**
		 * The meta object literal for the '<em><b>Start Local Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_WINDOW__START_LOCAL_TIME = eINSTANCE.getTimeWindow_StartLocalTime();

		/**
		 * The meta object literal for the '<em><b>End Local Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_WINDOW__END_LOCAL_TIME = eINSTANCE.getTimeWindow_EndLocalTime();

		/**
		 * The meta object literal for the '{@link historyModel.impl.SystemModelStateImpl <em>System Model State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see historyModel.impl.SystemModelStateImpl
		 * @see historyModel.impl.HistoryModelPackageImpl#getSystemModelState()
		 * @generated
		 */
		EClass SYSTEM_MODEL_STATE = eINSTANCE.getSystemModelState();

		/**
		 * The meta object literal for the '<em><b>First Commit Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYSTEM_MODEL_STATE__FIRST_COMMIT_TIME = eINSTANCE.getSystemModelState_FirstCommitTime();

		/**
		 * The meta object literal for the '<em><b>Point Me At Runtime Model Version</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM_MODEL_STATE__POINT_ME_AT_RUNTIME_MODEL_VERSION = eINSTANCE.getSystemModelState_PointMeAtRuntimeModelVersion();

		/**
		 * The meta object literal for the '{@link historyModel.impl.ProvenanceGraphImpl <em>Provenance Graph</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see historyModel.impl.ProvenanceGraphImpl
		 * @see historyModel.impl.HistoryModelPackageImpl#getProvenanceGraph()
		 * @generated
		 */
		EClass PROVENANCE_GRAPH = eINSTANCE.getProvenanceGraph();

		/**
		 * The meta object literal for the '<em><b>Agent</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROVENANCE_GRAPH__AGENT = eINSTANCE.getProvenanceGraph_Agent();

		/**
		 * The meta object literal for the '<em><b>Activity</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROVENANCE_GRAPH__ACTIVITY = eINSTANCE.getProvenanceGraph_Activity();

		/**
		 * The meta object literal for the '<em><b>Entity</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROVENANCE_GRAPH__ENTITY = eINSTANCE.getProvenanceGraph_Entity();

	}

} //HistoryModelPackage
