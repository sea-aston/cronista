# Cronista

[[_TOC_]]

## Introduction

Cronista is a provenance collection system for Java systems, especially those with a runtime model*. If the system has a well-defined runtime model, Cronista can track the provenance of its changes,  producing a history model that can be examined like a log file to understand “why” a system behaviour has occurred. However, unlike a log file, provenance shows the causes of a system’s state in relation to activities.

The system’s runtime model can be implemented using an Ecore model or plain Java objects. Cronista includes tools for Ecore models (CDO) and annotations + [AspectJ](https://www.eclipse.org/aspectj/) aspects for use with Java objects. There are several options for integrating Cronista, depending on the implementation of the runtime model:

* For [Eclipse Modeling Framework](https://www.eclipse.org/modeling/emf/) models, there are two options:
  * Customise the Ecore `.genmodel` to generate the model code with observer instrumentation added directly into the model code.
  * Annotate the generated code, and apply one of the included AspectJ aspects.
* Plain Java object runtime models can only be observed through annotations and AspectJ aspects.

The provenance collected from a system conforms to [W3C PROV-DM](https://www.w3.org/TR/prov-dm/) and is collected into Cronista’s History Model Store (HMS), which can use either an [Apache TinkerPop-compatible graph database](https://tinkerpop.apache.org/) (specifically, we have tested it with [JanusGraph](https://janusgraph.org/)) or an [Eclipse CDO](https://www.eclipse.org/cdo/) model repository. Both JanusGraph and CDO HMSes can be run in Docker containers on the same system as the system under study.

**Runtime model, in the case of Cronista, a runtime model is a stateful representation of the important system and environmental features.*

## Future work

Configuration for the HMS is still within the code for Cronista: future work could expose these settings in a configuration file. With the Janus Docker instance, the default configuration is for an in-memory database, and therefore turning the container off will lose the history model. With both CDO and JanusGraph, external tools are required to explore the history model: Cronista does not provide a user interface yet. Details of provenance exploring are discussed later.

## Publications

The following publications are about Cronista and may be of use in developing a deeper understanding of how the provenance is collected and can be used.

* Automated provenance graphs for models@run.time (October 2020) - [DOI](http://dx.doi.org/10.1145/3417990.3419503)
* Towards automated provenance collection for runtime models to record system history (October 2020) - [DOI](http://dx.doi.org/10.1145/3419804.3420262)
* Cronista: A multi-database automated provenance collection system for runtime-models (August 2021) - [DOI](http://dx.doi.org/10.1016/j.infsof.2021.106694)

## Downloading Cronista and related libraries

Cronista has a number of different components and relies on supporting technologies for some elements of its implementation. This section seeks to list out the various components which will be needed to use Cronista or reproduce our experiments.

### Start with a Git clone

The first step is to clone the Git repository, which contains 3 major directories. If you only want to use Cronista, then you will not require the `experiments` folder: the `CaseStudies` folder would serve as examples, so it may be still useful.

All of Cronista’s components are located in the Cronista folder. Both ECore and Java object versions are in this folder as they share many common components. Similarly, the Observer and Curator components are only separated at a project level. 

**Git clone the repo @ [https://gitlab.com/sea-aston/cronista.git](https://gitlab.com/sea-aston/cronista.git)**

### Main Folders

- CaseStudies
    - FIB
    - SUMO
    - AI-Checkers
- Cronista - Observer/Curator and instrumentation
- Experiments
    - SUMO - Code generation Vs AspectJ
    - AI-Checkers - AspectJ inspection of Java runtime model

## Cronista projects and dependencies

Having cloned the repositories, import the projects into a workspace within a recent [Eclipse Modeling](https://www.eclipse.org/downloads/packages/) distribution, then work through the dependencies for your intended use of Cronista.

### Import Cronista projects

The Cronista folder contains the Observer, Curator and related components to enable provenance collection using either ECore or AspectJ.

- *uk.ac.aston.provlayer*
- *uk.ac.aston.provlayer.annotations*
- *uk.ac.aston.provlayer.observer.instrument.aspectj*
- *uk.ac.aston.provlayer.observer.instrument.aspectj_CDO*
- *uk.ac.aston.provlayer.observer.instrument.aspectj_POJO*
- *uk.ac.aston.provlayer.storage.cdo*
- *uk.ac.aston.provlayer.storage.cdo.model*
- *uk.ac.aston.provlayer.storage.tinkerpop*
- *uk.ac.aston.provlayer.tests*

### Resolve the Ivy dependencies

**uk.ac.aston.provlayer.storage.tinkerpop** will report errors about missing required libraries.
These dependencies are managed with [Apache Ivy](https://ant.apache.org/ivy/), which will automatically retrieve the required libraries if an internet connection is available.

If using Eclipse, install [IvyDE](https://ant.apache.org/ivy/ivyde/), then right click on the project (`uk.ac.aston.provlayer.storage.tinkerpop`) and select "Ivy > Retrieve ‘dependencies’".

## Using Cronista with your own project

Cronista’s project files are Eclipse plugins, so they can be added to another Eclipse plugin project through the Manifest dependencies. If using AspectJ for Observer Instrumentation, then you will need to install AspectJ from the [Eclipse Marketplace](https://marketplace.eclipse.org/search/site/aspectj) and add AspectJ to your project.

Declare a ProvLayerControl in your main method, and call the startProvLayer method with a Provenance graph object. The provenance graph objects are returned from *TpProvenanceGraphStorage()* or *CDOProvenanceGraph()* for a Tinkerpop or CDO History Model Storage (HMS). Both methods assume the CDO/Tinkerpop Graph will be on 127.0.0.1 on the respective default ports; our docker containers for CDO/Tinkerpop do. However, the IP and Port for each can be changed in code if required.

While the implementation for configuration is still in code for most things, the Observer/Curator instances are managed by ProvLayerControl. Provenance collection can be started and stopped; using the stop provenance will process all outstanding provenance messages before closing the connection. Furthermore, it is worth mentioning the message queue buffer between the Observer and Curator is set at 10000, this could be extended if needed. However, the speed at which provenance messages are being created and Curated needs to be balanced so as not to fill the queue.

### Create the Provenance control layer, start and stop provenance collection

```java
import uk.ac.aston.provlayer.provlayercontrol.ProvLayerControl;

// PROV LAYER SETUP - pass in a ProvGraphStorage
// Starts collecting provenance
ProvLayerControl.startProvLayer(createProvenanceStorage());

// Selector for the history model type.
private static ProvenanceGraphStorage createProvenanceStorage() {
	// return new CDOProvenanceGraph();      // CDO
	return new TpProvenanceGraphStorage();   // TINKERPOP
}

// End the provenance collection
ProvLayerControl.shutdownProvLayer();
```

### Create Activity scopes

```java
import uk.ac.aston.provlayer.observer.thread.ActivityScope;

try (ActivityScope scope0 = new ActivityScope("NAME THE ACTIVTY HERE")) {
/* CODE */
}
```

### Using ECore extension (Code Generation)

The ECore extensions are intended for use with CDO based ECore models. As such a default ECore model will first need to be migrated to a CDO eDynamic.  This is done through the right-click context menu on the models ***genmodel > CDO > Migrate EMF Generator Model (Dynamic).***

In the properties of the Ecore model, find the **Model Class Defaults**

**Root Extends Class** is changed to the **LoggingCDOObjectImpl** to add the instrumented version of the base default class CDOObject.

> *LoggingCDOObjectImpl2 is the current version that should be used and can be found at: uk.ac.aston.provlayer.observer.model.cdo.LoggingCDOObjectImpl2*
> 

Run the model generator and produce a set of model classes that contain the Observer CDO Instrumentation, which will report Entities for model interactions.

### Using Model Annotations & Aspects (AspectJ)

- Add AspectJ to the target system projects
    - Right-click project folder (package explorer)
    - Configure > Convert to AspectJ Project
- Add dependencies to the Manifest
    - *uk.ac.aston.prolayer*
    - *uk.ac.aston.prolayer.storage.**
    - *uk.ac.aston.prolayer.observer.instrument.**
- Add the Observer instrument to the AspectJ build path
    - aspectj_CDO (EMF CDO)
    - aspectj_POJO (Java)
- Apply Annotations to the model (and methods for plain Java models)
    
    Can be found in **uk.ac.aston.provlayer.observer.annotation**. The annotations provided are named for the technologies/types they are intended to be used with.
    
    Examples of the usage can be found in case studies FIB3, which provides a simple Fibonacci implementation using different runtime model implementations.
    
    ```java
    public class main {
    	
    	@ModelPOJO
    	static Game game;
    	@ModelPOJO
    	static Move move;
    	
    	@CodePOJO
    	public static void main(String[] args) {
    	...
    	}
    }
    ```
    
    *****ECore models will need to use CDO eDynamic implementations***
    

## Additional development environment requirements

To work with Cronista in Eclipse Modelling Framework, there are a number of possible additional components that you might find are missing from your installation. This section will try to list out all the components that have been used in our development environment.

Cronista was developed on **Linux** using **Java-11-openjdk-amd64**

**The Case studies have been designed to run on a Linux system, using file system links and bash scripts. However, they could be run under windows but this is outside the scope of this document.*

**Eclipse Modelling Framework**

From the marketplace, you may need to install the following

- Apache IvyDE
- AspectJ Development Tools
- Epsilon2.2 (Query CDO HMS)

**Eclipse (Installer)**

- CDO Server (If you don't use a CDO Docker)

**Docker**

- Docker
- Docker-Compose
- Portainer (as a container) - WEB GUI for managing docker containers

**Docker containers**

- JanusGraph (History model storage)
- CDO (*History model storage or System Model storage)

**Provenance Exploration tools**

- Tinkerpop (JanusGraph)
    - GraphExp - [https://github.com/bricaud/graphexp](https://github.com/bricaud/graphexp)
    - Gremlin console - [https://tinkerpop.apache.org/](https://tinkerpop.apache.org/)
- CDO
    - EMF CDO Explorer (included in EMF 09-2020)
    - Epsilon2.2 (as above)

## Supportive Docker containers

Docker-compose files can be found in the experiments, these have been extracted below as examples. 

### Tinkerpop History Model Store (JanusGraph)

```yaml
version: "2.4"

services:
  repoSystem:
    image: registry.gitlab.com/sea-aston/cdo-docker:latest
    environment:
      CDO_PORT: 2037
      CDO_REPO: repoSystem
      CDO_AUDITS: "false"
      CDO_BRANCHES: "false"
    ports:
      - "2037:2037"
    tmpfs:
      - /tmp
    volumes:
      - repoSystem_data:/home/user/cdo-server/h2
      - repoSystem_config:/home/user/cdo-server/configuration
  janusgraph:
    # from https://github.com/JanusGraph/janusgraph-docker.git
    image: janusgraph/janusgraph:latest
    environment:
      JAVA_OPTIONS: -Xms2g -Xmx2g
      gremlinserver.threadPoolWorker: 4
      janusgraph.storage.backend: inmemory
    ports:
      - "8182:8182"
    # The mounted volume only makes sense if JanusGraph is being run with the BerekeleyDB storage.
    volumes:
      - "janusgraph-default-data:/var/lib/janusgraph"

volumes:
  repoSystem_data:
  repoSystem_config:
  janusgraph-default-data:

```

### CDO History Model Store / System Model

The configuration below shows two instances of CDO being used.

**repoSystem** -  CDO repository used in the SUMO case study as the system’s model repository

**repoHistory** - CDO repository being used by Cronista’s Curator

```yaml
version: "2.4"

services:
  repoSystem:
    image: registry.gitlab.com/sea-aston/cdo-docker:latest
    environment:
      CDO_PORT: 2037
      CDO_REPO: repoSystem
      CDO_AUDITS: "false"
      CDO_BRANCHES: "false"
    ports:
      - "2037:2037"
    tmpfs:
      - /tmp
    volumes:
      - repoSystem_data:/home/user/cdo-server/h2
      - repoSystem_config:/home/user/cdo-server/configuration
  repoHistory:
    image: registry.gitlab.com/sea-aston/cdo-docker:latest
    environment:
      CDO_PORT: 2036
      CDO_REPO: repoHistory
      CDO_AUDITS: "false"
      CDO_BRANCHES: "false"
    ports:
      - "2036:2036"
    tmpfs:
      - /tmp
    volumes:
      - repoHistory_data:/home/user/cdo-server/h2
      - repoHistory_config:/home/user/cdo-server/configuration

volumes:
  repoSystem_data:
  repoSystem_config:
  repoHistory_data:
  repoHistory_config:

```

## Case Studies

In this section, an overview is provided of each Case study in the CaseStudies folder. If you are more interested in implementing Cronista than reproducing our experiments, then consider these case studies as examples of how Cronista is implemented.

## FIB (Fibonacci)

FIB1 to 3 provide examples of the Cronista implementation using Java object models: FIB3 is the most complete, showing how to use Annotations with AspectJ against Java object models. The FIB3 shows how a basic runtime model can be implemented and which annotations are required for them (e.g. private/public fields).

### SUMO Traffic control

The SUMO case study is our original experimental system which was built to test the idea of collecting provenance from a runtime model such that a form of “self-explanation” can be enabled. 

#### **TrasS.jar missing?**

***TraaS.jar** Library required in /lib*

Download the all-inclusive-tarball of SUMO from: [https://sumo.dlr.de/docs/Downloads.php](https://sumo.dlr.de/docs/Downloads.php) 

In the SUMO /bin folder copy TrasS.jar to the lib folder in the SUMO project.

#### External applications for SUMO Case study

- Install **SUMO - Simulation of Urban MObility**
[https://www.eclipse.org/sumo/](https://www.eclipse.org/sumo/)
- Docker containers can be used for CDO and Tinkerpop. However, it is also possible to use CDO and Tinkerpop instances from other systems or local installations.

#### Configuration for system and history repositories

- CDO system model
    
    ```java
    // CDO Host DB info
    public static final String CDO_Hostname = "127.0.0.1";
    public static final String CDO_Port = "2037";
    public static final String CDO_Repository_System = "repoSystem";
    ```
    
- Requires a CDO/Graphdata base for History Model Storeage
    - CDO HMS
        
        ```java
        //CDO Host DB info  -- CURATOR
        private static final String DEFAULT_HOSTNAME = "127.0.0.1";
        private static final int DEFAULT_PORT = 2036;
        private static final String DEFAULT_REPONAME = "repoHistory";
        ```
        
    - Tinkerpop HMS
        
        ```java
        // Tinker-pop connection information
        private static final String DEFAULT_HOSTNAME = "localhost";
        private static final int DEFAULT_PORT = 8182;
        ```
        
    

#### Missing SUMO world?

You may need to copy the SUMO World folder from the case studies folder if you are trying to run the experiments on windows, or if the file system link is broken.

### AI-Checkers

In this case study Cronista was added to an existing Java project, which was written by another developer who was oblivious to Cronista and provenance collection. As such, Annotations are used against some refactored Java code to enable the Cronista to produce a history model of a Checkers game being played between two AI players. 

## Experiments

ReadMe files are provided in each experiment folder. However, the GIT repo does not contain compiled .jar files, these will need to be exported for each relevant Case study project needed.
