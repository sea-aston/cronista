#!/bin/bash

#experiment.sh -binary xxx -label xxx -ticks ### -historydb TINKERPOP -fault1 -fault2
# -binary (baseline/eDynCodeGen/aspect_full/aspect_part)
# -fault1 -fault2 activate a fault on controller 1 or 2 (leave out for no faults)

# Must have one number arg for the number of ticks to run the simulation
TICKS="$1"
if [ $# -ne 1 ]; then
    echo "TICKS SET TO DEFAULT 100"
    TICKS="100"
fi

for binary in baseline eDynCodeGen aspect_full aspect_part; do
    pushd $binary
    rm -rf logs docker docker_data java gremlin_results
    mkdir -p logs docker docker_data java gremlin_results
    ../verifyGraph.sh -binary "$binary" -label $binary"_"$TICKS"_Working" -ticks "$TICKS" -historydb "TINKERPOP"
    ../verifyGraph.sh -binary "$binary" -label $binary"_"$TICKS"_Fault1"  -ticks "$TICKS" -historydb "TINKERPOP" -fault1
    popd
done




#pushd baseline
#../verifyGraph.sh -binary baseline -label baseline_$TICKS_Working -ticks "$TICKS" -historydb TINKERPOP
#../verifyGraph.sh -binary baseline -label baseline_$TICKS_Fault1 -ticks "$TICKS" -historydb TINKERPOP -fault1
#popd

#pushd eDynCodeGen
#../verifyGraph.sh -binary eDynCodeGen -label eDynCodeGen_$TICKS_Working -ticks "$TICKS" -historydb TINKERPOP 
#../verifyGraph.sh -binary eDynCodeGen -label eDynCodeGen_$TICKS_Fault1 -ticks "$TICKS" -historydb TINKERPOP -fault1
#popd

#pushd aspect_full
#../verifyGraph.sh -binary aspect_full -label aspect_full_$TICKS_Working -ticks "$TICKS" -historydb TINKERPOP 
#../verifyGraph.sh -binary aspect_full -label aspect_full_$TICKS_Fault1 -ticks "$TICKS" -historydb TINKERPOP -fault1
#popd

#pushd aspect_part
#../verifyGraph.sh -binary aspect_part -label aspect_part_$TICKS_Working -ticks "$TICKS" -historydb TINKERPOP 
#../verifyGraph.sh -binary aspect_part -label aspect_part_$TICKS_Fault1 -ticks "$TICKS" -historydb TINKERPOP -fault1
#popd
