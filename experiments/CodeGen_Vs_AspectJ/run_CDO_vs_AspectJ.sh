#!/bin/bash

experimentFolder="AMD_CDO_vs_AspectJ"
ticksets=(200 1000 5000)

for tick in "${ticksets[@]}" ;do
	folder="$1-$experimentFolder-$tick"	
	echo "---COPYING---  $experimentFolder TO $folder"
	cp -R $experimentFolder $folder
	sleep 5s
done

for tick in "${ticksets[@]}" ;do
	folder="$1-$experimentFolder-$tick"
	pushd $folder
		echo "---EXPERIMENT---  $1-$experimentFolder-$tick"
		sleep 5s
		./run-all-experiments.sh "$tick"
	popd
done
		
for tick in "${ticksets[@]}" ;do
	folder="$1-$experimentFolder-$tick"
	pushd $folder		
		echo "---EXTRACT---  $1-$experimentFolder-$tick"
		./extract-stats.sh
		sleep 1s		
		echo "---RESULTS---  $folder-results.txt"
		sleep 1s
		./AspectJ-vs-CDO.R $tick > $folder-results.txt
	popd
done

resultsFolder="$1-$experimentFolder-RESULTS"
echo $resultsFolder
mkdir "$resultsFolder"

for tick in "${ticksets[@]}" ;do
	folder="$1-$experimentFolder-$tick"
	#cp "$folder/$tick"_PeakJavaWallTime.csv"" "$resultsFolder"
	echo "$folder" >> "$resultsFolder/PeakJavaWallTime.csv"
	cat "$folder/$tick"_PeakJavaWallTime.csv"" >> "$resultsFolder/PeakJavaWallTime.csv"

	#cp "$folder/$tick"_PeakJavaMemory.csv"" "$resultsFolder"
	echo "$folder" >> "$resultsFolder/PeakJavaMemory.csv"
	cat "$folder/$tick"_PeakJavaMemory.csv"" >> "$resultsFolder/PeakJavaMemory.csv"

	#cp "$folder/$tick"_PeakDockerNet.csv"" "$resultsFolder"
	echo "$folder" >> "$resultsFolder/PeakDockerNet.csv"
	cat "$folder/$tick"_PeakDockerNet.csv"" >> "$resultsFolder/PeakDockerNet.csv"

	#cp "$folder/$tick"_PeakDockerMemory.csv"" "$resultsFolder"
	echo "$folder" >> "$resultsFolder/PeakDockerMemory.csv"
	cat "$folder/$tick"_PeakDockerMemory.csv"" >> "$resultsFolder/PeakDockerMemory.csv"

done
