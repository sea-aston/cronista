#!/bin/bash

baseCopy="AMD_CDO_vs_AspectJ"
testCopy="$1_$baseCopy-VerifyTest"

rm -rf $testCopy
cp -r $baseCopy $testCopy

pushd $testCopy
    ./verify_ALL_graphs.sh "1000"
popd
