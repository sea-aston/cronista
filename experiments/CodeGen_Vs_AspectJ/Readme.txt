AMD_CDO_vs_AspectJ is the base folder of the experiment, it provides the framework where the JAR files for testing need to be added.

There are 4 version of the system that will need to be exported from EMF each resulting JAR is to be placed in the sub folder for the project.

The script "run_CDO_vs_AspectJ.sh ExperimentName" will copy the base folder 3 times using the "ExperimentName" and different tick counts. Each experiment subfolder contains an experiments for the given tick rate, which will be run against the 4 projects 10 times.

Once the experiements have completed (which will take hours), a folder with the "ExperimentName-AMD_CDO_vs_AspectJ-RESULTS" will appear containing 4 CSV files with the metrics collected.

A second script is provide, which can be run that will verify the graphs produced by the projects in the base folder.  "run_CDO_vs_AspectJ_VerifyTest.sh ExperimentName" will copy the base folder to "ExperimentName_CDO_vs_AspectJ-VerifyTest". Results for each project are in each projects subfolder in a folder called gremlin_results.
