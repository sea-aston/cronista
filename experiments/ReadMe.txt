This folder contains some experiments that can be performed on the Case studies.

The subfolders contain the following

CodeGen_Vs_AspectJ:
Can be used to compare Cronista's Code generation approach with AspectJ.

Experiment_Scripts:
Contains copies of the scripts used to create the experiments, these can be copied and modified to create new experiments

Our Results:
Copies of the experiments we ran and the results that were collected for papers.
