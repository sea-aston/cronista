#!/bin/bash
prefix=$1
if [ $# -ne 1 ]; then
    echo "DEFAULT LABEL"
    prefix="DEFAULT"
fi

experimentFolder="AIC_base_prov"
seedsets=(1 2 3 4 5)
#seedsets=(5)

for seed in "${seedsets[@]}" ;do
	folder="$prefix-$experimentFolder-$seed"	
	echo "---COPYING---  $experimentFolder TO $folder"
	cp -R $experimentFolder $folder
	sleep 5s
done

for seed in "${seedsets[@]}" ;do
	folder="$prefix-$experimentFolder-$seed"
	pushd $folder
		echo "---EXPERIMENT---  $1-$experimentFolder-$seed"
		sleep 5s
		./run-all-experiments.sh "$seed"
	popd
done
		
for seed in "${seedsets[@]}" ;do
	folder="$prefix-$experimentFolder-$seed"
	pushd $folder		
		echo "---EXTRACT---  $prefix-$experimentFolder-$seed"
		./extract-stats.sh
		sleep 1s		
		echo "---RESULTS---  $folder-results.txt"
		./AIC.R $seed > $folder-results.txt
	popd
done

resultsFolder="$prefix-$experimentFolder-RESULTS"
echo $resultsFolder
mkdir "$resultsFolder"

for seed in "${seedsets[@]}" ;do
	folder="$prefix-$experimentFolder-$seed"
	#cp "$folder/$seed"_PeakJavaWallseed.csv"" "$resultsFolder"
	echo "$folder" >> "$resultsFolder/PeakJavaWallTime.csv"
	cat "$folder/$seed"_PeakJavaWallTime.csv"" >> "$resultsFolder/PeakJavaWallTime.csv"

	#cp "$folder/$seed"_PeakJavaMemory.csv"" "$resultsFolder"
	echo "$folder" >> "$resultsFolder/PeakJavaMemory.csv"
	cat "$folder/$seed"_PeakJavaMemory.csv"" >> "$resultsFolder/PeakJavaMemory.csv"

	#cp "$folder/$seed"_PeakDockerNet.csv"" "$resultsFolder"
	echo "$folder" >> "$resultsFolder/PeakDockerNet.csv"
	cat "$folder/$seed"_PeakDockerNet.csv"" >> "$resultsFolder/PeakDockerNet.csv"

	#cp "$folder/$seed"_PeakDockerMemory.csv"" "$resultsFolder"
	echo "$folder" >> "$resultsFolder/PeakDockerMemory.csv"
	cat "$folder/$seed"_PeakDockerMemory.csv"" >> "$resultsFolder/PeakDockerMemory.csv"

done
