#!/bin/bash

SEED="$1"

pushd base_AIC
../experiment.sh -binary base_AIC -cpuplayer1 -cpuplayer2 -loadfile n -movesfirst 1 -time 5 -seed "$SEED" -watchdog 100
popd

pushd prov_AIC
../experiment.sh -binary prov_AIC -cpuplayer1 -cpuplayer2 -loadfile n -movesfirst 1 -time 5 -seed "$SEED" -watchdog 100
popd