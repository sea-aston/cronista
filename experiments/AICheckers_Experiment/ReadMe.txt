In this experiment two version of the AI-Checkers project with and without Cronista can be compared.

AIC_base_prov is the base experiment folder, which will be copied by the script run_AIC_base_prov.sh (ExperimentName-AIC_base_prov-Seed). 
In AIC_base_prov there are two subfolders which need to contain JAR files exported for each AI-Checkers project (base_AIC.jar/prov_AIC.jar).
Execute "run_AIC_base_prov.sh ExperimentName" script will run both projects for 10 runs of each game seed 1 to 5 and collect the metrics to a "ExperimentName-AIC_base_prov-RESULTS"

If needed there is a script pool_results.sh which can be run to collect results from a set of "ExperimentName-AIC_base_prov-Seed" folders.

Finally there are some scripts in the SeedFinder which can be used to test AIC game seeds against different watchdog timers, this is not required for the main experiment script as seeds have been found.
