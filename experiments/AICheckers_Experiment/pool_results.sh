#!/bin/bash

prefix=$1

experimentFolder="AIC_base_prov"
seedsets=(1 2 3 4 5)

resultsFolder="$prefix-$experimentFolder-RESULTS"
echo $resultsFolder
mkdir "$resultsFolder"

for seed in "${seedsets[@]}" ;do
	folder="$prefix-$experimentFolder-$seed"
	#cp "$folder/$seed"_PeakJavaWallseed.csv"" "$resultsFolder"
	echo "$folder" >> "$resultsFolder/PeakJavaWallTime.csv"
	cat "$folder/$seed"_PeakJavaWallTime.csv"" >> "$resultsFolder/PeakJavaWallTime.csv"

	#cp "$folder/$seed"_PeakJavaMemory.csv"" "$resultsFolder"
	echo "$folder" >> "$resultsFolder/PeakJavaMemory.csv"
	cat "$folder/$seed"_PeakJavaMemory.csv"" >> "$resultsFolder/PeakJavaMemory.csv"

	#cp "$folder/$seed"_PeakDockerNet.csv"" "$resultsFolder"
	echo "$folder" >> "$resultsFolder/PeakDockerNet.csv"
	cat "$folder/$seed"_PeakDockerNet.csv"" >> "$resultsFolder/PeakDockerNet.csv"

	#cp "$folder/$seed"_PeakDockerMemory.csv"" "$resultsFolder"
	echo "$folder" >> "$resultsFolder/PeakDockerMemory.csv"
	cat "$folder/$seed"_PeakDockerMemory.csv"" >> "$resultsFolder/PeakDockerMemory.csv"

done