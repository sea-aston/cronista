#!/usr/bin/env python3

import argparse
import csv
import sys


def read_csv(fp):
  with open(fp, 'r') as csvfile:
    reader = csv.DictReader(csvfile)
    result = {'file': fp, 'elapsedWallMillis': 0, 'peakMemoryBytes': 0}

    for row in reader:
      result['elapsedWallMillis'] = max(
        result['elapsedWallMillis'], int(row['elapsedWallClockMillis']))
      result['peakMemoryBytes'] = max(
        result['peakMemoryBytes'], int(row['totalMemoryBytes']))

    return result


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Aggregate runs into one file')
  parser.add_argument('files', nargs='+', help='memory-*.csv files to be processed')
  args = parser.parse_args()

  results = [read_csv(f) for f in args.files]
  writer = csv.DictWriter(
    sys.stdout, fieldnames=('file', 'elapsedWallMillis', 'peakMemoryBytes'))
  writer.writeheader()
  for result in results:
    writer.writerow(result)
