#!/bin/bash

wait_for_port() {
    PORT="$1"
    while ! nc -z localhost "$PORT"; do
        sleep 0.1
    done
}

experiment() {
    RUN_ID="$1"
    mkdir -p docker docker_data
    docker-compose down -v

    ./docker-stats-watch > docker/docker-stats-$RUN_ID.txt &
    DOCKER_STATS_PID="$!"

    docker-compose up -d &
    wait_for_port 2036
    wait_for_port 2037
    sleep 20s

    java -jar *.jar
    docker system df -v > docker_data/docker-volume-$RUN_ID.txt
    kill "$DOCKER_STATS_PID"
    wait
}

rm -rf logs docker docker_data
mkdir -p logs docker docker_data
for i in `seq 1 10`; do
    experiment "$i"
done
docker-compose down -v
