#!/bin/bash

wait_for_port() {
    PORT="$1"
    while ! nc -z localhost "$PORT"; do
        sleep 0.1
    done
}

experiment() {
    RUN_ID="$1"
    ./docker-stats-watch > docker-stats-$RUN_ID.txt &
    DOCKER_STATS_PID="$!"

    docker-compose up -d &
    wait_for_port 2037
    sleep 30s

    java -jar cdo-experiment-janus-inmemory.jar
    docker-compose down -v
    kill "$DOCKER_STATS_PID"
    wait
}

mkdir -p logs
for i in `seq 1 10`; do
    experiment "$i"
done
