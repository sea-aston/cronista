#!/usr/bin/env python3

import argparse
import csv
import re
import sys

from collections import defaultdict
from datetime import datetime

# Month, day, HH:MM:SS
RE_TIMESTAMP = re.compile(r'^[a-zA-Z]+ [0-9]+ [0-9]+:[0-9]+:[0-9]+')

RE_MIB = re.compile(r'([.0-9]+)(MiB|kiB|B)')
RE_MB_KB = re.compile(r'([.0-9]+)(MB|kB|B)')
RE_NAME = re.compile(r'\w\S*')

FROM_UNIT_TO_BYTE = {
  'B': 1,
  'kB': 1000,
  'MB': 1000**2,
  'kiB': 1024,
  'MiB': 1024**2,
}

def read_log(fp):
  with open(fp, 'r') as logfile:
    start_time = None
    for l in logfile:
      if 'CONTAINER ID' in l:
        # The 'docker stats' output is column-aligned and only uses spaces:
        # we can simply figure out where the column starts.
        mem_usage_column = l.index('MEM USAGE')
        net_io_column = l.index('NET I/O')
        name_column = l.index('NAME')
      elif mem_usage_column != -1 and net_io_column != -1:
        match_timestamp = RE_TIMESTAMP.match(l)
        match_name = RE_NAME.match(l[name_column:])
        match_mem = RE_MIB.match(l[mem_usage_column:])
        match_net = RE_MB_KB.match(l[net_io_column:])
        if match_timestamp and match_name and match_mem and match_net:
          line_time = datetime.strptime(match_timestamp.group(0), "%b %d %H:%M:%S").timestamp()
          if start_time is None:
            start_time = line_time

          machine_name = match_name.group(0)
          mem_bytes = float(match_mem.group(1)) * FROM_UNIT_TO_BYTE[match_mem.group(2)]
          net_bytes = float(match_net.group(1)) * FROM_UNIT_TO_BYTE[match_net.group(2)]

          yield {
            'file': fp,
            'machine': machine_name,
            'seconds': line_time - start_time,
            'memory_bytes': mem_bytes,
            'netIO_bytes': net_bytes
          }


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Aggregate Docker runs into one file')
  parser.add_argument('files', nargs='+', help='docker-stats-*.txt files to be processed')
  args = parser.parse_args()

  writer = csv.DictWriter(
    sys.stdout, fieldnames=('file', 'machine', 'seconds', 'memory_bytes', 'netIO_bytes'))
  writer.writeheader()
  for f in args.files:
    for row in read_log(f):
      writer.writerow(row)
