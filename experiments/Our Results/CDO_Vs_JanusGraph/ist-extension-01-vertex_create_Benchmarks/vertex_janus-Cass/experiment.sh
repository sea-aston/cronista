#!/bin/bash

wait_for_port() {
    PORT="$1"
    while ! nc -z localhost "$PORT"; do
        sleep 0.1
    done
}

experiment() {
    RUN_ID="$1"
    ./docker-stats-watch > ./docker_data/docker-stats-$RUN_ID.txt &
    DOCKER_STATS_PID="$!"

    docker-compose up -d &
    wait_for_port 8182
    sleep 60s

    echo start

    java -jar SpeedTest.jar > ./console/console-$RUN_ID.csv &
    
    sleep 60s
    echo end

    docker-compose down 
    kill "$DOCKER_STATS_PID"
    wait
}

mkdir -p logs
for i in `seq 1 10`; do
    experiment "$i"
done
