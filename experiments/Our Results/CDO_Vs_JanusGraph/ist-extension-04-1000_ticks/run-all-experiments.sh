#!/bin/bash

TICKS=1000

pushd cdo
../experiment.sh cdo "$TICKS"
popd

pushd janus-inmemory
../experiment.sh tinkerpop "$TICKS"
popd

pushd noprov
../experiment.sh none "$TICKS"
popd
