# No provenance resource consumption

This experiment has been done over 10 runs, with the settings below.

We measure the resource usage when there is no curator thread running over the example simulation.

The hardware information and Java/OS version are kept as files in the `info` folder.

The Java memory and time usage have been measured with a background thread in the experiment program.

## ExperimentMAIN

```java
public class ExperimentMAIN {
  // Controls for running Experiments
  private static final int SIMULATION_STEPS_TO_RUN = 200; // Simulation steps to run
  static boolean verbose = false; // Turn on Debug messages
	
  // EXPERIEMENT CONTROLS
  public static final String EXPERIMENTLABEL = "HISTORY_MARKUP";

  public static final Boolean CONSOLE1ON = false;
  public static final Boolean CONSOLE2ON = false;
  public static final Boolean FAULT1ON = false;
  public static final Boolean FAULT2ON = true;
```

## Curator

```java
public class Curator implements ObserverMessageProcessor, Runnable {
  public static final Long MAX_TIME_WINDOW_MILLIS = (long) 1000 * 10;

  // The curator prints the size of the message queue every X messages
  private static final int PRINT_QSIZE_EVERY = 100;
```

## JunctionController

```java
public class JunctionController implements Runnable {
  private static final int DECISION_FREQUENCY_INTERVAL = 1000;
  private static final int LANE_AREA_DETECTOR_CAPACITY = 6;
```
