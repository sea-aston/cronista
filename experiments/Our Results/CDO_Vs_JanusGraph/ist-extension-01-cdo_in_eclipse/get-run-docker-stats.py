#!/usr/bin/env python3

import argparse
import csv
import sys

import re

RE_MIB = re.compile(r'([.0-9]+)MiB')
RE_MB = re.compile(r'([.0-9]+)MB')

def read_log(fp):
  with open(fp, 'r') as logfile:
    peak_mem_mib = 0
    peak_net_mb = 0

    for l in logfile:
      if 'CONTAINER ID' in l:
        # The 'docker stats' output is column-aligned and only uses spaces:
        # we can simply figure out where the column starts.
        mem_usage_column = l.index('MEM USAGE')
        net_io_column = l.index('NET I/O')
      elif mem_usage_column != -1 and net_io_column != -1:
        match_mem = RE_MIB.match(l[mem_usage_column:])
        match_net = RE_MB.match(l[net_io_column:])
        if match_mem:
          mem_mib = float(match_mem.group(1))
          peak_mem_mib = max(peak_mem_mib, mem_mib)
        if match_net:
          net_mb = float(match_net.group(1))
          peak_net_mb = max(peak_net_mb, net_mb)

    return {
      'file': fp,
      'peakMemoryMiB': peak_mem_mib,
      'peakNetIOMiB': peak_net_mb,
    }


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Aggregate Docker runs into one file')
  parser.add_argument('files', nargs='+', help='docker-stats-*.txt files to be processed')
  args = parser.parse_args()

  results = [read_log(f) for f in args.files]
  writer = csv.DictWriter(
    sys.stdout, fieldnames=('file', 'peakMemoryMiB', 'peakNetIOMiB'))
  writer.writeheader()
  for result in results:
    writer.writerow(result)
