#!/bin/bash

PREFIX="$(date +'%Y%m%d')"

for folder in cdo janus-inmemory noprov; do
    ./get-run-docker-stats.py "$folder/docker/docker"* > "$PREFIX-$folder-docker.csv"
    ./get-timed-run-docker-stats.py "$folder/docker/docker"* > "$PREFIX-$folder-docker-timed.csv"
    ./get-run-java-stats.py "$folder/java/memory"*.csv > "$PREFIX-$folder-java.csv"
done
