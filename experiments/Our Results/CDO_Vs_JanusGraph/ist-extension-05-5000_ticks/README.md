# Third run: SUMO autoclosing

This experiment was done with a new build of the experiment which automatically
closes SUMO when the simulation is over, so it can run in an unattended manner.
The simulation has been run over 5000 ticks.

The experiment.sh scripts have also been tweaked so it does not count server
shutdown time in the "docker stats" log.
