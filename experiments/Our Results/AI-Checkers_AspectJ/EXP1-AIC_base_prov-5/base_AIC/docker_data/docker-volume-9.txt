Images space usage:

REPOSITORY                                 TAG             IMAGE ID       CREATED         SIZE      SHARED SIZE   UNIQUE SIZE   CONTAINERS
lscr.io/linuxserver/medusa                 latest          fac953c4d865   7 weeks ago     207MB     0B            207MB         0
portainer/portainer-ce                     latest          a1c22f3d250f   8 weeks ago     251.8MB   0B            251.8MB       1
ghcr.io/linuxserver/jackett                latest          86754eadd059   9 months ago    260.2MB   105.7MB       154.4MB       0
vsc-volume-inspect                         latest          1c1c7fa35444   9 months ago    133.1MB   5.591MB       127.5MB       1
ghcr.io/linuxserver/sonarr                 latest          f8dd7a87873b   9 months ago    627.3MB   105.7MB       521.5MB       0
ghcr.io/linuxserver/plex                   latest          9c2617e38cc7   9 months ago    679.5MB   0B            679.5MB       0
ghcr.io/linuxserver/transmission           latest          7dcd6a71dec5   9 months ago    92.63MB   24.73MB       67.9MB        0
ghcr.io/linuxserver/medusa                 latest          5a4add6ea5b5   9 months ago    201.1MB   0B            201.1MB       0
ghcr.io/linuxserver/couchpotato            latest          bca4abdcc819   9 months ago    75.23MB   24.73MB       50.5MB        0
cdo-server                                 latest          19c27a178612   10 months ago   658.7MB   647.4MB       11.29MB       0
maven                                      3-openjdk-11    4697fda0be04   10 months ago   658.6MB   647.4MB       11.26MB       0
openjdk                                    11              4a438d74f9ed   10 months ago   647.4MB   647.4MB       0B            0
registry.gitlab.com/sea-aston/cdo-docker   latest          a6795f5352c6   11 months ago   639.8MB   293.6MB       346.2MB       1
janusgraph/janusgraph                      latest          4d1040e88f56   11 months ago   1.09GB    513.8MB       576.3MB       2
openjdk                                    8-jdk           3d06e674f731   12 months ago   513.8MB   513.8MB       0B            0
hello-world                                latest          bf756fb1ae65   24 months ago   13.34kB   0B            13.34kB       0
alpine                                     3.11.2          cc0abc535e36   2 years ago     5.591MB   5.591MB       0B            0
python                                     3.7-alpine3.9   d009576af580   2 years ago     102.3MB   0B            102.3MB       0

Containers space usage:

CONTAINER ID   IMAGE                                             COMMAND                  LOCAL VOLUMES   SIZE      CREATED         STATUS                     NAMES
68017d231c83   janusgraph/janusgraph:latest                      "docker-entrypoint.s…"   1               38.8kB    4 minutes ago   Up 4 minutes               base_aic_janusgraph_1
175a7162e018   portainer/portainer-ce:latest                     "/portainer"             1               0B        4 weeks ago     Up 8 hours                 portainer
6ff6309f7dc6   registry.gitlab.com/sea-aston/cdo-docker:latest   "/usr/local/bin/entr…"   2               1.88kB    2 months ago    Exited (137) 7 weeks ago   cdo-docker_cdo_1
bb8ed3418b93   vsc-volume-inspect                                "/bin/sh -c 'echo Co…"   2               36.5kB    9 months ago    Exited (0) 9 months ago    heuristic_curran
751197f61322   janusgraph/janusgraph:latest                      "docker-entrypoint.s…"   1               10.1MB    9 months ago    Exited (137) 10 days ago   janusgraph-memory

Local Volumes space usage:

VOLUME NAME                                 LINKS     SIZE
vscode                                      1         106MB
base_aic_janusgraph-default-data            1         0B
janusgraph-docker_janusgraph-default-data   2         0B
portainer_data                              1         138.7kB
cdo-docker_repo_config                      1         124.7kB
cdo-docker_repo_data                        1         1.163MB

Build cache usage: 0B

CACHE ID   CACHE TYPE   SIZE      CREATED   LAST USED   USAGE     SHARED
