#!/usr/bin/env python3

import argparse
import csv
import sys

import re

from collections import defaultdict

RE_MIB = re.compile(r'([.0-9]+)MiB')
RE_MB_KB = re.compile(r'([.0-9]+)(MB|kB)')
NO_SPACES = re.compile(r'\S+')

def read_log(fp):
  with open(fp, 'r') as logfile:
    peak_mem_mib = defaultdict(lambda: 0)
    peak_net_kb = defaultdict(lambda: 0)

    for l in logfile:
      if 'CONTAINER ID' in l:
        # The 'docker stats' output is column-aligned and only uses spaces:
        # we can simply figure out where the column starts.
        mem_usage_column = l.index('MEM USAGE')
        net_io_column = l.index('NET I/O')
        name_column = l.index('NAME')
      elif mem_usage_column != -1 and net_io_column != -1:
        match_name = NO_SPACES.match(l[name_column:])
        match_mem = RE_MIB.match(l[mem_usage_column:])
        match_net = RE_MB_KB.match(l[net_io_column:])
        if match_name:
          machine_name = match_name.group(0)
          if match_mem:
            mem_mib = float(match_mem.group(1))
            peak_mem_mib[machine_name] = max(peak_mem_mib[machine_name], mem_mib)
          if match_net:
            net_kb = float(match_net.group(1)) * (1 if match_net.group(2) == 'kB' else 1000)
            peak_net_kb[machine_name] = max(peak_net_kb[machine_name], net_kb)

    for key, value in peak_mem_mib.items():
      yield {
        'file': fp,
        'machine': key,
        'peakMemory_MiB': value,
        'peakNetIO_kB': peak_net_kb[key]
      }


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Aggregate Docker runs into one file')
  parser.add_argument('files', nargs='+', help='docker-stats-*.txt files to be processed')
  args = parser.parse_args()

  writer = csv.DictWriter(
    sys.stdout, fieldnames=('file', 'machine', 'peakMemory_MiB', 'peakNetIO_kB'))
  writer.writeheader()
  for f in args.files:
    for row in read_log(f):
      writer.writerow(row)
