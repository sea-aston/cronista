Images space usage:

REPOSITORY                                 TAG             IMAGE ID       CREATED         SIZE      SHARED SIZE   UNIQUE SIZE   CONTAINERS
lscr.io/linuxserver/medusa                 latest          fac953c4d865   2 weeks ago     207MB     0B            207MB         0
ghcr.io/linuxserver/jackett                latest          86754eadd059   8 months ago    260.2MB   105.7MB       154.4MB       0
vsc-volume-inspect                         latest          1c1c7fa35444   8 months ago    133.1MB   5.591MB       127.5MB       1
ghcr.io/linuxserver/sonarr                 latest          f8dd7a87873b   8 months ago    627.3MB   105.7MB       521.5MB       0
ghcr.io/linuxserver/plex                   latest          9c2617e38cc7   8 months ago    679.5MB   0B            679.5MB       0
ghcr.io/linuxserver/transmission           latest          7dcd6a71dec5   8 months ago    92.63MB   24.73MB       67.9MB        0
ghcr.io/linuxserver/medusa                 latest          5a4add6ea5b5   8 months ago    201.1MB   0B            201.1MB       0
ghcr.io/linuxserver/couchpotato            latest          bca4abdcc819   8 months ago    75.23MB   24.73MB       50.5MB        0
cdo-server                                 latest          19c27a178612   9 months ago    658.7MB   647.4MB       11.29MB       0
maven                                      3-openjdk-11    4697fda0be04   9 months ago    658.6MB   647.4MB       11.26MB       0
openjdk                                    11              4a438d74f9ed   9 months ago    647.4MB   647.4MB       0B            0
registry.gitlab.com/sea-aston/cdo-docker   latest          a6795f5352c6   10 months ago   639.8MB   293.6MB       346.2MB       2
janusgraph/janusgraph                      latest          4d1040e88f56   10 months ago   1.09GB    513.8MB       576.3MB       2
openjdk                                    8-jdk           3d06e674f731   11 months ago   513.8MB   513.8MB       0B            0
hello-world                                latest          bf756fb1ae65   22 months ago   13.34kB   0B            13.34kB       0
alpine                                     3.11.2          cc0abc535e36   23 months ago   5.591MB   5.591MB       0B            0
python                                     3.7-alpine3.9   d009576af580   23 months ago   102.3MB   0B            102.3MB       0

Containers space usage:

CONTAINER ID   IMAGE                                             COMMAND                  LOCAL VOLUMES   SIZE      CREATED              STATUS                     NAMES
0f979197ba32   janusgraph/janusgraph:latest                      "docker-entrypoint.s…"   1               37.8kB    About a minute ago   Up About a minute          aspect_full_janusgraph_1
352846b715da   registry.gitlab.com/sea-aston/cdo-docker:latest   "/usr/local/bin/entr…"   2               1.88kB    About a minute ago   Up About a minute          aspect_full_repoSystem_1
6ff6309f7dc6   registry.gitlab.com/sea-aston/cdo-docker:latest   "/usr/local/bin/entr…"   2               1.88kB    4 weeks ago          Exited (137) 2 weeks ago   cdo-docker_cdo_1
bb8ed3418b93   vsc-volume-inspect                                "/bin/sh -c 'echo Co…"   2               36.5kB    8 months ago         Exited (0) 8 months ago    heuristic_curran
751197f61322   janusgraph/janusgraph:latest                      "docker-entrypoint.s…"   1               9.65MB    8 months ago         Exited (137) 6 days ago    janusgraph-memory

Local Volumes space usage:

VOLUME NAME                                 LINKS     SIZE
janusgraph-docker_janusgraph-default-data   2         0B
aspect_full_repoSystem_data                 1         2.181MB
cdo-docker_repo_config                      1         124.7kB
vscode                                      1         106MB
aspect_full_repoSystem_config               1         117.1kB
cdo-docker_repo_data                        1         1.163MB
aspect_full_janusgraph-default-data         1         0B

Build cache usage: 0B

CACHE ID   CACHE TYPE   SIZE      CREATED   LAST USED   USAGE     SHARED
