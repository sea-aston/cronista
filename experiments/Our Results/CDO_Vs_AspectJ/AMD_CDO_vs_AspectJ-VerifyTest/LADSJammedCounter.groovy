experimentLabel=args[0]
server="localhost"
query="g.V().has('Entity','AttributeName','PhaseEnded').has('AttributeValue','true').out('WasGeneratedBy').out('Used').has('AttributeName','EndPhase').out('WasGeneratedBy').out('Used').has('AttributeName','LADsJammed').valueMap()" 

println "Experiment Label: "+experimentLabel

println "Server: "+server
 cluster=Cluster.build(server).create()
 client=cluster.connect()

 println "Query: "+query
 r = client.submit(query).toList()

 println "Result: "
 r.each{println it}