#!/bin/bash

# If you gremlin.sh isn't in your $PATH then set this to point to gremlin.sh
Gremlin="gremlin.sh"

# Graph verification script
# To be run from each of the subfolders: use verify_ALL_Graphs.sh

experiment() {
    RUN_ID="$1"
    shift
    mkdir -p docker docker_data
    docker-compose down -v

    ../docker-stats-watch > docker/$4-docker-stats-$RUN_ID.txt &
    DOCKER_STATS_PID="$!"

    docker-compose up -d &
    sleep 40s

    java -jar ./"$2".jar "$@"
    docker system df -v > docker_data/$4-docker-volume-$RUN_ID.txt

    $Gremlin -e ../LADSJammedCounter.groovy $4 > gremlin_results/$4-gremlin-results-$RUN_ID.txt

    kill "$DOCKER_STATS_PID"
    wait
}

#rm -rf logs docker docker_data java gremlin_results
#mkdir -p logs docker docker_data java gremlin_results

# How many times to run the same test (1 of 1)
for i in `seq 1 1`; do
    experiment "$i" "$@"
done

docker-compose down -v
mv logs/memory* java
rm -rf logs result.xmi
mkdir -p logs
