#!/usr/bin/Rscript

options(echo=TRUE)

javaBase <- cbind(read.csv('20210213-noprov-java.csv'), version='base')
javaCDO <- cbind(read.csv('20210213-cdo-java.csv'), version='cdo')
javaJanus <- cbind(read.csv('20210213-janus-inmemory-java.csv'), version='janus-inmemory')
javaAll <- rbind(javaBase, javaCDO, javaJanus)

# Mean/SD peak memory + time elapsed for base, CDO and in-memory JanusGraph (JVM)
aggregate(elapsedWallMillis/1000 ~ version, javaAll, function(x) c(mean = mean(x), sd = sd(x)))
aggregate(peakMemoryBytes/(1024*1024) ~ version, javaAll, function(x) c(mean = mean(x), sd = sd(x)))

# Mean/SD peak memory/net usage for in-memory JanusGraph
dockerBase <- read.csv('20210213-noprov-docker.csv')
dockerCDO <- read.csv('20210213-cdo-docker.csv')
dockerJanus <- read.csv('20210213-janus-inmemory-docker.csv')
dockerAll <- rbind(dockerBase, dockerCDO, dockerJanus)

# Mean/SD peak memory + net I/O for base, CDO and in-memor JanusGraph (Docker containers)
aggregate(peakMemory_MiB ~ machine, dockerAll, function(x) c(mean = mean(x), sd = sd(x)))
aggregate(peakNetIO_kB ~ machine, dockerAll, function(x) c(mean = mean(x), sd = sd(x)))
