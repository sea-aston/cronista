# SUMO autoclosing resource experiments

This experiment was done with a new build of the experiment which automatically
closes SUMO when the simulation is over, so it can run in an unattended manner.
The simulation has been run over 5000 ticks.

The experiment.sh scripts have also been tweaked so it does not count server
shutdown time in the "docker stats" log.

Place the .jar file to be tested in the respective subfolder 
 -AspectJ_Insp_full
 -AspectJ_Insp_full
 -CDO_Inst

 If the subfolders are renamed the scripts will need to be updated.

 Run experiments using ./run-all-experiments.sh
 The run-all-experiments.sh will calls experiment.sh which executes
 for each sunfolder containing a jar. Once the runs are complete, 
 collect the data with ./extract-stats.sh.

 The R script needs to be edited to process the resulting csv files.

