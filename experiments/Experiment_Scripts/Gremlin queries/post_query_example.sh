#!/bin/bash
Server=$1
Query=$2

if [ "$#" -ne 2]; then
    echo "Usage: ./checkGraph.sh Server Query"
    exit 1
fi

gremlin.sh -e post_query.groovy "$Server" "$Query"