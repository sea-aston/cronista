#!/bin/bash

TICKS=200

pushd baseline
../experiment.sh -binary baseline -label baseline_$TICKS -ticks "$TICKS" -historydb TINKERPOP 
popd

pushd eDynCodeGen
../experiment.sh -binary eDynCodeGen -label eDynCodeGen_$TICKS -ticks "$TICKS" -historydb TINKERPOP 
popd

pushd aspect_full
../experiment.sh -binary aspect_full -label aspect_full_$TICKS -ticks "$TICKS" -historydb TINKERPOP 
popd

pushd aspect_part
../experiment.sh -binary aspect_part -label aspect_part_$TICKS -ticks "$TICKS" -historydb TINKERPOP 
popd
