#!/bin/bash

# Experiment script
# To be run from each of the technology subfolders: use run-all-experiments.sh

experiment() {
    RUN_ID="$1"
    shift
    mkdir -p docker docker_data
    docker-compose down -v

    ../docker-stats-watch > docker/docker-stats-$RUN_ID.txt &
    DOCKER_STATS_PID="$!"

    docker-compose up -d &
    sleep 20s

    java -jar ../experiment.jar "$@"
    docker system df -v > docker_data/docker-volume-$RUN_ID.txt
    kill "$DOCKER_STATS_PID"
    wait
}

rm -rf logs docker docker_data java
mkdir -p logs docker docker_data java

for i in `seq 1 10`; do
    experiment "$i" "$@"
done

docker-compose down -v
mv logs/memory* java
rm -rf logs result.xmi
