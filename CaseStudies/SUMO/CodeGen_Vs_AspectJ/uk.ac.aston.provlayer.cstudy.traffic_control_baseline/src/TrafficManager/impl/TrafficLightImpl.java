/**
 */
package TrafficManager.impl;

import TrafficManager.TrafficLight;
import TrafficManager.TrafficManagerPackage;

import java.util.Collection;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.internal.cdo.CDOObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Traffic Light</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.impl.TrafficLightImpl#getID <em>ID</em>}</li>
 *   <li>{@link TrafficManager.impl.TrafficLightImpl#getSumoID <em>Sumo ID</em>}</li>
 *   <li>{@link TrafficManager.impl.TrafficLightImpl#getRedYellowGreenState <em>Red Yellow Green State</em>}</li>
 *   <li>{@link TrafficManager.impl.TrafficLightImpl#getPhaseDuration <em>Phase Duration</em>}</li>
 *   <li>{@link TrafficManager.impl.TrafficLightImpl#getControlledLanes <em>Controlled Lanes</em>}</li>
 *   <li>{@link TrafficManager.impl.TrafficLightImpl#getPhase <em>Phase</em>}</li>
 *   <li>{@link TrafficManager.impl.TrafficLightImpl#getProgram <em>Program</em>}</li>
 *   <li>{@link TrafficManager.impl.TrafficLightImpl#getCompleteRedYellowGreenDefinition <em>Complete Red Yellow Green Definition</em>}</li>
 *   <li>{@link TrafficManager.impl.TrafficLightImpl#getNextSwitch <em>Next Switch</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TrafficLightImpl extends CDOObjectImpl implements TrafficLight {
	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getSumoID() <em>Sumo ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSumoID()
	 * @generated
	 * @ordered
	 */
	protected static final String SUMO_ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getRedYellowGreenState() <em>Red Yellow Green State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRedYellowGreenState()
	 * @generated
	 * @ordered
	 */
	protected static final String RED_YELLOW_GREEN_STATE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getPhaseDuration() <em>Phase Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhaseDuration()
	 * @generated
	 * @ordered
	 */
	protected static final double PHASE_DURATION_EDEFAULT = 0.0;

	/**
	 * The default value of the '{@link #getPhase() <em>Phase</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhase()
	 * @generated
	 * @ordered
	 */
	protected static final int PHASE_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #getProgram() <em>Program</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProgram()
	 * @generated
	 * @ordered
	 */
	protected static final String PROGRAM_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCompleteRedYellowGreenDefinition() <em>Complete Red Yellow Green Definition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompleteRedYellowGreenDefinition()
	 * @generated
	 * @ordered
	 */
	protected static final String COMPLETE_RED_YELLOW_GREEN_DEFINITION_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getNextSwitch() <em>Next Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNextSwitch()
	 * @generated
	 * @ordered
	 */
	protected static final double NEXT_SWITCH_EDEFAULT = 0.0;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TrafficLightImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TrafficManagerPackage.Literals.TRAFFIC_LIGHT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getID() {
		return (String)eDynamicGet(TrafficManagerPackage.TRAFFIC_LIGHT__ID, TrafficManagerPackage.Literals.TRAFFIC_LIGHT__ID, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(String newID) {
		eDynamicSet(TrafficManagerPackage.TRAFFIC_LIGHT__ID, TrafficManagerPackage.Literals.TRAFFIC_LIGHT__ID, newID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSumoID() {
		return (String)eDynamicGet(TrafficManagerPackage.TRAFFIC_LIGHT__SUMO_ID, TrafficManagerPackage.Literals.TRAFFIC_LIGHT__SUMO_ID, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSumoID(String newSumoID) {
		eDynamicSet(TrafficManagerPackage.TRAFFIC_LIGHT__SUMO_ID, TrafficManagerPackage.Literals.TRAFFIC_LIGHT__SUMO_ID, newSumoID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRedYellowGreenState() {
		return (String)eDynamicGet(TrafficManagerPackage.TRAFFIC_LIGHT__RED_YELLOW_GREEN_STATE, TrafficManagerPackage.Literals.TRAFFIC_LIGHT__RED_YELLOW_GREEN_STATE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRedYellowGreenState(String newRedYellowGreenState) {
		eDynamicSet(TrafficManagerPackage.TRAFFIC_LIGHT__RED_YELLOW_GREEN_STATE, TrafficManagerPackage.Literals.TRAFFIC_LIGHT__RED_YELLOW_GREEN_STATE, newRedYellowGreenState);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPhaseDuration() {
		return (Double)eDynamicGet(TrafficManagerPackage.TRAFFIC_LIGHT__PHASE_DURATION, TrafficManagerPackage.Literals.TRAFFIC_LIGHT__PHASE_DURATION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhaseDuration(double newPhaseDuration) {
		eDynamicSet(TrafficManagerPackage.TRAFFIC_LIGHT__PHASE_DURATION, TrafficManagerPackage.Literals.TRAFFIC_LIGHT__PHASE_DURATION, newPhaseDuration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<String> getControlledLanes() {
		return (EList<String>)eDynamicGet(TrafficManagerPackage.TRAFFIC_LIGHT__CONTROLLED_LANES, TrafficManagerPackage.Literals.TRAFFIC_LIGHT__CONTROLLED_LANES, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPhase() {
		return (Integer)eDynamicGet(TrafficManagerPackage.TRAFFIC_LIGHT__PHASE, TrafficManagerPackage.Literals.TRAFFIC_LIGHT__PHASE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhase(int newPhase) {
		eDynamicSet(TrafficManagerPackage.TRAFFIC_LIGHT__PHASE, TrafficManagerPackage.Literals.TRAFFIC_LIGHT__PHASE, newPhase);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getProgram() {
		return (String)eDynamicGet(TrafficManagerPackage.TRAFFIC_LIGHT__PROGRAM, TrafficManagerPackage.Literals.TRAFFIC_LIGHT__PROGRAM, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProgram(String newProgram) {
		eDynamicSet(TrafficManagerPackage.TRAFFIC_LIGHT__PROGRAM, TrafficManagerPackage.Literals.TRAFFIC_LIGHT__PROGRAM, newProgram);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCompleteRedYellowGreenDefinition() {
		return (String)eDynamicGet(TrafficManagerPackage.TRAFFIC_LIGHT__COMPLETE_RED_YELLOW_GREEN_DEFINITION, TrafficManagerPackage.Literals.TRAFFIC_LIGHT__COMPLETE_RED_YELLOW_GREEN_DEFINITION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCompleteRedYellowGreenDefinition(String newCompleteRedYellowGreenDefinition) {
		eDynamicSet(TrafficManagerPackage.TRAFFIC_LIGHT__COMPLETE_RED_YELLOW_GREEN_DEFINITION, TrafficManagerPackage.Literals.TRAFFIC_LIGHT__COMPLETE_RED_YELLOW_GREEN_DEFINITION, newCompleteRedYellowGreenDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getNextSwitch() {
		return (Double)eDynamicGet(TrafficManagerPackage.TRAFFIC_LIGHT__NEXT_SWITCH, TrafficManagerPackage.Literals.TRAFFIC_LIGHT__NEXT_SWITCH, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNextSwitch(double newNextSwitch) {
		eDynamicSet(TrafficManagerPackage.TRAFFIC_LIGHT__NEXT_SWITCH, TrafficManagerPackage.Literals.TRAFFIC_LIGHT__NEXT_SWITCH, newNextSwitch);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TrafficManagerPackage.TRAFFIC_LIGHT__ID:
				return getID();
			case TrafficManagerPackage.TRAFFIC_LIGHT__SUMO_ID:
				return getSumoID();
			case TrafficManagerPackage.TRAFFIC_LIGHT__RED_YELLOW_GREEN_STATE:
				return getRedYellowGreenState();
			case TrafficManagerPackage.TRAFFIC_LIGHT__PHASE_DURATION:
				return getPhaseDuration();
			case TrafficManagerPackage.TRAFFIC_LIGHT__CONTROLLED_LANES:
				return getControlledLanes();
			case TrafficManagerPackage.TRAFFIC_LIGHT__PHASE:
				return getPhase();
			case TrafficManagerPackage.TRAFFIC_LIGHT__PROGRAM:
				return getProgram();
			case TrafficManagerPackage.TRAFFIC_LIGHT__COMPLETE_RED_YELLOW_GREEN_DEFINITION:
				return getCompleteRedYellowGreenDefinition();
			case TrafficManagerPackage.TRAFFIC_LIGHT__NEXT_SWITCH:
				return getNextSwitch();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TrafficManagerPackage.TRAFFIC_LIGHT__ID:
				setID((String)newValue);
				return;
			case TrafficManagerPackage.TRAFFIC_LIGHT__SUMO_ID:
				setSumoID((String)newValue);
				return;
			case TrafficManagerPackage.TRAFFIC_LIGHT__RED_YELLOW_GREEN_STATE:
				setRedYellowGreenState((String)newValue);
				return;
			case TrafficManagerPackage.TRAFFIC_LIGHT__PHASE_DURATION:
				setPhaseDuration((Double)newValue);
				return;
			case TrafficManagerPackage.TRAFFIC_LIGHT__CONTROLLED_LANES:
				getControlledLanes().clear();
				getControlledLanes().addAll((Collection<? extends String>)newValue);
				return;
			case TrafficManagerPackage.TRAFFIC_LIGHT__PHASE:
				setPhase((Integer)newValue);
				return;
			case TrafficManagerPackage.TRAFFIC_LIGHT__PROGRAM:
				setProgram((String)newValue);
				return;
			case TrafficManagerPackage.TRAFFIC_LIGHT__COMPLETE_RED_YELLOW_GREEN_DEFINITION:
				setCompleteRedYellowGreenDefinition((String)newValue);
				return;
			case TrafficManagerPackage.TRAFFIC_LIGHT__NEXT_SWITCH:
				setNextSwitch((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.TRAFFIC_LIGHT__ID:
				setID(ID_EDEFAULT);
				return;
			case TrafficManagerPackage.TRAFFIC_LIGHT__SUMO_ID:
				setSumoID(SUMO_ID_EDEFAULT);
				return;
			case TrafficManagerPackage.TRAFFIC_LIGHT__RED_YELLOW_GREEN_STATE:
				setRedYellowGreenState(RED_YELLOW_GREEN_STATE_EDEFAULT);
				return;
			case TrafficManagerPackage.TRAFFIC_LIGHT__PHASE_DURATION:
				setPhaseDuration(PHASE_DURATION_EDEFAULT);
				return;
			case TrafficManagerPackage.TRAFFIC_LIGHT__CONTROLLED_LANES:
				getControlledLanes().clear();
				return;
			case TrafficManagerPackage.TRAFFIC_LIGHT__PHASE:
				setPhase(PHASE_EDEFAULT);
				return;
			case TrafficManagerPackage.TRAFFIC_LIGHT__PROGRAM:
				setProgram(PROGRAM_EDEFAULT);
				return;
			case TrafficManagerPackage.TRAFFIC_LIGHT__COMPLETE_RED_YELLOW_GREEN_DEFINITION:
				setCompleteRedYellowGreenDefinition(COMPLETE_RED_YELLOW_GREEN_DEFINITION_EDEFAULT);
				return;
			case TrafficManagerPackage.TRAFFIC_LIGHT__NEXT_SWITCH:
				setNextSwitch(NEXT_SWITCH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.TRAFFIC_LIGHT__ID:
				return ID_EDEFAULT == null ? getID() != null : !ID_EDEFAULT.equals(getID());
			case TrafficManagerPackage.TRAFFIC_LIGHT__SUMO_ID:
				return SUMO_ID_EDEFAULT == null ? getSumoID() != null : !SUMO_ID_EDEFAULT.equals(getSumoID());
			case TrafficManagerPackage.TRAFFIC_LIGHT__RED_YELLOW_GREEN_STATE:
				return RED_YELLOW_GREEN_STATE_EDEFAULT == null ? getRedYellowGreenState() != null : !RED_YELLOW_GREEN_STATE_EDEFAULT.equals(getRedYellowGreenState());
			case TrafficManagerPackage.TRAFFIC_LIGHT__PHASE_DURATION:
				return getPhaseDuration() != PHASE_DURATION_EDEFAULT;
			case TrafficManagerPackage.TRAFFIC_LIGHT__CONTROLLED_LANES:
				return !getControlledLanes().isEmpty();
			case TrafficManagerPackage.TRAFFIC_LIGHT__PHASE:
				return getPhase() != PHASE_EDEFAULT;
			case TrafficManagerPackage.TRAFFIC_LIGHT__PROGRAM:
				return PROGRAM_EDEFAULT == null ? getProgram() != null : !PROGRAM_EDEFAULT.equals(getProgram());
			case TrafficManagerPackage.TRAFFIC_LIGHT__COMPLETE_RED_YELLOW_GREEN_DEFINITION:
				return COMPLETE_RED_YELLOW_GREEN_DEFINITION_EDEFAULT == null ? getCompleteRedYellowGreenDefinition() != null : !COMPLETE_RED_YELLOW_GREEN_DEFINITION_EDEFAULT.equals(getCompleteRedYellowGreenDefinition());
			case TrafficManagerPackage.TRAFFIC_LIGHT__NEXT_SWITCH:
				return getNextSwitch() != NEXT_SWITCH_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //TrafficLightImpl
