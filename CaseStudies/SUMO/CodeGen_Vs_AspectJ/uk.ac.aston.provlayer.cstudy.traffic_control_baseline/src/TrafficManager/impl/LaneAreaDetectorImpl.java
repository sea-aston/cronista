/**
 */
package TrafficManager.impl;

import TrafficManager.LaneAreaDetector;
import TrafficManager.TrafficManagerPackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.internal.cdo.CDOObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Lane Area Detector</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.impl.LaneAreaDetectorImpl#getID <em>ID</em>}</li>
 *   <li>{@link TrafficManager.impl.LaneAreaDetectorImpl#getSumoID <em>Sumo ID</em>}</li>
 *   <li>{@link TrafficManager.impl.LaneAreaDetectorImpl#getPosition <em>Position</em>}</li>
 *   <li>{@link TrafficManager.impl.LaneAreaDetectorImpl#getLength <em>Length</em>}</li>
 *   <li>{@link TrafficManager.impl.LaneAreaDetectorImpl#getLaneID <em>Lane ID</em>}</li>
 *   <li>{@link TrafficManager.impl.LaneAreaDetectorImpl#getLastStepVehicleNumber <em>Last Step Vehicle Number</em>}</li>
 *   <li>{@link TrafficManager.impl.LaneAreaDetectorImpl#getLastStepMeanSpeed <em>Last Step Mean Speed</em>}</li>
 *   <li>{@link TrafficManager.impl.LaneAreaDetectorImpl#getLastStepVehicleIDs <em>Last Step Vehicle IDs</em>}</li>
 *   <li>{@link TrafficManager.impl.LaneAreaDetectorImpl#getLastStepOccupancy <em>Last Step Occupancy</em>}</li>
 *   <li>{@link TrafficManager.impl.LaneAreaDetectorImpl#getJamLengthVehicle <em>Jam Length Vehicle</em>}</li>
 *   <li>{@link TrafficManager.impl.LaneAreaDetectorImpl#getJamLenghtMeters <em>Jam Lenght Meters</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LaneAreaDetectorImpl extends CDOObjectImpl implements LaneAreaDetector {
	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getSumoID() <em>Sumo ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSumoID()
	 * @generated
	 * @ordered
	 */
	protected static final String SUMO_ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getPosition() <em>Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPosition()
	 * @generated
	 * @ordered
	 */
	protected static final double POSITION_EDEFAULT = 0.0;

	/**
	 * The default value of the '{@link #getLength() <em>Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLength()
	 * @generated
	 * @ordered
	 */
	protected static final double LENGTH_EDEFAULT = 0.0;

	/**
	 * The default value of the '{@link #getLaneID() <em>Lane ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLaneID()
	 * @generated
	 * @ordered
	 */
	protected static final String LANE_ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getLastStepVehicleNumber() <em>Last Step Vehicle Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastStepVehicleNumber()
	 * @generated
	 * @ordered
	 */
	protected static final int LAST_STEP_VEHICLE_NUMBER_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #getLastStepMeanSpeed() <em>Last Step Mean Speed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastStepMeanSpeed()
	 * @generated
	 * @ordered
	 */
	protected static final double LAST_STEP_MEAN_SPEED_EDEFAULT = 0.0;

	/**
	 * The default value of the '{@link #getLastStepVehicleIDs() <em>Last Step Vehicle IDs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastStepVehicleIDs()
	 * @generated
	 * @ordered
	 */
	protected static final String LAST_STEP_VEHICLE_IDS_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getLastStepOccupancy() <em>Last Step Occupancy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastStepOccupancy()
	 * @generated
	 * @ordered
	 */
	protected static final double LAST_STEP_OCCUPANCY_EDEFAULT = 0.0;

	/**
	 * The default value of the '{@link #getJamLengthVehicle() <em>Jam Length Vehicle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJamLengthVehicle()
	 * @generated
	 * @ordered
	 */
	protected static final int JAM_LENGTH_VEHICLE_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #getJamLenghtMeters() <em>Jam Lenght Meters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJamLenghtMeters()
	 * @generated
	 * @ordered
	 */
	protected static final double JAM_LENGHT_METERS_EDEFAULT = 0.0;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LaneAreaDetectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TrafficManagerPackage.Literals.LANE_AREA_DETECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getID() {
		return (String)eDynamicGet(TrafficManagerPackage.LANE_AREA_DETECTOR__ID, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__ID, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(String newID) {
		eDynamicSet(TrafficManagerPackage.LANE_AREA_DETECTOR__ID, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__ID, newID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSumoID() {
		return (String)eDynamicGet(TrafficManagerPackage.LANE_AREA_DETECTOR__SUMO_ID, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__SUMO_ID, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSumoID(String newSumoID) {
		eDynamicSet(TrafficManagerPackage.LANE_AREA_DETECTOR__SUMO_ID, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__SUMO_ID, newSumoID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPosition() {
		return (Double)eDynamicGet(TrafficManagerPackage.LANE_AREA_DETECTOR__POSITION, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__POSITION, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosition(double newPosition) {
		eDynamicSet(TrafficManagerPackage.LANE_AREA_DETECTOR__POSITION, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__POSITION, newPosition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getLength() {
		return (Double)eDynamicGet(TrafficManagerPackage.LANE_AREA_DETECTOR__LENGTH, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__LENGTH, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLength(double newLength) {
		eDynamicSet(TrafficManagerPackage.LANE_AREA_DETECTOR__LENGTH, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__LENGTH, newLength);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLaneID() {
		return (String)eDynamicGet(TrafficManagerPackage.LANE_AREA_DETECTOR__LANE_ID, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__LANE_ID, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLaneID(String newLaneID) {
		eDynamicSet(TrafficManagerPackage.LANE_AREA_DETECTOR__LANE_ID, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__LANE_ID, newLaneID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLastStepVehicleNumber() {
		return (Integer)eDynamicGet(TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_NUMBER, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_NUMBER, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastStepVehicleNumber(int newLastStepVehicleNumber) {
		eDynamicSet(TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_NUMBER, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_NUMBER, newLastStepVehicleNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getLastStepMeanSpeed() {
		return (Double)eDynamicGet(TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_MEAN_SPEED, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__LAST_STEP_MEAN_SPEED, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastStepMeanSpeed(double newLastStepMeanSpeed) {
		eDynamicSet(TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_MEAN_SPEED, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__LAST_STEP_MEAN_SPEED, newLastStepMeanSpeed);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLastStepVehicleIDs() {
		return (String)eDynamicGet(TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_IDS, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_IDS, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastStepVehicleIDs(String newLastStepVehicleIDs) {
		eDynamicSet(TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_IDS, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_IDS, newLastStepVehicleIDs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getLastStepOccupancy() {
		return (Double)eDynamicGet(TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_OCCUPANCY, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__LAST_STEP_OCCUPANCY, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastStepOccupancy(double newLastStepOccupancy) {
		eDynamicSet(TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_OCCUPANCY, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__LAST_STEP_OCCUPANCY, newLastStepOccupancy);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getJamLengthVehicle() {
		return (Integer)eDynamicGet(TrafficManagerPackage.LANE_AREA_DETECTOR__JAM_LENGTH_VEHICLE, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__JAM_LENGTH_VEHICLE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setJamLengthVehicle(int newJamLengthVehicle) {
		eDynamicSet(TrafficManagerPackage.LANE_AREA_DETECTOR__JAM_LENGTH_VEHICLE, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__JAM_LENGTH_VEHICLE, newJamLengthVehicle);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getJamLenghtMeters() {
		return (Double)eDynamicGet(TrafficManagerPackage.LANE_AREA_DETECTOR__JAM_LENGHT_METERS, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__JAM_LENGHT_METERS, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setJamLenghtMeters(double newJamLenghtMeters) {
		eDynamicSet(TrafficManagerPackage.LANE_AREA_DETECTOR__JAM_LENGHT_METERS, TrafficManagerPackage.Literals.LANE_AREA_DETECTOR__JAM_LENGHT_METERS, newJamLenghtMeters);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TrafficManagerPackage.LANE_AREA_DETECTOR__ID:
				return getID();
			case TrafficManagerPackage.LANE_AREA_DETECTOR__SUMO_ID:
				return getSumoID();
			case TrafficManagerPackage.LANE_AREA_DETECTOR__POSITION:
				return getPosition();
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LENGTH:
				return getLength();
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LANE_ID:
				return getLaneID();
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_NUMBER:
				return getLastStepVehicleNumber();
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_MEAN_SPEED:
				return getLastStepMeanSpeed();
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_IDS:
				return getLastStepVehicleIDs();
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_OCCUPANCY:
				return getLastStepOccupancy();
			case TrafficManagerPackage.LANE_AREA_DETECTOR__JAM_LENGTH_VEHICLE:
				return getJamLengthVehicle();
			case TrafficManagerPackage.LANE_AREA_DETECTOR__JAM_LENGHT_METERS:
				return getJamLenghtMeters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TrafficManagerPackage.LANE_AREA_DETECTOR__ID:
				setID((String)newValue);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__SUMO_ID:
				setSumoID((String)newValue);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__POSITION:
				setPosition((Double)newValue);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LENGTH:
				setLength((Double)newValue);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LANE_ID:
				setLaneID((String)newValue);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_NUMBER:
				setLastStepVehicleNumber((Integer)newValue);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_MEAN_SPEED:
				setLastStepMeanSpeed((Double)newValue);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_IDS:
				setLastStepVehicleIDs((String)newValue);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_OCCUPANCY:
				setLastStepOccupancy((Double)newValue);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__JAM_LENGTH_VEHICLE:
				setJamLengthVehicle((Integer)newValue);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__JAM_LENGHT_METERS:
				setJamLenghtMeters((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.LANE_AREA_DETECTOR__ID:
				setID(ID_EDEFAULT);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__SUMO_ID:
				setSumoID(SUMO_ID_EDEFAULT);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__POSITION:
				setPosition(POSITION_EDEFAULT);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LENGTH:
				setLength(LENGTH_EDEFAULT);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LANE_ID:
				setLaneID(LANE_ID_EDEFAULT);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_NUMBER:
				setLastStepVehicleNumber(LAST_STEP_VEHICLE_NUMBER_EDEFAULT);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_MEAN_SPEED:
				setLastStepMeanSpeed(LAST_STEP_MEAN_SPEED_EDEFAULT);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_IDS:
				setLastStepVehicleIDs(LAST_STEP_VEHICLE_IDS_EDEFAULT);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_OCCUPANCY:
				setLastStepOccupancy(LAST_STEP_OCCUPANCY_EDEFAULT);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__JAM_LENGTH_VEHICLE:
				setJamLengthVehicle(JAM_LENGTH_VEHICLE_EDEFAULT);
				return;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__JAM_LENGHT_METERS:
				setJamLenghtMeters(JAM_LENGHT_METERS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.LANE_AREA_DETECTOR__ID:
				return ID_EDEFAULT == null ? getID() != null : !ID_EDEFAULT.equals(getID());
			case TrafficManagerPackage.LANE_AREA_DETECTOR__SUMO_ID:
				return SUMO_ID_EDEFAULT == null ? getSumoID() != null : !SUMO_ID_EDEFAULT.equals(getSumoID());
			case TrafficManagerPackage.LANE_AREA_DETECTOR__POSITION:
				return getPosition() != POSITION_EDEFAULT;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LENGTH:
				return getLength() != LENGTH_EDEFAULT;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LANE_ID:
				return LANE_ID_EDEFAULT == null ? getLaneID() != null : !LANE_ID_EDEFAULT.equals(getLaneID());
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_NUMBER:
				return getLastStepVehicleNumber() != LAST_STEP_VEHICLE_NUMBER_EDEFAULT;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_MEAN_SPEED:
				return getLastStepMeanSpeed() != LAST_STEP_MEAN_SPEED_EDEFAULT;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_IDS:
				return LAST_STEP_VEHICLE_IDS_EDEFAULT == null ? getLastStepVehicleIDs() != null : !LAST_STEP_VEHICLE_IDS_EDEFAULT.equals(getLastStepVehicleIDs());
			case TrafficManagerPackage.LANE_AREA_DETECTOR__LAST_STEP_OCCUPANCY:
				return getLastStepOccupancy() != LAST_STEP_OCCUPANCY_EDEFAULT;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__JAM_LENGTH_VEHICLE:
				return getJamLengthVehicle() != JAM_LENGTH_VEHICLE_EDEFAULT;
			case TrafficManagerPackage.LANE_AREA_DETECTOR__JAM_LENGHT_METERS:
				return getJamLenghtMeters() != JAM_LENGHT_METERS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //LaneAreaDetectorImpl
