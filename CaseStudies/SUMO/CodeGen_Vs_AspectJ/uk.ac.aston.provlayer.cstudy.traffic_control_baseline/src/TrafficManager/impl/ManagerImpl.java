/**
 */
package TrafficManager.impl;

import TrafficManager.Manager;
import TrafficManager.SmartController;
import TrafficManager.TrafficManagerPackage;

import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.emf.internal.cdo.CDOObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.impl.ManagerImpl#getTime <em>Time</em>}</li>
 *   <li>{@link TrafficManager.impl.ManagerImpl#getSmartController <em>Smart Controller</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ManagerImpl extends CDOObjectImpl implements Manager {
	/**
	 * The default value of the '{@link #getTime() <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTime()
	 * @generated
	 * @ordered
	 */
	protected static final double TIME_EDEFAULT = 0.0;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ManagerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TrafficManagerPackage.Literals.MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getTime() {
		return (Double)eDynamicGet(TrafficManagerPackage.MANAGER__TIME, TrafficManagerPackage.Literals.MANAGER__TIME, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTime(double newTime) {
		eDynamicSet(TrafficManagerPackage.MANAGER__TIME, TrafficManagerPackage.Literals.MANAGER__TIME, newTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SmartController> getSmartController() {
		return (EList<SmartController>)eDynamicGet(TrafficManagerPackage.MANAGER__SMART_CONTROLLER, TrafficManagerPackage.Literals.MANAGER__SMART_CONTROLLER, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TrafficManagerPackage.MANAGER__SMART_CONTROLLER:
				return ((InternalEList<?>)getSmartController()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TrafficManagerPackage.MANAGER__TIME:
				return getTime();
			case TrafficManagerPackage.MANAGER__SMART_CONTROLLER:
				return getSmartController();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TrafficManagerPackage.MANAGER__TIME:
				setTime((Double)newValue);
				return;
			case TrafficManagerPackage.MANAGER__SMART_CONTROLLER:
				getSmartController().clear();
				getSmartController().addAll((Collection<? extends SmartController>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.MANAGER__TIME:
				setTime(TIME_EDEFAULT);
				return;
			case TrafficManagerPackage.MANAGER__SMART_CONTROLLER:
				getSmartController().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.MANAGER__TIME:
				return getTime() != TIME_EDEFAULT;
			case TrafficManagerPackage.MANAGER__SMART_CONTROLLER:
				return !getSmartController().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ManagerImpl
