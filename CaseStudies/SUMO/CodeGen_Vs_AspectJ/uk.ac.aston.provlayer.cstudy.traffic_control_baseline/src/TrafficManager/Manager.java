/**
 */
package TrafficManager;

import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.Manager#getTime <em>Time</em>}</li>
 *   <li>{@link TrafficManager.Manager#getSmartController <em>Smart Controller</em>}</li>
 * </ul>
 *
 * @see TrafficManager.TrafficManagerPackage#getManager()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface Manager extends CDOObject {
	/**
	 * Returns the value of the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time</em>' attribute.
	 * @see #setTime(double)
	 * @see TrafficManager.TrafficManagerPackage#getManager_Time()
	 * @model
	 * @generated
	 */
	double getTime();

	/**
	 * Sets the value of the '{@link TrafficManager.Manager#getTime <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time</em>' attribute.
	 * @see #getTime()
	 * @generated
	 */
	void setTime(double value);

	/**
	 * Returns the value of the '<em><b>Smart Controller</b></em>' containment reference list.
	 * The list contents are of type {@link TrafficManager.SmartController}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Smart Controller</em>' containment reference list.
	 * @see TrafficManager.TrafficManagerPackage#getManager_SmartController()
	 * @model containment="true"
	 * @generated
	 */
	EList<SmartController> getSmartController();

} // Manager
