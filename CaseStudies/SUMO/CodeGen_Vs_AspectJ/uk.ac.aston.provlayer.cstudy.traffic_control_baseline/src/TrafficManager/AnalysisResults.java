/**
 */
package TrafficManager;

import org.eclipse.emf.cdo.CDOObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Analysis Results</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.AnalysisResults#getID <em>ID</em>}</li>
 *   <li>{@link TrafficManager.AnalysisResults#isLightsFailed <em>Lights Failed</em>}</li>
 *   <li>{@link TrafficManager.AnalysisResults#getLADsJammed <em>LA Ds Jammed</em>}</li>
 *   <li>{@link TrafficManager.AnalysisResults#isPlanToCopyPhaseEnd <em>Plan To Copy Phase End</em>}</li>
 * </ul>
 *
 * @see TrafficManager.TrafficManagerPackage#getAnalysisResults()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface AnalysisResults extends CDOObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see TrafficManager.TrafficManagerPackage#getAnalysisResults_ID()
	 * @model id="true"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link TrafficManager.AnalysisResults#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>Lights Failed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lights Failed</em>' attribute.
	 * @see #setLightsFailed(boolean)
	 * @see TrafficManager.TrafficManagerPackage#getAnalysisResults_LightsFailed()
	 * @model required="true"
	 * @generated
	 */
	boolean isLightsFailed();

	/**
	 * Sets the value of the '{@link TrafficManager.AnalysisResults#isLightsFailed <em>Lights Failed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lights Failed</em>' attribute.
	 * @see #isLightsFailed()
	 * @generated
	 */
	void setLightsFailed(boolean value);

	/**
	 * Returns the value of the '<em><b>LA Ds Jammed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LA Ds Jammed</em>' attribute.
	 * @see #setLADsJammed(int)
	 * @see TrafficManager.TrafficManagerPackage#getAnalysisResults_LADsJammed()
	 * @model
	 * @generated
	 */
	int getLADsJammed();

	/**
	 * Sets the value of the '{@link TrafficManager.AnalysisResults#getLADsJammed <em>LA Ds Jammed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LA Ds Jammed</em>' attribute.
	 * @see #getLADsJammed()
	 * @generated
	 */
	void setLADsJammed(int value);

	/**
	 * Returns the value of the '<em><b>Plan To Copy Phase End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Plan To Copy Phase End</em>' attribute.
	 * @see #setPlanToCopyPhaseEnd(boolean)
	 * @see TrafficManager.TrafficManagerPackage#getAnalysisResults_PlanToCopyPhaseEnd()
	 * @model
	 * @generated
	 */
	boolean isPlanToCopyPhaseEnd();

	/**
	 * Sets the value of the '{@link TrafficManager.AnalysisResults#isPlanToCopyPhaseEnd <em>Plan To Copy Phase End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Plan To Copy Phase End</em>' attribute.
	 * @see #isPlanToCopyPhaseEnd()
	 * @generated
	 */
	void setPlanToCopyPhaseEnd(boolean value);

} // AnalysisResults
