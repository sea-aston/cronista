/**
 */
package TrafficManager;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see TrafficManager.TrafficManagerPackage
 * @generated
 */
public interface TrafficManagerFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TrafficManagerFactory eINSTANCE = TrafficManager.impl.TrafficManagerFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Manager</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Manager</em>'.
	 * @generated
	 */
	Manager createManager();

	/**
	 * Returns a new object of class '<em>Lane Area Detector</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lane Area Detector</em>'.
	 * @generated
	 */
	LaneAreaDetector createLaneAreaDetector();

	/**
	 * Returns a new object of class '<em>Traffic Light</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Traffic Light</em>'.
	 * @generated
	 */
	TrafficLight createTrafficLight();

	/**
	 * Returns a new object of class '<em>Monitor Controls</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Monitor Controls</em>'.
	 * @generated
	 */
	MonitorControls createMonitorControls();

	/**
	 * Returns a new object of class '<em>Monitor Results</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Monitor Results</em>'.
	 * @generated
	 */
	MonitorResults createMonitorResults();

	/**
	 * Returns a new object of class '<em>Analysis Controls</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Analysis Controls</em>'.
	 * @generated
	 */
	AnalysisControls createAnalysisControls();

	/**
	 * Returns a new object of class '<em>Analysis Results</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Analysis Results</em>'.
	 * @generated
	 */
	AnalysisResults createAnalysisResults();

	/**
	 * Returns a new object of class '<em>Plan To Execute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Plan To Execute</em>'.
	 * @generated
	 */
	PlanToExecute createPlanToExecute();

	/**
	 * Returns a new object of class '<em>Execution Results</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Execution Results</em>'.
	 * @generated
	 */
	ExecutionResults createExecutionResults();

	/**
	 * Returns a new object of class '<em>Smart Controller</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Smart Controller</em>'.
	 * @generated
	 */
	SmartController createSmartController();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TrafficManagerPackage getTrafficManagerPackage();

} //TrafficManagerFactory
