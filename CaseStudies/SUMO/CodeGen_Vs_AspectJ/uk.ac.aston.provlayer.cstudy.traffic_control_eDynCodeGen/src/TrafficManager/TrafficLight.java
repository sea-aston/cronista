/**
 */
package TrafficManager;

import org.eclipse.emf.cdo.CDOObject;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Traffic Light</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.TrafficLight#getID <em>ID</em>}</li>
 *   <li>{@link TrafficManager.TrafficLight#getSumoID <em>Sumo ID</em>}</li>
 *   <li>{@link TrafficManager.TrafficLight#getRedYellowGreenState <em>Red Yellow Green State</em>}</li>
 *   <li>{@link TrafficManager.TrafficLight#getPhaseDuration <em>Phase Duration</em>}</li>
 *   <li>{@link TrafficManager.TrafficLight#getControlledLanes <em>Controlled Lanes</em>}</li>
 *   <li>{@link TrafficManager.TrafficLight#getPhase <em>Phase</em>}</li>
 *   <li>{@link TrafficManager.TrafficLight#getProgram <em>Program</em>}</li>
 *   <li>{@link TrafficManager.TrafficLight#getCompleteRedYellowGreenDefinition <em>Complete Red Yellow Green Definition</em>}</li>
 *   <li>{@link TrafficManager.TrafficLight#getNextSwitch <em>Next Switch</em>}</li>
 * </ul>
 *
 * @see TrafficManager.TrafficManagerPackage#getTrafficLight()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface TrafficLight extends CDOObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see TrafficManager.TrafficManagerPackage#getTrafficLight_ID()
	 * @model id="true"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link TrafficManager.TrafficLight#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>Sumo ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sumo ID</em>' attribute.
	 * @see #setSumoID(String)
	 * @see TrafficManager.TrafficManagerPackage#getTrafficLight_SumoID()
	 * @model
	 * @generated
	 */
	String getSumoID();

	/**
	 * Sets the value of the '{@link TrafficManager.TrafficLight#getSumoID <em>Sumo ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sumo ID</em>' attribute.
	 * @see #getSumoID()
	 * @generated
	 */
	void setSumoID(String value);

	/**
	 * Returns the value of the '<em><b>Red Yellow Green State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Red Yellow Green State</em>' attribute.
	 * @see #setRedYellowGreenState(String)
	 * @see TrafficManager.TrafficManagerPackage#getTrafficLight_RedYellowGreenState()
	 * @model
	 * @generated
	 */
	String getRedYellowGreenState();

	/**
	 * Sets the value of the '{@link TrafficManager.TrafficLight#getRedYellowGreenState <em>Red Yellow Green State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Red Yellow Green State</em>' attribute.
	 * @see #getRedYellowGreenState()
	 * @generated
	 */
	void setRedYellowGreenState(String value);

	/**
	 * Returns the value of the '<em><b>Phase Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Phase Duration</em>' attribute.
	 * @see #setPhaseDuration(double)
	 * @see TrafficManager.TrafficManagerPackage#getTrafficLight_PhaseDuration()
	 * @model
	 * @generated
	 */
	double getPhaseDuration();

	/**
	 * Sets the value of the '{@link TrafficManager.TrafficLight#getPhaseDuration <em>Phase Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Phase Duration</em>' attribute.
	 * @see #getPhaseDuration()
	 * @generated
	 */
	void setPhaseDuration(double value);

	/**
	 * Returns the value of the '<em><b>Controlled Lanes</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controlled Lanes</em>' attribute list.
	 * @see TrafficManager.TrafficManagerPackage#getTrafficLight_ControlledLanes()
	 * @model unique="false" transient="true"
	 * @generated
	 */
	EList<String> getControlledLanes();

	/**
	 * Returns the value of the '<em><b>Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Phase</em>' attribute.
	 * @see #setPhase(int)
	 * @see TrafficManager.TrafficManagerPackage#getTrafficLight_Phase()
	 * @model
	 * @generated
	 */
	int getPhase();

	/**
	 * Sets the value of the '{@link TrafficManager.TrafficLight#getPhase <em>Phase</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Phase</em>' attribute.
	 * @see #getPhase()
	 * @generated
	 */
	void setPhase(int value);

	/**
	 * Returns the value of the '<em><b>Program</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Program</em>' attribute.
	 * @see #setProgram(String)
	 * @see TrafficManager.TrafficManagerPackage#getTrafficLight_Program()
	 * @model
	 * @generated
	 */
	String getProgram();

	/**
	 * Sets the value of the '{@link TrafficManager.TrafficLight#getProgram <em>Program</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Program</em>' attribute.
	 * @see #getProgram()
	 * @generated
	 */
	void setProgram(String value);

	/**
	 * Returns the value of the '<em><b>Complete Red Yellow Green Definition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Complete Red Yellow Green Definition</em>' attribute.
	 * @see #setCompleteRedYellowGreenDefinition(String)
	 * @see TrafficManager.TrafficManagerPackage#getTrafficLight_CompleteRedYellowGreenDefinition()
	 * @model
	 * @generated
	 */
	String getCompleteRedYellowGreenDefinition();

	/**
	 * Sets the value of the '{@link TrafficManager.TrafficLight#getCompleteRedYellowGreenDefinition <em>Complete Red Yellow Green Definition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Complete Red Yellow Green Definition</em>' attribute.
	 * @see #getCompleteRedYellowGreenDefinition()
	 * @generated
	 */
	void setCompleteRedYellowGreenDefinition(String value);

	/**
	 * Returns the value of the '<em><b>Next Switch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next Switch</em>' attribute.
	 * @see #setNextSwitch(double)
	 * @see TrafficManager.TrafficManagerPackage#getTrafficLight_NextSwitch()
	 * @model
	 * @generated
	 */
	double getNextSwitch();

	/**
	 * Sets the value of the '{@link TrafficManager.TrafficLight#getNextSwitch <em>Next Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next Switch</em>' attribute.
	 * @see #getNextSwitch()
	 * @generated
	 */
	void setNextSwitch(double value);

} // TrafficLight
