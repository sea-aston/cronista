/**
 */
package TrafficManager;

import org.eclipse.emf.cdo.CDOObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Analysis Controls</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.AnalysisControls#getID <em>ID</em>}</li>
 *   <li>{@link TrafficManager.AnalysisControls#getJamThreshold <em>Jam Threshold</em>}</li>
 *   <li>{@link TrafficManager.AnalysisControls#isExpect_Program_OFF <em>Expect Program OFF</em>}</li>
 *   <li>{@link TrafficManager.AnalysisControls#isCopyPhaseEnd <em>Copy Phase End</em>}</li>
 * </ul>
 *
 * @see TrafficManager.TrafficManagerPackage#getAnalysisControls()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface AnalysisControls extends CDOObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see TrafficManager.TrafficManagerPackage#getAnalysisControls_ID()
	 * @model id="true"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link TrafficManager.AnalysisControls#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>Jam Threshold</b></em>' attribute.
	 * The default value is <code>"3"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Jam Threshold</em>' attribute.
	 * @see #setJamThreshold(int)
	 * @see TrafficManager.TrafficManagerPackage#getAnalysisControls_JamThreshold()
	 * @model default="3" required="true"
	 * @generated
	 */
	int getJamThreshold();

	/**
	 * Sets the value of the '{@link TrafficManager.AnalysisControls#getJamThreshold <em>Jam Threshold</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Jam Threshold</em>' attribute.
	 * @see #getJamThreshold()
	 * @generated
	 */
	void setJamThreshold(int value);

	/**
	 * Returns the value of the '<em><b>Expect Program OFF</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expect Program OFF</em>' attribute.
	 * @see #setExpect_Program_OFF(boolean)
	 * @see TrafficManager.TrafficManagerPackage#getAnalysisControls_Expect_Program_OFF()
	 * @model default="true"
	 * @generated
	 */
	boolean isExpect_Program_OFF();

	/**
	 * Sets the value of the '{@link TrafficManager.AnalysisControls#isExpect_Program_OFF <em>Expect Program OFF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expect Program OFF</em>' attribute.
	 * @see #isExpect_Program_OFF()
	 * @generated
	 */
	void setExpect_Program_OFF(boolean value);

	/**
	 * Returns the value of the '<em><b>Copy Phase End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Copy Phase End</em>' attribute.
	 * @see #setCopyPhaseEnd(boolean)
	 * @see TrafficManager.TrafficManagerPackage#getAnalysisControls_CopyPhaseEnd()
	 * @model
	 * @generated
	 */
	boolean isCopyPhaseEnd();

	/**
	 * Sets the value of the '{@link TrafficManager.AnalysisControls#isCopyPhaseEnd <em>Copy Phase End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Copy Phase End</em>' attribute.
	 * @see #isCopyPhaseEnd()
	 * @generated
	 */
	void setCopyPhaseEnd(boolean value);

} // AnalysisControls
