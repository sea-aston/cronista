/**
 */
package TrafficManager.util;

import TrafficManager.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see TrafficManager.TrafficManagerPackage
 * @generated
 */
public class TrafficManagerAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TrafficManagerPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TrafficManagerAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = TrafficManagerPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TrafficManagerSwitch<Adapter> modelSwitch =
		new TrafficManagerSwitch<Adapter>() {
			@Override
			public Adapter caseManager(Manager object) {
				return createManagerAdapter();
			}
			@Override
			public Adapter caseLaneAreaDetector(LaneAreaDetector object) {
				return createLaneAreaDetectorAdapter();
			}
			@Override
			public Adapter caseTrafficLight(TrafficLight object) {
				return createTrafficLightAdapter();
			}
			@Override
			public Adapter caseMonitorControls(MonitorControls object) {
				return createMonitorControlsAdapter();
			}
			@Override
			public Adapter caseMonitorResults(MonitorResults object) {
				return createMonitorResultsAdapter();
			}
			@Override
			public Adapter caseAnalysisControls(AnalysisControls object) {
				return createAnalysisControlsAdapter();
			}
			@Override
			public Adapter caseAnalysisResults(AnalysisResults object) {
				return createAnalysisResultsAdapter();
			}
			@Override
			public Adapter casePlanToExecute(PlanToExecute object) {
				return createPlanToExecuteAdapter();
			}
			@Override
			public Adapter caseExecutionResults(ExecutionResults object) {
				return createExecutionResultsAdapter();
			}
			@Override
			public Adapter caseSmartController(SmartController object) {
				return createSmartControllerAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link TrafficManager.Manager <em>Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see TrafficManager.Manager
	 * @generated
	 */
	public Adapter createManagerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link TrafficManager.LaneAreaDetector <em>Lane Area Detector</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see TrafficManager.LaneAreaDetector
	 * @generated
	 */
	public Adapter createLaneAreaDetectorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link TrafficManager.TrafficLight <em>Traffic Light</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see TrafficManager.TrafficLight
	 * @generated
	 */
	public Adapter createTrafficLightAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link TrafficManager.MonitorControls <em>Monitor Controls</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see TrafficManager.MonitorControls
	 * @generated
	 */
	public Adapter createMonitorControlsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link TrafficManager.MonitorResults <em>Monitor Results</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see TrafficManager.MonitorResults
	 * @generated
	 */
	public Adapter createMonitorResultsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link TrafficManager.AnalysisControls <em>Analysis Controls</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see TrafficManager.AnalysisControls
	 * @generated
	 */
	public Adapter createAnalysisControlsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link TrafficManager.AnalysisResults <em>Analysis Results</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see TrafficManager.AnalysisResults
	 * @generated
	 */
	public Adapter createAnalysisResultsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link TrafficManager.PlanToExecute <em>Plan To Execute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see TrafficManager.PlanToExecute
	 * @generated
	 */
	public Adapter createPlanToExecuteAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link TrafficManager.ExecutionResults <em>Execution Results</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see TrafficManager.ExecutionResults
	 * @generated
	 */
	public Adapter createExecutionResultsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link TrafficManager.SmartController <em>Smart Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see TrafficManager.SmartController
	 * @generated
	 */
	public Adapter createSmartControllerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //TrafficManagerAdapterFactory
