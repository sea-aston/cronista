/**
 */
package TrafficManager.impl;

import TrafficManager.PlanToExecute;
import TrafficManager.TrafficManagerPackage;

import org.eclipse.emf.ecore.EClass;

import uk.ac.aston.provlayer.observer.model.cdo.LoggingCDOObjectImpl2;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Plan To Execute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.impl.PlanToExecuteImpl#getID <em>ID</em>}</li>
 *   <li>{@link TrafficManager.impl.PlanToExecuteImpl#isEndPhase <em>End Phase</em>}</li>
 *   <li>{@link TrafficManager.impl.PlanToExecuteImpl#isReportLightsFailed <em>Report Lights Failed</em>}</li>
 *   <li>{@link TrafficManager.impl.PlanToExecuteImpl#getJamThresholdChange <em>Jam Threshold Change</em>}</li>
 *   <li>{@link TrafficManager.impl.PlanToExecuteImpl#isCopyingPhaseEnd <em>Copying Phase End</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PlanToExecuteImpl extends LoggingCDOObjectImpl2 implements PlanToExecute {
	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #isEndPhase() <em>End Phase</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEndPhase()
	 * @generated
	 * @ordered
	 */
	protected static final boolean END_PHASE_EDEFAULT = false;

	/**
	 * The default value of the '{@link #isReportLightsFailed() <em>Report Lights Failed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isReportLightsFailed()
	 * @generated
	 * @ordered
	 */
	protected static final boolean REPORT_LIGHTS_FAILED_EDEFAULT = false;

	/**
	 * The default value of the '{@link #getJamThresholdChange() <em>Jam Threshold Change</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJamThresholdChange()
	 * @generated
	 * @ordered
	 */
	protected static final int JAM_THRESHOLD_CHANGE_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #isCopyingPhaseEnd() <em>Copying Phase End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCopyingPhaseEnd()
	 * @generated
	 * @ordered
	 */
	protected static final boolean COPYING_PHASE_END_EDEFAULT = false;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PlanToExecuteImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TrafficManagerPackage.Literals.PLAN_TO_EXECUTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getID() {
		return (String)eDynamicGet(TrafficManagerPackage.PLAN_TO_EXECUTE__ID, TrafficManagerPackage.Literals.PLAN_TO_EXECUTE__ID, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(String newID) {
		eDynamicSet(TrafficManagerPackage.PLAN_TO_EXECUTE__ID, TrafficManagerPackage.Literals.PLAN_TO_EXECUTE__ID, newID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEndPhase() {
		return (Boolean)eDynamicGet(TrafficManagerPackage.PLAN_TO_EXECUTE__END_PHASE, TrafficManagerPackage.Literals.PLAN_TO_EXECUTE__END_PHASE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndPhase(boolean newEndPhase) {
		eDynamicSet(TrafficManagerPackage.PLAN_TO_EXECUTE__END_PHASE, TrafficManagerPackage.Literals.PLAN_TO_EXECUTE__END_PHASE, newEndPhase);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isReportLightsFailed() {
		return (Boolean)eDynamicGet(TrafficManagerPackage.PLAN_TO_EXECUTE__REPORT_LIGHTS_FAILED, TrafficManagerPackage.Literals.PLAN_TO_EXECUTE__REPORT_LIGHTS_FAILED, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReportLightsFailed(boolean newReportLightsFailed) {
		eDynamicSet(TrafficManagerPackage.PLAN_TO_EXECUTE__REPORT_LIGHTS_FAILED, TrafficManagerPackage.Literals.PLAN_TO_EXECUTE__REPORT_LIGHTS_FAILED, newReportLightsFailed);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getJamThresholdChange() {
		return (Integer)eDynamicGet(TrafficManagerPackage.PLAN_TO_EXECUTE__JAM_THRESHOLD_CHANGE, TrafficManagerPackage.Literals.PLAN_TO_EXECUTE__JAM_THRESHOLD_CHANGE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setJamThresholdChange(int newJamThresholdChange) {
		eDynamicSet(TrafficManagerPackage.PLAN_TO_EXECUTE__JAM_THRESHOLD_CHANGE, TrafficManagerPackage.Literals.PLAN_TO_EXECUTE__JAM_THRESHOLD_CHANGE, newJamThresholdChange);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCopyingPhaseEnd() {
		return (Boolean)eDynamicGet(TrafficManagerPackage.PLAN_TO_EXECUTE__COPYING_PHASE_END, TrafficManagerPackage.Literals.PLAN_TO_EXECUTE__COPYING_PHASE_END, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCopyingPhaseEnd(boolean newCopyingPhaseEnd) {
		eDynamicSet(TrafficManagerPackage.PLAN_TO_EXECUTE__COPYING_PHASE_END, TrafficManagerPackage.Literals.PLAN_TO_EXECUTE__COPYING_PHASE_END, newCopyingPhaseEnd);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TrafficManagerPackage.PLAN_TO_EXECUTE__ID:
				return getID();
			case TrafficManagerPackage.PLAN_TO_EXECUTE__END_PHASE:
				return isEndPhase();
			case TrafficManagerPackage.PLAN_TO_EXECUTE__REPORT_LIGHTS_FAILED:
				return isReportLightsFailed();
			case TrafficManagerPackage.PLAN_TO_EXECUTE__JAM_THRESHOLD_CHANGE:
				return getJamThresholdChange();
			case TrafficManagerPackage.PLAN_TO_EXECUTE__COPYING_PHASE_END:
				return isCopyingPhaseEnd();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TrafficManagerPackage.PLAN_TO_EXECUTE__ID:
				setID((String)newValue);
				return;
			case TrafficManagerPackage.PLAN_TO_EXECUTE__END_PHASE:
				setEndPhase((Boolean)newValue);
				return;
			case TrafficManagerPackage.PLAN_TO_EXECUTE__REPORT_LIGHTS_FAILED:
				setReportLightsFailed((Boolean)newValue);
				return;
			case TrafficManagerPackage.PLAN_TO_EXECUTE__JAM_THRESHOLD_CHANGE:
				setJamThresholdChange((Integer)newValue);
				return;
			case TrafficManagerPackage.PLAN_TO_EXECUTE__COPYING_PHASE_END:
				setCopyingPhaseEnd((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.PLAN_TO_EXECUTE__ID:
				setID(ID_EDEFAULT);
				return;
			case TrafficManagerPackage.PLAN_TO_EXECUTE__END_PHASE:
				setEndPhase(END_PHASE_EDEFAULT);
				return;
			case TrafficManagerPackage.PLAN_TO_EXECUTE__REPORT_LIGHTS_FAILED:
				setReportLightsFailed(REPORT_LIGHTS_FAILED_EDEFAULT);
				return;
			case TrafficManagerPackage.PLAN_TO_EXECUTE__JAM_THRESHOLD_CHANGE:
				setJamThresholdChange(JAM_THRESHOLD_CHANGE_EDEFAULT);
				return;
			case TrafficManagerPackage.PLAN_TO_EXECUTE__COPYING_PHASE_END:
				setCopyingPhaseEnd(COPYING_PHASE_END_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.PLAN_TO_EXECUTE__ID:
				return ID_EDEFAULT == null ? getID() != null : !ID_EDEFAULT.equals(getID());
			case TrafficManagerPackage.PLAN_TO_EXECUTE__END_PHASE:
				return isEndPhase() != END_PHASE_EDEFAULT;
			case TrafficManagerPackage.PLAN_TO_EXECUTE__REPORT_LIGHTS_FAILED:
				return isReportLightsFailed() != REPORT_LIGHTS_FAILED_EDEFAULT;
			case TrafficManagerPackage.PLAN_TO_EXECUTE__JAM_THRESHOLD_CHANGE:
				return getJamThresholdChange() != JAM_THRESHOLD_CHANGE_EDEFAULT;
			case TrafficManagerPackage.PLAN_TO_EXECUTE__COPYING_PHASE_END:
				return isCopyingPhaseEnd() != COPYING_PHASE_END_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //PlanToExecuteImpl
