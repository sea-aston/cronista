/**
 */
package TrafficManager.impl;

import TrafficManager.AnalysisControls;
import TrafficManager.AnalysisResults;
import TrafficManager.ExecutionResults;
import TrafficManager.LaneAreaDetector;
import TrafficManager.Manager;
import TrafficManager.MonitorControls;
import TrafficManager.MonitorResults;
import TrafficManager.PlanToExecute;
import TrafficManager.SmartController;
import TrafficManager.TrafficLight;
import TrafficManager.TrafficManagerFactory;
import TrafficManager.TrafficManagerPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TrafficManagerPackageImpl extends EPackageImpl implements TrafficManagerPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass managerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass laneAreaDetectorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass trafficLightEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass monitorControlsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass monitorResultsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass analysisControlsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass analysisResultsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass planToExecuteEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass executionResultsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass smartControllerEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see TrafficManager.TrafficManagerPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TrafficManagerPackageImpl() {
		super(eNS_URI, TrafficManagerFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link TrafficManagerPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TrafficManagerPackage init() {
		if (isInited) return (TrafficManagerPackage)EPackage.Registry.INSTANCE.getEPackage(TrafficManagerPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredTrafficManagerPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		TrafficManagerPackageImpl theTrafficManagerPackage = registeredTrafficManagerPackage instanceof TrafficManagerPackageImpl ? (TrafficManagerPackageImpl)registeredTrafficManagerPackage : new TrafficManagerPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theTrafficManagerPackage.createPackageContents();

		// Initialize created meta-data
		theTrafficManagerPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTrafficManagerPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TrafficManagerPackage.eNS_URI, theTrafficManagerPackage);
		return theTrafficManagerPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getManager() {
		return managerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getManager_Time() {
		return (EAttribute)managerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getManager_SmartController() {
		return (EReference)managerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLaneAreaDetector() {
		return laneAreaDetectorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLaneAreaDetector_ID() {
		return (EAttribute)laneAreaDetectorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLaneAreaDetector_SumoID() {
		return (EAttribute)laneAreaDetectorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLaneAreaDetector_Position() {
		return (EAttribute)laneAreaDetectorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLaneAreaDetector_Length() {
		return (EAttribute)laneAreaDetectorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLaneAreaDetector_LaneID() {
		return (EAttribute)laneAreaDetectorEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLaneAreaDetector_LastStepVehicleNumber() {
		return (EAttribute)laneAreaDetectorEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLaneAreaDetector_LastStepMeanSpeed() {
		return (EAttribute)laneAreaDetectorEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLaneAreaDetector_LastStepVehicleIDs() {
		return (EAttribute)laneAreaDetectorEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLaneAreaDetector_LastStepOccupancy() {
		return (EAttribute)laneAreaDetectorEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLaneAreaDetector_JamLengthVehicle() {
		return (EAttribute)laneAreaDetectorEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLaneAreaDetector_JamLenghtMeters() {
		return (EAttribute)laneAreaDetectorEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTrafficLight() {
		return trafficLightEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTrafficLight_ID() {
		return (EAttribute)trafficLightEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTrafficLight_SumoID() {
		return (EAttribute)trafficLightEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTrafficLight_RedYellowGreenState() {
		return (EAttribute)trafficLightEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTrafficLight_PhaseDuration() {
		return (EAttribute)trafficLightEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTrafficLight_ControlledLanes() {
		return (EAttribute)trafficLightEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTrafficLight_Phase() {
		return (EAttribute)trafficLightEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTrafficLight_Program() {
		return (EAttribute)trafficLightEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTrafficLight_CompleteRedYellowGreenDefinition() {
		return (EAttribute)trafficLightEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTrafficLight_NextSwitch() {
		return (EAttribute)trafficLightEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMonitorControls() {
		return monitorControlsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitorControls_ID() {
		return (EAttribute)monitorControlsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitorControls_TrafficLight() {
		return (EAttribute)monitorControlsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitorControls_LaneAreaDetectors() {
		return (EAttribute)monitorControlsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitorControls_CheckPhaseEnd() {
		return (EAttribute)monitorControlsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMonitorResults() {
		return monitorResultsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitorResults_ID() {
		return (EAttribute)monitorResultsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitorResults_TrafficLightSeen() {
		return (EAttribute)monitorResultsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitorResults_LaneAreaDetectorSeen() {
		return (EAttribute)monitorResultsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitorResults_PhaseEndedSeen() {
		return (EAttribute)monitorResultsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnalysisControls() {
		return analysisControlsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnalysisControls_ID() {
		return (EAttribute)analysisControlsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnalysisControls_JamThreshold() {
		return (EAttribute)analysisControlsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnalysisControls_Expect_Program_OFF() {
		return (EAttribute)analysisControlsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnalysisControls_CopyPhaseEnd() {
		return (EAttribute)analysisControlsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnalysisResults() {
		return analysisResultsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnalysisResults_ID() {
		return (EAttribute)analysisResultsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnalysisResults_LightsFailed() {
		return (EAttribute)analysisResultsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnalysisResults_LADsJammed() {
		return (EAttribute)analysisResultsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnalysisResults_PlanToCopyPhaseEnd() {
		return (EAttribute)analysisResultsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPlanToExecute() {
		return planToExecuteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPlanToExecute_ID() {
		return (EAttribute)planToExecuteEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPlanToExecute_EndPhase() {
		return (EAttribute)planToExecuteEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPlanToExecute_ReportLightsFailed() {
		return (EAttribute)planToExecuteEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPlanToExecute_JamThresholdChange() {
		return (EAttribute)planToExecuteEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPlanToExecute_CopyingPhaseEnd() {
		return (EAttribute)planToExecuteEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExecutionResults() {
		return executionResultsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExecutionResults_ID() {
		return (EAttribute)executionResultsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExecutionResults_PhaseEnded() {
		return (EAttribute)executionResultsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExecutionResults_ReportedLightsFailed() {
		return (EAttribute)executionResultsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExecutionResults_ChangedJamThreshold() {
		return (EAttribute)executionResultsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSmartController() {
		return smartControllerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSmartController_ID() {
		return (EAttribute)smartControllerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSmartController_ExecutionResults() {
		return (EReference)smartControllerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSmartController_PlanToExecute() {
		return (EReference)smartControllerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSmartController_AnalysisResults() {
		return (EReference)smartControllerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSmartController_AnalysisControls() {
		return (EReference)smartControllerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSmartController_LaneAreaDetectors() {
		return (EReference)smartControllerEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSmartController_TrafficLight() {
		return (EReference)smartControllerEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSmartController_MonitorControls() {
		return (EReference)smartControllerEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSmartController_MonitorResults() {
		return (EReference)smartControllerEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TrafficManagerFactory getTrafficManagerFactory() {
		return (TrafficManagerFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		managerEClass = createEClass(MANAGER);
		createEAttribute(managerEClass, MANAGER__TIME);
		createEReference(managerEClass, MANAGER__SMART_CONTROLLER);

		laneAreaDetectorEClass = createEClass(LANE_AREA_DETECTOR);
		createEAttribute(laneAreaDetectorEClass, LANE_AREA_DETECTOR__ID);
		createEAttribute(laneAreaDetectorEClass, LANE_AREA_DETECTOR__SUMO_ID);
		createEAttribute(laneAreaDetectorEClass, LANE_AREA_DETECTOR__POSITION);
		createEAttribute(laneAreaDetectorEClass, LANE_AREA_DETECTOR__LENGTH);
		createEAttribute(laneAreaDetectorEClass, LANE_AREA_DETECTOR__LANE_ID);
		createEAttribute(laneAreaDetectorEClass, LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_NUMBER);
		createEAttribute(laneAreaDetectorEClass, LANE_AREA_DETECTOR__LAST_STEP_MEAN_SPEED);
		createEAttribute(laneAreaDetectorEClass, LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_IDS);
		createEAttribute(laneAreaDetectorEClass, LANE_AREA_DETECTOR__LAST_STEP_OCCUPANCY);
		createEAttribute(laneAreaDetectorEClass, LANE_AREA_DETECTOR__JAM_LENGTH_VEHICLE);
		createEAttribute(laneAreaDetectorEClass, LANE_AREA_DETECTOR__JAM_LENGHT_METERS);

		trafficLightEClass = createEClass(TRAFFIC_LIGHT);
		createEAttribute(trafficLightEClass, TRAFFIC_LIGHT__ID);
		createEAttribute(trafficLightEClass, TRAFFIC_LIGHT__SUMO_ID);
		createEAttribute(trafficLightEClass, TRAFFIC_LIGHT__RED_YELLOW_GREEN_STATE);
		createEAttribute(trafficLightEClass, TRAFFIC_LIGHT__PHASE_DURATION);
		createEAttribute(trafficLightEClass, TRAFFIC_LIGHT__CONTROLLED_LANES);
		createEAttribute(trafficLightEClass, TRAFFIC_LIGHT__PHASE);
		createEAttribute(trafficLightEClass, TRAFFIC_LIGHT__PROGRAM);
		createEAttribute(trafficLightEClass, TRAFFIC_LIGHT__COMPLETE_RED_YELLOW_GREEN_DEFINITION);
		createEAttribute(trafficLightEClass, TRAFFIC_LIGHT__NEXT_SWITCH);

		monitorControlsEClass = createEClass(MONITOR_CONTROLS);
		createEAttribute(monitorControlsEClass, MONITOR_CONTROLS__ID);
		createEAttribute(monitorControlsEClass, MONITOR_CONTROLS__TRAFFIC_LIGHT);
		createEAttribute(monitorControlsEClass, MONITOR_CONTROLS__LANE_AREA_DETECTORS);
		createEAttribute(monitorControlsEClass, MONITOR_CONTROLS__CHECK_PHASE_END);

		monitorResultsEClass = createEClass(MONITOR_RESULTS);
		createEAttribute(monitorResultsEClass, MONITOR_RESULTS__ID);
		createEAttribute(monitorResultsEClass, MONITOR_RESULTS__TRAFFIC_LIGHT_SEEN);
		createEAttribute(monitorResultsEClass, MONITOR_RESULTS__LANE_AREA_DETECTOR_SEEN);
		createEAttribute(monitorResultsEClass, MONITOR_RESULTS__PHASE_ENDED_SEEN);

		analysisControlsEClass = createEClass(ANALYSIS_CONTROLS);
		createEAttribute(analysisControlsEClass, ANALYSIS_CONTROLS__ID);
		createEAttribute(analysisControlsEClass, ANALYSIS_CONTROLS__JAM_THRESHOLD);
		createEAttribute(analysisControlsEClass, ANALYSIS_CONTROLS__EXPECT_PROGRAM_OFF);
		createEAttribute(analysisControlsEClass, ANALYSIS_CONTROLS__COPY_PHASE_END);

		analysisResultsEClass = createEClass(ANALYSIS_RESULTS);
		createEAttribute(analysisResultsEClass, ANALYSIS_RESULTS__ID);
		createEAttribute(analysisResultsEClass, ANALYSIS_RESULTS__LIGHTS_FAILED);
		createEAttribute(analysisResultsEClass, ANALYSIS_RESULTS__LA_DS_JAMMED);
		createEAttribute(analysisResultsEClass, ANALYSIS_RESULTS__PLAN_TO_COPY_PHASE_END);

		planToExecuteEClass = createEClass(PLAN_TO_EXECUTE);
		createEAttribute(planToExecuteEClass, PLAN_TO_EXECUTE__ID);
		createEAttribute(planToExecuteEClass, PLAN_TO_EXECUTE__END_PHASE);
		createEAttribute(planToExecuteEClass, PLAN_TO_EXECUTE__REPORT_LIGHTS_FAILED);
		createEAttribute(planToExecuteEClass, PLAN_TO_EXECUTE__JAM_THRESHOLD_CHANGE);
		createEAttribute(planToExecuteEClass, PLAN_TO_EXECUTE__COPYING_PHASE_END);

		executionResultsEClass = createEClass(EXECUTION_RESULTS);
		createEAttribute(executionResultsEClass, EXECUTION_RESULTS__ID);
		createEAttribute(executionResultsEClass, EXECUTION_RESULTS__PHASE_ENDED);
		createEAttribute(executionResultsEClass, EXECUTION_RESULTS__REPORTED_LIGHTS_FAILED);
		createEAttribute(executionResultsEClass, EXECUTION_RESULTS__CHANGED_JAM_THRESHOLD);

		smartControllerEClass = createEClass(SMART_CONTROLLER);
		createEAttribute(smartControllerEClass, SMART_CONTROLLER__ID);
		createEReference(smartControllerEClass, SMART_CONTROLLER__EXECUTION_RESULTS);
		createEReference(smartControllerEClass, SMART_CONTROLLER__PLAN_TO_EXECUTE);
		createEReference(smartControllerEClass, SMART_CONTROLLER__ANALYSIS_RESULTS);
		createEReference(smartControllerEClass, SMART_CONTROLLER__ANALYSIS_CONTROLS);
		createEReference(smartControllerEClass, SMART_CONTROLLER__LANE_AREA_DETECTORS);
		createEReference(smartControllerEClass, SMART_CONTROLLER__TRAFFIC_LIGHT);
		createEReference(smartControllerEClass, SMART_CONTROLLER__MONITOR_CONTROLS);
		createEReference(smartControllerEClass, SMART_CONTROLLER__MONITOR_RESULTS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(managerEClass, Manager.class, "Manager", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getManager_Time(), ecorePackage.getEDouble(), "Time", null, 0, 1, Manager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getManager_SmartController(), this.getSmartController(), null, "SmartController", null, 0, -1, Manager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(laneAreaDetectorEClass, LaneAreaDetector.class, "LaneAreaDetector", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLaneAreaDetector_ID(), ecorePackage.getEString(), "ID", null, 0, 1, LaneAreaDetector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLaneAreaDetector_SumoID(), ecorePackage.getEString(), "SumoID", null, 0, 1, LaneAreaDetector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLaneAreaDetector_Position(), ecorePackage.getEDouble(), "Position", null, 0, 1, LaneAreaDetector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLaneAreaDetector_Length(), ecorePackage.getEDouble(), "Length", null, 0, 1, LaneAreaDetector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLaneAreaDetector_LaneID(), ecorePackage.getEString(), "LaneID", null, 0, 1, LaneAreaDetector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLaneAreaDetector_LastStepVehicleNumber(), ecorePackage.getEInt(), "LastStepVehicleNumber", null, 0, 1, LaneAreaDetector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLaneAreaDetector_LastStepMeanSpeed(), ecorePackage.getEDouble(), "LastStepMeanSpeed", null, 0, 1, LaneAreaDetector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLaneAreaDetector_LastStepVehicleIDs(), ecorePackage.getEString(), "LastStepVehicleIDs", null, 0, 1, LaneAreaDetector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLaneAreaDetector_LastStepOccupancy(), ecorePackage.getEDouble(), "LastStepOccupancy", null, 0, 1, LaneAreaDetector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLaneAreaDetector_JamLengthVehicle(), ecorePackage.getEInt(), "JamLengthVehicle", null, 0, 1, LaneAreaDetector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLaneAreaDetector_JamLenghtMeters(), ecorePackage.getEDouble(), "JamLenghtMeters", null, 0, 1, LaneAreaDetector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(trafficLightEClass, TrafficLight.class, "TrafficLight", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTrafficLight_ID(), ecorePackage.getEString(), "ID", null, 0, 1, TrafficLight.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTrafficLight_SumoID(), ecorePackage.getEString(), "SumoID", null, 0, 1, TrafficLight.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTrafficLight_RedYellowGreenState(), ecorePackage.getEString(), "RedYellowGreenState", null, 0, 1, TrafficLight.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTrafficLight_PhaseDuration(), ecorePackage.getEDouble(), "PhaseDuration", null, 0, 1, TrafficLight.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTrafficLight_ControlledLanes(), ecorePackage.getEString(), "ControlledLanes", null, 0, -1, TrafficLight.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTrafficLight_Phase(), ecorePackage.getEInt(), "Phase", null, 0, 1, TrafficLight.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTrafficLight_Program(), ecorePackage.getEString(), "Program", null, 0, 1, TrafficLight.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTrafficLight_CompleteRedYellowGreenDefinition(), ecorePackage.getEString(), "CompleteRedYellowGreenDefinition", null, 0, 1, TrafficLight.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTrafficLight_NextSwitch(), ecorePackage.getEDouble(), "NextSwitch", null, 0, 1, TrafficLight.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(monitorControlsEClass, MonitorControls.class, "MonitorControls", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMonitorControls_ID(), ecorePackage.getEString(), "ID", null, 0, 1, MonitorControls.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitorControls_TrafficLight(), ecorePackage.getEString(), "TrafficLight", null, 0, 1, MonitorControls.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitorControls_LaneAreaDetectors(), ecorePackage.getEString(), "LaneAreaDetectors", null, 0, -1, MonitorControls.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitorControls_CheckPhaseEnd(), ecorePackage.getEBoolean(), "CheckPhaseEnd", null, 0, 1, MonitorControls.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(monitorResultsEClass, MonitorResults.class, "MonitorResults", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMonitorResults_ID(), ecorePackage.getEString(), "ID", null, 0, 1, MonitorResults.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitorResults_TrafficLightSeen(), ecorePackage.getEString(), "TrafficLightSeen", null, 0, 1, MonitorResults.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitorResults_LaneAreaDetectorSeen(), ecorePackage.getEString(), "LaneAreaDetectorSeen", null, 0, -1, MonitorResults.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitorResults_PhaseEndedSeen(), ecorePackage.getEBoolean(), "PhaseEndedSeen", null, 0, 1, MonitorResults.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(analysisControlsEClass, AnalysisControls.class, "AnalysisControls", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAnalysisControls_ID(), ecorePackage.getEString(), "ID", null, 0, 1, AnalysisControls.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAnalysisControls_JamThreshold(), ecorePackage.getEInt(), "JamThreshold", "3", 1, 1, AnalysisControls.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAnalysisControls_Expect_Program_OFF(), ecorePackage.getEBoolean(), "Expect_Program_OFF", "true", 0, 1, AnalysisControls.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAnalysisControls_CopyPhaseEnd(), ecorePackage.getEBoolean(), "CopyPhaseEnd", null, 0, 1, AnalysisControls.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(analysisResultsEClass, AnalysisResults.class, "AnalysisResults", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAnalysisResults_ID(), ecorePackage.getEString(), "ID", null, 0, 1, AnalysisResults.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAnalysisResults_LightsFailed(), ecorePackage.getEBoolean(), "LightsFailed", null, 1, 1, AnalysisResults.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAnalysisResults_LADsJammed(), ecorePackage.getEInt(), "LADsJammed", null, 0, 1, AnalysisResults.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAnalysisResults_PlanToCopyPhaseEnd(), ecorePackage.getEBoolean(), "PlanToCopyPhaseEnd", null, 0, 1, AnalysisResults.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(planToExecuteEClass, PlanToExecute.class, "PlanToExecute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPlanToExecute_ID(), ecorePackage.getEString(), "ID", null, 0, 1, PlanToExecute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPlanToExecute_EndPhase(), ecorePackage.getEBoolean(), "EndPhase", null, 0, 1, PlanToExecute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPlanToExecute_ReportLightsFailed(), ecorePackage.getEBoolean(), "ReportLightsFailed", null, 1, 1, PlanToExecute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPlanToExecute_JamThresholdChange(), ecorePackage.getEInt(), "JamThresholdChange", "0", 0, 1, PlanToExecute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPlanToExecute_CopyingPhaseEnd(), ecorePackage.getEBoolean(), "CopyingPhaseEnd", null, 0, 1, PlanToExecute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(executionResultsEClass, ExecutionResults.class, "ExecutionResults", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExecutionResults_ID(), ecorePackage.getEString(), "ID", null, 0, 1, ExecutionResults.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExecutionResults_PhaseEnded(), ecorePackage.getEBoolean(), "PhaseEnded", null, 0, 1, ExecutionResults.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExecutionResults_ReportedLightsFailed(), ecorePackage.getEBoolean(), "ReportedLightsFailed", null, 1, 1, ExecutionResults.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExecutionResults_ChangedJamThreshold(), ecorePackage.getEInt(), "ChangedJamThreshold", null, 0, 1, ExecutionResults.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(smartControllerEClass, SmartController.class, "SmartController", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSmartController_ID(), ecorePackage.getEString(), "ID", null, 0, 1, SmartController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSmartController_ExecutionResults(), this.getExecutionResults(), null, "ExecutionResults", null, 1, 1, SmartController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSmartController_PlanToExecute(), this.getPlanToExecute(), null, "PlanToExecute", null, 1, 1, SmartController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSmartController_AnalysisResults(), this.getAnalysisResults(), null, "AnalysisResults", null, 1, 1, SmartController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSmartController_AnalysisControls(), this.getAnalysisControls(), null, "AnalysisControls", null, 1, 1, SmartController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSmartController_LaneAreaDetectors(), this.getLaneAreaDetector(), null, "LaneAreaDetectors", null, 0, -1, SmartController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSmartController_TrafficLight(), this.getTrafficLight(), null, "TrafficLight", null, 0, 1, SmartController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSmartController_MonitorControls(), this.getMonitorControls(), null, "MonitorControls", null, 1, 1, SmartController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSmartController_MonitorResults(), this.getMonitorResults(), null, "MonitorResults", null, 1, 1, SmartController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //TrafficManagerPackageImpl
