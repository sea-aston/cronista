/**
 */
package TrafficManager.impl;

import TrafficManager.MonitorResults;
import TrafficManager.TrafficManagerPackage;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import uk.ac.aston.provlayer.observer.model.cdo.LoggingCDOObjectImpl2;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Monitor Results</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.impl.MonitorResultsImpl#getID <em>ID</em>}</li>
 *   <li>{@link TrafficManager.impl.MonitorResultsImpl#getTrafficLightSeen <em>Traffic Light Seen</em>}</li>
 *   <li>{@link TrafficManager.impl.MonitorResultsImpl#getLaneAreaDetectorSeen <em>Lane Area Detector Seen</em>}</li>
 *   <li>{@link TrafficManager.impl.MonitorResultsImpl#isPhaseEndedSeen <em>Phase Ended Seen</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MonitorResultsImpl extends LoggingCDOObjectImpl2 implements MonitorResults {
	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getTrafficLightSeen() <em>Traffic Light Seen</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrafficLightSeen()
	 * @generated
	 * @ordered
	 */
	protected static final String TRAFFIC_LIGHT_SEEN_EDEFAULT = null;

	/**
	 * The default value of the '{@link #isPhaseEndedSeen() <em>Phase Ended Seen</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPhaseEndedSeen()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PHASE_ENDED_SEEN_EDEFAULT = false;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MonitorResultsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TrafficManagerPackage.Literals.MONITOR_RESULTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getID() {
		return (String)eDynamicGet(TrafficManagerPackage.MONITOR_RESULTS__ID, TrafficManagerPackage.Literals.MONITOR_RESULTS__ID, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(String newID) {
		eDynamicSet(TrafficManagerPackage.MONITOR_RESULTS__ID, TrafficManagerPackage.Literals.MONITOR_RESULTS__ID, newID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTrafficLightSeen() {
		return (String)eDynamicGet(TrafficManagerPackage.MONITOR_RESULTS__TRAFFIC_LIGHT_SEEN, TrafficManagerPackage.Literals.MONITOR_RESULTS__TRAFFIC_LIGHT_SEEN, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTrafficLightSeen(String newTrafficLightSeen) {
		eDynamicSet(TrafficManagerPackage.MONITOR_RESULTS__TRAFFIC_LIGHT_SEEN, TrafficManagerPackage.Literals.MONITOR_RESULTS__TRAFFIC_LIGHT_SEEN, newTrafficLightSeen);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<String> getLaneAreaDetectorSeen() {
		return (EList<String>)eDynamicGet(TrafficManagerPackage.MONITOR_RESULTS__LANE_AREA_DETECTOR_SEEN, TrafficManagerPackage.Literals.MONITOR_RESULTS__LANE_AREA_DETECTOR_SEEN, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPhaseEndedSeen() {
		return (Boolean)eDynamicGet(TrafficManagerPackage.MONITOR_RESULTS__PHASE_ENDED_SEEN, TrafficManagerPackage.Literals.MONITOR_RESULTS__PHASE_ENDED_SEEN, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhaseEndedSeen(boolean newPhaseEndedSeen) {
		eDynamicSet(TrafficManagerPackage.MONITOR_RESULTS__PHASE_ENDED_SEEN, TrafficManagerPackage.Literals.MONITOR_RESULTS__PHASE_ENDED_SEEN, newPhaseEndedSeen);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TrafficManagerPackage.MONITOR_RESULTS__ID:
				return getID();
			case TrafficManagerPackage.MONITOR_RESULTS__TRAFFIC_LIGHT_SEEN:
				return getTrafficLightSeen();
			case TrafficManagerPackage.MONITOR_RESULTS__LANE_AREA_DETECTOR_SEEN:
				return getLaneAreaDetectorSeen();
			case TrafficManagerPackage.MONITOR_RESULTS__PHASE_ENDED_SEEN:
				return isPhaseEndedSeen();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TrafficManagerPackage.MONITOR_RESULTS__ID:
				setID((String)newValue);
				return;
			case TrafficManagerPackage.MONITOR_RESULTS__TRAFFIC_LIGHT_SEEN:
				setTrafficLightSeen((String)newValue);
				return;
			case TrafficManagerPackage.MONITOR_RESULTS__LANE_AREA_DETECTOR_SEEN:
				getLaneAreaDetectorSeen().clear();
				getLaneAreaDetectorSeen().addAll((Collection<? extends String>)newValue);
				return;
			case TrafficManagerPackage.MONITOR_RESULTS__PHASE_ENDED_SEEN:
				setPhaseEndedSeen((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.MONITOR_RESULTS__ID:
				setID(ID_EDEFAULT);
				return;
			case TrafficManagerPackage.MONITOR_RESULTS__TRAFFIC_LIGHT_SEEN:
				setTrafficLightSeen(TRAFFIC_LIGHT_SEEN_EDEFAULT);
				return;
			case TrafficManagerPackage.MONITOR_RESULTS__LANE_AREA_DETECTOR_SEEN:
				getLaneAreaDetectorSeen().clear();
				return;
			case TrafficManagerPackage.MONITOR_RESULTS__PHASE_ENDED_SEEN:
				setPhaseEndedSeen(PHASE_ENDED_SEEN_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.MONITOR_RESULTS__ID:
				return ID_EDEFAULT == null ? getID() != null : !ID_EDEFAULT.equals(getID());
			case TrafficManagerPackage.MONITOR_RESULTS__TRAFFIC_LIGHT_SEEN:
				return TRAFFIC_LIGHT_SEEN_EDEFAULT == null ? getTrafficLightSeen() != null : !TRAFFIC_LIGHT_SEEN_EDEFAULT.equals(getTrafficLightSeen());
			case TrafficManagerPackage.MONITOR_RESULTS__LANE_AREA_DETECTOR_SEEN:
				return !getLaneAreaDetectorSeen().isEmpty();
			case TrafficManagerPackage.MONITOR_RESULTS__PHASE_ENDED_SEEN:
				return isPhaseEndedSeen() != PHASE_ENDED_SEEN_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //MonitorResultsImpl
