/**
 */
package TrafficManager.impl;

import TrafficManager.ExecutionResults;
import TrafficManager.TrafficManagerPackage;
import uk.ac.aston.provlayer.observer.annotations.ObserveModelEMFeDynamic;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.internal.cdo.CDOObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Execution Results</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.impl.ExecutionResultsImpl#getID <em>ID</em>}</li>
 *   <li>{@link TrafficManager.impl.ExecutionResultsImpl#isPhaseEnded <em>Phase Ended</em>}</li>
 *   <li>{@link TrafficManager.impl.ExecutionResultsImpl#isReportedLightsFailed <em>Reported Lights Failed</em>}</li>
 *   <li>{@link TrafficManager.impl.ExecutionResultsImpl#getChangedJamThreshold <em>Changed Jam Threshold</em>}</li>
 * </ul>
 *
 * @generated
 */
@ObserveModelEMFeDynamic
public class ExecutionResultsImpl extends CDOObjectImpl implements ExecutionResults {
	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #isPhaseEnded() <em>Phase Ended</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPhaseEnded()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PHASE_ENDED_EDEFAULT = false;

	/**
	 * The default value of the '{@link #isReportedLightsFailed() <em>Reported Lights Failed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isReportedLightsFailed()
	 * @generated
	 * @ordered
	 */
	protected static final boolean REPORTED_LIGHTS_FAILED_EDEFAULT = false;

	/**
	 * The default value of the '{@link #getChangedJamThreshold() <em>Changed Jam Threshold</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChangedJamThreshold()
	 * @generated
	 * @ordered
	 */
	protected static final int CHANGED_JAM_THRESHOLD_EDEFAULT = 0;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExecutionResultsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TrafficManagerPackage.Literals.EXECUTION_RESULTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getID() {
		return (String)eDynamicGet(TrafficManagerPackage.EXECUTION_RESULTS__ID, TrafficManagerPackage.Literals.EXECUTION_RESULTS__ID, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(String newID) {
		eDynamicSet(TrafficManagerPackage.EXECUTION_RESULTS__ID, TrafficManagerPackage.Literals.EXECUTION_RESULTS__ID, newID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPhaseEnded() {
		return (Boolean)eDynamicGet(TrafficManagerPackage.EXECUTION_RESULTS__PHASE_ENDED, TrafficManagerPackage.Literals.EXECUTION_RESULTS__PHASE_ENDED, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhaseEnded(boolean newPhaseEnded) {
		eDynamicSet(TrafficManagerPackage.EXECUTION_RESULTS__PHASE_ENDED, TrafficManagerPackage.Literals.EXECUTION_RESULTS__PHASE_ENDED, newPhaseEnded);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isReportedLightsFailed() {
		return (Boolean)eDynamicGet(TrafficManagerPackage.EXECUTION_RESULTS__REPORTED_LIGHTS_FAILED, TrafficManagerPackage.Literals.EXECUTION_RESULTS__REPORTED_LIGHTS_FAILED, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReportedLightsFailed(boolean newReportedLightsFailed) {
		eDynamicSet(TrafficManagerPackage.EXECUTION_RESULTS__REPORTED_LIGHTS_FAILED, TrafficManagerPackage.Literals.EXECUTION_RESULTS__REPORTED_LIGHTS_FAILED, newReportedLightsFailed);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getChangedJamThreshold() {
		return (Integer)eDynamicGet(TrafficManagerPackage.EXECUTION_RESULTS__CHANGED_JAM_THRESHOLD, TrafficManagerPackage.Literals.EXECUTION_RESULTS__CHANGED_JAM_THRESHOLD, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangedJamThreshold(int newChangedJamThreshold) {
		eDynamicSet(TrafficManagerPackage.EXECUTION_RESULTS__CHANGED_JAM_THRESHOLD, TrafficManagerPackage.Literals.EXECUTION_RESULTS__CHANGED_JAM_THRESHOLD, newChangedJamThreshold);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TrafficManagerPackage.EXECUTION_RESULTS__ID:
				return getID();
			case TrafficManagerPackage.EXECUTION_RESULTS__PHASE_ENDED:
				return isPhaseEnded();
			case TrafficManagerPackage.EXECUTION_RESULTS__REPORTED_LIGHTS_FAILED:
				return isReportedLightsFailed();
			case TrafficManagerPackage.EXECUTION_RESULTS__CHANGED_JAM_THRESHOLD:
				return getChangedJamThreshold();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TrafficManagerPackage.EXECUTION_RESULTS__ID:
				setID((String)newValue);
				return;
			case TrafficManagerPackage.EXECUTION_RESULTS__PHASE_ENDED:
				setPhaseEnded((Boolean)newValue);
				return;
			case TrafficManagerPackage.EXECUTION_RESULTS__REPORTED_LIGHTS_FAILED:
				setReportedLightsFailed((Boolean)newValue);
				return;
			case TrafficManagerPackage.EXECUTION_RESULTS__CHANGED_JAM_THRESHOLD:
				setChangedJamThreshold((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.EXECUTION_RESULTS__ID:
				setID(ID_EDEFAULT);
				return;
			case TrafficManagerPackage.EXECUTION_RESULTS__PHASE_ENDED:
				setPhaseEnded(PHASE_ENDED_EDEFAULT);
				return;
			case TrafficManagerPackage.EXECUTION_RESULTS__REPORTED_LIGHTS_FAILED:
				setReportedLightsFailed(REPORTED_LIGHTS_FAILED_EDEFAULT);
				return;
			case TrafficManagerPackage.EXECUTION_RESULTS__CHANGED_JAM_THRESHOLD:
				setChangedJamThreshold(CHANGED_JAM_THRESHOLD_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.EXECUTION_RESULTS__ID:
				return ID_EDEFAULT == null ? getID() != null : !ID_EDEFAULT.equals(getID());
			case TrafficManagerPackage.EXECUTION_RESULTS__PHASE_ENDED:
				return isPhaseEnded() != PHASE_ENDED_EDEFAULT;
			case TrafficManagerPackage.EXECUTION_RESULTS__REPORTED_LIGHTS_FAILED:
				return isReportedLightsFailed() != REPORTED_LIGHTS_FAILED_EDEFAULT;
			case TrafficManagerPackage.EXECUTION_RESULTS__CHANGED_JAM_THRESHOLD:
				return getChangedJamThreshold() != CHANGED_JAM_THRESHOLD_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //ExecutionResultsImpl
