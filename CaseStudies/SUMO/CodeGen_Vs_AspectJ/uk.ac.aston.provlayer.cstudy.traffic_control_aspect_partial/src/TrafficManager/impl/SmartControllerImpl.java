/**
 */
package TrafficManager.impl;

import TrafficManager.AnalysisControls;
import TrafficManager.AnalysisResults;
import TrafficManager.ExecutionResults;
import TrafficManager.LaneAreaDetector;
import TrafficManager.MonitorControls;
import TrafficManager.MonitorResults;
import TrafficManager.PlanToExecute;
import TrafficManager.SmartController;
import TrafficManager.TrafficLight;
import TrafficManager.TrafficManagerPackage;
import uk.ac.aston.provlayer.observer.annotations.ObserveModelEMFeDynamic;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.emf.internal.cdo.CDOObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Smart Controller</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.impl.SmartControllerImpl#getID <em>ID</em>}</li>
 *   <li>{@link TrafficManager.impl.SmartControllerImpl#getExecutionResults <em>Execution Results</em>}</li>
 *   <li>{@link TrafficManager.impl.SmartControllerImpl#getPlanToExecute <em>Plan To Execute</em>}</li>
 *   <li>{@link TrafficManager.impl.SmartControllerImpl#getAnalysisResults <em>Analysis Results</em>}</li>
 *   <li>{@link TrafficManager.impl.SmartControllerImpl#getAnalysisControls <em>Analysis Controls</em>}</li>
 *   <li>{@link TrafficManager.impl.SmartControllerImpl#getLaneAreaDetectors <em>Lane Area Detectors</em>}</li>
 *   <li>{@link TrafficManager.impl.SmartControllerImpl#getTrafficLight <em>Traffic Light</em>}</li>
 *   <li>{@link TrafficManager.impl.SmartControllerImpl#getMonitorControls <em>Monitor Controls</em>}</li>
 *   <li>{@link TrafficManager.impl.SmartControllerImpl#getMonitorResults <em>Monitor Results</em>}</li>
 * </ul>
 *
 * @generated
 */
//@ObserveModelEMFeDynamic
public class SmartControllerImpl extends CDOObjectImpl implements SmartController {
	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SmartControllerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TrafficManagerPackage.Literals.SMART_CONTROLLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getID() {
		return (String)eDynamicGet(TrafficManagerPackage.SMART_CONTROLLER__ID, TrafficManagerPackage.Literals.SMART_CONTROLLER__ID, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(String newID) {
		eDynamicSet(TrafficManagerPackage.SMART_CONTROLLER__ID, TrafficManagerPackage.Literals.SMART_CONTROLLER__ID, newID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionResults getExecutionResults() {
		return (ExecutionResults)eDynamicGet(TrafficManagerPackage.SMART_CONTROLLER__EXECUTION_RESULTS, TrafficManagerPackage.Literals.SMART_CONTROLLER__EXECUTION_RESULTS, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExecutionResults(ExecutionResults newExecutionResults, NotificationChain msgs) {
		msgs = eDynamicInverseAdd((InternalEObject)newExecutionResults, TrafficManagerPackage.SMART_CONTROLLER__EXECUTION_RESULTS, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutionResults(ExecutionResults newExecutionResults) {
		eDynamicSet(TrafficManagerPackage.SMART_CONTROLLER__EXECUTION_RESULTS, TrafficManagerPackage.Literals.SMART_CONTROLLER__EXECUTION_RESULTS, newExecutionResults);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PlanToExecute getPlanToExecute() {
		return (PlanToExecute)eDynamicGet(TrafficManagerPackage.SMART_CONTROLLER__PLAN_TO_EXECUTE, TrafficManagerPackage.Literals.SMART_CONTROLLER__PLAN_TO_EXECUTE, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPlanToExecute(PlanToExecute newPlanToExecute, NotificationChain msgs) {
		msgs = eDynamicInverseAdd((InternalEObject)newPlanToExecute, TrafficManagerPackage.SMART_CONTROLLER__PLAN_TO_EXECUTE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPlanToExecute(PlanToExecute newPlanToExecute) {
		eDynamicSet(TrafficManagerPackage.SMART_CONTROLLER__PLAN_TO_EXECUTE, TrafficManagerPackage.Literals.SMART_CONTROLLER__PLAN_TO_EXECUTE, newPlanToExecute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnalysisResults getAnalysisResults() {
		return (AnalysisResults)eDynamicGet(TrafficManagerPackage.SMART_CONTROLLER__ANALYSIS_RESULTS, TrafficManagerPackage.Literals.SMART_CONTROLLER__ANALYSIS_RESULTS, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnalysisResults(AnalysisResults newAnalysisResults, NotificationChain msgs) {
		msgs = eDynamicInverseAdd((InternalEObject)newAnalysisResults, TrafficManagerPackage.SMART_CONTROLLER__ANALYSIS_RESULTS, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnalysisResults(AnalysisResults newAnalysisResults) {
		eDynamicSet(TrafficManagerPackage.SMART_CONTROLLER__ANALYSIS_RESULTS, TrafficManagerPackage.Literals.SMART_CONTROLLER__ANALYSIS_RESULTS, newAnalysisResults);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnalysisControls getAnalysisControls() {
		return (AnalysisControls)eDynamicGet(TrafficManagerPackage.SMART_CONTROLLER__ANALYSIS_CONTROLS, TrafficManagerPackage.Literals.SMART_CONTROLLER__ANALYSIS_CONTROLS, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnalysisControls(AnalysisControls newAnalysisControls, NotificationChain msgs) {
		msgs = eDynamicInverseAdd((InternalEObject)newAnalysisControls, TrafficManagerPackage.SMART_CONTROLLER__ANALYSIS_CONTROLS, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnalysisControls(AnalysisControls newAnalysisControls) {
		eDynamicSet(TrafficManagerPackage.SMART_CONTROLLER__ANALYSIS_CONTROLS, TrafficManagerPackage.Literals.SMART_CONTROLLER__ANALYSIS_CONTROLS, newAnalysisControls);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<LaneAreaDetector> getLaneAreaDetectors() {
		return (EList<LaneAreaDetector>)eDynamicGet(TrafficManagerPackage.SMART_CONTROLLER__LANE_AREA_DETECTORS, TrafficManagerPackage.Literals.SMART_CONTROLLER__LANE_AREA_DETECTORS, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TrafficLight getTrafficLight() {
		return (TrafficLight)eDynamicGet(TrafficManagerPackage.SMART_CONTROLLER__TRAFFIC_LIGHT, TrafficManagerPackage.Literals.SMART_CONTROLLER__TRAFFIC_LIGHT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTrafficLight(TrafficLight newTrafficLight, NotificationChain msgs) {
		msgs = eDynamicInverseAdd((InternalEObject)newTrafficLight, TrafficManagerPackage.SMART_CONTROLLER__TRAFFIC_LIGHT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTrafficLight(TrafficLight newTrafficLight) {
		eDynamicSet(TrafficManagerPackage.SMART_CONTROLLER__TRAFFIC_LIGHT, TrafficManagerPackage.Literals.SMART_CONTROLLER__TRAFFIC_LIGHT, newTrafficLight);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MonitorControls getMonitorControls() {
		return (MonitorControls)eDynamicGet(TrafficManagerPackage.SMART_CONTROLLER__MONITOR_CONTROLS, TrafficManagerPackage.Literals.SMART_CONTROLLER__MONITOR_CONTROLS, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMonitorControls(MonitorControls newMonitorControls, NotificationChain msgs) {
		msgs = eDynamicInverseAdd((InternalEObject)newMonitorControls, TrafficManagerPackage.SMART_CONTROLLER__MONITOR_CONTROLS, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMonitorControls(MonitorControls newMonitorControls) {
		eDynamicSet(TrafficManagerPackage.SMART_CONTROLLER__MONITOR_CONTROLS, TrafficManagerPackage.Literals.SMART_CONTROLLER__MONITOR_CONTROLS, newMonitorControls);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MonitorResults getMonitorResults() {
		return (MonitorResults)eDynamicGet(TrafficManagerPackage.SMART_CONTROLLER__MONITOR_RESULTS, TrafficManagerPackage.Literals.SMART_CONTROLLER__MONITOR_RESULTS, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMonitorResults(MonitorResults newMonitorResults, NotificationChain msgs) {
		msgs = eDynamicInverseAdd((InternalEObject)newMonitorResults, TrafficManagerPackage.SMART_CONTROLLER__MONITOR_RESULTS, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMonitorResults(MonitorResults newMonitorResults) {
		eDynamicSet(TrafficManagerPackage.SMART_CONTROLLER__MONITOR_RESULTS, TrafficManagerPackage.Literals.SMART_CONTROLLER__MONITOR_RESULTS, newMonitorResults);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TrafficManagerPackage.SMART_CONTROLLER__EXECUTION_RESULTS:
				return basicSetExecutionResults(null, msgs);
			case TrafficManagerPackage.SMART_CONTROLLER__PLAN_TO_EXECUTE:
				return basicSetPlanToExecute(null, msgs);
			case TrafficManagerPackage.SMART_CONTROLLER__ANALYSIS_RESULTS:
				return basicSetAnalysisResults(null, msgs);
			case TrafficManagerPackage.SMART_CONTROLLER__ANALYSIS_CONTROLS:
				return basicSetAnalysisControls(null, msgs);
			case TrafficManagerPackage.SMART_CONTROLLER__LANE_AREA_DETECTORS:
				return ((InternalEList<?>)getLaneAreaDetectors()).basicRemove(otherEnd, msgs);
			case TrafficManagerPackage.SMART_CONTROLLER__TRAFFIC_LIGHT:
				return basicSetTrafficLight(null, msgs);
			case TrafficManagerPackage.SMART_CONTROLLER__MONITOR_CONTROLS:
				return basicSetMonitorControls(null, msgs);
			case TrafficManagerPackage.SMART_CONTROLLER__MONITOR_RESULTS:
				return basicSetMonitorResults(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TrafficManagerPackage.SMART_CONTROLLER__ID:
				return getID();
			case TrafficManagerPackage.SMART_CONTROLLER__EXECUTION_RESULTS:
				return getExecutionResults();
			case TrafficManagerPackage.SMART_CONTROLLER__PLAN_TO_EXECUTE:
				return getPlanToExecute();
			case TrafficManagerPackage.SMART_CONTROLLER__ANALYSIS_RESULTS:
				return getAnalysisResults();
			case TrafficManagerPackage.SMART_CONTROLLER__ANALYSIS_CONTROLS:
				return getAnalysisControls();
			case TrafficManagerPackage.SMART_CONTROLLER__LANE_AREA_DETECTORS:
				return getLaneAreaDetectors();
			case TrafficManagerPackage.SMART_CONTROLLER__TRAFFIC_LIGHT:
				return getTrafficLight();
			case TrafficManagerPackage.SMART_CONTROLLER__MONITOR_CONTROLS:
				return getMonitorControls();
			case TrafficManagerPackage.SMART_CONTROLLER__MONITOR_RESULTS:
				return getMonitorResults();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TrafficManagerPackage.SMART_CONTROLLER__ID:
				setID((String)newValue);
				return;
			case TrafficManagerPackage.SMART_CONTROLLER__EXECUTION_RESULTS:
				setExecutionResults((ExecutionResults)newValue);
				return;
			case TrafficManagerPackage.SMART_CONTROLLER__PLAN_TO_EXECUTE:
				setPlanToExecute((PlanToExecute)newValue);
				return;
			case TrafficManagerPackage.SMART_CONTROLLER__ANALYSIS_RESULTS:
				setAnalysisResults((AnalysisResults)newValue);
				return;
			case TrafficManagerPackage.SMART_CONTROLLER__ANALYSIS_CONTROLS:
				setAnalysisControls((AnalysisControls)newValue);
				return;
			case TrafficManagerPackage.SMART_CONTROLLER__LANE_AREA_DETECTORS:
				getLaneAreaDetectors().clear();
				getLaneAreaDetectors().addAll((Collection<? extends LaneAreaDetector>)newValue);
				return;
			case TrafficManagerPackage.SMART_CONTROLLER__TRAFFIC_LIGHT:
				setTrafficLight((TrafficLight)newValue);
				return;
			case TrafficManagerPackage.SMART_CONTROLLER__MONITOR_CONTROLS:
				setMonitorControls((MonitorControls)newValue);
				return;
			case TrafficManagerPackage.SMART_CONTROLLER__MONITOR_RESULTS:
				setMonitorResults((MonitorResults)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.SMART_CONTROLLER__ID:
				setID(ID_EDEFAULT);
				return;
			case TrafficManagerPackage.SMART_CONTROLLER__EXECUTION_RESULTS:
				setExecutionResults((ExecutionResults)null);
				return;
			case TrafficManagerPackage.SMART_CONTROLLER__PLAN_TO_EXECUTE:
				setPlanToExecute((PlanToExecute)null);
				return;
			case TrafficManagerPackage.SMART_CONTROLLER__ANALYSIS_RESULTS:
				setAnalysisResults((AnalysisResults)null);
				return;
			case TrafficManagerPackage.SMART_CONTROLLER__ANALYSIS_CONTROLS:
				setAnalysisControls((AnalysisControls)null);
				return;
			case TrafficManagerPackage.SMART_CONTROLLER__LANE_AREA_DETECTORS:
				getLaneAreaDetectors().clear();
				return;
			case TrafficManagerPackage.SMART_CONTROLLER__TRAFFIC_LIGHT:
				setTrafficLight((TrafficLight)null);
				return;
			case TrafficManagerPackage.SMART_CONTROLLER__MONITOR_CONTROLS:
				setMonitorControls((MonitorControls)null);
				return;
			case TrafficManagerPackage.SMART_CONTROLLER__MONITOR_RESULTS:
				setMonitorResults((MonitorResults)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.SMART_CONTROLLER__ID:
				return ID_EDEFAULT == null ? getID() != null : !ID_EDEFAULT.equals(getID());
			case TrafficManagerPackage.SMART_CONTROLLER__EXECUTION_RESULTS:
				return getExecutionResults() != null;
			case TrafficManagerPackage.SMART_CONTROLLER__PLAN_TO_EXECUTE:
				return getPlanToExecute() != null;
			case TrafficManagerPackage.SMART_CONTROLLER__ANALYSIS_RESULTS:
				return getAnalysisResults() != null;
			case TrafficManagerPackage.SMART_CONTROLLER__ANALYSIS_CONTROLS:
				return getAnalysisControls() != null;
			case TrafficManagerPackage.SMART_CONTROLLER__LANE_AREA_DETECTORS:
				return !getLaneAreaDetectors().isEmpty();
			case TrafficManagerPackage.SMART_CONTROLLER__TRAFFIC_LIGHT:
				return getTrafficLight() != null;
			case TrafficManagerPackage.SMART_CONTROLLER__MONITOR_CONTROLS:
				return getMonitorControls() != null;
			case TrafficManagerPackage.SMART_CONTROLLER__MONITOR_RESULTS:
				return getMonitorResults() != null;
		}
		return super.eIsSet(featureID);
	}

} //SmartControllerImpl
