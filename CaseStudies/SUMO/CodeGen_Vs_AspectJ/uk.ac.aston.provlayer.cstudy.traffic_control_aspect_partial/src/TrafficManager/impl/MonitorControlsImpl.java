/**
 */
package TrafficManager.impl;

import TrafficManager.MonitorControls;
import TrafficManager.TrafficManagerPackage;
import uk.ac.aston.provlayer.observer.annotations.ObserveModelEMFeDynamic;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.internal.cdo.CDOObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Monitor Controls</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.impl.MonitorControlsImpl#getID <em>ID</em>}</li>
 *   <li>{@link TrafficManager.impl.MonitorControlsImpl#getTrafficLight <em>Traffic Light</em>}</li>
 *   <li>{@link TrafficManager.impl.MonitorControlsImpl#getLaneAreaDetectors <em>Lane Area Detectors</em>}</li>
 *   <li>{@link TrafficManager.impl.MonitorControlsImpl#isCheckPhaseEnd <em>Check Phase End</em>}</li>
 * </ul>
 *
 * @generated
 */
//@ObserveModelEMFeDynamic
public class MonitorControlsImpl extends CDOObjectImpl implements MonitorControls {
	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getTrafficLight() <em>Traffic Light</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrafficLight()
	 * @generated
	 * @ordered
	 */
	protected static final String TRAFFIC_LIGHT_EDEFAULT = null;

	/**
	 * The default value of the '{@link #isCheckPhaseEnd() <em>Check Phase End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCheckPhaseEnd()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CHECK_PHASE_END_EDEFAULT = false;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MonitorControlsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TrafficManagerPackage.Literals.MONITOR_CONTROLS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getID() {
		return (String)eDynamicGet(TrafficManagerPackage.MONITOR_CONTROLS__ID, TrafficManagerPackage.Literals.MONITOR_CONTROLS__ID, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(String newID) {
		eDynamicSet(TrafficManagerPackage.MONITOR_CONTROLS__ID, TrafficManagerPackage.Literals.MONITOR_CONTROLS__ID, newID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTrafficLight() {
		return (String)eDynamicGet(TrafficManagerPackage.MONITOR_CONTROLS__TRAFFIC_LIGHT, TrafficManagerPackage.Literals.MONITOR_CONTROLS__TRAFFIC_LIGHT, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTrafficLight(String newTrafficLight) {
		eDynamicSet(TrafficManagerPackage.MONITOR_CONTROLS__TRAFFIC_LIGHT, TrafficManagerPackage.Literals.MONITOR_CONTROLS__TRAFFIC_LIGHT, newTrafficLight);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<String> getLaneAreaDetectors() {
		return (EList<String>)eDynamicGet(TrafficManagerPackage.MONITOR_CONTROLS__LANE_AREA_DETECTORS, TrafficManagerPackage.Literals.MONITOR_CONTROLS__LANE_AREA_DETECTORS, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCheckPhaseEnd() {
		return (Boolean)eDynamicGet(TrafficManagerPackage.MONITOR_CONTROLS__CHECK_PHASE_END, TrafficManagerPackage.Literals.MONITOR_CONTROLS__CHECK_PHASE_END, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckPhaseEnd(boolean newCheckPhaseEnd) {
		eDynamicSet(TrafficManagerPackage.MONITOR_CONTROLS__CHECK_PHASE_END, TrafficManagerPackage.Literals.MONITOR_CONTROLS__CHECK_PHASE_END, newCheckPhaseEnd);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TrafficManagerPackage.MONITOR_CONTROLS__ID:
				return getID();
			case TrafficManagerPackage.MONITOR_CONTROLS__TRAFFIC_LIGHT:
				return getTrafficLight();
			case TrafficManagerPackage.MONITOR_CONTROLS__LANE_AREA_DETECTORS:
				return getLaneAreaDetectors();
			case TrafficManagerPackage.MONITOR_CONTROLS__CHECK_PHASE_END:
				return isCheckPhaseEnd();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TrafficManagerPackage.MONITOR_CONTROLS__ID:
				setID((String)newValue);
				return;
			case TrafficManagerPackage.MONITOR_CONTROLS__TRAFFIC_LIGHT:
				setTrafficLight((String)newValue);
				return;
			case TrafficManagerPackage.MONITOR_CONTROLS__LANE_AREA_DETECTORS:
				getLaneAreaDetectors().clear();
				getLaneAreaDetectors().addAll((Collection<? extends String>)newValue);
				return;
			case TrafficManagerPackage.MONITOR_CONTROLS__CHECK_PHASE_END:
				setCheckPhaseEnd((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.MONITOR_CONTROLS__ID:
				setID(ID_EDEFAULT);
				return;
			case TrafficManagerPackage.MONITOR_CONTROLS__TRAFFIC_LIGHT:
				setTrafficLight(TRAFFIC_LIGHT_EDEFAULT);
				return;
			case TrafficManagerPackage.MONITOR_CONTROLS__LANE_AREA_DETECTORS:
				getLaneAreaDetectors().clear();
				return;
			case TrafficManagerPackage.MONITOR_CONTROLS__CHECK_PHASE_END:
				setCheckPhaseEnd(CHECK_PHASE_END_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.MONITOR_CONTROLS__ID:
				return ID_EDEFAULT == null ? getID() != null : !ID_EDEFAULT.equals(getID());
			case TrafficManagerPackage.MONITOR_CONTROLS__TRAFFIC_LIGHT:
				return TRAFFIC_LIGHT_EDEFAULT == null ? getTrafficLight() != null : !TRAFFIC_LIGHT_EDEFAULT.equals(getTrafficLight());
			case TrafficManagerPackage.MONITOR_CONTROLS__LANE_AREA_DETECTORS:
				return !getLaneAreaDetectors().isEmpty();
			case TrafficManagerPackage.MONITOR_CONTROLS__CHECK_PHASE_END:
				return isCheckPhaseEnd() != CHECK_PHASE_END_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //MonitorControlsImpl
