package trafficController;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.net4j.CDONet4jSessionConfiguration;
import org.eclipse.emf.cdo.net4j.CDONet4jUtil;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CommitException;
import org.eclipse.emf.cdo.util.ConcurrentAccessException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.net4j.Net4jUtil;
import org.eclipse.net4j.connector.IConnector;
import org.eclipse.net4j.tcp.TCPUtil;
import org.eclipse.net4j.util.container.ContainerUtil;
import org.eclipse.net4j.util.container.IManagedContainer;

import TrafficManager.AnalysisControls;
import TrafficManager.AnalysisResults;
import TrafficManager.ExecutionResults;
import TrafficManager.LaneAreaDetector;
import TrafficManager.Manager;
import TrafficManager.MonitorControls;
import TrafficManager.MonitorResults;
import TrafficManager.PlanToExecute;
import TrafficManager.SmartController;
import TrafficManager.TrafficLight;
import de.tudresden.sumo.cmd.Trafficlight;
import it.polito.appeal.traci.SumoTraciConnection;
import uk.ac.aston.provlayer.observer.annotations.ObserveMe;
import uk.ac.aston.provlayer.observer.thread.ActivityScope;
import uk.ac.aston.provlayer.observer.thread.ThreadObserver;

//import observerThreadInstruments.PROVScope;

public class JunctionController implements Runnable {
	
	private static final int DECISION_FREQUENCY_INTERVAL = 1000;
	private static final int LANE_AREA_DETECTOR_CAPACITY = 6;

	// CDO Host DB info -- CURATOR
	public static final String CDO_Hostname = "127.0.0.1";
	public static final String CDO_Port = "2037";
	public static final String CDO_Repository = "repoSystem";
	// public static final String CDO_Resource = "/Folder1/model1";

	private IManagedContainer container;
	private IConnector connector;
	private CDONet4jSessionConfiguration configuration;
	private CDOSession session;
	private CDOTransaction transaction;
	private CDOResource resource;
	private String resourceRef;

	private int SmartControllerIndex;
	// private SmartController currentSC;
	private sumoInterface mySumoIF = new sumoInterface();
	private SumoTraciConnection conn;
	private boolean faulty = false;
	private boolean outputToConsole = false;
	private boolean done = false;
	private long startTime, endTime;
	private String lastCommitTransactionData;

	private static final String LOGPATH = "./logs/";
	private static String ExperimentLabel;
	private FileWriter fWrite = null;
	private PrintWriter pLine;

	public JunctionController(int SmartControllerIndex, SumoTraciConnection tc, String resourceRef, boolean faulty,
			boolean outputToConsole, String experimentLabel) {
		this.SmartControllerIndex = SmartControllerIndex;
		conn = tc;
		// this.session = session;
		this.resourceRef = resourceRef;
		this.faulty = faulty;
		this.outputToConsole = outputToConsole;
		this.ExperimentLabel = experimentLabel;

		if (fWrite == null) {
			try {
				fWrite = new FileWriter(LOGPATH + ExperimentLabel + "SC" + SmartControllerIndex + ".txt", false);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		pLine = new PrintWriter(fWrite);

	}

	private void writeLog(String log) {

		pLine.println(log);

	}

	public boolean getDone() {
		return done;
	}

	public void setDone(boolean stop) {
		done = stop;
	}

	public long getMAPELoopTime(long start, long end) {
		return end - start;
	}

	public void reportRuntimeStats() {

		long time = getMAPELoopTime(startTime, endTime);

		if (outputToConsole) {
			System.out.println(String.format("%S MAPELoopTime = %dms : %s", Thread.currentThread().getName(), time,
					lastCommitTransactionData));
		}

		writeLog(String.format("%S MAPELoopTime = %dms : %s", Thread.currentThread().getName(), time,
				lastCommitTransactionData));
		lastCommitTransactionData = null;
	}

	public void DatabaseConnection() {
		System.out.println(Thread.currentThread().getName() + " Connecting Database");
		// Prepare container
		container = ContainerUtil.createContainer();
		Net4jUtil.prepareContainer(container); // Register Net4j factories
		TCPUtil.prepareContainer(container); // Register TCP factories
		CDONet4jUtil.prepareContainer(container); // Register CDO factories
		container.activate();

		// Create connector
		connector = TCPUtil.getConnector(container, CDO_Hostname + ":" + CDO_Port); //$NON-NLS-1$

		// Create configuration
		configuration = CDONet4jUtil.createNet4jSessionConfiguration();
		configuration.setConnector(connector);
		configuration.setRepositoryName(CDO_Repository); // $NON-NLS-1$

		// Open session
		session = configuration.openNet4jSession();
		// Model to use with Session?
		// session.getPackageRegistry().putEPackage(causation.CausationPackage.eINSTANCE);

	}

	@ObserveMe
	public void run() {

		System.out.println("Started : " + Thread.currentThread().getName() + " Controlling " + SmartControllerIndex);

		DatabaseConnection();
		transaction = session.openTransaction();
		resource = transaction.getOrCreateResource(resourceRef);
		Manager theManager = (Manager) resource.getContents().get(0);
		SmartController currentSC = theManager.getSmartController().get(SmartControllerIndex);

		// DO NOT OPEN AND CLOSE THE TRANSACTION IN THE LOOP IT BREAKS
		// I guess this is causing too much work to clear the objects from memory and
		// reload them all from CDO.

		while (!done) {
			startTime = System.currentTimeMillis();
			transaction.setCommitComment(Thread.currentThread().getName() + " ST:" + startTime);

			// --------------------------------------------
			// MONITOR - SMART CONTROLLERS UPDATE TOKENS
			// --------------------------------------------
			try (ActivityScope scope0 = new ActivityScope("Monitor")) {
				// Control Inputs
				// System.out.println(String.format("MonitorControls: TrafficLight [%s] :
				// LaneAreaDetectors [%s]", currentSC.getMonitorControls().getTrafficLight(),
				// currentSC.getMonitorControls().getLaneAreaDetectors()));

				// Traffic lights
				try (ActivityScope scope1 = new ActivityScope("monitorTrafficLight")) {
					monitorTrafficLight(currentSC.getTrafficLight(), currentSC.getMonitorControls(),
							currentSC.getMonitorResults());
				}

				// Land Area Detectors
				try (ActivityScope scope1 = new ActivityScope("monitorLaneAreaDetector")) {
					currentSC.getMonitorResults().getLaneAreaDetectorSeen().clear();
					for (LaneAreaDetector lad : currentSC.getLaneAreaDetectors()) {
						monitorLaneAreaDetector(lad, currentSC.getMonitorControls(), currentSC.getMonitorResults());
					}
				}

				try (ActivityScope scope1 = new ActivityScope("monitorOtherControllerPhaseEnd")) {
					SmartController otherSC = null;
					if (SmartControllerIndex != 0) {
						otherSC = theManager.getSmartController().get(0);

					} else {
						otherSC = theManager.getSmartController().get(1);
					}
					monitorOtherControllerPhaseEnd(otherSC.getExecutionResults(), currentSC.getMonitorResults());
				}
				commit(transaction);
				// Result Output
				// System.out.println(String.format("MonitorResults: TrafficLightSeen [%s] :
				// LaneAreaDetectorsSeen[%s]",
				// currentSC.getMonitorResults().getLaneAreaDetectorSeen(),
				// currentSC.getMonitorResults().getLaneAreaDetectorSeen()));
			}

			try (ActivityScope scope0 = new ActivityScope("Analysis")) {
				// -----------------------------------------------------------------------
				// ANALYSIS - WHAT DO THE TRAFFIC LIGHT AND LANE AREA DETECTORS TELL ME
				// -----------------------------------------------------------------------

				// Control Inputs
				// System.out.println(String.format("AnalysisControls: JamThreshold [%s] :
				// Expected_Program_OFF [%s]",
				// currentSC.getAnalysisControls().getJamThreshold(),
				// currentSC.getAnalysisControls().isExpect_Program_OFF()));

				// Test traffic lights
				try (ActivityScope scope1 = new ActivityScope("analysisTrafficLightFailureOUTER")) {
					analysisTrafficLightFailure(currentSC.getTrafficLight(), currentSC.getMonitorResults(),
							currentSC.getAnalysisControls(), currentSC.getAnalysisResults());
				}

				// Check LADs for "Jams"
				try (ActivityScope scope1 = new ActivityScope("analysisLADJammed")) {
					analysisLADs(currentSC.getLaneAreaDetectors(), currentSC.getMonitorResults(),
							currentSC.getAnalysisControls(), currentSC.getAnalysisResults());
				}
				/*
				 * for(LaneAreaDetector lad : currentSC.getLaneAreaDetectors()) {
				 * analysisLADJammed (lad ,
				 * currentSC.getMonitorResults(),currentSC.getAnalysisControls(),currentSC.
				 * getAnalysisResults()); }
				 */

				try (ActivityScope scope1 = new ActivityScope("analysisPhaseEndEvent")) {
					// Everyone else did it I should plan to do the same!
					analysisPhaseEndEvent(currentSC.getMonitorResults(), currentSC.getAnalysisControls(),
							currentSC.getAnalysisResults());
				}
				commit(transaction);
				// Result Outputs
				// System.out.println(String.format("AnalysisResults: setLightsFailed [%s]
				// setLADSJammed [%s]", currentSC.getAnalysisResults().isLightsFailed(),
				// currentSC.getAnalysisResults().getLADsJammed()));
			}

			try (ActivityScope scope0 = new ActivityScope("Plan")) {
				// ------------------------------------------
				// PLAN - WHAT SHOULD I DO
				// ------------------------------------------

				// ReportLightsFailed
				try (ActivityScope scope1 = new ActivityScope("planReportLightsFailed")) {
					planReportLightsFailed(currentSC.getAnalysisResults(), currentSC.getPlanToExecute());
				}
				// Change the Jam threshold?
				try (ActivityScope scope1 = new ActivityScope("planJamThresholdChange")) {
					planJamThresholdChange(currentSC.getAnalysisResults(), currentSC.getPlanToExecute());
				}
				// Attempt to clear a Jam? (EndPhase)
				try (ActivityScope scope1 = new ActivityScope("planEndPhase")) {
					planEndPhase(currentSC.getAnalysisResults(), currentSC.getPlanToExecute());
				}
				commit(transaction);
				// Plan Output
				// System.out.println(String.format("PlanToExecute: setReportLightsFailed [%s] :
				// setJamThresholdChange [%s] : setEndPhase [%s]",
				// currentSC.getPlanToExecute().isReportLightsFailed(),currentSC.getPlanToExecute().getJamThresholdChange(),currentSC.getPlanToExecute().isEndPhase()));
			}

			try (ActivityScope scope0 = new ActivityScope("Execute")) {
				// ------------------------------------------
				// EXECUTE - DO SOMETHING TO SUMO
				// ------------------------------------------

				// Report lights failed
				try (ActivityScope scope1 = new ActivityScope("executeReportLightsFailed")) {
					executeReportLightsFailed(currentSC.getPlanToExecute(), currentSC.getExecutionResults());
				}
				// Change the Jam Threshold
				try (ActivityScope scope1 = new ActivityScope("executeJamThresholdChange")) {
					executeJamThresholdChange(currentSC.getPlanToExecute(), currentSC.getExecutionResults(),
							currentSC.getAnalysisControls());
				}
				// Clear a Jam by ending a phase
				try (ActivityScope scope1 = new ActivityScope("executeEndPhase")) {
					executeEndPhase(currentSC.getPlanToExecute(), currentSC.getExecutionResults(),
							currentSC.getTrafficLight(), currentSC.getAnalysisControls());
				}
				commit(transaction);
				// Execution Results
				// System.out.print(String.format("PlanExecutionResults: ReportedLightFailure
				// [%s] : ChangedJamThreshold [%s] : PhaseShift [%s]
				// ",currentSC.getExecutionResults().isReportedLightsFailed(),currentSC.getExecutionResults().getChangedJamThreshold(),currentSC.getExecutionResults().isPhaseEnded()));
				// System.out.print(String.format("Phase Time %s(%s)",
				// currentSC.getTrafficLight().getNextSwitch()-timeSlice,
				// (currentSC.getTrafficLight().getPhaseDuration()/2)));
			}

			// transaction.close(); // DONT DO THIS UNLESS YOU WANT TO RE-RUN A DBConnection
			endTime = System.currentTimeMillis();

			reportRuntimeStats();
			snooze(DECISION_FREQUENCY_INTERVAL);

		}
		// EXIT
		System.out.println("Ended : " + Thread.currentThread().getName() + " Controlling " + SmartControllerIndex);
		transaction.close();
		pLine.close();
	}

	private void commit(CDOTransaction transaction) {
		boolean DEBUG = false;
		lastCommitTransactionData = lastCommitTransactionData + " : " + String.format("Commit:%s %s",
				Thread.currentThread().getName(), transaction.getChangeSetData().toString());

		transaction.setCommitComment(Thread.currentThread().getName());
		if (DEBUG) {
			System.out.println("==Transaction info==  " + Thread.currentThread().getName());
			System.out.println("Transaction last commit time: " + transaction.getLastCommitTime());
			System.out.println("Transaction time: " + transaction.getTimeStamp());
			System.out.println(transaction.getChangeSetData());
			System.out.println(transaction.getDirtyObjects());
			// System.out.println(cdoTransaction.getRevisionDeltas());
			System.out.println("--------------------");
		}

		try (ActivityScope scope0 = new ActivityScope("Commit")) {
			
			// How do we commit?
			if (ExperimentMAIN.COMMIT_VIA_OBSERVER) {
				ThreadObserver theThreadObserver = ThreadObserver.INSTANCE.get();
				theThreadObserver.commit(transaction); // Via Observer (CDO Instruments)
			} else {
				transaction.commit();	// Directly to CDO (AspectJ Inspection)
			}
			
		} catch (ConcurrentAccessException e) {
			// TODO Auto-generated catch block
			System.out.println("!! " + Thread.currentThread().getName() + "ConcurrentAccessException");
			e.printStackTrace();
		} catch (CommitException e) {
			System.out.println("!! " + Thread.currentThread().getName() + "CommitException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void snooze(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Monitoring Functions
	public void monitorTrafficLight(TrafficLight sysTL, MonitorControls sysMC, MonitorResults sysMR) {

		if (sysMC.getTrafficLight() == sysTL.getSumoID()) {

			mySumoIF.getRuntimeFromSumoTrafficLight(sysTL, conn);
			// try (PROVScope scope = new PROVScope("monitorTrafficLight"))
			{
				sysMR.setTrafficLightSeen(sysTL.getSumoID());
			}
		}
	}

	public void monitorLaneAreaDetector(LaneAreaDetector sysLAD, MonitorControls sysMC, MonitorResults sysMR) {
//		try (PROVScope scope = new PROVScope("monitorLaneAreaDetector"+sysLAD.getSumoID())) 
		{

			if (sysMC.getLaneAreaDetectors().contains(sysLAD.getSumoID())) {
				mySumoIF.getRuntimeFromSumoLaneAreaDetector(sysLAD, conn);
				sysMR.getLaneAreaDetectorSeen().add(sysLAD.getSumoID());
			}
		}
	}

	public void monitorOtherControllerPhaseEnd(ExecutionResults otherSC, MonitorResults sysMR) {
//		try (PROVScope scope = new PROVScope("monitorOtherControllerPhaseEnd"))
		{
			// if(otherSC.getExecutionResults()!=null)
			sysMR.setPhaseEndedSeen(otherSC.isPhaseEnded());

			if (sysMR.isPhaseEndedSeen()) {
				if (outputToConsole)
					System.out.println(String.format("OO %s Saw a Phase Ended", Thread.currentThread().getName()));
			}
		}
	}

	// Analysis functions
	public void analysisTrafficLightFailure(TrafficLight sysTL, MonitorResults sysMR, AnalysisControls sysAC,
			AnalysisResults sysAR) { // try (PROVScope scope = new PROVScope("analysisTrafficLightFailureIN"))
		{
			if (sysTL.getID() == sysMR.getTrafficLightSeen()) // Only test if monitored
			{
				if (sysTL.getProgram().contentEquals("off"))
					if (sysAC.isExpect_Program_OFF())
						sysAR.setLightsFailed(false); // OFF expected
					else
						sysAR.setLightsFailed(true); // OFF NOT Expected FAULT!
			}
		}
	}

	public void analysisLADs(EList<LaneAreaDetector> sysLADs, MonitorResults sysMR, AnalysisControls sysAC,
			AnalysisResults sysAR) {// ACTIVE

		// ----------------------------
		// THE RUN AWAY COUNTER PROBLEM
		// ----------------------------

		// try (PROVScope scope = new PROVScope("analysisLADs","SYSTEM"))
		{
			// try (PROVScope scope = new PROVScope("analysisLADsRESETJamCount","SYSTEM"))
			{
				if (!faulty) {
					sysAR.setLADsJammed(0); // Comment this out to cause a runaway

					if (outputToConsole)
						System.out
								.println(String.format("-- %s Controller WORKING ", Thread.currentThread().getName()));
				} else {
					if (outputToConsole)
						System.out.println(String.format("!! %s Controller faulty", Thread.currentThread().getName()));
				}

			}

			if (outputToConsole)
				System.out.print(String.format("!! %s ", Thread.currentThread().getName()));

			for (LaneAreaDetector sysLAD : sysLADs) {
				// try (PROVScope scope1 = new PROVScope("analysisLADs"+sysLAD.getSumoID()))
				{
					if (sysMR.getLaneAreaDetectorSeen().contains(sysLAD.getSumoID())) {
						if (sysLAD.getJamLengthVehicle() > sysAC.getJamThreshold()) {
							sysAR.setLADsJammed(sysAR.getLADsJammed() + 1);
							if (outputToConsole)
								System.out.print(String.format("JAM DETECTED [%s] \n", sysAR.getLADsJammed())); // DEBUG
						}
					}
				}
			}
			if (outputToConsole)
				System.out.println(String.format("-- %s LADsJammed [%s]", Thread.currentThread().getName(),
						sysAR.getLADsJammed()));
		}
	}

	public void analysisLADJammed(LaneAreaDetector sysLAD, MonitorResults sysMR, AnalysisControls sysAC,
			AnalysisResults sysAR) {
		// try (PROVScope scope = new PROVScope("analysisLADJammed","SYSTEM"))
		{
			// System.out.println("sysMR LADs Seen "+sysMR.getLaneAreaDetectorSeen());
			// System.out.println("sysLAD ID "+sysLAD.getSumoID());
			if (sysMR.getLaneAreaDetectorSeen().contains(sysLAD.getSumoID())) {
				// System.out.println("Jam " + sysLAD.getJamLengthVehicle()+" / " +
				// sysAC.getJamThreshold());
				if (sysLAD.getJamLengthVehicle() > sysAC.getJamThreshold()) {
					sysAR.setLADsJammed(sysAR.getLADsJammed() + 1);
					// System.out.print(String.format("JAM DETECTED [%s] ",sysAR.getLADsJammed() ));
					// // DEBUG
				}
			}
		}

	}

	public void analysisPhaseEndEvent(MonitorResults sysMR, AnalysisControls sysAC, AnalysisResults sysAR) {
		if (sysMR.isPhaseEndedSeen() && sysAC.isCopyPhaseEnd()) {
			sysAR.setPlanToCopyPhaseEnd(true);
		} else {
			sysAR.setPlanToCopyPhaseEnd(false);
		}
	}

	// Plan Functions
	public void planReportLightsFailed(AnalysisResults sysAR, PlanToExecute sysPE) {
		// try (PROVScope scope = new PROVScope("planReportLightsFailed","SYSTEM"))
		{
			if (sysAR.isLightsFailed())
				if (sysPE.isReportLightsFailed() != true) {
					sysPE.setReportLightsFailed(true);
				} else if (sysPE.isReportLightsFailed() != false) {
					sysPE.setReportLightsFailed(false);
				}
		}
	}

	public void planJamThresholdChange(AnalysisResults sysAR, PlanToExecute sysPE) {
		// try (PROVScope scope = new PROVScope("planJamThresholdChange","SYSTEM"))
		{
			if (sysAR.getLADsJammed() > 2)
				sysPE.setJamThresholdChange(1);
			else if (sysAR.getLADsJammed() < 2)
				sysPE.setJamThresholdChange(-1);
			else
				sysPE.setJamThresholdChange(0);
		}
	}

	public void planEndPhase(AnalysisResults sysAR, PlanToExecute sysPE) {
		// try (PROVScope scope = new PROVScope("planEndPhase","SYSTEM"))
		{
			if (sysAR.getLADsJammed() > 2 || sysAR.isPlanToCopyPhaseEnd())
				sysPE.setEndPhase(true);
			else
				sysPE.setEndPhase(false);
		}
	}

	// Execute Functions
	public void executeReportLightsFailed(PlanToExecute sysPe, ExecutionResults sysEr) {
		// try (PROVScope scope = new PROVScope("executeReportLightsFailed","SYSTEM"))
		{
			if (sysPe.isReportLightsFailed()) {
				sysEr.setReportedLightsFailed(true);
			}
		}
	}

	public void executeJamThresholdChange(PlanToExecute sysPe, ExecutionResults sysEr, AnalysisControls sysAc) {
		// try (PROVScope scope = new PROVScope("executeJamThresholdChange","SYSTEM"))
		{
			if (sysPe.getJamThresholdChange() != 0) {
				int nJamThresholds = sysAc.getJamThreshold() + sysPe.getJamThresholdChange();
				if ((nJamThresholds > 0) & (nJamThresholds < LANE_AREA_DETECTOR_CAPACITY)) {
					sysAc.setJamThreshold(nJamThresholds);
					sysEr.setChangedJamThreshold(nJamThresholds);
				}
			}
		}
	}

	public void executeEndPhase(PlanToExecute sysPe, ExecutionResults sysEr, TrafficLight tL, AnalysisControls sysAc) {
		int timeSlice = ExperimentMAIN.getTimeSlice(); // Quick fix for the timeslice requirement here

		// try (PROVScope scope = new PROVScope("executeEndPhase","SYSTEM"))
		{
			// RESET THESE
			sysEr.setPhaseEnded(false);
			sysAc.setCopyPhaseEnd(true);

			if (sysPe.isEndPhase()) { // We only end a phase if there is less than 50% of the phase duration left
				if ((tL.getNextSwitch() - timeSlice) < (tL.getPhaseDuration() / 2)) {
					try {
						conn.do_job_set(Trafficlight.setPhaseDuration(tL.getSumoID(), 0));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					if (outputToConsole) {
					}
					System.out.println(String.format("!! %s - ENDED PHASE", Thread.currentThread().getName()));
					sysAc.setCopyPhaseEnd(false);
					sysEr.setPhaseEnded(true);
				}
			}
		}
	}

}
