package trafficController;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.net4j.CDONet4jSessionConfiguration;
import org.eclipse.emf.cdo.net4j.CDONet4jUtil;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CommitException;
import org.eclipse.emf.cdo.util.ConcurrentAccessException;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.net4j.Net4jUtil;
import org.eclipse.net4j.connector.IConnector;
import org.eclipse.net4j.tcp.TCPUtil;
import org.eclipse.net4j.util.container.ContainerUtil;
import org.eclipse.net4j.util.container.IManagedContainer;

import TrafficManager.LaneAreaDetector;
import TrafficManager.Manager;
import TrafficManager.SmartController;
import TrafficManager.TrafficLight;
import TrafficManager.TrafficManagerFactory;
import de.tudresden.sumo.cmd.Lanearea;
import de.tudresden.sumo.cmd.Trafficlight;
import de.tudresden.ws.container.SumoStringList;
import it.polito.appeal.traci.SumoTraciConnection;
import uk.ac.aston.provlayer.curator.Curator;
import uk.ac.aston.provlayer.curator.memqueue.BlockingQueueProcessor;
import uk.ac.aston.provlayer.curator.messages.ObserverMessage;
import uk.ac.aston.provlayer.observer.thread.ThreadObserver;
import uk.ac.aston.provlayer.provlayercontrol.ProvLayerControl;
import uk.ac.aston.provlayer.storage.api.ProvenanceGraphStorage;
import uk.ac.aston.provlayer.storage.cdo.CDOProvenanceGraph;
import uk.ac.aston.provlayer.storage.tinkerpop.TpProvenanceGraphStorage;

public class ExperimentMAIN {
	public static final String BINARY_MARKER = "aspect_part";
	// Controls for running Experiments
	private static int SIMULATION_STEPS_TO_RUN = 100; // Simulation steps to run
	static boolean verbose = false; // Turn on Debug messages
	
	// CRONISTA SETTINGS
	public static String HISTORY_DB = "TINKERPOP"; // Tinkerpop OR CDO
	public static final boolean COMMIT_VIA_OBSERVER = false; // Commit via the Observer? True = yes
	
	// EXPERIMENT CONTROLS
	public static String EXPERIMENTLABEL = "NO_LABEL_SET";

	public static final Boolean CONSOLE1ON = false;
	public static final Boolean CONSOLE2ON = false;
	public static Boolean FAULT1ON = false;
	public static Boolean FAULT2ON = false;

	private static final int LANE_AREA_DETECTOR_CAPACITY = 5;

	// CDO Host DB info
	public static final String CDO_Hostname = "127.0.0.1";
	public static final String CDO_Port = "2037";
	public static final String CDO_Repository_System = "repoSystem";
	// public static final String CDO_Resource = "/Folder1/model1";

	private static IManagedContainer container;
	private static IConnector connector;
	private static CDONet4jSessionConfiguration configuration;
	private static CDOSession session;
	private static CDOTransaction cdoTransaction;
	private static CDOResource cdoResource, auditRes;
	private static CDOView cdoView;
	private static String modelReference;

	// Tagging text

	// Decision Threads
	public static final String dtTLFailure = "TrafficLightFailure";
	public static final String dtTrafficJam = "TrafficJam";
	public static final String dtJamThreshold = "JamThreshold";

	// System part descriptions
	public static final String sys_TrafficLight = "TrafficLight";
	public static final String sys_LaneAreaDetector = "LaneAreaDetector";
	public static final String sys_MonitorControls = "MonitorControls";
	public static final String sys_MonitorResults = "MonitorResults";
	public static final String sys_AnalysisControls = "AnalysisControls";
	public static final String sys_AnalysisResults = "AnalysisResults";
	public static final String sys_PlanToExecute = "PlanToExecute";
	public static final String sys_ExecutionResults = "ExecutionResults";

	// SUMO

	private static final String WORLDVER = "1-2";
	static String sumo_bin = "sumo-gui"; // You need SUMO installed and paths set to run!!
	static String config_file = "../../SUMO worlds/World" + WORLDVER + "/world" + WORLDVER + ".sumocfg"; // SUMO World
																											// to load
	// static double step_length = 0.1;
	static double step_length = 1; // Step size for the simulation

	public static Copier copier;
	public static SumoTraciConnection conn; // Object for connection to SUMO

	public static String graphPath = "./Graphs/";

	public static Manager myManager;
	public static TrafficManagerFactory myTrafficManagerfactory; // Lazy way to get to Factory from anywhere
	public static int timeSlice; // Lazy way to get time from anywhere

	// public static int mapePhase; // DELETE?

	private static void parseArgs (String[] args) {
		if (args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				switch (args[i].toLowerCase()) {
				case "-binary":
					if(BINARY_MARKER.equals(args[i+1])) {
						System.out.println("BINARY: OK " + BINARY_MARKER + "!=" + args[i+1]);						
					} else {
						System.out.println("BINARY: Wrong? " + BINARY_MARKER + "!=" + args[i+1]);
					}					
					i++;
					break;
				case "-label":
					EXPERIMENTLABEL = args[i+1];
					i++;
					break;
				case "-ticks":
					try {
						SIMULATION_STEPS_TO_RUN = Integer.parseInt(args[i+1]);
						i++;
					} catch (NumberFormatException e) {
						System.err.println("Arg required integer: " + args[i]);
					}
					break;
				case "-fault1":
					FAULT1ON = true;
					break;
				case "-fault2":
					FAULT2ON = true;
					break;
				case "-historydb":
					HISTORY_DB = args[i+1].toUpperCase();
					i++;
					break;
				default:
					System.err.println("Unknown arg: " + args[i]);	
				}
			}
		}
		// DEBUG
		
		System.out.println("EXPERIMENTLABEL: " + EXPERIMENTLABEL);
		System.out.println("SIMULATION_STEPS_TO_RUN: " + SIMULATION_STEPS_TO_RUN);
		System.out.println("FAULT1ON: "+FAULT1ON);
		System.out.println("FAULT2ON: "+FAULT2ON);
		System.out.println("HISTORY_DB: "+HISTORY_DB);
		
	}

	public static int getTimeSlice() {
		return timeSlice;
	}

	private static void commit(CDOTransaction transaction) {
		boolean DEBUG = false;

		transaction.setCommitComment(Thread.currentThread().getName());
		if (DEBUG) {
			System.out.println("==Transaction info==  " + Thread.currentThread().getName());
			System.out.println("Transaction last commit time: " + transaction.getLastCommitTime());
			System.out.println("Transaction time: " + transaction.getTimeStamp());
			System.out.println(transaction.getChangeSetData());
			System.out.println(transaction.getDirtyObjects());
			// System.out.println(cdoTransaction.getRevisionDeltas());
			System.out.println("--------------------");
		}

		try {
			transaction.commit();

		} catch (ConcurrentAccessException e) {
			// TODO Auto-generated catch block
			System.out.println("!! " + Thread.currentThread().getName() + "ConcurrentAccessException");
			e.printStackTrace();
		} catch (CommitException e) {
			// TODO Auto-generated catch block
			System.out.println("!! " + Thread.currentThread().getName() + "CommitException");
			e.printStackTrace();
		}
		System.out.println("MAIN commit called:" + Thread.currentThread().getName());
	}

	public static void main(String[] args) {
		parseArgs(args);
		System.out.println(EXPERIMENTLABEL + " " + BINARY_MARKER);
		final Thread tMemoryLogger = new Thread(new MemoryTimeUsageLogger());
		tMemoryLogger.start();

		final sumoInterface mySumoIF = new sumoInterface();

		// PROV LAYER SETUP
		ProvLayerControl.startProvLayer(createProvenanceStorage());

		// File output
		ResourceSet rs = new ResourceSetImpl();
		rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("*", new XMIResourceFactoryImpl());
		Resource resourceFile = rs.createResource(URI.createFileURI(new File("result.xmi").getAbsolutePath()));

		myTrafficManagerfactory = TrafficManager.TrafficManagerFactory.eINSTANCE;

		// Set up a CDO resource for the Traffic System
		getCDOresource();

		// myManager is a base object for Traffic Manager model
		cdoResource.getContents().add(myTrafficManagerfactory.createManager());
		myManager = (Manager) cdoResource.getContents().get(0);

		commit(cdoTransaction);

		// Connect TRAAS for the rest of the program
		try {
			// Launch SUMO and setup conn as the connection object
			conn = new SumoTraciConnection(sumo_bin, config_file);
			conn.addOption("step-length", step_length + "");
			conn.addOption("start", "true"); // start SUMO immediately
			conn.addOption("quit-on-end", "true");

			// start Traci Server
			conn.runServer();
			conn.setOrder(1);

			// -----------------------------------------------
			// MODEL BUILDING based on SUMO Model discovery
			// -----------------------------------------------

			// Build a model of the Junction in SUMO in CDO

			buildSystemModel(myManager, mySumoIF);
			commit(cdoTransaction);

			JunctionController number1 = new JunctionController(0, conn, modelReference, FAULT1ON, CONSOLE1ON,
					EXPERIMENTLABEL);
			final Thread tNumber1 = new Thread(number1);
			tNumber1.start();

			Thread tNumber2 = null;
			JunctionController number2 = null;
			if (myManager.getSmartController().get(1) != null) {
				number2 = new JunctionController(1, conn, modelReference, FAULT2ON, CONSOLE2ON, EXPERIMENTLABEL);
				tNumber2 = new Thread(number2);
				tNumber2.start();
			}

			cdoTransaction.close();

			// Loop the simulation for 3600 steps
			for (int i = 0; i < SIMULATION_STEPS_TO_RUN; i++) {
				timeSlice = i; // Set the time

				// Step the Simulation forwards
				conn.do_timestep();

				// TIME Keeping VIA model -- option
				// myManager.setTime((double)conn.do_job_get(Simulation.getTime()));
				// System.out.println(String.format("Time: %s", myManager.getTime()));
				// commit(cdoTransaction);

				// -- Print the time on the console every step
				// System.out.println(String.format("Time: %s", i));
				snooze(25);
			}

			number1.setDone(true);

			if (number2 != null)
				number2.setDone(true);
			snooze(20);
			conn.close();

			tNumber1.join();
			tNumber2.join();
			// cdoSnapshotReview();

			// dump snapshot data

			// for(int i = 1; i<=simulationStepsToRun+1;i++)
			// recall(i);

			// CDO_Curator.getInstance().graphs();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		try {
			resourceFile.save(null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ProvLayerControl.shutdownProvLayer();
		
		tMemoryLogger.interrupt();
		try {
			tMemoryLogger.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static ProvenanceGraphStorage createProvenanceStorage() {
		// If we try to put these into ProvLayerControl we get circular dependencies.
		switch (HISTORY_DB) {
		case "TINKERPOP":
			return new TpProvenanceGraphStorage();
		case "CDO":
			return new CDOProvenanceGraph();
		default:
			System.out.println ("NO History Model Database set");		
			return null;
		}
	}

	public static void getCDOresource() {
		// Prepare container
		container = ContainerUtil.createContainer();
		Net4jUtil.prepareContainer(container); // Register Net4j factories
		TCPUtil.prepareContainer(container); // Register TCP factories
		CDONet4jUtil.prepareContainer(container); // Register CDO factories
		container.activate();

		// Create connector -- PORT CHANGES DEPENDING ON CDO SERVER
		// connector = TCPUtil.getConnector(container, "localhost:2037"); //$NON-NLS-1$
		connector = TCPUtil.getConnector(container, CDO_Hostname + ":" + CDO_Port); //$NON-NLS-1$

		// Create configuration
		configuration = CDONet4jUtil.createNet4jSessionConfiguration();
		configuration.setConnector(connector);
		configuration.setRepositoryName(CDO_Repository_System); // $NON-NLS-1$

		// Open session
		session = configuration.openNet4jSession();
		// Model to use with Session?
		// session.getPackageRegistry().putEPackage(TrafficManager.TrafficManagerPackage.eINSTANCE);

		// Make a new model for the run
		modelReference = trafficControllerReference();
		cdoTransaction = session.openTransaction();
		cdoResource = cdoTransaction.getOrCreateResource(modelReference);

		try {
			cdoTransaction.commit();
		} catch (CommitException e) {
			System.out.println("Problem with initial commit");
			e.printStackTrace();
		}

		System.out.print("\n-!-NEW TRAFFIC CONTROLLER-!-");
		System.out.print("\nresourceURI:" + cdoResource.getURI());
		System.out.print("\n");
	}

	private static String trafficControllerReference() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		System.out.println(dtf.format(now));
		return ("/TrafficController/Manager_Started@" + dtf.format(now));
	}

	// This is a bit too tied to this class to move quickly
	public static void buildSystemModel(Manager myManager, sumoInterface sumo) {
		SmartController newSC;
		LaneAreaDetector nLad;
		SumoStringList listTLs;
		SumoStringList listSumoLADIDs;

		// myObserver.startActivity("Build Model", "SYSTEM");

		// try (PROVScope scope = new PROVScope("Build Model"))
		{

			// Get a list of Traffic Lights from SUMO and create a Smart Controller in
			// mySystem for each
			// This model building section needs a rewrite, there is a better way

			try {
				listTLs = (SumoStringList) conn.do_job_get(Trafficlight.getIDList());

				// Creating the runtime model for the SUMO World
				for (String tL : listTLs) {

					// try (PROVScope scope1 = new PROVScope("SetupTrafficLight"))
					{
						// Make a Smart Controller for each Traffic Light
						newSC = myTrafficManagerfactory.createSmartController();
						newSC.setID(tL);

						// Add the Traffic Light to the Smart Controller
						TrafficLight tlTemp = myTrafficManagerfactory.createTrafficLight();
						// newSC.setTrafficLight(myTrafficManagerfactory.createTrafficLight());
						newSC.setTrafficLight(tlTemp);
						newSC.getTrafficLight().setID(sys_TrafficLight + tL);
						newSC.getTrafficLight().setSumoID(tL);

						// Get the values from SUMO for the traffic light
						sumo.getFromSumoTrafficLight(newSC.getTrafficLight(), conn);
					}

					// try (PROVScope scope1 = new PROVScope("SetupLADs"))
					{
						// Get a list of Lane Area Detectors (from sumo), pull each LAD in turn by ID
						// and check LaneID make a LAD model for each match
						listSumoLADIDs = (SumoStringList) conn.do_job_get(Lanearea.getIDList());
						for (String sumoLadId : listSumoLADIDs) {
							// try (PROVScope scope2 = new PROVScope("SetupLAD_"+sumoLadId))
							{
								String sumoLadLaneId = (String) conn.do_job_get(Lanearea.getLaneID(sumoLadId));
								if (newSC.getTrafficLight().getControlledLanes().contains(sumoLadLaneId)) {
									nLad = myTrafficManagerfactory.createLaneAreaDetector();
									nLad.setID(sys_LaneAreaDetector + sumoLadId);
									nLad.setSumoID(sumoLadId);
									sumo.getFromSumoLaneAreaDetector(nLad, conn);
									newSC.getLaneAreaDetectors().add(nLad);
								}
							}
						}
					}

					// try (PROVScope scope1 = new PROVScope("MAPEcontrolObjects"))
					{
						// The other stuff needed in the Smart Controller
						newSC.setMonitorControls(myTrafficManagerfactory.createMonitorControls());
						newSC.setMonitorResults(myTrafficManagerfactory.createMonitorResults());
						newSC.setAnalysisControls(myTrafficManagerfactory.createAnalysisControls());
						newSC.setAnalysisResults(myTrafficManagerfactory.createAnalysisResults());
						newSC.setPlanToExecute(myTrafficManagerfactory.createPlanToExecute());
						newSC.setExecutionResults(myTrafficManagerfactory.createExecutionResults());
					}

					// try (PROVScope scope1 = new PROVScope("ID_MAPEcontrolObjects"))
					{
						// Set the IDs on all these parts
						newSC.getMonitorControls().setID(sys_MonitorControls + newSC.getID());
						newSC.getMonitorResults().setID(sys_MonitorResults + newSC.getID());
						newSC.getAnalysisControls().setID(sys_AnalysisControls + newSC.getID());
						newSC.getAnalysisResults().setID(sys_AnalysisResults + newSC.getID());
						newSC.getPlanToExecute().setID(sys_PlanToExecute + newSC.getID());
						newSC.getExecutionResults().setID(sys_ExecutionResults + newSC.getID());
					}

					// try (PROVScope scope1 = new PROVScope("SetMonitorControls"))
					{
						// Put everything on the monitor control
						// Traffic Light
						newSC.getMonitorControls().setTrafficLight(newSC.getTrafficLight().getSumoID());
						// Lane Area Detectors
						for (LaneAreaDetector lad : newSC.getLaneAreaDetectors()) {
							newSC.getMonitorControls().getLaneAreaDetectors().add(lad.getSumoID());
						}
					}

					// try (PROVScope scope1 = new PROVScope("AddSmartControllerToModel"))
					{
						// ADD the SmartController to the Model
						myManager.getSmartController().add(newSC);
						outputSmartControllerConfig(newSC);
					}
				}
			} catch (Exception e) {
				System.out.println("Problem creating model, exited Try block");

				e.printStackTrace();
			}
		}
	}

	// OUTPUT to console

	// Output to Console Functions
	public static void outputSmartControllerConfig(SmartController currentSC) {
		System.out.print(String.format("ID: %s TL: %s LAD: ", currentSC.getID(), currentSC.getTrafficLight().getID()));
		for (LaneAreaDetector lad : currentSC.getLaneAreaDetectors()) {
			System.out.print(String.format("[%s] ", lad.getID()));
		}
		System.out.println();
	}

	public static void outputSmartControllerState(SmartController currentSC) {
		System.out.println(String.format("[ID: %s]", currentSC.getID()));
		System.out.println(String.format("[TL: %s] Prog: %s -- Lights: %s", currentSC.getTrafficLight().getID(),
				currentSC.getTrafficLight().getProgram(), currentSC.getTrafficLight().getRedYellowGreenState()));
		for (LaneAreaDetector lad : currentSC.getLaneAreaDetectors()) {
			System.out.println(String.format("[%s] cars %s ", lad.getID(), lad.getJamLengthVehicle()));
		}
	}

	public static void outputSmartControllerMAPE(SmartController currentSC) {
		System.out.println(String.format("MonitorControls: TrafficLight [%s] : LaneAreaDetectors [%s]",
				currentSC.getMonitorControls().getTrafficLight(),
				currentSC.getMonitorControls().getLaneAreaDetectors()));
		System.out.println(String.format("MonitorResults: TrafficLightSeen [%s] : LaneAreaDetectorsSeen[%s]",
				currentSC.getMonitorResults().getLaneAreaDetectorSeen(),
				currentSC.getMonitorResults().getLaneAreaDetectorSeen()));
		System.out.println(String.format("AnalysisControls: JamThreshold [%s] : Expected_Program_OFF [%s]",
				currentSC.getAnalysisControls().getJamThreshold(),
				currentSC.getAnalysisControls().isExpect_Program_OFF()));
		System.out.println(String.format("AnalysisResults: setLightsFailed [%s] setLADSJammed [%s]",
				currentSC.getAnalysisResults().isLightsFailed(), currentSC.getAnalysisResults().getLADsJammed()));
		System.out.println(String.format(
				"PlanToExecute: setReportLightsFailed [%s] : setJamThresholdChange [%s] : setEndPhase [%s]",
				currentSC.getPlanToExecute().isReportLightsFailed(),
				currentSC.getPlanToExecute().getJamThresholdChange(), currentSC.getPlanToExecute().isEndPhase()));
		System.out.println(String.format(
				"PlanExecutionResults: ReportedLightFailure [%s] : ChangedJamThreshold [%s] : PhaseShift [%s] ",
				currentSC.getExecutionResults().isReportedLightsFailed(),
				currentSC.getExecutionResults().getChangedJamThreshold(),
				currentSC.getExecutionResults().isPhaseEnded()));
		// System.out.print(String.format("Phase Time %s(%s)",
		// currentSC.getTrafficLight().getNextSwitch()-timeSlice,
		// (currentSC.getTrafficLight().getPhaseDuration()/2)));

	}

	private static void snooze(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * 
	 * public static void cdoSnapshotReview () {
	 * 
	 * 
	 * System.out.println("----CDO REPORT----");
	 * 
	 * //History int version = 1; CDOView audit =
	 * myManager.cdoView().getSession().openView(CDOUtil.getRevisionByVersion(
	 * myManager, version).getTimeStamp()); CDOObject auditManager =
	 * audit.getObject(myManager.cdoID());
	 * System.out.println(auditManager.cdoRevision()); Manager managerAtTime =
	 * (Manager) CDOUtil.getEObject(auditManager);
	 * System.out.println("Manager time "+managerAtTime.getTime()); SmartController
	 * smartControllerAtTime = (SmartController)
	 * managerAtTime.getSmartController().get(0);
	 * System.out.println("Smart Controller : TL Prog = " +
	 * smartControllerAtTime.getTrafficLight().getProgram());
	 * 
	 * 
	 * version = 2; audit =
	 * myManager.cdoView().getSession().openView(CDOUtil.getRevisionByVersion(
	 * myManager, version).getTimeStamp()); auditManager =
	 * audit.getObject(myManager.cdoID());
	 * System.out.println(auditManager.cdoRevision()); managerAtTime = (Manager)
	 * CDOUtil.getEObject(auditManager);
	 * System.out.println("Manager time "+managerAtTime.getTime());
	 * smartControllerAtTime = (SmartController)
	 * managerAtTime.getSmartController().get(0);
	 * System.out.println("Smart Controller : TL Prog = " +
	 * smartControllerAtTime.getTrafficLight().getProgram());
	 * 
	 * }
	 * 
	 * public static void recall(int version) { //myObserver.isSystemCall = false;
	 * // blind the observer
	 * 
	 * CDOView audit =
	 * myManager.cdoView().getSession().openView(CDOUtil.getRevisionByVersion(
	 * myManager, version).getTimeStamp()); CDOObject auditManager =
	 * audit.getObject(myManager.cdoID());
	 * System.out.println(auditManager.cdoRevision()); Manager managerAtTime =
	 * (Manager) CDOUtil.getEObject(auditManager);
	 * System.out.println("Manager time "+managerAtTime.getTime());
	 * if(managerAtTime.getSmartController().size()>0) { SmartController
	 * smartControllerAtTime = (SmartController)
	 * managerAtTime.getSmartController().get(0);
	 * System.out.println("Smart Controller : TL Prog = " +
	 * smartControllerAtTime.getTrafficLight().getProgram());
	 * outputSmartControllerState(smartControllerAtTime);
	 * outputSmartControllerMAPE(smartControllerAtTime); }
	 * System.out.println("- - - - - - - - - - - - - - - - - ");
	 * 
	 * //myObserver.isSystemCall = true; // blind the observer }
	 * 
	 */

}
