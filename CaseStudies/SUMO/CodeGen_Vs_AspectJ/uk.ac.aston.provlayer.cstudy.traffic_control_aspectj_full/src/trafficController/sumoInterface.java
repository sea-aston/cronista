package trafficController;

import java.util.List;

import TrafficManager.LaneAreaDetector;
import TrafficManager.TrafficLight;
import de.tudresden.sumo.cmd.Lanearea;
import de.tudresden.sumo.cmd.Trafficlight;
import de.tudresden.ws.container.SumoStringList;
import it.polito.appeal.traci.SumoTraciConnection;

public class sumoInterface {
	
		private static final boolean VERBOSE = false; 
	
		// ----------------
		// SUMO Functions
		// ----------------
		
		
		
		
		
		public void getFromSumoTrafficLight(TrafficLight updateTL, SumoTraciConnection conn)
		{
			
			// TRAFFIC LIGHT UPDATE 
	    	try 
	    	{
	    		updateTL.setRedYellowGreenState				((String)	conn.do_job_get(Trafficlight.getRedYellowGreenState	(updateTL.getSumoID())));
	    		updateTL.setPhaseDuration					((double)	conn.do_job_get(Trafficlight.getPhaseDuration		(updateTL.getSumoID())));
	        	
	    		updateTL.setPhase							((int)		conn.do_job_get(Trafficlight.getPhase				(updateTL.getSumoID())));
	    		updateTL.setProgram							((String)	conn.do_job_get(Trafficlight.getProgram				(updateTL.getSumoID())));
	        // 	updateTL.setCompleteRedYellowGreenDefinition((String)	conn.do_job_get(Trafficlight.getCompleteRedYellowGreenDefinition(updateTL.getSumoID())));  //compound object
	    		updateTL.setNextSwitch						((double)	conn.do_job_get(Trafficlight.getNextSwitch			(updateTL.getSumoID())));
	    		
	    		// Getting the Controlled Lanes (Sumo String List)
	    		List<String> lanesControlled = (SumoStringList) conn.do_job_get(Trafficlight.getControlledLanes(updateTL.getSumoID()));  // Pull list from SUMO, not in worldModel
	    		for(String laneControlled : lanesControlled)
				{
	    			updateTL.getControlledLanes().add((String) laneControlled);
				} 		
			} 
	    	catch (Exception e) 
	    	{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        // TRAFFIC LIGHT SHOW MODEL
	        if(VERBOSE)
	        {
	        	System.out.println(String.format("\nTraffic Light"));	
	        	System.out.println(String.format("ID: %s",updateTL.getID()));											
	        	System.out.println(String.format("RedYellowGreenState: %s",updateTL.getRedYellowGreenState()));
	        	System.out.println(String.format("PhaseDuration: %s",updateTL.getPhaseDuration()));
	        	System.out.println(String.format("ControlledLanes: %s",updateTL.getControlledLanes()));
	        	System.out.println(String.format("Phase: %s",updateTL.getPhase()));
	        	System.out.println(String.format("Program: %s",updateTL.getProgram()));
	        	System.out.println(String.format("CompleteRedYellowGreenDefinition: %s",updateTL.getCompleteRedYellowGreenDefinition()));
	        	System.out.println(String.format("NextSwitch: %s",updateTL.getNextSwitch()));
	        }	
		}
				
		public void getRuntimeFromSumoTrafficLight(TrafficLight updateTL, SumoTraciConnection conn)
		{
			
			// TRAFFIC LIGHT UPDATE 
	    	try 
	    	{
	    		updateTL.setRedYellowGreenState				((String)	conn.do_job_get(Trafficlight.getRedYellowGreenState	(updateTL.getSumoID())));
	    		updateTL.setPhaseDuration					((double)	conn.do_job_get(Trafficlight.getPhaseDuration		(updateTL.getSumoID())));
	        	
	    		updateTL.setPhase							((int)		conn.do_job_get(Trafficlight.getPhase				(updateTL.getSumoID())));
	    		updateTL.setProgram							((String)	conn.do_job_get(Trafficlight.getProgram				(updateTL.getSumoID())));
	        // 	updateTL.setCompleteRedYellowGreenDefinition((String)	conn.do_job_get(Trafficlight.getCompleteRedYellowGreenDefinition(updateTL.getSumoID())));  //compound object
	    		updateTL.setNextSwitch						((double)	conn.do_job_get(Trafficlight.getNextSwitch			(updateTL.getSumoID())));
	    		
	    		/*  -- Dont think lanes are going any where at runtime!
	    		// Getting the Controlled Lanes (Sumo String List)
	    		List<String> lanesControlled = (SumoStringList) conn.do_job_get(Trafficlight.getControlledLanes(updateTL.getSumoID()));  // Pull list from SUMO, not in worldModel
	    		for(String laneControlled : lanesControlled)
				{
	    			updateTL.getControlledLanes().add((String) laneControlled);
				} 
				*/		
			} 
	    	catch (Exception e) 
	    	{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        // TRAFFIC LIGHT SHOW MODEL
	        if(VERBOSE)
	        {
	        	System.out.println(String.format("\nTraffic Light"));	
	        	System.out.println(String.format("ID: %s",updateTL.getID()));											
	        	System.out.println(String.format("RedYellowGreenState: %s",updateTL.getRedYellowGreenState()));
	        	System.out.println(String.format("PhaseDuration: %s",updateTL.getPhaseDuration()));
	        	System.out.println(String.format("ControlledLanes: %s",updateTL.getControlledLanes()));
	        	System.out.println(String.format("Phase: %s",updateTL.getPhase()));
	        	System.out.println(String.format("Program: %s",updateTL.getProgram()));
	        	System.out.println(String.format("CompleteRedYellowGreenDefinition: %s",updateTL.getCompleteRedYellowGreenDefinition()));
	        	System.out.println(String.format("NextSwitch: %s",updateTL.getNextSwitch()));
	        }	
		}

		public void getFromSumoLaneAreaDetector(LaneAreaDetector updateLAD, SumoTraciConnection conn)
		{
			// LANE AREA DETECTOR UPDATE 
	    	try 
	    	{
	    		updateLAD.setPosition				((double)	conn.do_job_get(Lanearea.getPosition				(updateLAD.getSumoID())));
	    		updateLAD.setLength					((double)	conn.do_job_get(Lanearea.getLength					(updateLAD.getSumoID())));
	    		updateLAD.setLaneID					((String)	conn.do_job_get(Lanearea.getLaneID					(updateLAD.getSumoID())));
	    		updateLAD.setLastStepVehicleNumber	((int)		conn.do_job_get(Lanearea.getLastStepVehicleNumber	(updateLAD.getSumoID())));	                	
	    		updateLAD.setLastStepMeanSpeed		((double)	conn.do_job_get(Lanearea.getLastStepMeanSpeed		(updateLAD.getSumoID())));
	       // 	updateLAD.setLastStepVehicleIDs		((String)	conn.do_job_get(Lanearea.getLastStepVehicleIDs		(updateLAD.getSumoID())));  //RETURNS A LIST OF STRING
	    		updateLAD.setLastStepOccupancy		((double)	conn.do_job_get(Lanearea.getLastStepOccupancy		(updateLAD.getSumoID())));
	    		updateLAD.setJamLengthVehicle		((int)		conn.do_job_get(Lanearea.getJamLengthVehicle		(updateLAD.getSumoID())));
	    		updateLAD.setJamLenghtMeters		((double)	conn.do_job_get(Lanearea.getJamLengthMeters			(updateLAD.getSumoID())));
			} 
	    	catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	 
	        // LANE AREA DETECTOR SHOW MODEL
	        if(VERBOSE)
	        {
	        	System.out.println(String.format("\nLane Area Detector"));	                	
	        	System.out.println(String.format("ID: %s"					,updateLAD.getID()));
	        	System.out.println(String.format("Position: %s"				,updateLAD.getPosition()));
	        	System.out.println(String.format("Length: %s"				,updateLAD.getLength()));
	        	System.out.println(String.format("LaneID: %s"				,updateLAD.getLaneID()));
	        	System.out.println(String.format("LastStepVehicleNumber: %s",updateLAD.getLastStepVehicleNumber()));
	        	System.out.println(String.format("LastStepMeanSpeed: %s"	,updateLAD.getLastStepMeanSpeed()));
	        	System.out.println(String.format("LastStepVehicleIDs: %s"	,updateLAD.getLastStepVehicleIDs()));
	        	System.out.println(String.format("LastStepOccupancy: %s"	,updateLAD.getLastStepOccupancy()));
	        	System.out.println(String.format("JamLengthVehicle: %s"		,updateLAD.getJamLengthVehicle()));
	        	System.out.println(String.format("JamLengthMeters: %s"		,updateLAD.getJamLenghtMeters()));           	
	        }
		}
		
		public void getRuntimeFromSumoLaneAreaDetector(LaneAreaDetector updateLAD, SumoTraciConnection conn)
		{
			// LANE AREA DETECTOR UPDATE 
	    	try 
	    	{
//	    		updateLAD.setPosition				((double)	conn.do_job_get(Lanearea.getPosition				(updateLAD.getSumoID())));
//	    		updateLAD.setLength					((double)	conn.do_job_get(Lanearea.getLength					(updateLAD.getSumoID())));
//	    		updateLAD.setLaneID					((String)	conn.do_job_get(Lanearea.getLaneID					(updateLAD.getSumoID())));
//	    		updateLAD.setLastStepVehicleNumber	((int)		conn.do_job_get(Lanearea.getLastStepVehicleNumber	(updateLAD.getSumoID())));	                	
//	    		updateLAD.setLastStepMeanSpeed		((double)	conn.do_job_get(Lanearea.getLastStepMeanSpeed		(updateLAD.getSumoID())));
	  //		updateLAD.setLastStepVehicleIDs		((String)	conn.do_job_get(Lanearea.getLastStepVehicleIDs		(updateLAD.getSumoID())));  //RETURNS A LIST OF STRING
//	    		updateLAD.setLastStepOccupancy		((double)	conn.do_job_get(Lanearea.getLastStepOccupancy		(updateLAD.getSumoID())));
	    		updateLAD.setJamLengthVehicle		((int)		conn.do_job_get(Lanearea.getJamLengthVehicle		(updateLAD.getSumoID())));
//	    		updateLAD.setJamLenghtMeters		((double)	conn.do_job_get(Lanearea.getJamLengthMeters			(updateLAD.getSumoID())));
			} 
	    	catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	 
	        // LANE AREA DETECTOR SHOW MODEL
	        if(VERBOSE)
	        {
	        	System.out.println(String.format("\nLane Area Detector"));	                	
	        	System.out.println(String.format("ID: %s"					,updateLAD.getID()));
	        	System.out.println(String.format("Position: %s"				,updateLAD.getPosition()));
	        	System.out.println(String.format("Length: %s"				,updateLAD.getLength()));
	        	System.out.println(String.format("LaneID: %s"				,updateLAD.getLaneID()));
	        	System.out.println(String.format("LastStepVehicleNumber: %s",updateLAD.getLastStepVehicleNumber()));
	        	System.out.println(String.format("LastStepMeanSpeed: %s"	,updateLAD.getLastStepMeanSpeed()));
	        	System.out.println(String.format("LastStepVehicleIDs: %s"	,updateLAD.getLastStepVehicleIDs()));
	        	System.out.println(String.format("LastStepOccupancy: %s"	,updateLAD.getLastStepOccupancy()));
	        	System.out.println(String.format("JamLengthVehicle: %s"		,updateLAD.getJamLengthVehicle()));
	        	System.out.println(String.format("JamLengthMeters: %s"		,updateLAD.getJamLenghtMeters()));           	
	        }
		}
		

}
