/**
 */
package TrafficManager;

import org.eclipse.emf.cdo.CDOObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Plan To Execute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.PlanToExecute#getID <em>ID</em>}</li>
 *   <li>{@link TrafficManager.PlanToExecute#isEndPhase <em>End Phase</em>}</li>
 *   <li>{@link TrafficManager.PlanToExecute#isReportLightsFailed <em>Report Lights Failed</em>}</li>
 *   <li>{@link TrafficManager.PlanToExecute#getJamThresholdChange <em>Jam Threshold Change</em>}</li>
 *   <li>{@link TrafficManager.PlanToExecute#isCopyingPhaseEnd <em>Copying Phase End</em>}</li>
 * </ul>
 *
 * @see TrafficManager.TrafficManagerPackage#getPlanToExecute()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface PlanToExecute extends CDOObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see TrafficManager.TrafficManagerPackage#getPlanToExecute_ID()
	 * @model id="true"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link TrafficManager.PlanToExecute#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>End Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Phase</em>' attribute.
	 * @see #setEndPhase(boolean)
	 * @see TrafficManager.TrafficManagerPackage#getPlanToExecute_EndPhase()
	 * @model
	 * @generated
	 */
	boolean isEndPhase();

	/**
	 * Sets the value of the '{@link TrafficManager.PlanToExecute#isEndPhase <em>End Phase</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Phase</em>' attribute.
	 * @see #isEndPhase()
	 * @generated
	 */
	void setEndPhase(boolean value);

	/**
	 * Returns the value of the '<em><b>Report Lights Failed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Report Lights Failed</em>' attribute.
	 * @see #setReportLightsFailed(boolean)
	 * @see TrafficManager.TrafficManagerPackage#getPlanToExecute_ReportLightsFailed()
	 * @model required="true"
	 * @generated
	 */
	boolean isReportLightsFailed();

	/**
	 * Sets the value of the '{@link TrafficManager.PlanToExecute#isReportLightsFailed <em>Report Lights Failed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Report Lights Failed</em>' attribute.
	 * @see #isReportLightsFailed()
	 * @generated
	 */
	void setReportLightsFailed(boolean value);

	/**
	 * Returns the value of the '<em><b>Jam Threshold Change</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Jam Threshold Change</em>' attribute.
	 * @see #setJamThresholdChange(int)
	 * @see TrafficManager.TrafficManagerPackage#getPlanToExecute_JamThresholdChange()
	 * @model default="0"
	 * @generated
	 */
	int getJamThresholdChange();

	/**
	 * Sets the value of the '{@link TrafficManager.PlanToExecute#getJamThresholdChange <em>Jam Threshold Change</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Jam Threshold Change</em>' attribute.
	 * @see #getJamThresholdChange()
	 * @generated
	 */
	void setJamThresholdChange(int value);

	/**
	 * Returns the value of the '<em><b>Copying Phase End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Copying Phase End</em>' attribute.
	 * @see #setCopyingPhaseEnd(boolean)
	 * @see TrafficManager.TrafficManagerPackage#getPlanToExecute_CopyingPhaseEnd()
	 * @model
	 * @generated
	 */
	boolean isCopyingPhaseEnd();

	/**
	 * Sets the value of the '{@link TrafficManager.PlanToExecute#isCopyingPhaseEnd <em>Copying Phase End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Copying Phase End</em>' attribute.
	 * @see #isCopyingPhaseEnd()
	 * @generated
	 */
	void setCopyingPhaseEnd(boolean value);

} // PlanToExecute
