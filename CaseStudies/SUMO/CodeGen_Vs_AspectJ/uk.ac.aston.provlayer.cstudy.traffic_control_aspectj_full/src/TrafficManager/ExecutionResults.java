/**
 */
package TrafficManager;

import org.eclipse.emf.cdo.CDOObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Execution Results</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.ExecutionResults#getID <em>ID</em>}</li>
 *   <li>{@link TrafficManager.ExecutionResults#isPhaseEnded <em>Phase Ended</em>}</li>
 *   <li>{@link TrafficManager.ExecutionResults#isReportedLightsFailed <em>Reported Lights Failed</em>}</li>
 *   <li>{@link TrafficManager.ExecutionResults#getChangedJamThreshold <em>Changed Jam Threshold</em>}</li>
 * </ul>
 *
 * @see TrafficManager.TrafficManagerPackage#getExecutionResults()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface ExecutionResults extends CDOObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see TrafficManager.TrafficManagerPackage#getExecutionResults_ID()
	 * @model id="true"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link TrafficManager.ExecutionResults#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>Phase Ended</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Phase Ended</em>' attribute.
	 * @see #setPhaseEnded(boolean)
	 * @see TrafficManager.TrafficManagerPackage#getExecutionResults_PhaseEnded()
	 * @model
	 * @generated
	 */
	boolean isPhaseEnded();

	/**
	 * Sets the value of the '{@link TrafficManager.ExecutionResults#isPhaseEnded <em>Phase Ended</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Phase Ended</em>' attribute.
	 * @see #isPhaseEnded()
	 * @generated
	 */
	void setPhaseEnded(boolean value);

	/**
	 * Returns the value of the '<em><b>Reported Lights Failed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reported Lights Failed</em>' attribute.
	 * @see #setReportedLightsFailed(boolean)
	 * @see TrafficManager.TrafficManagerPackage#getExecutionResults_ReportedLightsFailed()
	 * @model required="true"
	 * @generated
	 */
	boolean isReportedLightsFailed();

	/**
	 * Sets the value of the '{@link TrafficManager.ExecutionResults#isReportedLightsFailed <em>Reported Lights Failed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reported Lights Failed</em>' attribute.
	 * @see #isReportedLightsFailed()
	 * @generated
	 */
	void setReportedLightsFailed(boolean value);

	/**
	 * Returns the value of the '<em><b>Changed Jam Threshold</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Changed Jam Threshold</em>' attribute.
	 * @see #setChangedJamThreshold(int)
	 * @see TrafficManager.TrafficManagerPackage#getExecutionResults_ChangedJamThreshold()
	 * @model
	 * @generated
	 */
	int getChangedJamThreshold();

	/**
	 * Sets the value of the '{@link TrafficManager.ExecutionResults#getChangedJamThreshold <em>Changed Jam Threshold</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Changed Jam Threshold</em>' attribute.
	 * @see #getChangedJamThreshold()
	 * @generated
	 */
	void setChangedJamThreshold(int value);

} // ExecutionResults
