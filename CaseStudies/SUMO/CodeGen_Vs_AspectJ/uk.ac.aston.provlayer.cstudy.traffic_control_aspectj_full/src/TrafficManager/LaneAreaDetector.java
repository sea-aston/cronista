/**
 */
package TrafficManager;

import org.eclipse.emf.cdo.CDOObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lane Area Detector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.LaneAreaDetector#getID <em>ID</em>}</li>
 *   <li>{@link TrafficManager.LaneAreaDetector#getSumoID <em>Sumo ID</em>}</li>
 *   <li>{@link TrafficManager.LaneAreaDetector#getPosition <em>Position</em>}</li>
 *   <li>{@link TrafficManager.LaneAreaDetector#getLength <em>Length</em>}</li>
 *   <li>{@link TrafficManager.LaneAreaDetector#getLaneID <em>Lane ID</em>}</li>
 *   <li>{@link TrafficManager.LaneAreaDetector#getLastStepVehicleNumber <em>Last Step Vehicle Number</em>}</li>
 *   <li>{@link TrafficManager.LaneAreaDetector#getLastStepMeanSpeed <em>Last Step Mean Speed</em>}</li>
 *   <li>{@link TrafficManager.LaneAreaDetector#getLastStepVehicleIDs <em>Last Step Vehicle IDs</em>}</li>
 *   <li>{@link TrafficManager.LaneAreaDetector#getLastStepOccupancy <em>Last Step Occupancy</em>}</li>
 *   <li>{@link TrafficManager.LaneAreaDetector#getJamLengthVehicle <em>Jam Length Vehicle</em>}</li>
 *   <li>{@link TrafficManager.LaneAreaDetector#getJamLenghtMeters <em>Jam Lenght Meters</em>}</li>
 * </ul>
 *
 * @see TrafficManager.TrafficManagerPackage#getLaneAreaDetector()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface LaneAreaDetector extends CDOObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see TrafficManager.TrafficManagerPackage#getLaneAreaDetector_ID()
	 * @model id="true"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link TrafficManager.LaneAreaDetector#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>Sumo ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sumo ID</em>' attribute.
	 * @see #setSumoID(String)
	 * @see TrafficManager.TrafficManagerPackage#getLaneAreaDetector_SumoID()
	 * @model
	 * @generated
	 */
	String getSumoID();

	/**
	 * Sets the value of the '{@link TrafficManager.LaneAreaDetector#getSumoID <em>Sumo ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sumo ID</em>' attribute.
	 * @see #getSumoID()
	 * @generated
	 */
	void setSumoID(String value);

	/**
	 * Returns the value of the '<em><b>Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position</em>' attribute.
	 * @see #setPosition(double)
	 * @see TrafficManager.TrafficManagerPackage#getLaneAreaDetector_Position()
	 * @model
	 * @generated
	 */
	double getPosition();

	/**
	 * Sets the value of the '{@link TrafficManager.LaneAreaDetector#getPosition <em>Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position</em>' attribute.
	 * @see #getPosition()
	 * @generated
	 */
	void setPosition(double value);

	/**
	 * Returns the value of the '<em><b>Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Length</em>' attribute.
	 * @see #setLength(double)
	 * @see TrafficManager.TrafficManagerPackage#getLaneAreaDetector_Length()
	 * @model
	 * @generated
	 */
	double getLength();

	/**
	 * Sets the value of the '{@link TrafficManager.LaneAreaDetector#getLength <em>Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Length</em>' attribute.
	 * @see #getLength()
	 * @generated
	 */
	void setLength(double value);

	/**
	 * Returns the value of the '<em><b>Lane ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lane ID</em>' attribute.
	 * @see #setLaneID(String)
	 * @see TrafficManager.TrafficManagerPackage#getLaneAreaDetector_LaneID()
	 * @model
	 * @generated
	 */
	String getLaneID();

	/**
	 * Sets the value of the '{@link TrafficManager.LaneAreaDetector#getLaneID <em>Lane ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lane ID</em>' attribute.
	 * @see #getLaneID()
	 * @generated
	 */
	void setLaneID(String value);

	/**
	 * Returns the value of the '<em><b>Last Step Vehicle Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Step Vehicle Number</em>' attribute.
	 * @see #setLastStepVehicleNumber(int)
	 * @see TrafficManager.TrafficManagerPackage#getLaneAreaDetector_LastStepVehicleNumber()
	 * @model
	 * @generated
	 */
	int getLastStepVehicleNumber();

	/**
	 * Sets the value of the '{@link TrafficManager.LaneAreaDetector#getLastStepVehicleNumber <em>Last Step Vehicle Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Step Vehicle Number</em>' attribute.
	 * @see #getLastStepVehicleNumber()
	 * @generated
	 */
	void setLastStepVehicleNumber(int value);

	/**
	 * Returns the value of the '<em><b>Last Step Mean Speed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Step Mean Speed</em>' attribute.
	 * @see #setLastStepMeanSpeed(double)
	 * @see TrafficManager.TrafficManagerPackage#getLaneAreaDetector_LastStepMeanSpeed()
	 * @model
	 * @generated
	 */
	double getLastStepMeanSpeed();

	/**
	 * Sets the value of the '{@link TrafficManager.LaneAreaDetector#getLastStepMeanSpeed <em>Last Step Mean Speed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Step Mean Speed</em>' attribute.
	 * @see #getLastStepMeanSpeed()
	 * @generated
	 */
	void setLastStepMeanSpeed(double value);

	/**
	 * Returns the value of the '<em><b>Last Step Vehicle IDs</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Step Vehicle IDs</em>' attribute.
	 * @see #setLastStepVehicleIDs(String)
	 * @see TrafficManager.TrafficManagerPackage#getLaneAreaDetector_LastStepVehicleIDs()
	 * @model
	 * @generated
	 */
	String getLastStepVehicleIDs();

	/**
	 * Sets the value of the '{@link TrafficManager.LaneAreaDetector#getLastStepVehicleIDs <em>Last Step Vehicle IDs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Step Vehicle IDs</em>' attribute.
	 * @see #getLastStepVehicleIDs()
	 * @generated
	 */
	void setLastStepVehicleIDs(String value);

	/**
	 * Returns the value of the '<em><b>Last Step Occupancy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Step Occupancy</em>' attribute.
	 * @see #setLastStepOccupancy(double)
	 * @see TrafficManager.TrafficManagerPackage#getLaneAreaDetector_LastStepOccupancy()
	 * @model
	 * @generated
	 */
	double getLastStepOccupancy();

	/**
	 * Sets the value of the '{@link TrafficManager.LaneAreaDetector#getLastStepOccupancy <em>Last Step Occupancy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Step Occupancy</em>' attribute.
	 * @see #getLastStepOccupancy()
	 * @generated
	 */
	void setLastStepOccupancy(double value);

	/**
	 * Returns the value of the '<em><b>Jam Length Vehicle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Jam Length Vehicle</em>' attribute.
	 * @see #setJamLengthVehicle(int)
	 * @see TrafficManager.TrafficManagerPackage#getLaneAreaDetector_JamLengthVehicle()
	 * @model
	 * @generated
	 */
	int getJamLengthVehicle();

	/**
	 * Sets the value of the '{@link TrafficManager.LaneAreaDetector#getJamLengthVehicle <em>Jam Length Vehicle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Jam Length Vehicle</em>' attribute.
	 * @see #getJamLengthVehicle()
	 * @generated
	 */
	void setJamLengthVehicle(int value);

	/**
	 * Returns the value of the '<em><b>Jam Lenght Meters</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Jam Lenght Meters</em>' attribute.
	 * @see #setJamLenghtMeters(double)
	 * @see TrafficManager.TrafficManagerPackage#getLaneAreaDetector_JamLenghtMeters()
	 * @model
	 * @generated
	 */
	double getJamLenghtMeters();

	/**
	 * Sets the value of the '{@link TrafficManager.LaneAreaDetector#getJamLenghtMeters <em>Jam Lenght Meters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Jam Lenght Meters</em>' attribute.
	 * @see #getJamLenghtMeters()
	 * @generated
	 */
	void setJamLenghtMeters(double value);

} // LaneAreaDetector
