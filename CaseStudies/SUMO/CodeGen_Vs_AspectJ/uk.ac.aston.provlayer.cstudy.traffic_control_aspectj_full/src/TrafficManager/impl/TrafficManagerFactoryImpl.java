/**
 */
package TrafficManager.impl;

import TrafficManager.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TrafficManagerFactoryImpl extends EFactoryImpl implements TrafficManagerFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TrafficManagerFactory init() {
		try {
			TrafficManagerFactory theTrafficManagerFactory = (TrafficManagerFactory)EPackage.Registry.INSTANCE.getEFactory(TrafficManagerPackage.eNS_URI);
			if (theTrafficManagerFactory != null) {
				return theTrafficManagerFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TrafficManagerFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TrafficManagerFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TrafficManagerPackage.MANAGER: return (EObject)createManager();
			case TrafficManagerPackage.LANE_AREA_DETECTOR: return (EObject)createLaneAreaDetector();
			case TrafficManagerPackage.TRAFFIC_LIGHT: return (EObject)createTrafficLight();
			case TrafficManagerPackage.MONITOR_CONTROLS: return (EObject)createMonitorControls();
			case TrafficManagerPackage.MONITOR_RESULTS: return (EObject)createMonitorResults();
			case TrafficManagerPackage.ANALYSIS_CONTROLS: return (EObject)createAnalysisControls();
			case TrafficManagerPackage.ANALYSIS_RESULTS: return (EObject)createAnalysisResults();
			case TrafficManagerPackage.PLAN_TO_EXECUTE: return (EObject)createPlanToExecute();
			case TrafficManagerPackage.EXECUTION_RESULTS: return (EObject)createExecutionResults();
			case TrafficManagerPackage.SMART_CONTROLLER: return (EObject)createSmartController();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Manager createManager() {
		ManagerImpl manager = new ManagerImpl();
		return manager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LaneAreaDetector createLaneAreaDetector() {
		LaneAreaDetectorImpl laneAreaDetector = new LaneAreaDetectorImpl();
		return laneAreaDetector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TrafficLight createTrafficLight() {
		TrafficLightImpl trafficLight = new TrafficLightImpl();
		return trafficLight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MonitorControls createMonitorControls() {
		MonitorControlsImpl monitorControls = new MonitorControlsImpl();
		return monitorControls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MonitorResults createMonitorResults() {
		MonitorResultsImpl monitorResults = new MonitorResultsImpl();
		return monitorResults;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnalysisControls createAnalysisControls() {
		AnalysisControlsImpl analysisControls = new AnalysisControlsImpl();
		return analysisControls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnalysisResults createAnalysisResults() {
		AnalysisResultsImpl analysisResults = new AnalysisResultsImpl();
		return analysisResults;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PlanToExecute createPlanToExecute() {
		PlanToExecuteImpl planToExecute = new PlanToExecuteImpl();
		return planToExecute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionResults createExecutionResults() {
		ExecutionResultsImpl executionResults = new ExecutionResultsImpl();
		return executionResults;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SmartController createSmartController() {
		SmartControllerImpl smartController = new SmartControllerImpl();
		return smartController;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TrafficManagerPackage getTrafficManagerPackage() {
		return (TrafficManagerPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TrafficManagerPackage getPackage() {
		return TrafficManagerPackage.eINSTANCE;
	}

} //TrafficManagerFactoryImpl
