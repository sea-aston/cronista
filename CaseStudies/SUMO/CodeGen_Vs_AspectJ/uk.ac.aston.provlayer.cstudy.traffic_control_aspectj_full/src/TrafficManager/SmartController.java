/**
 */
package TrafficManager;

import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Smart Controller</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.SmartController#getID <em>ID</em>}</li>
 *   <li>{@link TrafficManager.SmartController#getExecutionResults <em>Execution Results</em>}</li>
 *   <li>{@link TrafficManager.SmartController#getPlanToExecute <em>Plan To Execute</em>}</li>
 *   <li>{@link TrafficManager.SmartController#getAnalysisResults <em>Analysis Results</em>}</li>
 *   <li>{@link TrafficManager.SmartController#getAnalysisControls <em>Analysis Controls</em>}</li>
 *   <li>{@link TrafficManager.SmartController#getLaneAreaDetectors <em>Lane Area Detectors</em>}</li>
 *   <li>{@link TrafficManager.SmartController#getTrafficLight <em>Traffic Light</em>}</li>
 *   <li>{@link TrafficManager.SmartController#getMonitorControls <em>Monitor Controls</em>}</li>
 *   <li>{@link TrafficManager.SmartController#getMonitorResults <em>Monitor Results</em>}</li>
 * </ul>
 *
 * @see TrafficManager.TrafficManagerPackage#getSmartController()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface SmartController extends CDOObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see TrafficManager.TrafficManagerPackage#getSmartController_ID()
	 * @model id="true"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link TrafficManager.SmartController#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>Execution Results</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Results</em>' containment reference.
	 * @see #setExecutionResults(ExecutionResults)
	 * @see TrafficManager.TrafficManagerPackage#getSmartController_ExecutionResults()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ExecutionResults getExecutionResults();

	/**
	 * Sets the value of the '{@link TrafficManager.SmartController#getExecutionResults <em>Execution Results</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Results</em>' containment reference.
	 * @see #getExecutionResults()
	 * @generated
	 */
	void setExecutionResults(ExecutionResults value);

	/**
	 * Returns the value of the '<em><b>Plan To Execute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Plan To Execute</em>' containment reference.
	 * @see #setPlanToExecute(PlanToExecute)
	 * @see TrafficManager.TrafficManagerPackage#getSmartController_PlanToExecute()
	 * @model containment="true" required="true"
	 * @generated
	 */
	PlanToExecute getPlanToExecute();

	/**
	 * Sets the value of the '{@link TrafficManager.SmartController#getPlanToExecute <em>Plan To Execute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Plan To Execute</em>' containment reference.
	 * @see #getPlanToExecute()
	 * @generated
	 */
	void setPlanToExecute(PlanToExecute value);

	/**
	 * Returns the value of the '<em><b>Analysis Results</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Analysis Results</em>' containment reference.
	 * @see #setAnalysisResults(AnalysisResults)
	 * @see TrafficManager.TrafficManagerPackage#getSmartController_AnalysisResults()
	 * @model containment="true" required="true"
	 * @generated
	 */
	AnalysisResults getAnalysisResults();

	/**
	 * Sets the value of the '{@link TrafficManager.SmartController#getAnalysisResults <em>Analysis Results</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Analysis Results</em>' containment reference.
	 * @see #getAnalysisResults()
	 * @generated
	 */
	void setAnalysisResults(AnalysisResults value);

	/**
	 * Returns the value of the '<em><b>Analysis Controls</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Analysis Controls</em>' containment reference.
	 * @see #setAnalysisControls(AnalysisControls)
	 * @see TrafficManager.TrafficManagerPackage#getSmartController_AnalysisControls()
	 * @model containment="true" required="true"
	 * @generated
	 */
	AnalysisControls getAnalysisControls();

	/**
	 * Sets the value of the '{@link TrafficManager.SmartController#getAnalysisControls <em>Analysis Controls</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Analysis Controls</em>' containment reference.
	 * @see #getAnalysisControls()
	 * @generated
	 */
	void setAnalysisControls(AnalysisControls value);

	/**
	 * Returns the value of the '<em><b>Lane Area Detectors</b></em>' containment reference list.
	 * The list contents are of type {@link TrafficManager.LaneAreaDetector}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lane Area Detectors</em>' containment reference list.
	 * @see TrafficManager.TrafficManagerPackage#getSmartController_LaneAreaDetectors()
	 * @model containment="true"
	 * @generated
	 */
	EList<LaneAreaDetector> getLaneAreaDetectors();

	/**
	 * Returns the value of the '<em><b>Traffic Light</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Traffic Light</em>' containment reference.
	 * @see #setTrafficLight(TrafficLight)
	 * @see TrafficManager.TrafficManagerPackage#getSmartController_TrafficLight()
	 * @model containment="true"
	 * @generated
	 */
	TrafficLight getTrafficLight();

	/**
	 * Sets the value of the '{@link TrafficManager.SmartController#getTrafficLight <em>Traffic Light</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Traffic Light</em>' containment reference.
	 * @see #getTrafficLight()
	 * @generated
	 */
	void setTrafficLight(TrafficLight value);

	/**
	 * Returns the value of the '<em><b>Monitor Controls</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Monitor Controls</em>' containment reference.
	 * @see #setMonitorControls(MonitorControls)
	 * @see TrafficManager.TrafficManagerPackage#getSmartController_MonitorControls()
	 * @model containment="true" required="true"
	 * @generated
	 */
	MonitorControls getMonitorControls();

	/**
	 * Sets the value of the '{@link TrafficManager.SmartController#getMonitorControls <em>Monitor Controls</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Monitor Controls</em>' containment reference.
	 * @see #getMonitorControls()
	 * @generated
	 */
	void setMonitorControls(MonitorControls value);

	/**
	 * Returns the value of the '<em><b>Monitor Results</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Monitor Results</em>' containment reference.
	 * @see #setMonitorResults(MonitorResults)
	 * @see TrafficManager.TrafficManagerPackage#getSmartController_MonitorResults()
	 * @model containment="true" required="true"
	 * @generated
	 */
	MonitorResults getMonitorResults();

	/**
	 * Sets the value of the '{@link TrafficManager.SmartController#getMonitorResults <em>Monitor Results</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Monitor Results</em>' containment reference.
	 * @see #getMonitorResults()
	 * @generated
	 */
	void setMonitorResults(MonitorResults value);

} // SmartController
