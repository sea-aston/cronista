/**
 */
package TrafficManager;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see TrafficManager.TrafficManagerFactory
 * @model kind="package"
 * @generated
 */
public interface TrafficManagerPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "TrafficManager";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/TrafficManager";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "TrafficManager";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TrafficManagerPackage eINSTANCE = TrafficManager.impl.TrafficManagerPackageImpl.init();

	/**
	 * The meta object id for the '{@link TrafficManager.impl.ManagerImpl <em>Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TrafficManager.impl.ManagerImpl
	 * @see TrafficManager.impl.TrafficManagerPackageImpl#getManager()
	 * @generated
	 */
	int MANAGER = 0;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGER__TIME = 0;

	/**
	 * The feature id for the '<em><b>Smart Controller</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGER__SMART_CONTROLLER = 1;

	/**
	 * The number of structural features of the '<em>Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANAGER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link TrafficManager.impl.LaneAreaDetectorImpl <em>Lane Area Detector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TrafficManager.impl.LaneAreaDetectorImpl
	 * @see TrafficManager.impl.TrafficManagerPackageImpl#getLaneAreaDetector()
	 * @generated
	 */
	int LANE_AREA_DETECTOR = 1;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA_DETECTOR__ID = 0;

	/**
	 * The feature id for the '<em><b>Sumo ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA_DETECTOR__SUMO_ID = 1;

	/**
	 * The feature id for the '<em><b>Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA_DETECTOR__POSITION = 2;

	/**
	 * The feature id for the '<em><b>Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA_DETECTOR__LENGTH = 3;

	/**
	 * The feature id for the '<em><b>Lane ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA_DETECTOR__LANE_ID = 4;

	/**
	 * The feature id for the '<em><b>Last Step Vehicle Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_NUMBER = 5;

	/**
	 * The feature id for the '<em><b>Last Step Mean Speed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA_DETECTOR__LAST_STEP_MEAN_SPEED = 6;

	/**
	 * The feature id for the '<em><b>Last Step Vehicle IDs</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_IDS = 7;

	/**
	 * The feature id for the '<em><b>Last Step Occupancy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA_DETECTOR__LAST_STEP_OCCUPANCY = 8;

	/**
	 * The feature id for the '<em><b>Jam Length Vehicle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA_DETECTOR__JAM_LENGTH_VEHICLE = 9;

	/**
	 * The feature id for the '<em><b>Jam Lenght Meters</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA_DETECTOR__JAM_LENGHT_METERS = 10;

	/**
	 * The number of structural features of the '<em>Lane Area Detector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA_DETECTOR_FEATURE_COUNT = 11;

	/**
	 * The number of operations of the '<em>Lane Area Detector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA_DETECTOR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link TrafficManager.impl.TrafficLightImpl <em>Traffic Light</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TrafficManager.impl.TrafficLightImpl
	 * @see TrafficManager.impl.TrafficManagerPackageImpl#getTrafficLight()
	 * @generated
	 */
	int TRAFFIC_LIGHT = 2;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRAFFIC_LIGHT__ID = 0;

	/**
	 * The feature id for the '<em><b>Sumo ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRAFFIC_LIGHT__SUMO_ID = 1;

	/**
	 * The feature id for the '<em><b>Red Yellow Green State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRAFFIC_LIGHT__RED_YELLOW_GREEN_STATE = 2;

	/**
	 * The feature id for the '<em><b>Phase Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRAFFIC_LIGHT__PHASE_DURATION = 3;

	/**
	 * The feature id for the '<em><b>Controlled Lanes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRAFFIC_LIGHT__CONTROLLED_LANES = 4;

	/**
	 * The feature id for the '<em><b>Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRAFFIC_LIGHT__PHASE = 5;

	/**
	 * The feature id for the '<em><b>Program</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRAFFIC_LIGHT__PROGRAM = 6;

	/**
	 * The feature id for the '<em><b>Complete Red Yellow Green Definition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRAFFIC_LIGHT__COMPLETE_RED_YELLOW_GREEN_DEFINITION = 7;

	/**
	 * The feature id for the '<em><b>Next Switch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRAFFIC_LIGHT__NEXT_SWITCH = 8;

	/**
	 * The number of structural features of the '<em>Traffic Light</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRAFFIC_LIGHT_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Traffic Light</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRAFFIC_LIGHT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link TrafficManager.impl.MonitorControlsImpl <em>Monitor Controls</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TrafficManager.impl.MonitorControlsImpl
	 * @see TrafficManager.impl.TrafficManagerPackageImpl#getMonitorControls()
	 * @generated
	 */
	int MONITOR_CONTROLS = 3;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOR_CONTROLS__ID = 0;

	/**
	 * The feature id for the '<em><b>Traffic Light</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOR_CONTROLS__TRAFFIC_LIGHT = 1;

	/**
	 * The feature id for the '<em><b>Lane Area Detectors</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOR_CONTROLS__LANE_AREA_DETECTORS = 2;

	/**
	 * The feature id for the '<em><b>Check Phase End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOR_CONTROLS__CHECK_PHASE_END = 3;

	/**
	 * The number of structural features of the '<em>Monitor Controls</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOR_CONTROLS_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Monitor Controls</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOR_CONTROLS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link TrafficManager.impl.MonitorResultsImpl <em>Monitor Results</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TrafficManager.impl.MonitorResultsImpl
	 * @see TrafficManager.impl.TrafficManagerPackageImpl#getMonitorResults()
	 * @generated
	 */
	int MONITOR_RESULTS = 4;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOR_RESULTS__ID = 0;

	/**
	 * The feature id for the '<em><b>Traffic Light Seen</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOR_RESULTS__TRAFFIC_LIGHT_SEEN = 1;

	/**
	 * The feature id for the '<em><b>Lane Area Detector Seen</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOR_RESULTS__LANE_AREA_DETECTOR_SEEN = 2;

	/**
	 * The feature id for the '<em><b>Phase Ended Seen</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOR_RESULTS__PHASE_ENDED_SEEN = 3;

	/**
	 * The number of structural features of the '<em>Monitor Results</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOR_RESULTS_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Monitor Results</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOR_RESULTS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link TrafficManager.impl.AnalysisControlsImpl <em>Analysis Controls</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TrafficManager.impl.AnalysisControlsImpl
	 * @see TrafficManager.impl.TrafficManagerPackageImpl#getAnalysisControls()
	 * @generated
	 */
	int ANALYSIS_CONTROLS = 5;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_CONTROLS__ID = 0;

	/**
	 * The feature id for the '<em><b>Jam Threshold</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_CONTROLS__JAM_THRESHOLD = 1;

	/**
	 * The feature id for the '<em><b>Expect Program OFF</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_CONTROLS__EXPECT_PROGRAM_OFF = 2;

	/**
	 * The feature id for the '<em><b>Copy Phase End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_CONTROLS__COPY_PHASE_END = 3;

	/**
	 * The number of structural features of the '<em>Analysis Controls</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_CONTROLS_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Analysis Controls</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_CONTROLS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link TrafficManager.impl.AnalysisResultsImpl <em>Analysis Results</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TrafficManager.impl.AnalysisResultsImpl
	 * @see TrafficManager.impl.TrafficManagerPackageImpl#getAnalysisResults()
	 * @generated
	 */
	int ANALYSIS_RESULTS = 6;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_RESULTS__ID = 0;

	/**
	 * The feature id for the '<em><b>Lights Failed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_RESULTS__LIGHTS_FAILED = 1;

	/**
	 * The feature id for the '<em><b>LA Ds Jammed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_RESULTS__LA_DS_JAMMED = 2;

	/**
	 * The feature id for the '<em><b>Plan To Copy Phase End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_RESULTS__PLAN_TO_COPY_PHASE_END = 3;

	/**
	 * The number of structural features of the '<em>Analysis Results</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_RESULTS_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Analysis Results</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_RESULTS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link TrafficManager.impl.PlanToExecuteImpl <em>Plan To Execute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TrafficManager.impl.PlanToExecuteImpl
	 * @see TrafficManager.impl.TrafficManagerPackageImpl#getPlanToExecute()
	 * @generated
	 */
	int PLAN_TO_EXECUTE = 7;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAN_TO_EXECUTE__ID = 0;

	/**
	 * The feature id for the '<em><b>End Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAN_TO_EXECUTE__END_PHASE = 1;

	/**
	 * The feature id for the '<em><b>Report Lights Failed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAN_TO_EXECUTE__REPORT_LIGHTS_FAILED = 2;

	/**
	 * The feature id for the '<em><b>Jam Threshold Change</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAN_TO_EXECUTE__JAM_THRESHOLD_CHANGE = 3;

	/**
	 * The feature id for the '<em><b>Copying Phase End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAN_TO_EXECUTE__COPYING_PHASE_END = 4;

	/**
	 * The number of structural features of the '<em>Plan To Execute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAN_TO_EXECUTE_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Plan To Execute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAN_TO_EXECUTE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link TrafficManager.impl.ExecutionResultsImpl <em>Execution Results</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TrafficManager.impl.ExecutionResultsImpl
	 * @see TrafficManager.impl.TrafficManagerPackageImpl#getExecutionResults()
	 * @generated
	 */
	int EXECUTION_RESULTS = 8;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_RESULTS__ID = 0;

	/**
	 * The feature id for the '<em><b>Phase Ended</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_RESULTS__PHASE_ENDED = 1;

	/**
	 * The feature id for the '<em><b>Reported Lights Failed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_RESULTS__REPORTED_LIGHTS_FAILED = 2;

	/**
	 * The feature id for the '<em><b>Changed Jam Threshold</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_RESULTS__CHANGED_JAM_THRESHOLD = 3;

	/**
	 * The number of structural features of the '<em>Execution Results</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_RESULTS_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Execution Results</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_RESULTS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link TrafficManager.impl.SmartControllerImpl <em>Smart Controller</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TrafficManager.impl.SmartControllerImpl
	 * @see TrafficManager.impl.TrafficManagerPackageImpl#getSmartController()
	 * @generated
	 */
	int SMART_CONTROLLER = 9;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SMART_CONTROLLER__ID = 0;

	/**
	 * The feature id for the '<em><b>Execution Results</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SMART_CONTROLLER__EXECUTION_RESULTS = 1;

	/**
	 * The feature id for the '<em><b>Plan To Execute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SMART_CONTROLLER__PLAN_TO_EXECUTE = 2;

	/**
	 * The feature id for the '<em><b>Analysis Results</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SMART_CONTROLLER__ANALYSIS_RESULTS = 3;

	/**
	 * The feature id for the '<em><b>Analysis Controls</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SMART_CONTROLLER__ANALYSIS_CONTROLS = 4;

	/**
	 * The feature id for the '<em><b>Lane Area Detectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SMART_CONTROLLER__LANE_AREA_DETECTORS = 5;

	/**
	 * The feature id for the '<em><b>Traffic Light</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SMART_CONTROLLER__TRAFFIC_LIGHT = 6;

	/**
	 * The feature id for the '<em><b>Monitor Controls</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SMART_CONTROLLER__MONITOR_CONTROLS = 7;

	/**
	 * The feature id for the '<em><b>Monitor Results</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SMART_CONTROLLER__MONITOR_RESULTS = 8;

	/**
	 * The number of structural features of the '<em>Smart Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SMART_CONTROLLER_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Smart Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SMART_CONTROLLER_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link TrafficManager.Manager <em>Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Manager</em>'.
	 * @see TrafficManager.Manager
	 * @generated
	 */
	EClass getManager();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.Manager#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see TrafficManager.Manager#getTime()
	 * @see #getManager()
	 * @generated
	 */
	EAttribute getManager_Time();

	/**
	 * Returns the meta object for the containment reference list '{@link TrafficManager.Manager#getSmartController <em>Smart Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Smart Controller</em>'.
	 * @see TrafficManager.Manager#getSmartController()
	 * @see #getManager()
	 * @generated
	 */
	EReference getManager_SmartController();

	/**
	 * Returns the meta object for class '{@link TrafficManager.LaneAreaDetector <em>Lane Area Detector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lane Area Detector</em>'.
	 * @see TrafficManager.LaneAreaDetector
	 * @generated
	 */
	EClass getLaneAreaDetector();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.LaneAreaDetector#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see TrafficManager.LaneAreaDetector#getID()
	 * @see #getLaneAreaDetector()
	 * @generated
	 */
	EAttribute getLaneAreaDetector_ID();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.LaneAreaDetector#getSumoID <em>Sumo ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sumo ID</em>'.
	 * @see TrafficManager.LaneAreaDetector#getSumoID()
	 * @see #getLaneAreaDetector()
	 * @generated
	 */
	EAttribute getLaneAreaDetector_SumoID();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.LaneAreaDetector#getPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Position</em>'.
	 * @see TrafficManager.LaneAreaDetector#getPosition()
	 * @see #getLaneAreaDetector()
	 * @generated
	 */
	EAttribute getLaneAreaDetector_Position();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.LaneAreaDetector#getLength <em>Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Length</em>'.
	 * @see TrafficManager.LaneAreaDetector#getLength()
	 * @see #getLaneAreaDetector()
	 * @generated
	 */
	EAttribute getLaneAreaDetector_Length();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.LaneAreaDetector#getLaneID <em>Lane ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lane ID</em>'.
	 * @see TrafficManager.LaneAreaDetector#getLaneID()
	 * @see #getLaneAreaDetector()
	 * @generated
	 */
	EAttribute getLaneAreaDetector_LaneID();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.LaneAreaDetector#getLastStepVehicleNumber <em>Last Step Vehicle Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Step Vehicle Number</em>'.
	 * @see TrafficManager.LaneAreaDetector#getLastStepVehicleNumber()
	 * @see #getLaneAreaDetector()
	 * @generated
	 */
	EAttribute getLaneAreaDetector_LastStepVehicleNumber();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.LaneAreaDetector#getLastStepMeanSpeed <em>Last Step Mean Speed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Step Mean Speed</em>'.
	 * @see TrafficManager.LaneAreaDetector#getLastStepMeanSpeed()
	 * @see #getLaneAreaDetector()
	 * @generated
	 */
	EAttribute getLaneAreaDetector_LastStepMeanSpeed();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.LaneAreaDetector#getLastStepVehicleIDs <em>Last Step Vehicle IDs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Step Vehicle IDs</em>'.
	 * @see TrafficManager.LaneAreaDetector#getLastStepVehicleIDs()
	 * @see #getLaneAreaDetector()
	 * @generated
	 */
	EAttribute getLaneAreaDetector_LastStepVehicleIDs();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.LaneAreaDetector#getLastStepOccupancy <em>Last Step Occupancy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Step Occupancy</em>'.
	 * @see TrafficManager.LaneAreaDetector#getLastStepOccupancy()
	 * @see #getLaneAreaDetector()
	 * @generated
	 */
	EAttribute getLaneAreaDetector_LastStepOccupancy();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.LaneAreaDetector#getJamLengthVehicle <em>Jam Length Vehicle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Jam Length Vehicle</em>'.
	 * @see TrafficManager.LaneAreaDetector#getJamLengthVehicle()
	 * @see #getLaneAreaDetector()
	 * @generated
	 */
	EAttribute getLaneAreaDetector_JamLengthVehicle();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.LaneAreaDetector#getJamLenghtMeters <em>Jam Lenght Meters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Jam Lenght Meters</em>'.
	 * @see TrafficManager.LaneAreaDetector#getJamLenghtMeters()
	 * @see #getLaneAreaDetector()
	 * @generated
	 */
	EAttribute getLaneAreaDetector_JamLenghtMeters();

	/**
	 * Returns the meta object for class '{@link TrafficManager.TrafficLight <em>Traffic Light</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Traffic Light</em>'.
	 * @see TrafficManager.TrafficLight
	 * @generated
	 */
	EClass getTrafficLight();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.TrafficLight#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see TrafficManager.TrafficLight#getID()
	 * @see #getTrafficLight()
	 * @generated
	 */
	EAttribute getTrafficLight_ID();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.TrafficLight#getSumoID <em>Sumo ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sumo ID</em>'.
	 * @see TrafficManager.TrafficLight#getSumoID()
	 * @see #getTrafficLight()
	 * @generated
	 */
	EAttribute getTrafficLight_SumoID();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.TrafficLight#getRedYellowGreenState <em>Red Yellow Green State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Red Yellow Green State</em>'.
	 * @see TrafficManager.TrafficLight#getRedYellowGreenState()
	 * @see #getTrafficLight()
	 * @generated
	 */
	EAttribute getTrafficLight_RedYellowGreenState();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.TrafficLight#getPhaseDuration <em>Phase Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Phase Duration</em>'.
	 * @see TrafficManager.TrafficLight#getPhaseDuration()
	 * @see #getTrafficLight()
	 * @generated
	 */
	EAttribute getTrafficLight_PhaseDuration();

	/**
	 * Returns the meta object for the attribute list '{@link TrafficManager.TrafficLight#getControlledLanes <em>Controlled Lanes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Controlled Lanes</em>'.
	 * @see TrafficManager.TrafficLight#getControlledLanes()
	 * @see #getTrafficLight()
	 * @generated
	 */
	EAttribute getTrafficLight_ControlledLanes();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.TrafficLight#getPhase <em>Phase</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Phase</em>'.
	 * @see TrafficManager.TrafficLight#getPhase()
	 * @see #getTrafficLight()
	 * @generated
	 */
	EAttribute getTrafficLight_Phase();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.TrafficLight#getProgram <em>Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Program</em>'.
	 * @see TrafficManager.TrafficLight#getProgram()
	 * @see #getTrafficLight()
	 * @generated
	 */
	EAttribute getTrafficLight_Program();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.TrafficLight#getCompleteRedYellowGreenDefinition <em>Complete Red Yellow Green Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Complete Red Yellow Green Definition</em>'.
	 * @see TrafficManager.TrafficLight#getCompleteRedYellowGreenDefinition()
	 * @see #getTrafficLight()
	 * @generated
	 */
	EAttribute getTrafficLight_CompleteRedYellowGreenDefinition();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.TrafficLight#getNextSwitch <em>Next Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Next Switch</em>'.
	 * @see TrafficManager.TrafficLight#getNextSwitch()
	 * @see #getTrafficLight()
	 * @generated
	 */
	EAttribute getTrafficLight_NextSwitch();

	/**
	 * Returns the meta object for class '{@link TrafficManager.MonitorControls <em>Monitor Controls</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Monitor Controls</em>'.
	 * @see TrafficManager.MonitorControls
	 * @generated
	 */
	EClass getMonitorControls();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.MonitorControls#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see TrafficManager.MonitorControls#getID()
	 * @see #getMonitorControls()
	 * @generated
	 */
	EAttribute getMonitorControls_ID();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.MonitorControls#getTrafficLight <em>Traffic Light</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Traffic Light</em>'.
	 * @see TrafficManager.MonitorControls#getTrafficLight()
	 * @see #getMonitorControls()
	 * @generated
	 */
	EAttribute getMonitorControls_TrafficLight();

	/**
	 * Returns the meta object for the attribute list '{@link TrafficManager.MonitorControls#getLaneAreaDetectors <em>Lane Area Detectors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Lane Area Detectors</em>'.
	 * @see TrafficManager.MonitorControls#getLaneAreaDetectors()
	 * @see #getMonitorControls()
	 * @generated
	 */
	EAttribute getMonitorControls_LaneAreaDetectors();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.MonitorControls#isCheckPhaseEnd <em>Check Phase End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Check Phase End</em>'.
	 * @see TrafficManager.MonitorControls#isCheckPhaseEnd()
	 * @see #getMonitorControls()
	 * @generated
	 */
	EAttribute getMonitorControls_CheckPhaseEnd();

	/**
	 * Returns the meta object for class '{@link TrafficManager.MonitorResults <em>Monitor Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Monitor Results</em>'.
	 * @see TrafficManager.MonitorResults
	 * @generated
	 */
	EClass getMonitorResults();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.MonitorResults#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see TrafficManager.MonitorResults#getID()
	 * @see #getMonitorResults()
	 * @generated
	 */
	EAttribute getMonitorResults_ID();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.MonitorResults#getTrafficLightSeen <em>Traffic Light Seen</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Traffic Light Seen</em>'.
	 * @see TrafficManager.MonitorResults#getTrafficLightSeen()
	 * @see #getMonitorResults()
	 * @generated
	 */
	EAttribute getMonitorResults_TrafficLightSeen();

	/**
	 * Returns the meta object for the attribute list '{@link TrafficManager.MonitorResults#getLaneAreaDetectorSeen <em>Lane Area Detector Seen</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Lane Area Detector Seen</em>'.
	 * @see TrafficManager.MonitorResults#getLaneAreaDetectorSeen()
	 * @see #getMonitorResults()
	 * @generated
	 */
	EAttribute getMonitorResults_LaneAreaDetectorSeen();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.MonitorResults#isPhaseEndedSeen <em>Phase Ended Seen</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Phase Ended Seen</em>'.
	 * @see TrafficManager.MonitorResults#isPhaseEndedSeen()
	 * @see #getMonitorResults()
	 * @generated
	 */
	EAttribute getMonitorResults_PhaseEndedSeen();

	/**
	 * Returns the meta object for class '{@link TrafficManager.AnalysisControls <em>Analysis Controls</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Analysis Controls</em>'.
	 * @see TrafficManager.AnalysisControls
	 * @generated
	 */
	EClass getAnalysisControls();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.AnalysisControls#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see TrafficManager.AnalysisControls#getID()
	 * @see #getAnalysisControls()
	 * @generated
	 */
	EAttribute getAnalysisControls_ID();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.AnalysisControls#getJamThreshold <em>Jam Threshold</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Jam Threshold</em>'.
	 * @see TrafficManager.AnalysisControls#getJamThreshold()
	 * @see #getAnalysisControls()
	 * @generated
	 */
	EAttribute getAnalysisControls_JamThreshold();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.AnalysisControls#isExpect_Program_OFF <em>Expect Program OFF</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expect Program OFF</em>'.
	 * @see TrafficManager.AnalysisControls#isExpect_Program_OFF()
	 * @see #getAnalysisControls()
	 * @generated
	 */
	EAttribute getAnalysisControls_Expect_Program_OFF();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.AnalysisControls#isCopyPhaseEnd <em>Copy Phase End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Copy Phase End</em>'.
	 * @see TrafficManager.AnalysisControls#isCopyPhaseEnd()
	 * @see #getAnalysisControls()
	 * @generated
	 */
	EAttribute getAnalysisControls_CopyPhaseEnd();

	/**
	 * Returns the meta object for class '{@link TrafficManager.AnalysisResults <em>Analysis Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Analysis Results</em>'.
	 * @see TrafficManager.AnalysisResults
	 * @generated
	 */
	EClass getAnalysisResults();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.AnalysisResults#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see TrafficManager.AnalysisResults#getID()
	 * @see #getAnalysisResults()
	 * @generated
	 */
	EAttribute getAnalysisResults_ID();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.AnalysisResults#isLightsFailed <em>Lights Failed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lights Failed</em>'.
	 * @see TrafficManager.AnalysisResults#isLightsFailed()
	 * @see #getAnalysisResults()
	 * @generated
	 */
	EAttribute getAnalysisResults_LightsFailed();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.AnalysisResults#getLADsJammed <em>LA Ds Jammed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>LA Ds Jammed</em>'.
	 * @see TrafficManager.AnalysisResults#getLADsJammed()
	 * @see #getAnalysisResults()
	 * @generated
	 */
	EAttribute getAnalysisResults_LADsJammed();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.AnalysisResults#isPlanToCopyPhaseEnd <em>Plan To Copy Phase End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Plan To Copy Phase End</em>'.
	 * @see TrafficManager.AnalysisResults#isPlanToCopyPhaseEnd()
	 * @see #getAnalysisResults()
	 * @generated
	 */
	EAttribute getAnalysisResults_PlanToCopyPhaseEnd();

	/**
	 * Returns the meta object for class '{@link TrafficManager.PlanToExecute <em>Plan To Execute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Plan To Execute</em>'.
	 * @see TrafficManager.PlanToExecute
	 * @generated
	 */
	EClass getPlanToExecute();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.PlanToExecute#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see TrafficManager.PlanToExecute#getID()
	 * @see #getPlanToExecute()
	 * @generated
	 */
	EAttribute getPlanToExecute_ID();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.PlanToExecute#isEndPhase <em>End Phase</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Phase</em>'.
	 * @see TrafficManager.PlanToExecute#isEndPhase()
	 * @see #getPlanToExecute()
	 * @generated
	 */
	EAttribute getPlanToExecute_EndPhase();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.PlanToExecute#isReportLightsFailed <em>Report Lights Failed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Report Lights Failed</em>'.
	 * @see TrafficManager.PlanToExecute#isReportLightsFailed()
	 * @see #getPlanToExecute()
	 * @generated
	 */
	EAttribute getPlanToExecute_ReportLightsFailed();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.PlanToExecute#getJamThresholdChange <em>Jam Threshold Change</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Jam Threshold Change</em>'.
	 * @see TrafficManager.PlanToExecute#getJamThresholdChange()
	 * @see #getPlanToExecute()
	 * @generated
	 */
	EAttribute getPlanToExecute_JamThresholdChange();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.PlanToExecute#isCopyingPhaseEnd <em>Copying Phase End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Copying Phase End</em>'.
	 * @see TrafficManager.PlanToExecute#isCopyingPhaseEnd()
	 * @see #getPlanToExecute()
	 * @generated
	 */
	EAttribute getPlanToExecute_CopyingPhaseEnd();

	/**
	 * Returns the meta object for class '{@link TrafficManager.ExecutionResults <em>Execution Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Execution Results</em>'.
	 * @see TrafficManager.ExecutionResults
	 * @generated
	 */
	EClass getExecutionResults();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.ExecutionResults#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see TrafficManager.ExecutionResults#getID()
	 * @see #getExecutionResults()
	 * @generated
	 */
	EAttribute getExecutionResults_ID();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.ExecutionResults#isPhaseEnded <em>Phase Ended</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Phase Ended</em>'.
	 * @see TrafficManager.ExecutionResults#isPhaseEnded()
	 * @see #getExecutionResults()
	 * @generated
	 */
	EAttribute getExecutionResults_PhaseEnded();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.ExecutionResults#isReportedLightsFailed <em>Reported Lights Failed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reported Lights Failed</em>'.
	 * @see TrafficManager.ExecutionResults#isReportedLightsFailed()
	 * @see #getExecutionResults()
	 * @generated
	 */
	EAttribute getExecutionResults_ReportedLightsFailed();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.ExecutionResults#getChangedJamThreshold <em>Changed Jam Threshold</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Changed Jam Threshold</em>'.
	 * @see TrafficManager.ExecutionResults#getChangedJamThreshold()
	 * @see #getExecutionResults()
	 * @generated
	 */
	EAttribute getExecutionResults_ChangedJamThreshold();

	/**
	 * Returns the meta object for class '{@link TrafficManager.SmartController <em>Smart Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Smart Controller</em>'.
	 * @see TrafficManager.SmartController
	 * @generated
	 */
	EClass getSmartController();

	/**
	 * Returns the meta object for the attribute '{@link TrafficManager.SmartController#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see TrafficManager.SmartController#getID()
	 * @see #getSmartController()
	 * @generated
	 */
	EAttribute getSmartController_ID();

	/**
	 * Returns the meta object for the containment reference '{@link TrafficManager.SmartController#getExecutionResults <em>Execution Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Execution Results</em>'.
	 * @see TrafficManager.SmartController#getExecutionResults()
	 * @see #getSmartController()
	 * @generated
	 */
	EReference getSmartController_ExecutionResults();

	/**
	 * Returns the meta object for the containment reference '{@link TrafficManager.SmartController#getPlanToExecute <em>Plan To Execute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Plan To Execute</em>'.
	 * @see TrafficManager.SmartController#getPlanToExecute()
	 * @see #getSmartController()
	 * @generated
	 */
	EReference getSmartController_PlanToExecute();

	/**
	 * Returns the meta object for the containment reference '{@link TrafficManager.SmartController#getAnalysisResults <em>Analysis Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Analysis Results</em>'.
	 * @see TrafficManager.SmartController#getAnalysisResults()
	 * @see #getSmartController()
	 * @generated
	 */
	EReference getSmartController_AnalysisResults();

	/**
	 * Returns the meta object for the containment reference '{@link TrafficManager.SmartController#getAnalysisControls <em>Analysis Controls</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Analysis Controls</em>'.
	 * @see TrafficManager.SmartController#getAnalysisControls()
	 * @see #getSmartController()
	 * @generated
	 */
	EReference getSmartController_AnalysisControls();

	/**
	 * Returns the meta object for the containment reference list '{@link TrafficManager.SmartController#getLaneAreaDetectors <em>Lane Area Detectors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Lane Area Detectors</em>'.
	 * @see TrafficManager.SmartController#getLaneAreaDetectors()
	 * @see #getSmartController()
	 * @generated
	 */
	EReference getSmartController_LaneAreaDetectors();

	/**
	 * Returns the meta object for the containment reference '{@link TrafficManager.SmartController#getTrafficLight <em>Traffic Light</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Traffic Light</em>'.
	 * @see TrafficManager.SmartController#getTrafficLight()
	 * @see #getSmartController()
	 * @generated
	 */
	EReference getSmartController_TrafficLight();

	/**
	 * Returns the meta object for the containment reference '{@link TrafficManager.SmartController#getMonitorControls <em>Monitor Controls</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Monitor Controls</em>'.
	 * @see TrafficManager.SmartController#getMonitorControls()
	 * @see #getSmartController()
	 * @generated
	 */
	EReference getSmartController_MonitorControls();

	/**
	 * Returns the meta object for the containment reference '{@link TrafficManager.SmartController#getMonitorResults <em>Monitor Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Monitor Results</em>'.
	 * @see TrafficManager.SmartController#getMonitorResults()
	 * @see #getSmartController()
	 * @generated
	 */
	EReference getSmartController_MonitorResults();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TrafficManagerFactory getTrafficManagerFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link TrafficManager.impl.ManagerImpl <em>Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TrafficManager.impl.ManagerImpl
		 * @see TrafficManager.impl.TrafficManagerPackageImpl#getManager()
		 * @generated
		 */
		EClass MANAGER = eINSTANCE.getManager();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MANAGER__TIME = eINSTANCE.getManager_Time();

		/**
		 * The meta object literal for the '<em><b>Smart Controller</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MANAGER__SMART_CONTROLLER = eINSTANCE.getManager_SmartController();

		/**
		 * The meta object literal for the '{@link TrafficManager.impl.LaneAreaDetectorImpl <em>Lane Area Detector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TrafficManager.impl.LaneAreaDetectorImpl
		 * @see TrafficManager.impl.TrafficManagerPackageImpl#getLaneAreaDetector()
		 * @generated
		 */
		EClass LANE_AREA_DETECTOR = eINSTANCE.getLaneAreaDetector();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANE_AREA_DETECTOR__ID = eINSTANCE.getLaneAreaDetector_ID();

		/**
		 * The meta object literal for the '<em><b>Sumo ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANE_AREA_DETECTOR__SUMO_ID = eINSTANCE.getLaneAreaDetector_SumoID();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANE_AREA_DETECTOR__POSITION = eINSTANCE.getLaneAreaDetector_Position();

		/**
		 * The meta object literal for the '<em><b>Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANE_AREA_DETECTOR__LENGTH = eINSTANCE.getLaneAreaDetector_Length();

		/**
		 * The meta object literal for the '<em><b>Lane ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANE_AREA_DETECTOR__LANE_ID = eINSTANCE.getLaneAreaDetector_LaneID();

		/**
		 * The meta object literal for the '<em><b>Last Step Vehicle Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_NUMBER = eINSTANCE.getLaneAreaDetector_LastStepVehicleNumber();

		/**
		 * The meta object literal for the '<em><b>Last Step Mean Speed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANE_AREA_DETECTOR__LAST_STEP_MEAN_SPEED = eINSTANCE.getLaneAreaDetector_LastStepMeanSpeed();

		/**
		 * The meta object literal for the '<em><b>Last Step Vehicle IDs</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANE_AREA_DETECTOR__LAST_STEP_VEHICLE_IDS = eINSTANCE.getLaneAreaDetector_LastStepVehicleIDs();

		/**
		 * The meta object literal for the '<em><b>Last Step Occupancy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANE_AREA_DETECTOR__LAST_STEP_OCCUPANCY = eINSTANCE.getLaneAreaDetector_LastStepOccupancy();

		/**
		 * The meta object literal for the '<em><b>Jam Length Vehicle</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANE_AREA_DETECTOR__JAM_LENGTH_VEHICLE = eINSTANCE.getLaneAreaDetector_JamLengthVehicle();

		/**
		 * The meta object literal for the '<em><b>Jam Lenght Meters</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANE_AREA_DETECTOR__JAM_LENGHT_METERS = eINSTANCE.getLaneAreaDetector_JamLenghtMeters();

		/**
		 * The meta object literal for the '{@link TrafficManager.impl.TrafficLightImpl <em>Traffic Light</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TrafficManager.impl.TrafficLightImpl
		 * @see TrafficManager.impl.TrafficManagerPackageImpl#getTrafficLight()
		 * @generated
		 */
		EClass TRAFFIC_LIGHT = eINSTANCE.getTrafficLight();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRAFFIC_LIGHT__ID = eINSTANCE.getTrafficLight_ID();

		/**
		 * The meta object literal for the '<em><b>Sumo ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRAFFIC_LIGHT__SUMO_ID = eINSTANCE.getTrafficLight_SumoID();

		/**
		 * The meta object literal for the '<em><b>Red Yellow Green State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRAFFIC_LIGHT__RED_YELLOW_GREEN_STATE = eINSTANCE.getTrafficLight_RedYellowGreenState();

		/**
		 * The meta object literal for the '<em><b>Phase Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRAFFIC_LIGHT__PHASE_DURATION = eINSTANCE.getTrafficLight_PhaseDuration();

		/**
		 * The meta object literal for the '<em><b>Controlled Lanes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRAFFIC_LIGHT__CONTROLLED_LANES = eINSTANCE.getTrafficLight_ControlledLanes();

		/**
		 * The meta object literal for the '<em><b>Phase</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRAFFIC_LIGHT__PHASE = eINSTANCE.getTrafficLight_Phase();

		/**
		 * The meta object literal for the '<em><b>Program</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRAFFIC_LIGHT__PROGRAM = eINSTANCE.getTrafficLight_Program();

		/**
		 * The meta object literal for the '<em><b>Complete Red Yellow Green Definition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRAFFIC_LIGHT__COMPLETE_RED_YELLOW_GREEN_DEFINITION = eINSTANCE.getTrafficLight_CompleteRedYellowGreenDefinition();

		/**
		 * The meta object literal for the '<em><b>Next Switch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRAFFIC_LIGHT__NEXT_SWITCH = eINSTANCE.getTrafficLight_NextSwitch();

		/**
		 * The meta object literal for the '{@link TrafficManager.impl.MonitorControlsImpl <em>Monitor Controls</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TrafficManager.impl.MonitorControlsImpl
		 * @see TrafficManager.impl.TrafficManagerPackageImpl#getMonitorControls()
		 * @generated
		 */
		EClass MONITOR_CONTROLS = eINSTANCE.getMonitorControls();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITOR_CONTROLS__ID = eINSTANCE.getMonitorControls_ID();

		/**
		 * The meta object literal for the '<em><b>Traffic Light</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITOR_CONTROLS__TRAFFIC_LIGHT = eINSTANCE.getMonitorControls_TrafficLight();

		/**
		 * The meta object literal for the '<em><b>Lane Area Detectors</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITOR_CONTROLS__LANE_AREA_DETECTORS = eINSTANCE.getMonitorControls_LaneAreaDetectors();

		/**
		 * The meta object literal for the '<em><b>Check Phase End</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITOR_CONTROLS__CHECK_PHASE_END = eINSTANCE.getMonitorControls_CheckPhaseEnd();

		/**
		 * The meta object literal for the '{@link TrafficManager.impl.MonitorResultsImpl <em>Monitor Results</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TrafficManager.impl.MonitorResultsImpl
		 * @see TrafficManager.impl.TrafficManagerPackageImpl#getMonitorResults()
		 * @generated
		 */
		EClass MONITOR_RESULTS = eINSTANCE.getMonitorResults();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITOR_RESULTS__ID = eINSTANCE.getMonitorResults_ID();

		/**
		 * The meta object literal for the '<em><b>Traffic Light Seen</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITOR_RESULTS__TRAFFIC_LIGHT_SEEN = eINSTANCE.getMonitorResults_TrafficLightSeen();

		/**
		 * The meta object literal for the '<em><b>Lane Area Detector Seen</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITOR_RESULTS__LANE_AREA_DETECTOR_SEEN = eINSTANCE.getMonitorResults_LaneAreaDetectorSeen();

		/**
		 * The meta object literal for the '<em><b>Phase Ended Seen</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITOR_RESULTS__PHASE_ENDED_SEEN = eINSTANCE.getMonitorResults_PhaseEndedSeen();

		/**
		 * The meta object literal for the '{@link TrafficManager.impl.AnalysisControlsImpl <em>Analysis Controls</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TrafficManager.impl.AnalysisControlsImpl
		 * @see TrafficManager.impl.TrafficManagerPackageImpl#getAnalysisControls()
		 * @generated
		 */
		EClass ANALYSIS_CONTROLS = eINSTANCE.getAnalysisControls();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANALYSIS_CONTROLS__ID = eINSTANCE.getAnalysisControls_ID();

		/**
		 * The meta object literal for the '<em><b>Jam Threshold</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANALYSIS_CONTROLS__JAM_THRESHOLD = eINSTANCE.getAnalysisControls_JamThreshold();

		/**
		 * The meta object literal for the '<em><b>Expect Program OFF</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANALYSIS_CONTROLS__EXPECT_PROGRAM_OFF = eINSTANCE.getAnalysisControls_Expect_Program_OFF();

		/**
		 * The meta object literal for the '<em><b>Copy Phase End</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANALYSIS_CONTROLS__COPY_PHASE_END = eINSTANCE.getAnalysisControls_CopyPhaseEnd();

		/**
		 * The meta object literal for the '{@link TrafficManager.impl.AnalysisResultsImpl <em>Analysis Results</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TrafficManager.impl.AnalysisResultsImpl
		 * @see TrafficManager.impl.TrafficManagerPackageImpl#getAnalysisResults()
		 * @generated
		 */
		EClass ANALYSIS_RESULTS = eINSTANCE.getAnalysisResults();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANALYSIS_RESULTS__ID = eINSTANCE.getAnalysisResults_ID();

		/**
		 * The meta object literal for the '<em><b>Lights Failed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANALYSIS_RESULTS__LIGHTS_FAILED = eINSTANCE.getAnalysisResults_LightsFailed();

		/**
		 * The meta object literal for the '<em><b>LA Ds Jammed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANALYSIS_RESULTS__LA_DS_JAMMED = eINSTANCE.getAnalysisResults_LADsJammed();

		/**
		 * The meta object literal for the '<em><b>Plan To Copy Phase End</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANALYSIS_RESULTS__PLAN_TO_COPY_PHASE_END = eINSTANCE.getAnalysisResults_PlanToCopyPhaseEnd();

		/**
		 * The meta object literal for the '{@link TrafficManager.impl.PlanToExecuteImpl <em>Plan To Execute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TrafficManager.impl.PlanToExecuteImpl
		 * @see TrafficManager.impl.TrafficManagerPackageImpl#getPlanToExecute()
		 * @generated
		 */
		EClass PLAN_TO_EXECUTE = eINSTANCE.getPlanToExecute();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLAN_TO_EXECUTE__ID = eINSTANCE.getPlanToExecute_ID();

		/**
		 * The meta object literal for the '<em><b>End Phase</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLAN_TO_EXECUTE__END_PHASE = eINSTANCE.getPlanToExecute_EndPhase();

		/**
		 * The meta object literal for the '<em><b>Report Lights Failed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLAN_TO_EXECUTE__REPORT_LIGHTS_FAILED = eINSTANCE.getPlanToExecute_ReportLightsFailed();

		/**
		 * The meta object literal for the '<em><b>Jam Threshold Change</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLAN_TO_EXECUTE__JAM_THRESHOLD_CHANGE = eINSTANCE.getPlanToExecute_JamThresholdChange();

		/**
		 * The meta object literal for the '<em><b>Copying Phase End</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLAN_TO_EXECUTE__COPYING_PHASE_END = eINSTANCE.getPlanToExecute_CopyingPhaseEnd();

		/**
		 * The meta object literal for the '{@link TrafficManager.impl.ExecutionResultsImpl <em>Execution Results</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TrafficManager.impl.ExecutionResultsImpl
		 * @see TrafficManager.impl.TrafficManagerPackageImpl#getExecutionResults()
		 * @generated
		 */
		EClass EXECUTION_RESULTS = eINSTANCE.getExecutionResults();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_RESULTS__ID = eINSTANCE.getExecutionResults_ID();

		/**
		 * The meta object literal for the '<em><b>Phase Ended</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_RESULTS__PHASE_ENDED = eINSTANCE.getExecutionResults_PhaseEnded();

		/**
		 * The meta object literal for the '<em><b>Reported Lights Failed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_RESULTS__REPORTED_LIGHTS_FAILED = eINSTANCE.getExecutionResults_ReportedLightsFailed();

		/**
		 * The meta object literal for the '<em><b>Changed Jam Threshold</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_RESULTS__CHANGED_JAM_THRESHOLD = eINSTANCE.getExecutionResults_ChangedJamThreshold();

		/**
		 * The meta object literal for the '{@link TrafficManager.impl.SmartControllerImpl <em>Smart Controller</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TrafficManager.impl.SmartControllerImpl
		 * @see TrafficManager.impl.TrafficManagerPackageImpl#getSmartController()
		 * @generated
		 */
		EClass SMART_CONTROLLER = eINSTANCE.getSmartController();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SMART_CONTROLLER__ID = eINSTANCE.getSmartController_ID();

		/**
		 * The meta object literal for the '<em><b>Execution Results</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SMART_CONTROLLER__EXECUTION_RESULTS = eINSTANCE.getSmartController_ExecutionResults();

		/**
		 * The meta object literal for the '<em><b>Plan To Execute</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SMART_CONTROLLER__PLAN_TO_EXECUTE = eINSTANCE.getSmartController_PlanToExecute();

		/**
		 * The meta object literal for the '<em><b>Analysis Results</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SMART_CONTROLLER__ANALYSIS_RESULTS = eINSTANCE.getSmartController_AnalysisResults();

		/**
		 * The meta object literal for the '<em><b>Analysis Controls</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SMART_CONTROLLER__ANALYSIS_CONTROLS = eINSTANCE.getSmartController_AnalysisControls();

		/**
		 * The meta object literal for the '<em><b>Lane Area Detectors</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SMART_CONTROLLER__LANE_AREA_DETECTORS = eINSTANCE.getSmartController_LaneAreaDetectors();

		/**
		 * The meta object literal for the '<em><b>Traffic Light</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SMART_CONTROLLER__TRAFFIC_LIGHT = eINSTANCE.getSmartController_TrafficLight();

		/**
		 * The meta object literal for the '<em><b>Monitor Controls</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SMART_CONTROLLER__MONITOR_CONTROLS = eINSTANCE.getSmartController_MonitorControls();

		/**
		 * The meta object literal for the '<em><b>Monitor Results</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SMART_CONTROLLER__MONITOR_RESULTS = eINSTANCE.getSmartController_MonitorResults();

	}

} //TrafficManagerPackage
