operation Activity name() {
  return self?.ActivityDescription?.Name;
}

@cached
operation Entity attr() {
  return self.EntityDescription.selectOne(ed |
    ed.AttributeNameAsString.isDefined())?.AttributeNameAsString;
}

@cached
operation Entity val() {
  for (i in (self.EntityDescription.size - 1).to(0)) {
    var val = self.EntityDescription.get(i).AttributeValueAsString;
    if (val.isDefined()) {
      return val;
    }
  }
  return null;
}

operation Entity bool(): Boolean {
  return self.val()?.asBoolean();
}

operation Entity int(): Integer {
  return self.val()?.asInteger();
}

operation Entity isWrite(): Boolean {
  return self.EntityDescription.exists(ed|ed.LastAccessType.asString() = 'W');
}

operation Entity genByName(): String {
  return self.wasGeneratedBy()?.ActivityDescription?.Name;
}

// This version can cross time windows
@cached
operation Entity derivedFrom() {
  if (self.WasDerivedFrom.isDefined()) {
    return self.WasDerivedFrom;
  } else {
    // TODO optimize by looking at previous time windows?
    // TODO this may not work if the same entity appears across > 2 windows
    var otherEntity = Entity.all.selectOne(e |
      e.ID.contains(self.ID) and e <> self and e.Derives.isEmpty());
    return otherEntity;
  }
}

// This version can cross time windows and follow WasDerivedFrom
@cached
operation Entity wasGeneratedBy() {
  var e = self;
  while (e.WasGeneratedBy.isUndefined()
         and e.derivedFrom().isDefined()
         and e.val() == e.derivedFrom().val()) {
    e = e.derivedFrom();
  }
  return e.WasGeneratedBy;
}

operation Entity printLayer(layer: Integer) {
  if (layer > 0) {
    // 1.to(0) counts *down* instead of retuning an empty sequence
    for (i in 1.to(layer)) {
      '  '.print();
    }
  }

  ('Layer ' + layer + ' - Entity: ' + self.attr() + ' ' + self.val()).print();
  if (self.wasGeneratedBy().isDefined()) {
    (' > WasGeneratedBy > ' + self.genByName()).println();
  } else {
    ''.println();
  }
}
