/**
 */
package TrafficManager.impl;

import TrafficManager.AnalysisResults;
import TrafficManager.TrafficManagerPackage;

import org.eclipse.emf.ecore.EClass;

import uk.ac.aston.provlayer.observer.model.cdo.LoggingCDOObjectImpl2;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Analysis Results</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.impl.AnalysisResultsImpl#getID <em>ID</em>}</li>
 *   <li>{@link TrafficManager.impl.AnalysisResultsImpl#isLightsFailed <em>Lights Failed</em>}</li>
 *   <li>{@link TrafficManager.impl.AnalysisResultsImpl#getLADsJammed <em>LA Ds Jammed</em>}</li>
 *   <li>{@link TrafficManager.impl.AnalysisResultsImpl#isPlanToCopyPhaseEnd <em>Plan To Copy Phase End</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AnalysisResultsImpl extends LoggingCDOObjectImpl2 implements AnalysisResults {
	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #isLightsFailed() <em>Lights Failed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLightsFailed()
	 * @generated
	 * @ordered
	 */
	protected static final boolean LIGHTS_FAILED_EDEFAULT = false;

	/**
	 * The default value of the '{@link #getLADsJammed() <em>LA Ds Jammed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLADsJammed()
	 * @generated
	 * @ordered
	 */
	protected static final int LA_DS_JAMMED_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #isPlanToCopyPhaseEnd() <em>Plan To Copy Phase End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPlanToCopyPhaseEnd()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PLAN_TO_COPY_PHASE_END_EDEFAULT = false;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnalysisResultsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TrafficManagerPackage.Literals.ANALYSIS_RESULTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getID() {
		return (String)eDynamicGet(TrafficManagerPackage.ANALYSIS_RESULTS__ID, TrafficManagerPackage.Literals.ANALYSIS_RESULTS__ID, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(String newID) {
		eDynamicSet(TrafficManagerPackage.ANALYSIS_RESULTS__ID, TrafficManagerPackage.Literals.ANALYSIS_RESULTS__ID, newID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isLightsFailed() {
		return (Boolean)eDynamicGet(TrafficManagerPackage.ANALYSIS_RESULTS__LIGHTS_FAILED, TrafficManagerPackage.Literals.ANALYSIS_RESULTS__LIGHTS_FAILED, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLightsFailed(boolean newLightsFailed) {
		eDynamicSet(TrafficManagerPackage.ANALYSIS_RESULTS__LIGHTS_FAILED, TrafficManagerPackage.Literals.ANALYSIS_RESULTS__LIGHTS_FAILED, newLightsFailed);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLADsJammed() {
		return (Integer)eDynamicGet(TrafficManagerPackage.ANALYSIS_RESULTS__LA_DS_JAMMED, TrafficManagerPackage.Literals.ANALYSIS_RESULTS__LA_DS_JAMMED, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLADsJammed(int newLADsJammed) {
		eDynamicSet(TrafficManagerPackage.ANALYSIS_RESULTS__LA_DS_JAMMED, TrafficManagerPackage.Literals.ANALYSIS_RESULTS__LA_DS_JAMMED, newLADsJammed);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPlanToCopyPhaseEnd() {
		return (Boolean)eDynamicGet(TrafficManagerPackage.ANALYSIS_RESULTS__PLAN_TO_COPY_PHASE_END, TrafficManagerPackage.Literals.ANALYSIS_RESULTS__PLAN_TO_COPY_PHASE_END, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPlanToCopyPhaseEnd(boolean newPlanToCopyPhaseEnd) {
		eDynamicSet(TrafficManagerPackage.ANALYSIS_RESULTS__PLAN_TO_COPY_PHASE_END, TrafficManagerPackage.Literals.ANALYSIS_RESULTS__PLAN_TO_COPY_PHASE_END, newPlanToCopyPhaseEnd);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TrafficManagerPackage.ANALYSIS_RESULTS__ID:
				return getID();
			case TrafficManagerPackage.ANALYSIS_RESULTS__LIGHTS_FAILED:
				return isLightsFailed();
			case TrafficManagerPackage.ANALYSIS_RESULTS__LA_DS_JAMMED:
				return getLADsJammed();
			case TrafficManagerPackage.ANALYSIS_RESULTS__PLAN_TO_COPY_PHASE_END:
				return isPlanToCopyPhaseEnd();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TrafficManagerPackage.ANALYSIS_RESULTS__ID:
				setID((String)newValue);
				return;
			case TrafficManagerPackage.ANALYSIS_RESULTS__LIGHTS_FAILED:
				setLightsFailed((Boolean)newValue);
				return;
			case TrafficManagerPackage.ANALYSIS_RESULTS__LA_DS_JAMMED:
				setLADsJammed((Integer)newValue);
				return;
			case TrafficManagerPackage.ANALYSIS_RESULTS__PLAN_TO_COPY_PHASE_END:
				setPlanToCopyPhaseEnd((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.ANALYSIS_RESULTS__ID:
				setID(ID_EDEFAULT);
				return;
			case TrafficManagerPackage.ANALYSIS_RESULTS__LIGHTS_FAILED:
				setLightsFailed(LIGHTS_FAILED_EDEFAULT);
				return;
			case TrafficManagerPackage.ANALYSIS_RESULTS__LA_DS_JAMMED:
				setLADsJammed(LA_DS_JAMMED_EDEFAULT);
				return;
			case TrafficManagerPackage.ANALYSIS_RESULTS__PLAN_TO_COPY_PHASE_END:
				setPlanToCopyPhaseEnd(PLAN_TO_COPY_PHASE_END_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.ANALYSIS_RESULTS__ID:
				return ID_EDEFAULT == null ? getID() != null : !ID_EDEFAULT.equals(getID());
			case TrafficManagerPackage.ANALYSIS_RESULTS__LIGHTS_FAILED:
				return isLightsFailed() != LIGHTS_FAILED_EDEFAULT;
			case TrafficManagerPackage.ANALYSIS_RESULTS__LA_DS_JAMMED:
				return getLADsJammed() != LA_DS_JAMMED_EDEFAULT;
			case TrafficManagerPackage.ANALYSIS_RESULTS__PLAN_TO_COPY_PHASE_END:
				return isPlanToCopyPhaseEnd() != PLAN_TO_COPY_PHASE_END_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //AnalysisResultsImpl
