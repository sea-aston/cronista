/**
 */
package TrafficManager.impl;

import TrafficManager.AnalysisControls;
import TrafficManager.TrafficManagerPackage;

import org.eclipse.emf.ecore.EClass;

import uk.ac.aston.provlayer.observer.model.cdo.LoggingCDOObjectImpl2;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Analysis Controls</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.impl.AnalysisControlsImpl#getID <em>ID</em>}</li>
 *   <li>{@link TrafficManager.impl.AnalysisControlsImpl#getJamThreshold <em>Jam Threshold</em>}</li>
 *   <li>{@link TrafficManager.impl.AnalysisControlsImpl#isExpect_Program_OFF <em>Expect Program OFF</em>}</li>
 *   <li>{@link TrafficManager.impl.AnalysisControlsImpl#isCopyPhaseEnd <em>Copy Phase End</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AnalysisControlsImpl extends LoggingCDOObjectImpl2 implements AnalysisControls {
	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getJamThreshold() <em>Jam Threshold</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJamThreshold()
	 * @generated
	 * @ordered
	 */
	protected static final int JAM_THRESHOLD_EDEFAULT = 3;

	/**
	 * The default value of the '{@link #isExpect_Program_OFF() <em>Expect Program OFF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExpect_Program_OFF()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EXPECT_PROGRAM_OFF_EDEFAULT = true;

	/**
	 * The default value of the '{@link #isCopyPhaseEnd() <em>Copy Phase End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCopyPhaseEnd()
	 * @generated
	 * @ordered
	 */
	protected static final boolean COPY_PHASE_END_EDEFAULT = false;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnalysisControlsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TrafficManagerPackage.Literals.ANALYSIS_CONTROLS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getID() {
		return (String)eDynamicGet(TrafficManagerPackage.ANALYSIS_CONTROLS__ID, TrafficManagerPackage.Literals.ANALYSIS_CONTROLS__ID, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(String newID) {
		eDynamicSet(TrafficManagerPackage.ANALYSIS_CONTROLS__ID, TrafficManagerPackage.Literals.ANALYSIS_CONTROLS__ID, newID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getJamThreshold() {
		return (Integer)eDynamicGet(TrafficManagerPackage.ANALYSIS_CONTROLS__JAM_THRESHOLD, TrafficManagerPackage.Literals.ANALYSIS_CONTROLS__JAM_THRESHOLD, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setJamThreshold(int newJamThreshold) {
		eDynamicSet(TrafficManagerPackage.ANALYSIS_CONTROLS__JAM_THRESHOLD, TrafficManagerPackage.Literals.ANALYSIS_CONTROLS__JAM_THRESHOLD, newJamThreshold);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isExpect_Program_OFF() {
		return (Boolean)eDynamicGet(TrafficManagerPackage.ANALYSIS_CONTROLS__EXPECT_PROGRAM_OFF, TrafficManagerPackage.Literals.ANALYSIS_CONTROLS__EXPECT_PROGRAM_OFF, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpect_Program_OFF(boolean newExpect_Program_OFF) {
		eDynamicSet(TrafficManagerPackage.ANALYSIS_CONTROLS__EXPECT_PROGRAM_OFF, TrafficManagerPackage.Literals.ANALYSIS_CONTROLS__EXPECT_PROGRAM_OFF, newExpect_Program_OFF);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCopyPhaseEnd() {
		return (Boolean)eDynamicGet(TrafficManagerPackage.ANALYSIS_CONTROLS__COPY_PHASE_END, TrafficManagerPackage.Literals.ANALYSIS_CONTROLS__COPY_PHASE_END, true, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCopyPhaseEnd(boolean newCopyPhaseEnd) {
		eDynamicSet(TrafficManagerPackage.ANALYSIS_CONTROLS__COPY_PHASE_END, TrafficManagerPackage.Literals.ANALYSIS_CONTROLS__COPY_PHASE_END, newCopyPhaseEnd);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TrafficManagerPackage.ANALYSIS_CONTROLS__ID:
				return getID();
			case TrafficManagerPackage.ANALYSIS_CONTROLS__JAM_THRESHOLD:
				return getJamThreshold();
			case TrafficManagerPackage.ANALYSIS_CONTROLS__EXPECT_PROGRAM_OFF:
				return isExpect_Program_OFF();
			case TrafficManagerPackage.ANALYSIS_CONTROLS__COPY_PHASE_END:
				return isCopyPhaseEnd();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TrafficManagerPackage.ANALYSIS_CONTROLS__ID:
				setID((String)newValue);
				return;
			case TrafficManagerPackage.ANALYSIS_CONTROLS__JAM_THRESHOLD:
				setJamThreshold((Integer)newValue);
				return;
			case TrafficManagerPackage.ANALYSIS_CONTROLS__EXPECT_PROGRAM_OFF:
				setExpect_Program_OFF((Boolean)newValue);
				return;
			case TrafficManagerPackage.ANALYSIS_CONTROLS__COPY_PHASE_END:
				setCopyPhaseEnd((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.ANALYSIS_CONTROLS__ID:
				setID(ID_EDEFAULT);
				return;
			case TrafficManagerPackage.ANALYSIS_CONTROLS__JAM_THRESHOLD:
				setJamThreshold(JAM_THRESHOLD_EDEFAULT);
				return;
			case TrafficManagerPackage.ANALYSIS_CONTROLS__EXPECT_PROGRAM_OFF:
				setExpect_Program_OFF(EXPECT_PROGRAM_OFF_EDEFAULT);
				return;
			case TrafficManagerPackage.ANALYSIS_CONTROLS__COPY_PHASE_END:
				setCopyPhaseEnd(COPY_PHASE_END_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TrafficManagerPackage.ANALYSIS_CONTROLS__ID:
				return ID_EDEFAULT == null ? getID() != null : !ID_EDEFAULT.equals(getID());
			case TrafficManagerPackage.ANALYSIS_CONTROLS__JAM_THRESHOLD:
				return getJamThreshold() != JAM_THRESHOLD_EDEFAULT;
			case TrafficManagerPackage.ANALYSIS_CONTROLS__EXPECT_PROGRAM_OFF:
				return isExpect_Program_OFF() != EXPECT_PROGRAM_OFF_EDEFAULT;
			case TrafficManagerPackage.ANALYSIS_CONTROLS__COPY_PHASE_END:
				return isCopyPhaseEnd() != COPY_PHASE_END_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //AnalysisControlsImpl
