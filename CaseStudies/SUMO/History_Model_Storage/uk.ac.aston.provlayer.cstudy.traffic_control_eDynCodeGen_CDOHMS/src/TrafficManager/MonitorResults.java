/**
 */
package TrafficManager;

import org.eclipse.emf.cdo.CDOObject;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Monitor Results</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.MonitorResults#getID <em>ID</em>}</li>
 *   <li>{@link TrafficManager.MonitorResults#getTrafficLightSeen <em>Traffic Light Seen</em>}</li>
 *   <li>{@link TrafficManager.MonitorResults#getLaneAreaDetectorSeen <em>Lane Area Detector Seen</em>}</li>
 *   <li>{@link TrafficManager.MonitorResults#isPhaseEndedSeen <em>Phase Ended Seen</em>}</li>
 * </ul>
 *
 * @see TrafficManager.TrafficManagerPackage#getMonitorResults()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface MonitorResults extends CDOObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see TrafficManager.TrafficManagerPackage#getMonitorResults_ID()
	 * @model id="true"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link TrafficManager.MonitorResults#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>Traffic Light Seen</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Traffic Light Seen</em>' attribute.
	 * @see #setTrafficLightSeen(String)
	 * @see TrafficManager.TrafficManagerPackage#getMonitorResults_TrafficLightSeen()
	 * @model
	 * @generated
	 */
	String getTrafficLightSeen();

	/**
	 * Sets the value of the '{@link TrafficManager.MonitorResults#getTrafficLightSeen <em>Traffic Light Seen</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Traffic Light Seen</em>' attribute.
	 * @see #getTrafficLightSeen()
	 * @generated
	 */
	void setTrafficLightSeen(String value);

	/**
	 * Returns the value of the '<em><b>Lane Area Detector Seen</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lane Area Detector Seen</em>' attribute list.
	 * @see TrafficManager.TrafficManagerPackage#getMonitorResults_LaneAreaDetectorSeen()
	 * @model
	 * @generated
	 */
	EList<String> getLaneAreaDetectorSeen();

	/**
	 * Returns the value of the '<em><b>Phase Ended Seen</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Phase Ended Seen</em>' attribute.
	 * @see #setPhaseEndedSeen(boolean)
	 * @see TrafficManager.TrafficManagerPackage#getMonitorResults_PhaseEndedSeen()
	 * @model
	 * @generated
	 */
	boolean isPhaseEndedSeen();

	/**
	 * Sets the value of the '{@link TrafficManager.MonitorResults#isPhaseEndedSeen <em>Phase Ended Seen</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Phase Ended Seen</em>' attribute.
	 * @see #isPhaseEndedSeen()
	 * @generated
	 */
	void setPhaseEndedSeen(boolean value);

} // MonitorResults
