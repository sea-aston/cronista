/**
 */
package TrafficManager;

import org.eclipse.emf.cdo.CDOObject;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Monitor Controls</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TrafficManager.MonitorControls#getID <em>ID</em>}</li>
 *   <li>{@link TrafficManager.MonitorControls#getTrafficLight <em>Traffic Light</em>}</li>
 *   <li>{@link TrafficManager.MonitorControls#getLaneAreaDetectors <em>Lane Area Detectors</em>}</li>
 *   <li>{@link TrafficManager.MonitorControls#isCheckPhaseEnd <em>Check Phase End</em>}</li>
 * </ul>
 *
 * @see TrafficManager.TrafficManagerPackage#getMonitorControls()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface MonitorControls extends CDOObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see TrafficManager.TrafficManagerPackage#getMonitorControls_ID()
	 * @model id="true"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link TrafficManager.MonitorControls#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>Traffic Light</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Traffic Light</em>' attribute.
	 * @see #setTrafficLight(String)
	 * @see TrafficManager.TrafficManagerPackage#getMonitorControls_TrafficLight()
	 * @model
	 * @generated
	 */
	String getTrafficLight();

	/**
	 * Sets the value of the '{@link TrafficManager.MonitorControls#getTrafficLight <em>Traffic Light</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Traffic Light</em>' attribute.
	 * @see #getTrafficLight()
	 * @generated
	 */
	void setTrafficLight(String value);

	/**
	 * Returns the value of the '<em><b>Lane Area Detectors</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lane Area Detectors</em>' attribute list.
	 * @see TrafficManager.TrafficManagerPackage#getMonitorControls_LaneAreaDetectors()
	 * @model
	 * @generated
	 */
	EList<String> getLaneAreaDetectors();

	/**
	 * Returns the value of the '<em><b>Check Phase End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Check Phase End</em>' attribute.
	 * @see #setCheckPhaseEnd(boolean)
	 * @see TrafficManager.TrafficManagerPackage#getMonitorControls_CheckPhaseEnd()
	 * @model
	 * @generated
	 */
	boolean isCheckPhaseEnd();

	/**
	 * Sets the value of the '{@link TrafficManager.MonitorControls#isCheckPhaseEnd <em>Check Phase End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Check Phase End</em>' attribute.
	 * @see #isCheckPhaseEnd()
	 * @generated
	 */
	void setCheckPhaseEnd(boolean value);

} // MonitorControls
