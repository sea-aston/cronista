package trafficController;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryType;
import java.lang.management.MemoryUsage;
import java.util.ArrayList;
import java.util.List;

public class MemoryTimeUsageLogger implements Runnable {

	@Override
	public void run() {
		final long startMillis = System.currentTimeMillis();
		final File fLog = new File("logs/memory-" + startMillis + ".csv");
		try (FileWriter fw = new FileWriter(fLog); PrintWriter pw = new PrintWriter(fw)) {
			pw.println("elapsedWallClockMillis,totalMemoryBytes");
			System.err.println("Memory logger started");
			try {
				while (true) {
					takeMeasurement(startMillis, pw);
					Thread.sleep(1_000);
				}
			} catch (InterruptedException e) {
				// Take one last measurement before we go (for total runtime)
				takeMeasurement(startMillis, pw);
			} 
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			System.err.println("Memory logger closed");
		}
	}

	private void takeMeasurement(final long startMillis, final PrintWriter pw) {
		System.gc();
		final long elapsedWallClockMillis = System.currentTimeMillis() - startMillis;

		// Runtime.getRuntime().getTotalMemory() is too coarse
		List<MemoryPoolMXBean> memoryPools = new ArrayList<MemoryPoolMXBean>(
				ManagementFactory.getMemoryPoolMXBeans());
		long usedHeapMemoryAfterLastGC = 0;
		for (MemoryPoolMXBean memoryPool : memoryPools) {
			if (memoryPool.getType().equals(MemoryType.HEAP)) {
				MemoryUsage poolCollectionMemoryUsage = memoryPool.getCollectionUsage();
				usedHeapMemoryAfterLastGC += poolCollectionMemoryUsage.getUsed();
			}
		}
		pw.println(String.format("%d,%d", elapsedWallClockMillis, usedHeapMemoryAfterLastGC));
	}
}
