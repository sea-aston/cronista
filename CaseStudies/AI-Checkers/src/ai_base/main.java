package ai_base;

import java.util.Scanner;
//import javax.lang.model.util.ElementScanner14;
import java.util.*;

public class main {
	
	public static boolean useWatchdog = false;
	public static int watchdog;
	
	// Added seed support
	public static boolean useSeed = false;
	public static int seed;

	// Relocated to enable Arg parser access
	private static String player1Output = "0";
	private static String player2Output = "0";
	private static String loadGame = "0";
	private static String loadedFile = "0";
	private static int movesFirst = 0;
	private static int timeLimit = 0;

	public static void main(String[] args) {

		final Thread tMemoryLogger = new Thread(new MemoryTimeUsageLogger());
		tMemoryLogger.start();

		parseArgs(args);

		Heuristic cpu1 = new Heuristic(1, 3);
		Heuristic cpu2 = new Heuristic(2, 3);
		boolean player1iscomputer;
		boolean player2iscomputer;
		Scanner scan = new Scanner(System.in);

		boolean ended = false;
		Game game = new Game();
		System.out.println(
				"Welcome to Chess! Player 1 is Blue and Player 2 is Red. Please respond to the following prompts:");
		System.out.println("\n");
		while (!player1Output.equals("Y") && !player1Output.equals("N")) {
			System.out.println("Will player #1 be a computer? (Y/N):");
			player1Output = scan.nextLine();
			System.out.println("\n");
		}

		player1iscomputer = player1Output.equals("Y") ? true : false;
		while (!player2Output.equals("Y") && !player2Output.equals("N")) {
			System.out.println("Will player #2 be a computer? (Y/N):");
			player2Output = scan.nextLine();
		}
		System.out.println("\n");
		player2iscomputer = player2Output.equals("Y") ? true : false;
		while (!loadGame.equals("Y") && !loadGame.equals("N")) {
			System.out.println("Do you want to load a game from a file? (Y/N): ");
			loadGame = scan.nextLine();
		}
		System.out.println("\n");

		if (loadGame.equals("Y")) {

			while (!game.loadFile(loadedFile)) {

				System.out.println("Enter the name of the file:");
				loadedFile = scan.nextLine();

				System.out.println("\n");
			}
			boolean b = game.loadFile(loadedFile);
			cpu1.timeLimit = game.timeLimit;
			cpu2.timeLimit = game.timeLimit;

		} else {
			game.initializeValues();
			while (movesFirst != 1 && movesFirst != 2) {
				System.out.println("Which player would you like to move first? (1/2) : ");
				movesFirst = scan.nextInt();
				System.out.println("\n");
			}
			game.playerTurn = movesFirst;
			while (timeLimit < 5 || timeLimit > 100) {
				System.out.println("Enter a time limit between 5 and 100: ");
				timeLimit = scan.nextInt();
				System.out.println("\n");
			}

			cpu1.timeLimit = timeLimit;
			cpu2.timeLimit = timeLimit;
			game.timeLimit = timeLimit;
		}

		while (game.end != true && !(watchdog == 0 && useWatchdog) ) {
			
			if(useWatchdog) {
				System.out.println("\033[H\033[2J --Watchdog " + watchdog);
				watchdog--;
			}
			
			game = new Game(game);
			game.loadBoard();

			switch (game.playerTurn) {
			case 1:
				game.printMove(player1iscomputer, game.board, game);
				if (game.end == true) {
					game.changePlayerTurn();
					List<Move> legalMoves = game.getLegalMoves(game.board, game);

					if (legalMoves.isEmpty()) {
						System.out.println("DRAW");
						break;
					} else {
						System.out.println("Player 2 Wins!");
						break;
					}
				}
				if (player1iscomputer) {
					Move move = cpu1.alphaBeta(game);
					game.applyMove(move, game.board);
					System.out.println("Player 1's move is ");
					move.printMove();
				}
				break;
			case 2:

				game.printMove(player2iscomputer, game.board, game);
				if (game.end == true) {
					game.changePlayerTurn();
					List<Move> legalMoves = game.getLegalMoves(game.board, game);

					if (legalMoves.isEmpty()) {
						System.out.println("DRAW");
						break;
					} else {
						System.out.println("Player 1 Wins!");
						break;
					}
				}
				if (player2iscomputer) {
					Move move = cpu2.alphaBeta(game);
					game.applyMove(move, game.board);
					System.out.println("Player 2's move is ");
					move.printMove();
				}
				break;
			}

			game.changePlayerTurn();
		}

		tMemoryLogger.interrupt();
		try {
			tMemoryLogger.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void parseArgs (String[] args) {
		if(args.length > 0) {
			for (int i = 0; i <args.length; i++) {
				switch(args[i].toLowerCase()) {
				case "-cpuplayer1":
					System.out.println(i + " -cpuplayer1: " + args[i].toLowerCase());
					player1Output = "Y";
					break;
				case "-cpuplayer2":
					System.out.println(i + " -cpuplayer2: " + args[i].toLowerCase());
					player2Output = "Y";
					break;
				case "-loadfile":
					System.out.println(i + " -loadfile: " + args[i+1].toLowerCase());					
					// IF (loadGame "Y") THEN loadedFile needs to be set					
					if(args[i+1].toLowerCase().equals("n")) {
						loadGame = "N";
					} else {										
						loadGame = "Y";
						loadedFile = args[i+1];
					}
					i++;
					break;
				case "-movesfirst":
					System.out.println(i + " -movesfirst: " + args[i+1].toLowerCase());
					movesFirst =  Integer.parseInt(args[i+1]);  // 1 or 2
					i++;
					break;
				case "-time":
					System.out.println(i + " -time: " + args[i+1].toLowerCase());
					timeLimit =  Integer.parseInt(args[i+1]);  // 5 - 100					
					i++;
					break;
				case "-seed":
					System.out.println(i + " -seed: " + args[i+1].toLowerCase());
					seed = Integer.parseInt(args[i+1]);
					useSeed=true;
					i++;
					break;
				case "-watchdog":
					System.out.println(i + " -watchdog: " + args[i+1].toLowerCase());
					watchdog = Integer.parseInt(args[i+1]);
					useWatchdog = true;
					i++;
					break;
				
				}
			}
		}
	}
}