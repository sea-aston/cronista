package ai_base_SeedChecker;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class SeedLogger {

	private File sLog = null;

	public SeedLogger(int watchdog) {
		sLog = new File("logs/watchdog-" + watchdog + ".csv");
	}

	public void logSeed(int timeLimit, int seed, int watchdogRemainder) {
		try (FileWriter sFw = new FileWriter(sLog, true); PrintWriter sPw = new PrintWriter(sFw)) {

			sPw.println(String.format("time limit, %d, seed, %d, wdgr, %d ", timeLimit, seed, watchdogRemainder));
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			System.err.println("Seed Logger closed");
		}
	}

}
