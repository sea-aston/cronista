package uk.ac.aston.provlayer.cstudy.fib3;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import uk.ac.aston.provlayer.curator.Curator;
import uk.ac.aston.provlayer.curator.memqueue.BlockingQueueProcessor;
import uk.ac.aston.provlayer.curator.messages.ObserverMessage;
import uk.ac.aston.provlayer.observer.instrument.aspectj_POJO.*;
import uk.ac.aston.provlayer.observer.thread.ActivityScope;
import uk.ac.aston.provlayer.observer.thread.ThreadObserver;
import uk.ac.aston.provlayer.provlayercontrol.ProvLayerControl;
import uk.ac.aston.provlayer.storage.api.ProvenanceGraphStorage;
import uk.ac.aston.provlayer.storage.tinkerpop.TpProvenanceGraphStorage;

public class Fib3 {

	private static final int UPPERLIMIT = 3;

	public static void main(String[] args) {

		// ProvLayerControl.startProvLayer(new CDOProvenanceGraph());
		ProvLayerControl.startProvLayer(new TpProvenanceGraphStorage());

		System.out.println("Sleeping 3 seconds... BEFORE TEST");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ModelDefault();
		ModelPrivate();
		ModelPublic();
		ModelProtected();
		ModelStaticDefault();

	}

	// Selector for the history model type.
	private static ProvenanceGraphStorage createProvenanceStorage() {
		// return new CDOProvenanceGraph();
		return new TpProvenanceGraphStorage();
	}

	@CodePOJO
	private static void ModelDefault() {
		System.out.println("ModelDefault");
		ModelDefault model = new ModelDefault();
		try (ActivityScope scope0 = new ActivityScope("ModelDefault")) {
			do {

				try (ActivityScope scope1 = new ActivityScope("A + B")) {
					model.C = model.A + model.B;
				}

				try (ActivityScope scope1 = new ActivityScope("Output to Console ")) {
					System.out.print("A: " + model.A + " B: " + model.B);
					System.out.println(" C: " + model.C);
				}

				try (ActivityScope scope1 = new ActivityScope("Move B to A")) {
					model.A = model.B;
				}

				try (ActivityScope scope1 = new ActivityScope("Move C to B")) {
					model.B = model.C;
				}

			} while (model.A < UPPERLIMIT);
		}
	}

	@CodePOJO
	private static void ModelPrivate() {
		System.out.println("ModelPrivate");
		ModelPrivates model = new ModelPrivates();
		try (ActivityScope scope0 = new ActivityScope("ModelPrivate")) {
			do {
				try (ActivityScope scope1 = new ActivityScope("A + B")) {
					model.setC(model.getA() + model.getB());
				}

				try (ActivityScope scope1 = new ActivityScope("Output to Console ")) {
					System.out.print("A: " + model.getA() + " B: " + model.getB());
					System.out.println(" C: " + model.getC());
				}

				try (ActivityScope scope1 = new ActivityScope("Move B to A")) {
					model.setA(model.getB());
				}

				try (ActivityScope scope1 = new ActivityScope("Move C to B")) {
					model.setB(model.getC());
				}

			} while (model.getA() < UPPERLIMIT);
		}
	}

	@CodePOJO
	private static void ModelProtected() {
		System.out.println("ModelProtected");
		ModelProtecteds model = new ModelProtecteds();
		try (ActivityScope scope0 = new ActivityScope("ModelProtected")) {
			do {

				try (ActivityScope scope1 = new ActivityScope("A + B")) {
					model.C = model.A + model.B;
				}

				try (ActivityScope scope1 = new ActivityScope("Output to Console ")) {
					System.out.print("A: " + model.A + " B: " + model.B);
					System.out.println(" C: " + model.C);
				}

				try (ActivityScope scope1 = new ActivityScope("Move B to A")) {
					model.A = model.B;
				}

				try (ActivityScope scope1 = new ActivityScope("Move C to B")) {
					model.B = model.C;
				}

			} while (model.A < UPPERLIMIT);
		}
	}

	@CodePOJO
	private static void ModelPublic() {
		System.out.println("ModelPublic");
		ModelPublics model = new ModelPublics();
		try (ActivityScope scope0 = new ActivityScope("ModelPublic")) {
			do {

				try (ActivityScope scope1 = new ActivityScope("A + B")) {
					model.C = model.A + model.B;
				}

				try (ActivityScope scope1 = new ActivityScope("Output to Console ")) {
					System.out.print("A: " + model.A + " B: " + model.B);
					System.out.println(" C: " + model.C);
				}

				try (ActivityScope scope1 = new ActivityScope("Move B to A")) {
					model.A = model.B;
				}

				try (ActivityScope scope1 = new ActivityScope("Move C to B")) {
					model.B = model.C;
				}

			} while (model.A < UPPERLIMIT);
		}
	}

	@CodePOJO
	private static void ModelStaticDefault() {
		System.out.println("ModelStaticDefault");
		ModelStaticDefault model = new ModelStaticDefault();
		try (ActivityScope scope0 = new ActivityScope("ModelStaticDefault")) {
			do {

				try (ActivityScope scope1 = new ActivityScope("A + B")) {
					model.C = model.A + model.B;
				}

				try (ActivityScope scope1 = new ActivityScope("Output to Console ")) {
					System.out.print("A: " + model.A + " B: " + model.B);
					System.out.println(" C: " + model.C);
				}

				try (ActivityScope scope1 = new ActivityScope("Move B to A")) {
					model.A = model.B;
				}

				try (ActivityScope scope1 = new ActivityScope("Move C to B")) {
					model.B = model.C;
				}

			} while (model.A < UPPERLIMIT);
		}
	}

}
