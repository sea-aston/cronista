package uk.ac.aston.provlayer.cstudy.fib3;

import uk.ac.aston.provlayer.observer.instrument.aspectj_POJO.ModelPOJO;

@ModelPOJO
public class ModelStaticDefault {

	static int A, B, C;
	
	
	public ModelStaticDefault() {
		A = 0;
		B = 1;
		C = 0;
	}
}
