package uk.ac.aston.provlayer.cstudy.fib3;

import uk.ac.aston.provlayer.observer.instrument.aspectj_POJO.ModelPOJO;

@ModelPOJO
public class ModelProtecteds {

	protected int A, B, C;
	
	
	public ModelProtecteds() {
		A = 0;
		B = 1;
		C = 0;
	}
}
