package uk.ac.aston.provlayer.cstudy.fib3;

import uk.ac.aston.provlayer.observer.instrument.aspectj_POJO.*;

@ModelPOJO
public class ModelPublics {

	public int A, B, C;
	
	
	public ModelPublics() {
		A = 0;
		B = 1;
		C = 0;
	}
}
