package uk.ac.aston.provlayer.cstudy.fib3;

import uk.ac.aston.provlayer.observer.instrument.aspectj_POJO.*;

@CodePOJO @ModelPOJO
public class ModelPrivates {
	
	private int A, B, C;
	
	public ModelPrivates () {
		A = 0;
		B = 1;
		C = 0;
	}
	
	public int getA() {
		return this.A;
	}
	
	public void setA(int a) {
		this.A = a;
	}
	
	public int getB() {
		return this.B;
	}
	
	public void setB(int b) {
		this.B = b;
	}

	public int getC() {
		return this.C;
	}
	
	public void setC(int c) {
		this.C = c;
	}

}
