package uk.ac.aston.provlayer.cstudy.fibRuntimeModel;

import uk.ac.aston.provlayer.observer.annotations.ObserveModelEncapsulation;


@ObserveModelEncapsulation
public class Model_Stringy {
	
	private String name;
	private String value;
	
	public Model_Stringy(String name) {
		this.name = name;
		this.value = null;
	}
	
	public String getName ()
	{
		return name;
	}
	
	public String getValue() {
		return value;
	}
	
	public boolean setValue(String setString)
	{
		this.value = setString;
		if (setString != null)	
		{	
			if	(setString.contains(this.value))	return true;
			else									return false;
		}
		else
			return true;
	}

}
