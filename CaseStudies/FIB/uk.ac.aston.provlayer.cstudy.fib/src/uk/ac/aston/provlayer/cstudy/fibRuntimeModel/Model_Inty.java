
package uk.ac.aston.provlayer.cstudy.fibRuntimeModel;

import uk.ac.aston.provlayer.observer.annotations.*;

@ObserveModelEncapsulation
public class Model_Inty {
	
	private String name;
	private int value;
	
	public Model_Inty(String name) {
		this.name = name;
		this.value = 0;
	}
	
	public String getName ()
	{
		return name;
	}
	
	public int getValue() 
	{
		return value;
	}
	
	public boolean setValue(int setValue)
	{
		this.value = setValue;
		
		if	(setValue == this.value)	return true;
		else							return false;
	}

}
