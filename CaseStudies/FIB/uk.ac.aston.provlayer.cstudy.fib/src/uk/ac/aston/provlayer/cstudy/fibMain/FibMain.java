package uk.ac.aston.provlayer.cstudy.fibMain;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import uk.ac.aston.provlayer.cstudy.fibRuntimeModel.Model_Inty;
import uk.ac.aston.provlayer.cstudy.fibRuntimeModel.Model_Stringy;
import uk.ac.aston.provlayer.curator.Curator;
import uk.ac.aston.provlayer.curator.memqueue.BlockingQueueProcessor;
import uk.ac.aston.provlayer.curator.messages.ObserverMessage;
import uk.ac.aston.provlayer.observer.annotations.ObserveMe;
import uk.ac.aston.provlayer.observer.thread.ActivityScope;
import uk.ac.aston.provlayer.observer.thread.ThreadObserver;
import uk.ac.aston.provlayer.storage.api.ProvenanceGraphStorage;
import uk.ac.aston.provlayer.storage.tinkerpop.TpProvenanceGraphStorage;

@ObserveMe
public class FibMain {

	public static void main(String[] args) {
		Model_Stringy Console;
		Model_Inty A, B, C;

		// PROV LAYER SETUP - NO NEED FOR SINGLETON
		Thread tCurator = null;
		Curator curator = null;
		final ProvenanceGraphStorage storage = createProvenanceStorage();
		if (storage != null) {
			BlockingQueue<ObserverMessage> mQueue = new ArrayBlockingQueue<>(10000);
			BlockingQueueProcessor mQueueProcessor = new BlockingQueueProcessor(mQueue);
			ThreadObserver.addNewObserverHook((obs) -> obs.subscribe(mQueueProcessor));
			curator = new Curator(mQueueProcessor, storage);
			tCurator = new Thread(curator);
			tCurator.start();
		}

		// Activity
		try (ActivityScope scope0 = new ActivityScope("Init")) {
			Console = new Model_Stringy("Console");
			A = new Model_Inty("A");
			B = new Model_Inty("B");
			C = new Model_Inty("C");

			A.setValue(0);
			B.setValue(1);
			C.setValue(0);
			Console.setValue(null);
		}

		do {
			try (ActivityScope scope0 = new ActivityScope("A + B")) {
				C.setValue(A.getValue() + B.getValue());
			}
			try (ActivityScope scope0 = new ActivityScope("A is B")) {
				A.setValue(B.getValue());
			}
			try (ActivityScope scope0 = new ActivityScope("B is C")) {
				B.setValue(C.getValue());
			}

			Console.setValue(C.getName() + " is " + C.getValue() + "\n");
			System.out.println(Console.getValue()); // Turn off the output here and look at Aspects

		} while (A.getValue() < 255);

	}

	// Selector for the history model type.
	private static ProvenanceGraphStorage createProvenanceStorage() {
		// return new CDOProvenanceGraph();
		return new TpProvenanceGraphStorage();
	}

}
