package uk.ac.aston.provlayer.cstudy.fib2;

import uk.ac.aston.provlayer.observer.annotations.ObserveModelDefaults;

@ObserveModelDefaults
public class ModelDefault {

	int A, B, C;
	
	
	public ModelDefault() {
		A = 0;
		B = 1;
		C = 0;
	}
}
