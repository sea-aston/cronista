package uk.ac.aston.provlayer.cstudy.fib2;

import uk.ac.aston.provlayer.observer.annotations.ObserveModelPOJOStatic;

@ObserveModelPOJOStatic
public class ModelStaticDefault {

	static int A, B, C;
	
	
	public ModelStaticDefault() {
		A = 0;
		B = 1;
		C = 0;
	}
}
