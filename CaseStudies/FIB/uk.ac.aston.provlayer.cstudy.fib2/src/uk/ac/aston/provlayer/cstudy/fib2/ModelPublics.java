package uk.ac.aston.provlayer.cstudy.fib2;

import uk.ac.aston.provlayer.observer.annotations.ObserveModelPublics;

@ObserveModelPublics
public class ModelPublics {

	public int A, B, C;
	
	
	public ModelPublics() {
		A = 0;
		B = 1;
		C = 0;
	}
}
