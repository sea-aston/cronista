package uk.ac.aston.provlayer.cstudy.fib2;

import uk.ac.aston.provlayer.observer.annotations.ObserveModelProtecteds;

@ObserveModelProtecteds
public class ModelProtecteds {

	protected int A, B, C;
	
	
	public ModelProtecteds() {
		A = 0;
		B = 1;
		C = 0;
	}
}
