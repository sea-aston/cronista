package uk.ac.aston.provlayer.cstudy.fib2;

import java.io.Serializable;

import uk.ac.aston.provlayer.observer.annotations.*;

@ObserveModelBeanFeature
public class ModelBeanFeature {
	
	private int A, B, C;
	
	public ModelBeanFeature () {
		A = 0;
		B = 1;
		C = 0;
	}
	
	public int getA() {
		return this.A;
	}
	
	public void setA(int a) {
		this.A = a;
	}
	
	public int getB() {
		return this.B;
	}
	
	public void setB(int b) {
		this.B = b;
	}

	public int getC() {
		return this.C;
	}
	
	public void setC(int c) {
		this.C = c;
	}
	
	public String toString() {
		return String.format("A=%d B=%d C=%d", this.A, this.B, this.C);
		
	}

}
